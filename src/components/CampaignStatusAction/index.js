import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { withTranslation, Trans } from "react-i18next";
import { connect } from "react-redux";
import {
  changeStatusAction,
} from "./../../store/actions/campaign";
import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import closePopup from "./../../images/close-popup.svg";
import InfiniteScroll from "react-infinite-scroll-component";
import * as routeName from "./../../routes/routeName"
import { CAMPAIGN_STATUS } from "./../../constants/index"

class CampaignStatusAction extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  clickChangeStatus = (status) => {
    this.props.changeStatus(this.props.id, status)
      .then(() => {
        if (typeof window !== 'undefined' && window.document && window.document.createElement) {
          window.location.reload()
        }
      })
  }

  render() {
    return (
      <div className="mt-2">
        <button type="button" className="btn btn-primary mr-2" onClick={() => this.clickChangeStatus(CAMPAIGN_STATUS.MATCHING)}>Matching</button>
        <button type="button" className="btn btn-secondary mr-2" onClick={() => this.clickChangeStatus(CAMPAIGN_STATUS.READY)}>Ready</button>
        <button type="button" className="btn btn-success mr-2" onClick={() => this.clickChangeStatus(CAMPAIGN_STATUS.ONGOING)}>On-going</button>
        <button type="button" className="btn btn-danger" onClick={() => this.clickChangeStatus(CAMPAIGN_STATUS.CLOSED)}>Closed</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeStatus: (id, status) =>
      dispatch(changeStatusAction(id, status))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(CampaignStatusAction));
