import React from "react";
import { render } from "react-dom";
// Import Highcharts
import Highcharts from "highcharts/highstock";
//import HighchartsReact from "./HighchartsReact.js";
import PieChart from "highcharts-react-official";

class PieChartComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const options = {
      chart: {
        backgroundColor: this.props.fill ? this.props.fill : "#f5f5f5",
        plotBackgroundColor: this.props.fill ? this.props.fill : "#f5f5f5",
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
      },
      title: {
        text: this.props.title || ""
      },
      tooltip: {
        pointFormat: ' <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          colors: this.props.data.map(e => e.color),
          dataLabels: {
            enabled: true,
            format: "{point.percentage:.1f} %",
              distance: "-20%",
            style: {
              color:
                (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                "#252525"
            },
            connectorColor: "none"
          }
        }
      },
      series: [
        {
          name: "Sharhe",
          data: this.props.data.map(e => ({ name: e.name, y: e.value })),
          showInLegend: true
        }
      ],
      credits: {
        enabled: false
      }
    };

    return <PieChart highcharts={Highcharts} options={options} />;
  }
}

export default PieChartComponent;
