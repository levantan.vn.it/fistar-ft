import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { withTranslation, Trans } from "react-i18next";
import { connect } from "react-redux";
import { scrapAction } from "./../../store/actions/scrap";
import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import closePopup from "./../../images/close-popup.svg";
import InfiniteScroll from "react-infinite-scroll-component";
import * as routeName from "./../../routes/routeName";
import { CAMPAIGN_STATUS } from "./../../constants/index";
import "./scrap.css";

class Scrap extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      is_scrap: props.is_scrap,
      scraps_count: props.scraps_count || 0
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.is_scrap !== prevProps.is_scrap) {
      this.setState({
        is_scrap: this.props.is_scrap,
        scraps_count: this.props.scraps_count || 0
      });
    }
  }

  scrap = () => {
    this.props.scrap(this.props.id).then(() => {
      this.setState({
        is_scrap: !this.state.is_scrap,
        scraps_count: !this.state.is_scrap
          ? this.state.scraps_count + 1
          : this.state.scraps_count - 1
      });
      if (this.props.callback) {
        this.props.callback()
      }
    });
  };

  render() {
    return (
      <Fragment>
        <button
          className={`${
            this.props.className
              ? this.props.className
              : "overlay-box scrap-myfistar"
          }${this.state.is_scrap ? "" : " disnabled-scrap scrap-myfistar"}`}
          onClick={this.scrap}
        >
          <i
            className={`fas ${
              this.props.iconClassName ? this.props.iconClassName : "fa-star"
            } m-0`}
          />{" "}
          <span>{this.state.scraps_count.toLocaleString()}</span>
        </button>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    scrap: id => dispatch(scrapAction(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Scrap));
