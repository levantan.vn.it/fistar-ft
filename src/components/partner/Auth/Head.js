import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import * as routeName from "./../../../routes/routeName";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";

class PartnerAuthHead extends Component {
  render() {
    const { t } = this.props;
    return (
      <div className="header-form">
        <div className="logo partner-text">
          <h1>
            <a href="">{t("PARTNER_LOGIN.FLC_account")}</a>
          </h1>
        </div>

        <Navbar bg="light" expand="lg" className=" navbar-light bg-light">
          <Navbar.Toggle aria-controls="basic-navbar-nav-register">
            <span className="toggler-icon">
              <i className="fas fa-bars" />
            </span>
          </Navbar.Toggle>
          <Navbar.Collapse id="basic-navbar-nav-register">
            <Nav>
              <NavLink
                className="nav-item nav-link"
                to={routeName.PARTNER_LOGIN}
                activeClassName="active"
              >
                {t("PARTNER_LOGIN.PLG_login")}
              </NavLink>
              <NavLink
                className="nav-item nav-link"
                to={routeName.PARTNER_FIND_EMAIL}
                activeClassName="active"
              >
                {t("PARTNER_LOGIN.PLG_find_id")}
              </NavLink>
              <NavLink
                className="nav-item nav-link"
                to={routeName.PARTNER_FORGOT_PASSWORD}
                activeClassName="active"
              >
                {t("PARTNER_LOGIN.PLG_ResetPW")}
              </NavLink>
              <NavLink
                className="nav-item nav-link"
                to={routeName.PARTNER_REGISTER}
                activeClassName="active"
              >
                {t("PARTNER_LOGIN.PLG_Join")}
              </NavLink>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
  activeClassName = "active";
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(PartnerAuthHead)
);
