import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import PartnerAuthHead from "./Head";
import PartnerAuthFooter from "./Footer";

class PartnerAuthWrapper extends Component {
  render() {
    const { t } = this.props;
    return (
      <main className="main-container">
        <section className="form-content">
          <div className="container">
            <div className="main-form">
              <PartnerAuthHead />
              <div className="body-form">
                <div className="container-form">{this.props.children}</div>
              </div>
            </div>
          </div>
        </section>
      </main>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(PartnerAuthWrapper)
);
