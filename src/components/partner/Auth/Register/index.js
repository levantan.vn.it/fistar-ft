import React, { Component } from 'react';
import { withTranslation, Trans } from 'react-i18next';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'



class PartnerAuthWrapperRegister extends Component {
  render() {
    const { t } = this.props;
    return (
        <section className="register-partner-form">
            <div className="container">
                <div className="content-form-register">
                    <div className="row text-form">
                        <div className="col-md-12">
                            <h3><strong>Partner JOIN</strong>Let’s be with a new marketing platform, fi :Star!</h3>
                        </div>
                    </div>
                    <div className="row step-form">
                        {this.props.children}
                    </div>
                </div>
            </div>
        </section>
    );
  }
}

function mapStateToProps (state) {
  return {}
}

export default connect(mapStateToProps)(withTranslation('translations')(PartnerAuthWrapperRegister))
