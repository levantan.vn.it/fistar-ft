import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import { getRecentSNSAction } from "./../../../../store/actions/channel";
import { Tab, Nav } from "react-bootstrap";
import { nFormatter } from "./../../../../common/helper";
import imageFime from "../../../../images/mk.png";
import imageFb from "../../../../images/face.png";
import imageYt from "../../../../images/toutube.png";
import imageInstar from "../../../../images/intar.png";
import { CAMPAIGN_STATUS, SNS } from "../../../../constants";
import PieChart from "./../../../../components/charts/PieChart";
import { getImageLink } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";
import { DateFormatYMD } from "../../../../common/helper";

// import {AmCharts} from "@amcharts/amcharts3-react";

class CampaignHistory extends Component {
  renderReview = () => {
    const { campaigns, fistar } = this.props;
    if (!campaigns || !fistar) {
      return null;
    }
    return campaigns.map(campaign => {
      if (
        [CAMPAIGN_STATUS.ONGOING, CAMPAIGN_STATUS.CLOSED].includes(
          campaign.cp_status
        )
      ) {
        return campaign.matching_channel.map((sns, key) => {
          if (sns.m_ch_selected == 1 && sns.m_ch_url) {
            let imageSns = "";
            switch (sns.sns_id) {
              case 4: {
                imageSns = imageInstar;
                break;
              }
              case 3: {
                imageSns = imageYt;
                break;
              }
              case 2: {
                imageSns = imageFb;
                break;
              }
              case 1: {
                imageSns = imageFime;
                break;
              }
              default:
                break;
            }
            let dateObj = new Date(sns.updated_at);
            let month = dateObj.getUTCMonth() + 1; //months from 1-12
            let day = dateObj.getUTCDate();
            let year = dateObj.getUTCFullYear();

            // let newdate = year + "/" + month + "/" + day;
            let newdate = DateFormatYMD(sns.updated_at);
            return (
              <div className="item col-md-3 col-xs-6" key={key}>
                <div className="card">
                  <div className="top">
                    <div className="left text-truncate">
                      <div className="male mr-3">
                        <a href="javascript:void(0)">
                          <img src={imageSns} alt="" />
                        </a>
                      </div>
                      <div className="address-old">
                        <h4>
                          <a href="javascript:void(0)">{sns.m_ch_title}</a>
                        </h4>
                        <span>{newdate}</span>
                      </div>
                    </div>
                  </div>
                  <div className="img-partner">
                    <a href={sns.m_ch_url} target="_blank">
                      {" "}
                      <img
                        src={getImageLink(
                          fistar.picture,
                          IMAGE_TYPE.FISTARS,
                          IMAGE_SIZE.ORIGINAL
                        )}
                        alt={fistar.fullname}
                      />
                    </a>
                  </div>
                  <div className="card-footer">
                    <ul className="list-footer">
                      <li className="item">
                        <a href="javascript:void(0)">
                          <i className="far fa-heart" />
                        </a>
                        <span>{nFormatter(sns.m_ch_like)}</span>
                      </li>
                      <li className="item">
                        <a href="javascript:void(0)">
                          <i className="far fa-comment-dots" />
                        </a>
                        <span>{nFormatter(sns.m_ch_comment)}</span>
                      </li>
                      <li className="item">
                        <a href="javascript:void(0)">
                          <i className="fas fa-share-alt" />
                        </a>
                        <span>{nFormatter(sns.m_ch_share)}</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            );
          }
        });
      }
      return null;
    });
  };

  renderHistory = () => {
    const { campaigns } = this.props;
    if (!campaigns) {
      return null;
    }
    let dataFime = [];
    let dataSNS = [];
    let fime = {
      post: 0,
      like: 0,
      share: 0,
      comment: 0
    };
    let facebook = {
      post: 0,
      like: 0,
      share: 0,
      comment: 0
    };
    let youtube = {
      post: 0,
      like: 0,
      share: 0,
      comment: 0
    };
    let instagram = {
      post: 0,
      like: 0,
      share: 0,
      comment: 0
    };
    let totalPost = 0;
    campaigns.map(campaign => {
      if (
        [CAMPAIGN_STATUS.ONGOING, CAMPAIGN_STATUS.CLOSED].includes(
          campaign.cp_status
        )
      ) {
        let matching_channel = campaign.matching_channel.map(e => {
          if (e.m_ch_selected === 1 && e.m_ch_url) {
            switch (e.sns_id) {
              case SNS.FIME: {
                fime = {
                  post: fime.post + 1,
                  like: fime.like + e.m_ch_like,
                  share: fime.share + e.m_ch_share,
                  comment: fime.comment + e.m_ch_comment
                };
                totalPost += 1;
                break;
              }
              case SNS.FACEBOOK: {
                facebook = {
                  post: facebook.post + 1,
                  like: facebook.like + e.m_ch_like,
                  share: facebook.share + e.m_ch_share,
                  comment: facebook.comment + e.m_ch_comment
                };
                totalPost += 1;
                break;
              }
              case SNS.YOUTUBE: {
                youtube = {
                  post: youtube.post + 1,
                  like: youtube.like + e.m_ch_like,
                  share: youtube.share + e.m_ch_share,
                  comment: youtube.comment + e.m_ch_comment
                };
                totalPost += 1;
                break;
              }
              case SNS.INSTAGRAM: {
                instagram = {
                  post: instagram.post + 1,
                  like: instagram.like + e.m_ch_like,
                  share: instagram.share + e.m_ch_share,
                  comment: instagram.comment + e.m_ch_comment
                };
                totalPost += 1;
                break;
              }
              default:
                break;
            }
          }
        });
      }
    });
    if (!(!fime.like && !fime.comment && !fime.share)) {
      dataFime = [
        { name: "Like", value: fime.like, color: "#FC3667" },
        { name: "Reply", value: fime.comment, color: "#FF9FB6" },
        { name: "Share", value: fime.share, color: "#FFDEE6" }
      ];
    }
    if (!(!facebook.like && !youtube.like && !instagram.like)) {
      dataSNS = [
        { name: "Facebook", value: facebook.like, color: "#48BAFD" },
        { name: "Youtube", value: youtube.like, color: "#7FC4FD" },
        { name: "Instagram", value: instagram.like, color: "#BCE0FD" }
      ];
    }
    return (
      <div className="list-campaihistory">
        <div className="post-campai w-40">
          <p className="text-center">
            {this.props.t("SEARCH_DETAIL.SDL_posts_campaign")}
          </p>
          <h2 className="text-center">{totalPost}</h2>
          <div className="list-solical-post">
            <div className="item">
              <a href="javascript:void(0)">
                <img src={imageFime} alt="" />
              </a>
              <span>{fime.post}</span>
            </div>
            <div className="item">
              <a href="javascript:void(0)">
                <img src={imageFb} alt="" />
              </a>
              <span>{facebook.post}</span>
            </div>
            <div className="item">
              <a href="javascript:void(0)">
                <img src={imageInstar} alt="" />
              </a>
              <span>{instagram.post}</span>
            </div>
            <div className="item">
              <a href="javascript:void(0)">
                <img src={imageYt} alt="" />
              </a>
              <span>{youtube.post}</span>
            </div>
          </div>
        </div>
        <div className="campaign-engagement w-30 p-0">
          <p className="text-center">
            {this.props.t("SEARCH_DETAIL.SDL_fi_me_engagement")}
          </p>
          {dataFime.length > 0 ? (
            <PieChart data={dataFime} fill="#ffffff" />
          ) : (
            <div className="chart-empty">
              {this.props.t("SEARCH_DETAIL.SDL_data_chart")}
            </div>
          )}
        </div>
        <div className="campaign-sns-Chanel w-30 p-0">
          <p className="text-center">
            {this.props.t("SEARCH_DETAIL.SDL_sns_channel")}
          </p>
          {dataSNS.length > 0 ? (
            <PieChart data={dataSNS} fill="#ffffff" />
          ) : (
            <div className="chart-empty">
              {this.props.t("SEARCH_DETAIL.SDL_data_chart")}
            </div>
          )}

          <p />
        </div>
      </div>
    );
  };

  render() {
    return (
      <Fragment>
        <div className="campai-history">{this.renderHistory()}</div>
        <div className="list-flow-partner">
          <div className="list-username-partner list-partner-detail">
            <div className="list-item  list-partner row">
              {this.renderReview()}
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    getRecentSNS: (uid, channel) => dispatch(getRecentSNSAction(uid, channel))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(CampaignHistory));
