import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";

// import calendar style
// You can customize style by copying asset folder.
import "@y0c/react-datepicker/assets/styles/calendar.scss";
import { format } from "date-fns";
// import PushMessageComponent from './../../../components/PushMessage'

import imgaeIdol from "./../../../../images/dash-02.png";
import closePopup from "./../../../../images/close-popup.svg";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";

class PopupChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }

  CloseModal = () => {
    this.props.closeModal();
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  handleShow = () => {
    this.setState({ show: true });
  };

  render() {
    const { t } = this.props;
    const { height } = this.state;

    return (
      <Fragment>
        <div className="history-information" onClick={this.handleShow}>
          <div className="text">
            <p>History</p>
            <h4>31</h4>
          </div>
        </div>

        <Modal
          size="lg"
          show={this.state.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="modal-partner-pass"
        >
          <div className="popup-partner-pass">
            <div className="top">
              <h4>Partner</h4>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <img src={closePopup} alt="" />
              </button>
            </div>
            <div className="content">
              <p>The Password has been reset. </p>
              <p>Please login again.</p>
              <button>OK</button>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PopupChangePassword));
