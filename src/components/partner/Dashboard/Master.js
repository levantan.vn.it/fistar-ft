import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import PartnerDashboardHead from "./Layout/Head";
import PartnerDashboardFooter from "./Layout/Footer";
import PartnerSideBar from "./Layout/SideBar";
import "./master.scss";

import BackToTop from "./../../partial/BackToTop";

class PartnerMaster extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isToggleOnSibar: true
    };
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    const { t } = this.props;
    const { dataTogle } = this.state.isToggleOnSibar;
    return (
      <div className="wrapper">
        <PartnerDashboardHead />

        {this.props.children}

        <BackToTop />
        <PartnerDashboardFooter />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(PartnerMaster)
);
