import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import imageClose from "../../../../images/close-popup.svg";
import { getHistoryAction } from "../../../../store/actions/fistar";
import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import { MATCHING_STATUS, GENDER_TYPE } from "../../../../constants/index";
import { DateFormatYMD } from "../../../../common/helper";
import { getImageLink } from './../../../../common/helper'
import { IMAGE_SIZE, IMAGE_TYPE } from './../../../../constants/index'

class ModalHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: null,
      loading: false,
      fistar: {},
      campaigns: []
    };
  }

  handleClose = () => {
    this.props.onCloseModal();
  };

  opened = () => {
    this.setState(
      {
        loading: true
      },
      () => {
        console.log("opend");
        this.props
          .getHistory(this.props.uid)
          .then(response => {
            console.log(response, "response");
            this.setState({
              fistar: response,
              campaigns: response.campaigns,
              loading: false
            });
          })
          .catch(() => {
            this.setState({
              loading: false
            });
          });
      }
    );
  };

  renderCampaignHistory = (campaigns, status, className = "") => {
    console.log(campaigns);
    return (
      <div className="process-campaign">
        {campaigns.map((campaign, key) => (
          <div className={`item ${className}`} key={key}>
            <div className="top">
              <h4>{campaign.cp_name}</h4>
              <h4>{status}</h4>
            </div>
            <div className="process">
              {campaign.matchings[0].matching_history.map((status, key) => {
                let last_status = status.label.partner_status;
                if (last_status) {
                  last_status = status.label.partner_status.split("→");
                  last_status = last_status[last_status.length - 1].trim();
                  if (
                    key ===
                    campaign.matchings[0].matching_history.length - 1
                  ) {
                    return (
                      <p className="text-partner" key={key}>
                        {DateFormatYMD(status.created_at)}{" "}
                        <span>{last_status}</span>
                      </p>
                    );
                  }
                  return (
                    <Fragment key={key}>
                      <p className="text-fistar">
                        {DateFormatYMD(status.created_at)}{" "}
                        <span>{last_status}</span>
                      </p>
                      <p className="dots" />
                      <p className="dots" />
                      <p className="dots" />
                    </Fragment>
                  );
                }
              })}
            </div>
          </div>
        ))}
      </div>
    );
  };

  render() {
    console.log(this.props, "this.props.aaa");
    const { loading, campaigns, fistar } = this.state;

    console.log(campaigns, "campaigns");
    let matched = [];
    let considering = [];
    let rejected = [];
    campaigns.map(campaign => {
      if (
        campaign.matchings &&
        campaign.matchings[0] &&
        campaign.matchings[0].matching_status
      ) {
        let m_status = campaign.matchings[0].matching_status.m_status;
        if (MATCHING_STATUS.MARCHED.includes(m_status)) {
          matched.push(campaign);
          return;
        }
        if (MATCHING_STATUS.REJECTED.includes(m_status)) {
          rejected.push(campaign);
          return;
        }
        // if (MATCHING_STATUS.CONSIDERING.includes(m_status)) {
        considering.push(campaign);
        return;
        // }
      }
    });

    return (
      <Fragment>
        <Modal
          size="lg"
          show={this.props.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="popup-history"
          onEntered={this.opened}
        >
          <div className="history">
            <div className="modal-header">
              <h4 id="exampleModalLabel">History</h4>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.handleClose}
              >
                <img src={imageClose} alt="" />
              </button>
            </div>
            <div className="body-popup">
              <div className="content-proceed">
                <div className="information-user">
                  <div className="left">
                    <div className="image-user">
                      <img src={getImageLink(fistar.picture, IMAGE_TYPE.FISTARS, IMAGE_SIZE.ORIGINAL)} alt={fistar.fullname} />
                    </div>
                    <div className="name">
                      {fistar.gender &&
                      fistar.gender.cd_id === GENDER_TYPE.MALE ? (
                        <i className="fas fa-venus" />
                      ) : (
                        <i className="fas fa-mars" />
                      )}
                      <h4>
                        <a href="javascript:void(0)">{fistar.fullname}</a>
                      </h4>
                    </div>
                  </div>
                  <div className="right">
                    <button>{campaigns.length} Cases</button>
                  </div>
                </div>
                <Tab.Container id="tabs-fistar" defaultActiveKey={"Matched"}>
                  <div className="tab-content">
                    <Nav variant="pills" className="list-tab">
                      <Nav.Item className="item">
                        <Nav.Link className="link-tab" eventKey="Matched">
                          <label>Matched</label>
                          <p className="amount pb-3">{matched.length}</p>
                        </Nav.Link>
                      </Nav.Item>
                      <Nav.Item className="item">
                        <Nav.Link className="link-tab" eventKey="Considering">
                          <label>Considering</label>
                          <p className="amount pb-3">{considering.length}</p>
                        </Nav.Link>
                      </Nav.Item>
                      <Nav.Item className="item">
                        <Nav.Link className="link-tab" eventKey="Rejected">
                          <label>Rejected</label>
                          <p className="amount pb-3">{rejected.length}</p>
                        </Nav.Link>
                      </Nav.Item>
                    </Nav>
                  </div>
                  <Tab.Content>
                    <Tab.Pane eventKey="Matched">
                      {this.renderCampaignHistory(matched, "Matched")}
                    </Tab.Pane>
                    <Tab.Pane eventKey="Considering">
                      {this.renderCampaignHistory(considering, "Considering")}
                    </Tab.Pane>
                    <Tab.Pane eventKey="Rejected">
                      {this.renderCampaignHistory(
                        rejected,
                        "Rejected",
                        "item-rejected"
                      )}
                    </Tab.Pane>
                  </Tab.Content>
                </Tab.Container>
              </div>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    getHistory: uid => dispatch(getHistoryAction(uid))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(ModalHistory));
