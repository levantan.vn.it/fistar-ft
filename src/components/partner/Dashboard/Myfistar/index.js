import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import imgaeIdol from "./../../../../images/dash-02.png";
import imageFace from "./../../../../images/dashboard/face.png";
import imageIntar from "./../../../../images/dashboard/intar.png";
import imageToutube from "./../../../../images/toutube.png";
import imageRead from "./../../../../images/mk.png";
import ModalStatusCampaign from "./../../../fiStar/Dashboard/MyCampaign/ModalStatusCampaign";
import {
  nextStepAction,
  countScrapAction
} from "./../../../../store/actions/campaign";
import ImgExample from "./../../../../images/example.jpg";

import { nFormatter } from "./../../../../common/helper";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import "./myfistar.scss";
import ModalHistory from "./ModalHistory";
import { getImageLink } from "./../../../../common/helper";
import {
  IMAGE_SIZE,
  IMAGE_TYPE,
  STATUS_STEP_PARTNER,
  PARTNER_FINISH_STEP,
  CAMPAIGN_STATUS
} from "./../../../../constants/index";
import Scrap from "./../../../../components/Scrap";
import ModalDetailFistar from "./../../../../components/partner/Dashboard/DetailFistar/detailFistar";

const MATCHING = {
  REQUEST: [1, 2, 3, 4, 5, 6, 7, 8],
  APPLY: [9, 10, 11, 12, 13, 14, 15, 16]
};

class Fistar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      step: null,
      loading: false,
      clickScrap: false,
      fime: 0,
      facebook: 0,
      youtube: 0,
      instagram: 0,
      step: null,
      showDetail: false,
      uid: ''
    };
  }

  componentDidMount() {
    const { fistar } = this.props;
    var fime = "";
    var facebook = "";
    var youtube = "";
    var instagram = "";

    fistar &&
      fistar.channels &&
      fistar.channels.map(channel => {
        if (channel.sns_id == 1) {
          fime = channel.usn_follower;
        }
        if (channel.sns_id == 2) {
          facebook = channel.usn_follower;
        }
        if (channel.sns_id == 3) {
          youtube = channel.usn_follower;
        }
        if (channel.sns_id == 4) {
          instagram = channel.usn_follower;
        }
      });
    this.setState({
      fime: fime,
      facebook: facebook,
      youtube: youtube,
      instagram: instagram
    });
  }

  OpenModal = e => {
    e.preventDefault();
    this.setState({ show: true });
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  onCloseModal = () => {
    this.setState({ show: !this.state.show });
  };
  clickScrap = () => {
    const { fistar } = this.props;
    this.props.countScrap(fistar.uid, fistar.cp_id).then(response => {
      this.setState({});
    });
    this.setState({
      clickScrap: !this.state.clickScrap
    });
  };

  showHistory = uid => {
    this.setState({
      show: true,
      uid: uid
    });
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  onCloseModal = () => {
    this.setState({ show: !this.state.show });
  };

  onClickNextStep = (m_id, stt_id) => {
    if (!this.state.loading) {
      this.setState(
        {
          loading: true
        },
        () => {
          this.props
            .nextStep(m_id, stt_id)
            .then(response => {
              this.setState({
                step: stt_id,
                loading: false
              });
              this.props.reloadData();
            })
            .catch(er => {
              this.setState({
                loading: false
              });
            });
        }
      );
    }
  };

  OpenModal = uid => {
    this.setState({
      showDetail: true,
      uid: uid
    });
  };

  onCloseModalDetail = () => {
    this.setState({ showDetail: false });
  };

  renderButtonNextStep = (m_id, step) => {
    let nextSteps = STATUS_STEP_PARTNER[step];
    return (
      <Fragment>
        {nextSteps.button.map((nextStep, key) => (
          <button
            key={key}
            type={"button"}
            onClick={() => this.onClickNextStep(m_id, nextStep.id)}
          >
            {this.state.loading ? (
              <div className="spinner-border" role="status">
                {/*<span className="sr-only">Loading...</span>*/}
              </div>
            ) : (
              <Fragment>{nextStep.text}</Fragment>
            )}
          </button>
        ))}
      </Fragment>
    );
  };

  render() {
    const { t, fistar, auth, pid } = this.props;

    // let apply = (fistar.matchings || []).map(fistar => {
    //   let status = fistar.matching_status.m_status;
    //   if (MATCHING.APPLY.includes(status)) {
    //     return fistar;
    //   }
    //   return null;
    // });
    // apply = apply.filter(e => e !== null);
    // let recruit = fistar.cp_total_influencer || 0;

    // let year = new Date(fistar.dob);
    // let yearConver = year.toDateString();
    // let yearFistar = yearConver.split(" ").pop();
    // fistar.dob = yearFistar;

    let { m_status, partner_status } = fistar;
    let lastCampaign = fistar.fistar_campaigns[fistar.fistar_campaigns.length - 1]
    let matching = null
    let stt_id = null
    let status = "";
    if (lastCampaign) {
      // lastCampaign.matchings.map((mat) => {
      //   if (fistar.uid == mat.uid) {
      //     matching = mat
      //   }
      // })
      stt_id = lastCampaign.m_status
      if (this.state.step) {
        partner_status = STATUS_STEP_PARTNER[this.state.step].display;
        stt_id = this.state.step;
      }
      switch (lastCampaign.cp_status) {
        case 59: {
          status = "Matching";
          break;
        }
        case 60: {
          status = "Ready";
          break;
        }
        case 61: {
          status = "On-Going";
          break;
        }
        case 62: {
          status = "Closed";
          break;
        }
        default: {
          break;
        }
      }
    }

    return (
      <div className="box-item">
        <div className="left">
          <div 
            className="image-box h-100"
          >
            <img
              src={getImageLink(
                fistar.avatar,
                IMAGE_TYPE.FISTARS,
                IMAGE_SIZE.ORIGINAL
              )}
              alt=""
              className="cursor-pointer"
              onClick={() => this.OpenModal(fistar.uid)}
            />
            {this.props.scrapChoose ? (
              <Scrap
                scraps_count={fistar.scrap_total}
                is_scrap={fistar.is_scrap}
                id={fistar.uid}
                callback={this.props.callback}
              />
            ) : (
              <Scrap
                scraps_count={fistar.scrap_total}
                is_scrap={fistar.is_scrap}
                id={fistar.uid}
                callback={this.props.callback}
              />
            )}
          </div>
          <div className="information-user">
            <div className="name">
              <h4 onClick={() => this.OpenModal(fistar.uid)}>
                <i
                  className={`fa ${
                    fistar &&
                    fistar.gender &&
                    fistar.gender === 9
                      ? " fa-mars"
                      : " fa-venus"
                  }`}
                />
                {fistar.name}
              </h4>
              <div className="address-old">
                <span>{fistar.birth_year}</span>
                <span className="address">{fistar.location_label}</span>
              </div>
            </div>
            <div className="solical-flow">
              <div className="item">
                <a href={fistar.facebook_url} target="_blank">
                  <i className="fab fa-facebook-f" />
                </a>
                <span>{nFormatter(fistar.facebook_follower, 0)}</span>
              </div>
              <div className="item">
                <a href={fistar.instagram_url} target="_blank">
                  <i className="fab fa-instagram" />
                </a>
                <span>{nFormatter(fistar.instagram_follower, 0)}</span>
              </div>
              <div className="item">
                <a href={fistar.youtube_url} target="_blank">
                  <i className="fab fa-youtube-square" />
                </a>
                <span>{nFormatter(fistar.youtube_follower, 0)}</span>
              </div>
            </div>
          </div>
        </div>
        <div className="suggested-price">
          {lastCampaign && (
            <Fragment>
              <div className="top-matching">
                <p>{status}</p>
                <h4>{lastCampaign.cp_name}</h4>
              </div>
              <div className="status-matching">
                <span className="icon-start" />
                <Fragment>
                  {lastCampaign.partner_status.split("→").map((step, key) => {
                    if (lastCampaign.partner_status.split("→").length === key + 1) {
                      return (
                        <Fragment key={key}>
                          <p className="font-weight-bold step-partner-active">{step}</p>
                        </Fragment>
                      );
                    }
                    return (
                      <Fragment key={key}>
                        <p>{step}</p>
                        <span />
                      </Fragment>
                    );
                  })}
                </Fragment>
                {!PARTNER_FINISH_STEP.includes(stt_id) && lastCampaign.cp_status !== CAMPAIGN_STATUS.CLOSED && 
                  <i className="fas fa-long-arrow-alt-right" />
                }
                {auth.id == lastCampaign.pid && lastCampaign.cp_status !== CAMPAIGN_STATUS.CLOSED
                  ? this.renderButtonNextStep(lastCampaign.m_id, stt_id)
                  : ""}
              </div>
            </Fragment>
          )}
          <div className="price">
            <div className="item">
              <a href="javascript:void(0)">
                {" "}
                <img src={imageRead} alt="" />
              </a>
              <p>
                {(+fistar.fime_cost).toLocaleString()} <span>VND</span>
              </p>
            </div>
            <div className="item">
              <a href="javascript:void(0)">
                <img src={imageFace} alt="" />
              </a>
              <p>
                {(+fistar.facebook_cost).toLocaleString()}{" "}
                <span>VND</span>
              </p>
            </div>
            <div className="item">
              <a href="javascript:void(0)">
                <img src={imageToutube} alt="" />
              </a>
              <p>
                {(+fistar.youtube_cost).toLocaleString()}{" "}
                <span>VND</span>
              </p>
            </div>
            <div className="item">
              <a href="javascript:void(0)">
                <img src={imageIntar} alt="" />
              </a>
              <p>
                {(+fistar.instagram_cost).toLocaleString()}{" "}
                <span>VND</span>
              </p>
            </div>
          </div>
        </div>
        <div
          className="history-information"
          onClick={() => this.showHistory(fistar.uid)}
        >
          <div className="text">
            <p>History</p>
            <h4>{fistar.history_count}</h4>
          </div>
        </div>

        {this.state.show &&
          this.state.uid && (
            <ModalHistory
              show={this.state.show}
              onCloseModal={this.onCloseModal}
              uid={this.state.uid}
            />
          )}

        {this.state.showDetail &&
          this.state.uid && (
            <ModalDetailFistar
              show={this.state.showDetail}
              onCloseModal={this.onCloseModalDetail}
              uid={this.state.uid}
            />
          )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    nextStep: (m_id, stt_id) => dispatch(nextStepAction(m_id, stt_id)),
    countScrap: (uid, cp_id) => dispatch(countScrapAction(uid, cp_id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Fistar));
