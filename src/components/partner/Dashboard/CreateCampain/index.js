import React, { Component } from 'react';
import { withTranslation, Trans } from 'react-i18next';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'



class PartnerCreateCampainWrapper extends Component {
  render() {
    const { t } = this.props;
    return (
        <div className="content-site-campai">
            <div className="container">
                <div className="content-campaign">
                    <div className="row">
                        {this.props.children}
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

function mapStateToProps (state) {
  return {}
}

export default connect(mapStateToProps)(withTranslation('translations')(PartnerCreateCampainWrapper))
