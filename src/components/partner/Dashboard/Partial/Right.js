import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getNewFistarAction } from "./../../../../store/actions/campaign";
import PartnerDashboardQA from "./QA";
import {
  getImageLink,
  nFormatter,
  getChannelFollower
} from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";
import Scrap from "./../../../../components/Scrap";
import { SNS_CHANNEL } from "../../../../constants/index";
import {calcAge} from "../../../../common/helper";
import ModalDetailFistar from "./../../../../components/partner/Dashboard/DetailFistar/detailFistar";


class PartnerDashboardRight extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fistars: [],
      matching: 0,
      scrap: 0,
      request: 0,
      apply: 0,
      recommended: 0,
      total: 0,
        show: false,
        newFistarDashboard: false,
        uid: "",
      isLoadingFistar: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingFistar: true
      },
      () => {
        this.props.getNewFistar().then(response => {
          this.setState({
            fistars: response.data,
            matching: response.matching,
            scrap: response.scrap,
            request: response.request,
            apply: response.apply,
            recommended: response.recommended,
            total: response.total,
            isLoadingFistar: false
          });
        });
      }
    );
  }

    onCloseModal = () => {
        this.setState({ show: !this.state.show });
    };

    OpenModal = uid => {
        this.setState({
            show: true,
            uid: uid,
            newFistarDashboard:true
        });
    };

  renderFistar = fistar => {
    if (!fistar) {
      return null;
    }

    var fime = "";
    var facebook = "";
    var youtube = "";
    var instagram = "";

    fistar &&
      fistar.channels &&
      fistar.channels.map(channel => {
        if (channel.sns_id == 1) {
          fime = channel.usn_follower;
        }
        if (channel.sns_id == 2) {
          facebook = channel.usn_follower;
        }
        if (channel.sns_id == 3) {
          youtube = channel.usn_follower;
        }
        if (channel.sns_id == 4) {
          instagram = channel.usn_follower;
        }
      });
    let yearCurrent = new Date().getFullYear();
    let oldCurrent = yearCurrent - fistar.dob.split("-")[0];

    return (
      <div className="card wf-card-qa" key={fistar.uid} onClick={() => this.OpenModal(fistar.uid)}>
        <div className="top">
          <div className="left-card">
            <div className="male">
              {fistar.gender && fistar.gender.cd_id === 10 ? (
                <i className="fas fa-venus" />
              ) : (
                <i className="fas fa-mars" />
              )}
            </div>
            <div className="address-old">
              <h4>
                <a href="javascript:void(0)">{fistar.fullname}</a>
              </h4>
              <div className="wf-address">
                <span>
                  {fistar.dob.split("-")[0]}({calcAge(fistar.dob)})
                </span>
                <span className="address">
                  {fistar.location ? fistar.location.cd_label : ""}
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="img-partner">
          <a href="javascript:void(0)">
            <img
              src={getImageLink(
                fistar.picture,
                IMAGE_TYPE.FISTARS,
                IMAGE_SIZE.ORIGINAL
              )}
              alt={fistar.fullname}
            />
          </a>
          <Scrap
            scraps_count={fistar.scraps_count}
            is_scrap={fistar.is_scrap}
            id={fistar.uid}
            className="btn over-flow"
            callback={this.props.callback}
          />
        </div>
        <div className="card-footer">
          <ul className="list-footer">
            <li className="item">
              <a href="javascript:void(0)">
                <i className="fab fa-facebook-f" />
              </a>
              <span>{nFormatter(facebook, 0)}</span>
            </li>
            <li className="item">
              <a href="javascript:void(0)">
                <i className="fab fa-instagram" />
              </a>
              <span>{nFormatter(instagram, 0)}</span>
            </li>
            <li className="item">
              <a href="javascript:void(0)">
                <i className="fab fa-youtube-square" />
              </a>
              <span>{nFormatter(youtube, 0)}</span>
            </li>
          </ul>
        </div>
      </div>
    );
  };

  render() {
    const { t } = this.props;
    const {
      fistars,
      isLoadingFistar,
      matching,
      scrap,
      request,
      apply,
      recommended,
      total
    } = this.state;

    // if (isLoadingFistar) {
    //   return <div className="campaign-track">
    //     <h1>loading...</h1>
    //   </div>
    // }
    return (
      <Fragment>
        <div className="left">
          <PartnerDashboardQA />

          <div className="content-q-a new-fistar">
            <div className="top">
              <h4>{this.props.t("DASHBOARD_FISTAR.DBF_new_fistar")}</h4>
              {/*<button>View All</button>*/}
            </div>
            <div className="card-group">
              {fistars &&
                fistars.map((value, i) => {
                  if (i <= 4) {
                    return this.renderFistar(fistars[i]);
                  }
                })}
            </div>
          </div>
        </div>
        <div className="right">
          <div className="content-q-a">
            <div className="card-group">
              {fistars &&
                fistars.map((value, i) => {
                  if (i > 4) {
                    return this.renderFistar(fistars[i]);
                  }
                })}
            </div>
          </div>
        </div>

          {this.state.show &&
          this.state.uid && (
              <ModalDetailFistar
                  show={this.state.show}
                  onCloseModal={this.onCloseModal}
                  newFistarDashboard={this.state.newFistarDashboard}
                  uid={this.state.uid}
              />
          )}

      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    getNewFistar: () => dispatch(getNewFistarAction())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerDashboardRight));
