import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import * as routeName from "./../../../../routes/routeName";
import count from "../../../../store/reducer/count";
import { getCampaignAction } from "./../../../../store/actions/campaign";
import InfiniteScroll from "react-infinite-scroll-component";
import { getImageLink, getId } from './../../../../common/helper'
import { IMAGE_SIZE, IMAGE_TYPE } from './../../../../constants/index'
import CampaignMainImage from './../../../../components/CampaignMainImage'
const VIDEO = {
  WIDTH: 450,
  HEIGHT: 200,
}

class PartnerCampaignTracking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      campaigns: [],
      matching: 0,
      ready: 0,
      ongoing: 0,
      closed: 0,
      total: 0,
      isLoadingCampaign: false,
      last_page: 1,
      page: 1,
      hasMore: true,
      isRedirect: false,
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingCampaign: true
      },
      () => {
        this.props.getCampaign(this.state.page).then(response => {
          this.setState({
            campaigns: response.data,
            matching: response.matching,
            ready: response.ready,
            ongoing: response.ongoing,
            closed: response.closed,
            total: response.total,
            isLoadingCampaign: false,
            last_page: response.last_page
          });
        });
      }
    );
  }

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }
    this.props.getCampaign(this.state.page + 1).then(response => {
      this.setState({
        campaigns: [...this.state.campaigns, ...response.data],
        page: response.current_page
      });
    });
  };

  redirectToCampaignTracking = () => {
    this.setState({
      isRedirect: true
    });
  };

  renderCountMatching = (recruit, count_status, campaign) => (
    <div className="footer-card">
      <div className="list-item">
        <div className="item">
          <p>
            <span>{recruit}</span>
          </p>
          <span>
            <a href="#">Recruiting fiStar</a>
          </span>
        </div>
        <div className="item">
          <p>
            <span>{count_status && count_status.matched ? count_status.matched : 0}</span>
          </p>
          <span>
            <a href="#">Matched</a>
          </span>
        </div>
        <div className="item">
          <p>
            <span>{count_status && count_status.requets_without_matched ? count_status.requets_without_matched : 0}</span>
          </p>
          <span>
            <a href="#">Request</a>
          </span>
        </div>
        <div className="item">
          <p>
            <span>{count_status && count_status.apply_without_matched ? count_status.apply_without_matched : 0}</span>
          </p>
          <span>
            <a href="#">Apply</a>
          </span>
        </div>
      </div>
    </div>
  );

  renderCampaign = () => {
    const { campaigns } = this.state;

    return campaigns.map((campaign, key) => {
      let status = "";
      switch (campaign.cp_status) {
        case 59: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_matching")}`;
          break;
        }
        case 60: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_ready")}`;
          break;
        }
        case 61: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_ongoing")}`;
          break;
        }
        case 62: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_close")}`;
          break;
        }
        default: {
          break;
        }
      }
      const { t } = this.props;
      return (
        <Fragment key={key}>
          <div className="item wf-item-matching">
            <div className="img-matching">
              <Link to={"/partner/campaign/" + campaign.cp_slug}>
                <CampaignMainImage campaign={campaign} className="w-100" />
              </Link>
              <span className="overlay-matching">{status}</span>
            </div>
            <div className="title">
              <Link to={"/partner/campaign/" + campaign.cp_slug}>
                <h4>{campaign.cp_name}</h4>
              </Link>
              <p>
                <Link to={"/partner/campaign/" + campaign.cp_slug}>
                  {campaign.cp_period_start.split(" ")[0]}{" "}
                  {t("DASHBOARD_FISTAR.DFR_open")}
                </Link>
              </p>
            </div>

            {/*{campaign.cp_status === 59 && (*/}
            {/*<Fragment>*/}
            {/*{this.renderCountMatching(*/}
            {/*campaign.cp_total_influencer,*/}
            {/*campaign.count_status*/}
            {/*)}*/}
            {/*</Fragment>*/}
            {/*)}*/}

            <Fragment>
              {this.renderCountMatching(
                campaign.cp_total_influencer,
                campaign.statitics,
                campaign,
              )}
            </Fragment>
          </div>
        </Fragment>
      );
    });
  };

  render() {
    const { t, height, count, auth} = this.props;
    const {
      campaigns,
      isLoadingCampaign,
      matching,
      ready,
      ongoing,
      closed,
      total,
      isRedirect
    } = this.state;
    console.log(auth);
    console.log(auth.name);
    if (isRedirect) {
      return <Redirect to={routeName.PARTNER_CAMPAIGN_TRACKING} />;
    }

    const loader = (
      <div className="campaign-track">
        <h1>{t("LOADING.LOADING")}</h1>
      </div>
    );

    // if (isLoadingCampaign) {
    //   return <div className="campaign-track">
    //     <h1>loading...</h1>
    //   </div>
    // }

    return (
      <Fragment>
        <div className="header-track">
          <div className="top">
            <h4>{t("DASHBOARD_FISTAR.DBF_partner")} {auth.name}'s  {t("DASHBOARD_FISTAR.DFR_campaign")}</h4>
            <button type="button" onClick={this.redirectToCampaignTracking}>
              {t("DASHBOARD_FISTAR.DBF_button_more")}
            </button>
          </div>
          <div className="amount-notification">
            <h2>{total}</h2>
          </div>
          <div className="list-amount">
            <div className="item">
              <p>
                <span>{count ? count.matching : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DBF_status_matching")}</span>
            </div>
            <div className="item">
              <p>
                <span>{count ? count.ready : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DBF_status_ready")}</span>
            </div>
            <div className="item">
              <p>
                <span>{count ? count.ongoing : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DBF_status_on_going")}</span>
            </div>
            <div className="item closed">
              <p>
                <span>{count ? count.closed : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DBF_close")}</span>
            </div>
          </div>
        </div>
        <div className="create-campai">
          <i className="fas fa-pen" />
          <span>
            <NavLink to={routeName.PARTNER_CREATE_CAMPAIN}>
              {t("DASHBOARD_FISTAR.DBF_button_create")}
            </NavLink>
          </span>
        </div>
        <div className="">
          {!isLoadingCampaign && this.state.campaigns.length === 0 ? (
            <Fragment>
              <div className={"text-center mt-5"}>
                {t("CAMPAIGN.CAMPAIGN_no_campaign")}
              </div>
            </Fragment>
          ) : (
            <InfiniteScroll
              dataLength={this.state.campaigns.length}
              next={this.fetchMoreData}
              loader={<h4>{t("LOADING.LOADING")}</h4>}
              hasMore={this.state.hasMore}
              height={height}
              className="card-matching"
            >
              {this.renderCampaign()}
            </InfiniteScroll>
          )}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth:state.auth
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getCampaign: (page = 1) => dispatch(getCampaignAction(page, "", 1))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withTranslation("translations")(PartnerCampaignTracking));
