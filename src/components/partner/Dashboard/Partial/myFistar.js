import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { getFistarAction } from "./../../../../store/actions/campaign";
import InfiniteScroll from "react-infinite-scroll-component";
import * as routeName from "./../../../../routes/routeName";
import ModalHistory from "./ModalHistory";
import { getImageLink } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";
import ModalDetailFistar from "./../../../partner/Dashboard/DetailFistar/detailFistar";
import {
  STATUS_STEP_PARTNER,
  PARTNER_FINISH_STEP,
  CAMPAIGN_STATUS
} from "../../../../constants/index";

class PartnerMyFistar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fistars: [],
      matching: 0,
      scrap: 0,
      request: 0,
      apply: 0,
      recommended: 0,
      total: 0,
      isLoadingFistar: false,
      last_page: 1,
      page: 1,
      hasMore: true,
      isRedirect: false,
      showFistarDetail: false,
      show: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingFistar: true
      },
      () => {
        this.props.getFistar().then(response => {
          this.setState({
            fistars: response.data,
            matching: response.matching,
            scrap: response.scrap,
            request: response.request,
            apply: response.apply,
            recommended: response.recommended,
            total: response.total,
            isLoadingFistar: false,
            last_page: response.last_page
          });
        });
      }
    );
  }

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }
    this.props.getFistar(this.state.page + 1).then(response => {
      this.setState({
        fistars: [...this.state.fistars, ...response.data],
        page: response.current_page
      });
    });
  };

  onClickMore = () => {
    this.setState({
      isRedirect: true
    });
  };

  showHistory = uid => {
    this.setState({
      show: true,
      uid: uid
    });
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  onCloseModal = () => {
    this.setState({ show: !this.state.show });
  };
  onCloseModalFistar = () => {
    this.setState({ showFistarDetail: !this.state.showFistarDetail });
  };
  openFistar = uid => {
    this.setState({
      showFistarDetail: true,
      uid: uid
    });
  };
  clickisSearchFiStar = () => {
    this.props.clickisSearchFiStar();
  };
  renderButtonNextStep = (m_id, step) => {
    let nextSteps = STATUS_STEP_PARTNER[step];
    return (
      <Fragment>
        {nextSteps.button.map((nextStep, key) => (
          <button
            key={key}
            type={"button"}
            onClick={() => this.onClickNextStep(m_id, nextStep.id)}
          >
            {this.state.loading ? (
              <div className="spinner-border" role="status">
                {/*<span className="sr-only">Loading...</span>*/}
              </div>
            ) : (
              <Fragment>{nextStep.text}</Fragment>
            )}
          </button>
        ))}
      </Fragment>
    );
  };
  renderFistars = () => {
    const { fistars } = this.state;
    console.log(fistars);
    const { t } = this.props;
    return fistars.map((fistar, key) => {
      let status = "";
      let stt_id = "";
      console.log(fistar, "44444444444444444444444");
      switch (fistar.cp_status) {
        case 59: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_matching")}`;
          break;
        }
        case 60: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_ready")}`;
          break;
        }
        case 61: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_ongoing")}`;
          break;
        }
        case 62: {
          status = `${this.props.t("DASHBOARD_FISTAR.DFR_status_close")}`;
          break;
        }
        default: {
          break;
        }
      }
      console.log(status, '234234234');
      console.log(fistar);
      let label = fistar.fistar_campaign &&
        fistar.fistar_campaign[0] &&
        fistar.fistar_campaign[0].cp_status
          ? fistar.fistar_campaign[0]
              .label.cd_label
          : ""
          console.log(label, 'labellabellabellabellabellabel');
      return (
        <div className="item wf-item-fistar" key={key}>
          <div className="img-fistar">
            <a
              href="javascript:void(0)"
              onClick={() => this.openFistar(fistar.uid)}
            >
              <img
                src={getImageLink(
                  fistar.picture,
                  IMAGE_TYPE.FISTARS,
                  IMAGE_SIZE.ORIGINAL
                )}
                alt={fistar.fullname}
                title={fistar.fullname}
              />
            </a>
            {label && <span className="overlay-fistar">
              {label}
            </span>}
          </div>
          <div className="title">
            <h4>{fistar.fullname}</h4>
            <p
              onClick={() => this.showHistory(fistar.uid)}
              className="histtory-fistar"
            >
              <a href="javascript:void(0)">
                {t("POPUP_HISTORY.PHY_history")} {fistar.history_from_partner}
              </a>
            </p>
          </div>
          {fistar.fistar_campaign[0] && fistar.fistar_campaign[0].partner_status && (
            <div className="footer-card">
              <span className="icon-start" />
              {fistar.fistar_campaign[0].partner_status.split("→").map((step, key) => {
                if (fistar.fistar_campaign[0].partner_status.split("→").length === key + 1) {
                  return (
                    <Fragment key={key}>
                      <p className="font-weight-bold step-fistar-active">{step}</p>
                    </Fragment>
                  );
                }
                return (
                  <Fragment key={key}>
                    <p>{step}</p>
                    <span />
                  </Fragment>
                );
              })}
              {!PARTNER_FINISH_STEP.includes(fistar.fistar_campaign[0].m_status) && fistar.fistar_campaign[0].cp_status !== CAMPAIGN_STATUS.CLOSED && (
                <i className="fas fa-long-arrow-alt-right" />
              )}
              {/*this.renderButtonNextStep(fistar.m_id, stt_id)*/}
            </div>
          )}
        </div>
      );
    });
  };

  render() {
    const { t, height, count } = this.props;
    const {
      fistars,
      isLoadingFistar,
      matching,
      scrap,
      request,
      apply,
      recommended,
      total,
      isRedirect
    } = this.state;

    if (isRedirect) {
      return <Redirect to={routeName.PARTNER_MY_FISTAR} />;
    }

    if (isLoadingFistar) {
      return (
        <div className="campaign-track">
          <h1>{t("LOADING.LOADING")}</h1>
        </div>
      );
    }
    console.log(this.state.show, this.state.showFistarDetail);

    return (
      <Fragment>
        <div className="header-my-fistar">
          <div className="top">
            <h4>{t("DASHBOARD_FISTAR.DBF_my_fistar")}</h4>
            <button onClick={this.onClickMore}>
              {t("DASHBOARD_FISTAR.DBF_button_more")}
            </button>
          </div>
          <div className="amount-notification">
            <h2>{count ? count.fistar_total : ""}</h2>
          </div>
          <div className="list-amount">
            <div className="item">
              <p>
                <span>{count ? count.fistar_matching : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DBF_status_matching")}</span>
            </div>
            <div className="item">
              <p>
                <span>
                  <i className="fas fa-star" />
                  {count ? count.fistar_scrap : ""}
                </span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DBF_status_scrap")}</span>
            </div>
            <div className="item">
              <p>
                <span>{count ? count.fistar_request : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DBF_status_request")}</span>
            </div>
            <div className="item">
              <p>
                <span>{count ? count.fistar_apply : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DBF_status_apply")}</span>
            </div>
            <div className="item closed">
              <p>
                <span>{count ? count.fistar_recommended : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DBF_status_recommended")}</span>
            </div>
          </div>
        </div>
        <div className="fistar-search">
          <i className="fas fa-search" />
          <span>
            <a href="javascript:;" onClick={this.clickisSearchFiStar}>
              {t("DASHBOARD_FISTAR.DBF_search")}
            </a>
          </span>
        </div>
        <div className="">
          {!isLoadingFistar && this.state.fistars.length === 0 ? (
            <Fragment>
              <div className={"text-center mt-5"}>{t("FISTAR.no_fistar")}</div>
            </Fragment>
          ) : (
            <InfiniteScroll
              dataLength={this.state.fistars.length}
              next={this.fetchMoreData}
              loader={<h4>{t("LOADING.LOADING")}</h4>}
              hasMore={this.state.hasMore}
              height={height}
              className="card-fistar"
            >
              {this.renderFistars()}
            </InfiniteScroll>
          )}
        </div>

        {this.state.showFistarDetail &&
          this.state.show == false &&
          this.state.uid && (
            <ModalDetailFistar
              show={this.state.showFistarDetail}
              onCloseModal={this.onCloseModalFistar}
              uid={this.state.uid}
            />
          )}

        {this.state.show &&
          this.state.showFistarDetail == false &&
          this.state.uid && (
            <ModalHistory
              show={this.state.show}
              onCloseModal={this.onCloseModal}
              uid={this.state.uid}
            />
          )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    getFistar: (page = 1, type = "") => dispatch(getFistarAction(page, type))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerMyFistar));
