import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { logoutAction } from "../../../store/actions/auth";
import * as routeName from "../../../routes/routeName";
import logoFistar from "./../../../images/logo-03.png";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";

class PartnerDashboardHead extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        p_image: ""
      },
      isToggleOn: true
    };
  }

  changeLanguage = lng => {
    this.props.i18n.changeLanguage(lng);
  };

  onToggle = e => {
    e.preventDefault();
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
  };

  logout = e => {
    // e.preventDefault()
    this.props.logout();
  };

  render() {
    const { t, auth, i18n } = this.props;
    const { data } = this.state;

    return (
      <header className="header-container">
        <div className="row container-fuid">
          <div className="col-md-12 ">
            <div className="header-top">
            <div className="breadcrumd-main">
              <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                  <li className="breadcrumb-item breadcrumb-home">
                    <Link to={routeName.HOME}>Home</Link>
                  </li>
                  <li className="breadcrumb-item active" aria-current="page">
                    <Link to={routeName.FISTAR_DASHBOARD}>Mypage</Link>
                  </li>
                </ol>
              </nav>
            </div>
              <ul className="list-language">
                <li className={`language-item${i18n.language === 'ko' ? ' active' : ''}`}>
                  <a
                    href="javascript:;"
                    className="language-link"
                    onClick={() => this.changeLanguage("ko")}
                  >
                    KO
                  </a>
                </li>
                <li className={`language-item${i18n.language === 'en' ? ' active' : ''}`}>
                  <a
                    href="javascript:;"
                    className="language-link"
                    onClick={() => this.changeLanguage("en")}
                  >
                    EN
                  </a>
                </li>
                <li className={`language-item${i18n.language === 'vi' ? ' active' : ''}`}>
                  <a
                    href="javascript:;"
                    className="language-link"
                    onClick={() => this.changeLanguage("vi")}
                  >
                    VN
                  </a>
                </li>
              </ul>
            </div>

            <div className="menu-top">
              <div className="logo">
                <Link to={routeName.FISTAR_DASHBOARD}>
                  <img src={logoFistar} alt="" />
                </Link>
              </div>
              <Navbar bg="light" expand="lg">
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                  <Nav className="mr-auto">
                    <NavLink
                      className="nav-item nav-link"
                      to={routeName.SERVICE}
                    >
                      {t("HEADER.HED_service")}
                    </NavLink>
                    <NavLink
                      className="nav-item nav-link"
                      to={routeName.CAMPAIGN}
                    >
                      {t("HEADER.HED_campaign")}
                    </NavLink>
                    <NavLink
                      className="nav-item nav-link"
                      to={routeName.FISTAR_FRONT}
                    >
                      {t("HEADER.HED_fistar")}
                    </NavLink>
                    <NavLink className="nav-item nav-link" to={routeName.FAQ}>
                      {t("HEADER.HED_faq")}
                    </NavLink>
                    <NavLink
                      className="nav-item nav-link"
                      to={routeName.CONTACTUS}
                    >
                      {t("HEADER.HED_contact_us")}
                    </NavLink>
                  </Nav>
                </Navbar.Collapse>
              </Navbar>
              <div className="login-info">
                {auth && auth.isAuthenticated ? (
                  <Fragment>
                    <div className="fistar-user">
                      <div className="thumnail-users">
                        <img src={auth.avatar} alt="" />
                      </div>
                      <div
                        className="droppdow-name name"
                        onClick={this.onToggle}
                      >
                        <a
                          className="btn dropdown-toggle"
                          href="#"
                          id="dropdownMenuButton"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          {auth.name}
                        </a>
                        <div
                          className={
                            this.state.isToggleOn
                              ? "dropdowns dropdown-menu"
                              : "dropdowns dropdown-menu d-block"
                          }
                          aria-labelledby="dropdownMenuLink"
                        >
                          <h4>{data.pm_name}</h4>
                          <p>{data.pc_name}</p>
                          <p>
                            <a href="">{data.email}</a>
                          </p>
                          <button onClick={this.logout}>
                            {t("HEADER.HEA_button_logout")}
                          </button>
                        </div>
                      </div>
                    </div>
                  </Fragment>
                ) : (
                  <Fragment>
                    <ul
                      className="list-item"
                      style={{ display: "-webkit-box" }}
                    >
                      <li className="item">
                        <a className="link-item">{t("HEADER.HED_login")}</a>
                        <div className="droppdown-login">
                          <Link
                            to={routeName.FISTAR_LOGIN}
                            className="droppdown-item"
                          >
                            {t("FISTAR.fi_star")}
                          </Link>
                          <Link
                            to={routeName.PARTNER_LOGIN}
                            className="droppdown-item"
                          >
                            {t("ANSWER_QUESTION.AQN_partner")}
                          </Link>
                        </div>
                      </li>
                      <li className="item">
                        <a className="link-item">{t("HEADER.HEA_join")}</a>
                        <div className="droppdown-login">
                          <Link
                            to={routeName.FISTAR_JOIN}
                            className="droppdown-item"
                          >
                            {t("FISTAR.fi_star")}
                          </Link>
                          <Link
                            to={routeName.PARTNER_REGISTER}
                            className="droppdown-item"
                          >
                            {t("ANSWER_QUESTION.AQN_partner")}
                          </Link>
                        </div>
                      </li>
                    </ul>
                  </Fragment>
                )}
              </div>
              {/*<div className="logo">
                <Link to={routeName.FISTAR_DASHBOARD}>
                  <img src={logo} alt="" />
                </Link>
              </div>
              <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <button
                  className="navbar-toggler"
                  type="button"
                  data-toggle="collapse"
                  data-target="#navbarNavAltMarkups"
                  aria-controls="navbarNavAltMarkup"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
                >
                  <span className="navbar-toggler-icon" />
                </button>
                <div
                  className="collapse navbar-collapse"
                  id="navbarNavAltMarkups"
                >
                  <div className="navbar-nav">
                    <a className="nav-item nav-link" href="javascript:viod(0)">
                      Service
                    </a>
                    <a className="nav-item nav-link" href="javascript:viod(0)">
                      Campaign
                    </a>
                    <a className="nav-item nav-link" href="javascript:viod(0)">
                      fiStar
                    </a>
                    <NavLink className="nav-item nav-link" to={routeName.FAQ}>
                      FAQ
                    </NavLink>
                    <NavLink
                      className="nav-item nav-link"
                      to={routeName.CONTACTUS}
                    >
                      Contact us
                    </NavLink>
                  </div>
                </div>
              </nav>
              <div className="login-info">
                {auth.isAuthenticated ? (
                  <Fragment>
                    <div className="fistar-user" style={{ display: "flex" }}>
                      <div className="thumnail-users">
                        <img src={auth.avatar} alt={auth.name} />
                      </div>
                      <div className="droppdow-name name">
                        <a
                          className="btn dropdown-toggle"
                          href="#"
                          onClick={this.clickToggleMenu}
                        >
                          {auth.name}
                        </a>
                        <div
                          className="dropdowns dropdown-menu"
                          style={{ display: isToggle ? "block" : "none" }}
                        >
                          <h4>{auth.name}</h4>

                          <p>
                            <NavLink
                              to={
                                auth && auth.type == "partner"
                                  ? routeName.PARTNER_MANAGER_INFORMATION
                                  : routeName.FISTAR_PROFILE_GANERAL
                              }
                            >
                              {auth && auth.email ? auth.email : ""}
                            </NavLink>
                          </p>
                          <p />
                          <button onClick={this.onLogout}>LOGOUT</button>
                        </div>
                      </div>
                    </div>
                  </Fragment>
                ) : (
                  <Fragment>
                    <ul
                      className="list-item"
                      style={{ display: "-webkit-box" }}
                    >
                      <li className="item">
                        <a className="link-item">LOGIN</a>
                        <div className="droppdown-login">
                          <Link
                            to={routeName.FISTAR_LOGIN}
                            className="droppdown-item"
                          >
                            fi:star
                          </Link>
                          <Link
                            to={routeName.PARTNER_LOGIN}
                            className="droppdown-item"
                          >
                            Partner
                          </Link>
                        </div>
                      </li>
                      <li className="item">
                        <a className="link-item">JOIN</a>
                        <div className="droppdown-login">
                          <Link
                            to={routeName.FISTAR_JOIN}
                            className="droppdown-item"
                          >
                            fi:star
                          </Link>
                          <Link
                            to={routeName.PARTNER_REGISTER}
                            className="droppdown-item"
                          >
                            Partner
                          </Link>
                        </div>
                      </li>
                    </ul>
                  </Fragment>
                )}
              </div>*/}
            </div>
          </div>
        </div>
      </header>
    );
  }
  activeClassName = "active";
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}
const mapDispatchToProps = dispatch => {
  return {
    logout: data => dispatch(logoutAction())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerDashboardHead));
