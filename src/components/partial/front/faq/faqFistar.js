import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import {
  Panel,
  PanelGroup,
  Accordion,
  Card,
  CustomToggle,
  Button
} from "react-bootstrap";
// import Accordion from 'react-bootstrap/Accordion'

class faqFistar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      activeKey: 0
    };
  }
  handleSelect = activeKey => {
    this.setState({ activeKey });
  };

  toggleShow = () => {
    console.log(this.state.show);
    this.setState({ show: !this.state.show });
  };

  render() {
    const { t, faq, keyTo } = this.props;
    console.log(faq);
    console.log(keyTo);
    let status = "";

    return (
      <div className="card">
        <div className="card-header" id="headingOne" onClick={this.toggleShow}>
          <div className="mb-0">
            <p className={this.state.show ? "collapsed active" : "collapsed"}>
              {faq.faq_title}
            </p>
            <button
              className="btn btn-link click-icon"
              type="button"
              data-toggle="collapse"
              data-target="#collapseOne"
              aria-expanded="true"
              aria-controls="collapseOne"
            >
              <i
                className={
                  this.state.show ? "fas fa-chevron-up" : "fas fa-chevron-down"
                }
              />
            </button>
          </div>
        </div>
        <div
          id="collapseOne"
          className={this.state.show ? "collapse show" : "collapse"}
          aria-labelledby="headingOne"
          data-parent="#accordionExample"
        >
          <div className="card-body">
            <span>A</span>
            <p>{faq.faq_content}</p>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(faqFistar)
);
