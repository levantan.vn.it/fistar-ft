import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import macbook from "./../../../../images/service/macbook.png";
import Image111 from "./../../../../images/service/111.png";
import Image121 from "./../../../../images/service/121.png";
import Image13 from "./../../../../images/service/13.png";
import ImageNext from "./../../../../images/service/next-fistare.svg";
import imgUrl from "./../../../../images/service/09.png";
import * as routeName from "./../../../../routes/routeName"

import ModalStatusCampaign from "./../../../fiStar/Dashboard/MyCampaign/ModalStatusCampaign";

import { nextStepAction } from "./../../../../store/actions/campaign";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";

class Partner extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { t } = this.props;
    const bg = {
      color: "blue",
      backgroundImage: "url(" + imgUrl + ")"
    };
    return (
      <div className="content">
        <div className="top-partner-platform" style={bg}>
          <div className="text-partner">
            <h2>{t("SERVICE.SER_PARTNER_title")}</h2>
            <p>{t("SERVICE.SER_PARTNER_des_1")}</p>
            <p>
                {t("SERVICE.SER_PARTNER_des_2")}
            </p>
            <p>{t("SERVICE.SER_PARTNER_des_3")}</p>
          </div>
        </div>
        <div className="wrap-platform">
          <div className="content-search-platform">
            <h3>
              <span>{t("SERVICE.SER_PARTNER_search")} :</span> {t("SERVICE.SER_PARTNER_find_title")}
            </h3>
            <p>
                {t("SERVICE.SER_PARTNER_find_des")}

            </p>
          </div>
          <div className="img-search-platform">
            <img src={macbook} alt="" />
          </div>
        </div>
        <div className="wrap-platform selects-platform">
          <div className="content-search-platform">
            <h2>
              <span>{t("SERVICE.SER_PARTNER_select")} :</span> {t("SERVICE.SER_PARTNER_select_title")}
            </h2>
            <p>
                {t("SERVICE.SER_PARTNER_select_des")}

            </p>
          </div>
          <div className="img-search-platform">
            <img src={Image111} alt="" />
          </div>
        </div>
        <div className="wrap-platform wrap-platform-report">
          <div className="content-search-platform">
            <h3>
              <span>{t("SERVICE.SER_PARTNER_report")} :</span> {t("SERVICE.SER_PARTNER_report_title")}
            </h3>
            <p>
                {t("SERVICE.SER_PARTNER_report_des")}

            </p>
          </div>
          <div className="img-search-platform">
            <img src={Image121} alt="" />
          </div>
        </div>
        <div className="sytar-of">
          <h3>{t("SERVICE.SER_PARTNER_partner_title")}</h3>
          <p>
              {t("SERVICE.SER_PARTNER_partner_des")}

          </p>
        </div>
        <div className="banner-worlk">
          <img src={Image13} alt="" />
        </div>
        <div className="btn-fistars">
          <Link to={routeName.PARTNER_LOGIN} className="btn btn-flatform-fistar">
            <span>
              {t("SERVICE.SER_PARTNER_btn_title")}
              <span className="text">
                {t("SERVICE.SER_PARTNER_btn_des")}

              </span>
            </span>
            <img src={ImageNext} alt="" />
          </Link>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Partner));
