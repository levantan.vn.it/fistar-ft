import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import "./campaign.scss";

let lastScrollY = 0;
let ticking = false;

class campaignWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transform: false
    };
  }

  componentDidMount() {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      window.addEventListener("scroll", this.handleScroll);
    }
  }

  componentWillUnmount() {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      window.removeEventListener("scroll", this.handleScroll);
    }
  }

  nav = React.createRef();

  handleScroll = () => {
    lastScrollY = (typeof window !== 'undefined' && window.document && window.document.createElement) ? window.pageYOffset : 0;

    if (lastScrollY > 150) {
      this.setState({
        transform: true
      });
    } else {
      this.setState({
        transform: false
      });
    }
  };

  render() {
    const { t } = this.props;
    const { transform } = this.state;

    return (
      <Fragment>
        <main className="main-container">
          <section
            className={
              transform
                ? "form-login mm-faq mm-campaign  mm-campaign-scroll"
                : "form-login mm-faq mm-campaign"
            }
            id="on-scroll"
            ref={this.nav}
          >
            <div className="banner-container">
              <div className="row">
                <div className="col-md-12" />
              </div>
            </div>
            {/*<div className="bg-overlay">*/}
            {/*<div className="bg-overcolor" />*/}
            {/*</div>*/}

            {this.props.children}
          </section>
        </main>
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(campaignWrapper)
);
