import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import imageIdol from "./../../../../images/dash-02.png";
import imageClose from "./../../../../images/close-popup.svg";
import imageCloseNew from "./../../../../images/close-new.png";
import imageFb from "./../../../../images/face.png";
import imageYt from "./../../../../images/toutube.png";
import imageIstar from "./../../../../images/intar.png";
import imageRead from "./../../../../images/mk.png";

import ModalStatusCampaign from "./../../../fiStar/Dashboard/MyCampaign/ModalStatusCampaign";

import { nextStepAction } from "./../../../../store/actions/campaign";

import PieChart from "./../../../charts/PieChartCampaignFront/index";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import Highcharts from "highcharts/highstock";
import { getCodeAction } from "../../../../store/actions/code";

import { nFormatter } from "./../../../../common/helper";
import { getImageLink } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";
import CampaignMainImage from "./../../../../components/CampaignMainImage/index"

const MATCHING = {
  REQUEST: [1, 2, 3, 4, 5, 6, 7, 8],
  APPLY: [9, 10, 11, 12, 13, 14, 15, 16],
  MATCHED: [8, 16]
};
function cutString(text) {
  var wordsToCut = 75;
  var wordsArray = text.split(" ");
  if (wordsArray.length > wordsToCut) {
    var strShort = "";
    for (var i = 0; i < wordsToCut; i++) {
      strShort += wordsArray[i] + " ";
    }
    return strShort + "...";
  } else {
    return text;
  }
}

// var userAgent, ieReg, ie;
// userAgent = window.navigator.userAgent;
// ieReg = /msie|Trident.*rv[ :]*11\./gi;
// ie = ieReg.test(userAgent);

// if(ie) {
//     $(".img-container").each(function () {
//         var $container = $(this),
//             imgUrl = $container.find("img").prop("src");
//         if (imgUrl) {
//             $container.css("backgroundImage", 'url(' + imgUrl + ')').addClass("custom-object-fit");
//         }
//     });
// }

class Campaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      step: null,
      loading: false,
      dataFistar: {},
      showMore: false,
      fime: 0,
      facebook: 0,
      youtube: 0,
      instagram: 0
    };
  }

  OpenModal = data => {
    console.log(data);
    let year = new Date(data.dob);
    let yearConver = year.toDateString();
    let yearFistar = yearConver.split(" ").pop();
    data.dob = yearFistar;

    const {
      code: {
        data: { location }
      }
    } = this.props;
    if (!location) return;
    console.log(this.state);
    var locationAddress = location.code.filter(
      location => location.cd_id == data.location
    );

    data.location =
      locationAddress.length > 0 ? locationAddress[0].cd_label : data.location;

    var fime = "";
    var facebook = "";
    var youtube = "";
    var instagram = "";

    data &&
      data.channels &&
      data.channels.map(channel => {
        if (channel.sns_id == 1) {
          fime = channel.usn_follower;
        }
        if (channel.sns_id == 2) {
          facebook = channel.usn_follower;
        }
        if (channel.sns_id == 3) {
          youtube = channel.usn_follower;
        }
        if (channel.sns_id == 4) {
          instagram = channel.usn_follower;
        }
      });
    this.setState({
      fime: fime,
      facebook: facebook,
      youtube: youtube,
      instagram: instagram,
      show: true,
      dataFistar: data
    });

    console.log(fime, facebook, youtube, instagram);
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  onCloseModal = () => {
    console.log(this.state.show);
    this.setState({ show: !this.state.show });
  };

  showMore = e => {
    e.preventDefault();
    this.setState({
      showMore: !this.state.showMore
    });
  };
  renderPieChart = () => {
    const { t, campaign } = this.props;
    console.log(campaign);
    let likeFime = 0;
    let likeFimeStatus = false;
    let likeFacebook = 0;
    let likeFacebookStatus = false;
    let likeYouube = 0;
    let likeYouubeStatus = false;
    let likeInstagram = 0;
    let likeInstagramStatus = false;

    let channels = campaign.review_statitics_channel.length > 0 &&
      campaign.review_statitics_channel.map(like => {
        if (like.sns_id && like.sns_id == 1) {
          likeFime = like.total_like;
            likeFimeStatus=true
        }
        if (like.sns_id && like.sns_id == 2) {
          likeFacebook = like.total_like;
            likeFacebookStatus=true
        }
        if (like.sns_id && like.sns_id == 3) {
          likeYouube = like.total_like;
            likeYouubeStatus=true
        }
        if (like.sns_id && like.sns_id == 4) {
          likeInstagram = like.total_like;
            likeInstagramStatus=true
        }
      });
      console.log(likeFime, likeFacebook, likeYouube, likeInstagram)


      if(likeFime) {
          likeFime = (likeFime == null || likeFime == "" || likeFime == "0") ? 0 : +likeFime
      } else if(likeFacebook) {
          likeFacebook = (likeFacebook == null || likeFacebook == "" || likeFacebook == "0") ? 0 : +likeFacebook
      } else if(likeYouube) {
          likeYouube = (likeYouube == null || likeYouube == "" || likeYouube == "0") ? 0 : +likeYouube
      }else if(likeInstagram) {
          likeInstagram = (likeInstagram == null || likeInstagram == "" || likeInstagram == "0") ? 0 : +likeInstagram
      }

      console.log(likeFime, likeFacebook, likeYouube, likeInstagram)

      let totaLike = likeFime + likeFacebook + likeYouube + likeInstagram;
      console.log(totaLike)
      likeFime = likeFime ? (likeFime == 0 ? 0 :((+likeFime / totaLike) * 100)) : 0;
      likeFacebook = likeFacebook ? (likeFacebook == 0 ? 0 : ((+likeFacebook / totaLike) * 100)) : 0;
      likeYouube = likeYouube ? (likeYouube == 0 ? 0 : ((+likeYouube / totaLike) * 100)) : 0;
      likeInstagram = likeInstagram ? (likeInstagram == 0 ? 0 : ((+likeInstagram / totaLike) * 100)) : 0;

      let dataChart =  [
          { name: "Fi:me", value: likeFime, color: "#EE467F" },
          { name: "Facebook", value: likeFacebook, color: "#48BAFD" },
          { name: "Youtube", value: likeYouube, color: "#7FC4FD" },
          { name: "Instagram", value: likeInstagram, color: "#BCE0FD" }
      ]
      let testarray = []
      console.log(likeFime, likeFacebook, likeYouube, likeInstagram)
      console.log(likeFimeStatus, likeFacebookStatus, likeYouubeStatus, likeInstagramStatus)
         if (likeFimeStatus) {
             testarray.push({name: "Fi:me", value: likeFime, color: "#EE467F" })
         }
         if (likeFacebookStatus) {
             testarray.push({name: "Facebook", value: likeFacebook, color: "#48BAFD" })
         }
         if (likeYouubeStatus) {
             testarray.push({name: "Youtube", value: likeYouube, color: "#7FC4FD" })
         }
         if (likeInstagramStatus) {
             testarray.push({name: "Instagram", value: likeInstagram, color: "#BCE0FD" })
         }
      console.log(testarray)


    console.log(likeFime, likeFacebook, likeYouube, likeInstagram)
    return campaign.cp_status !== 60 &&
      campaign.review_statitics_channel.length > 0 ? (
      <div className="right-content">
        <div className="chart chart-pie-main">
          <PieChart
            style={{ height: "10%" }}
            data={testarray}
          />
        </div>
      </div>
    ) : campaign.cp_status !== 60 ? (
      <div className="right-content main-chart-empty">
        <div className="chart chart-pie-main">
          <div className="chart-empty-front">{t("DATA_CHART.empty")}</div>
        </div>
      </div>
    ) : (
      ""
    );
  };

  render() {
    const { t, campaign } = this.props;
    const { dataFistar } = this.state;
    let status = "";
    let classStatus = "";
    switch (campaign.cp_status) {
      case 59: {
        status = `${t("DASHBOARD_FISTAR.DBF_status_matching")}`;

        break;
      }
      case 60: {
        status = `${t("CAMPAIGN.CAN_ready")}`;
        classStatus = "wrap-matching to-be-opened";
        break;
      }
      case 61: {
        status = `${t("CAMPAIGN.CAN_on_going")}`;
        classStatus = "wrap-matching ";
        break;
      }
      case 62: {
        status = `${t("CAMPAIGN.CAN_close")}`;
        classStatus = "wrap-matching closed";
        break;
      }
      default: {
        break;
      }
    }
    console.log(campaign);
    let apply = (campaign.matchings || []).map(fistar => {
      if (fistar.matching_status.m_status) {
        let status = fistar.matching_status.m_status;
        if (MATCHING.MATCHED.includes(status)) {
          return fistar;
        }
      }
      return null;
    });
    apply = apply.filter(e => e !== null);
    let recruit = campaign.cp_total_influencer || 0;

    console.log(apply);

    let FistarApply = "";
    if (apply.length > 5) {
      if (this.state.showMore) {
        FistarApply = (apply.slice(5, 5) || []).map((fistar, key) => (
          <Fragment key={key}>
            <li className="item more" onClick={this.showMore}>
              <span>{apply.length - 5}+</span>
            </li>
            <li
              className="item"
              onClick={() => this.OpenModal(fistar.influencer)}
            >
              <a href="javascript:void(0)">
                <img
                  src={getImageLink(
                    fistar.influencer.picture,
                    IMAGE_TYPE.FISTARS,
                    IMAGE_SIZE.ORIGINAL
                  )}
                  alt={fistar.influencer.fullname}
                />
              </a>
            </li>
          </Fragment>
        ));
      } else {
        FistarApply = (apply.slice(0, 5) || []).map((fistar, key) => (
          <Fragment key={key}>
            <li
              className="item"
              onClick={() => this.OpenModal(fistar.influencer)}
            >
              <a href="javascript:void(0)">
                <img
                  src={getImageLink(
                    fistar.influencer.picture,
                    IMAGE_TYPE.FISTARS,
                    IMAGE_SIZE.ORIGINAL
                  )}
                  alt={fistar.influencer.fullname}
                />
              </a>
            </li>
            <li className="item more" onClick={this.showMore}>
              <span>{apply.length - 5}+</span>
            </li>
          </Fragment>
        ));
      }
    } else {
      FistarApply = (apply || []).map((fistar, key) => (
        <Fragment key={key}>
          <li
            className="item"
            onClick={() => this.OpenModal(fistar.influencer)}
          >
            <a href="javascript:void(0)">
              <img
                src={getImageLink(
                  fistar.influencer.picture,
                  IMAGE_TYPE.FISTARS,
                  IMAGE_SIZE.ORIGINAL
                )}
                alt={fistar.influencer.fullname}
              />
            </a>
          </li>
        </Fragment>
      ));
    }

    console.log(FistarApply);

    return (
      <div className="card">
        <div className="left">
          <div className="image-card">
            <a
              target={campaign.fimetry ? "_blank" : "_self"}
              href={
                campaign.fimetry
                  ? `https://fime.vn/tries/${campaign.fimetry.SLUG}`
                  : "javascript:void(0)"
              }
            >
              {/*<img*/}
                {/*src={getImageLink(*/}
                  {/*campaign.cp_main_image,*/}
                  {/*IMAGE_TYPE.CAMPAIGNS,*/}
                  {/*IMAGE_SIZE.ORIGINAL*/}
                {/*)}*/}
                {/*alt=""*/}
              {/*/>*/}

                <CampaignMainImage campaign={campaign} />

            </a>
          </div>
          <div className="footer-card">
            <div className="footer-left">
              <div className="title">
                <p>
                  {t("CAMPAIGN.CAN_fistar")} {campaign.cp_total_influencer}
                </p>
              </div>
              <ul className="list-sd-user">
                {FistarApply.length > 0
                  ? FistarApply
                  : ``}
              </ul>
            </div>
            <div className="footer-right">
              <ul className="list-fllow">
                <li className="item">
                  <a href="javascript:void(0)" className="disabled-hover">
                    <i className="far fa-heart" />
                  </a>
                  <span>
                    {campaign.review_statitics &&
                    campaign.review_statitics.sum_like
                      ? campaign.review_statitics.sum_like
                      : 0}
                  </span>
                </li>
                <li className="item">
                  <a href="javascript:void(0)" className="disabled-hover">
                    <i className="far fa-comment-dots" />
                  </a>
                  <span>
                    {campaign.review_statitics &&
                    campaign.review_statitics.sum_comment
                      ? campaign.review_statitics.sum_comment
                      : 0}
                  </span>
                </li>
                <li className="item">
                  <a href="javascript:void(0)" className="disabled-hover">
                    <i className="fas fa-share-alt" />
                  </a>
                  <span>
                    {campaign.review_statitics &&
                    campaign.review_statitics.sum_share
                      ? campaign.review_statitics.sum_share
                      : 0}
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="right">
          <div className="top">
            <div className="name">
              <span className={classStatus}>{status}</span>
              <h2><a
                  target={campaign.fimetry ? "_blank" : "_self"}
                  href={
                      campaign.fimetry
                          ? `https://fime.vn/tries/${campaign.fimetry.SLUG}`
                          : "javascript:void(0)"
                  }
              >{campaign.cp_name}</a></h2>
            </div>
          </div>
          <div className="content-card">
            <div
              className={`left-content ${
                campaign.cp_status !== 60 ? "" : " w-100"
              }`}
            >
              <div className="amount-open">
                <p>
                  {t("CAMPAIGN.CAN_open")}{" "}
                  <span>
                    {(campaign.cp_period_start || "").substr(
                      0,
                      (campaign.cp_period_start || "").indexOf(" ")
                    )}
                  </span>
                </p>
              </div>
              {/*<div className="text">“ {campaign.cp_description}”</div>*/}
              {console.log(campaign.cp_description.length)}
              <div
                className="text"
                dangerouslySetInnerHTML={{
                  __html: cutString(campaign.cp_description)
                }}
              />
            </div>

            {this.renderPieChart()}
          </div>
        </div>

        <Modal
          size="lg"
          show={this.state.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="popup-mm-fistar mmfistar  modal-dialog-centered "
        >
          <div className="container-mmfaq mt-4">
            <div className="header">
              <div className="top">
                <div className="left">
                  <h4>{t("SEARCH_DETAIL.SDL_fistar_detail")}</h4>
                </div>
                <div className="right">
                  <button
                    type="button"
                    className="btn btn-close close"
                    data-dismiss="modal"
                    aria-label="Close"
                    onClick={this.handleClose}
                  >
                    <img src={imageCloseNew} alt="" />
                  </button>
                </div>
              </div>
            </div>
            <div className="body-form">
              <div className="container-dmmfaq">
                <div className="card-mmfistar">
                  <div className="card align-items-center">
                    <div className="left">
                      <a href="javascript:void(0)" className="disabled-hover">
                        <img
                          src={getImageLink(
                            dataFistar.picture,
                            IMAGE_TYPE.FISTARS,
                            IMAGE_SIZE.ORIGINAL
                          )}
                          alt=""
                        />
                      </a>
                      <div className="btn over-flow">
                        <i className="fas fa-star" />
                        <span>{dataFistar.scraps_count}</span>
                      </div>
                    </div>
                    <div className="right">
                      <div className="top-name">
                        <div className="name">
                          <i className="fas fa-mars" />
                          <h4>
                            <a href="">{dataFistar.fullname}</a>
                          </h4>
                        </div>
                        <div className="address-old">
                          <span>{dataFistar.dob}</span>
                          <span className="address">{dataFistar.location}</span>
                        </div>
                      </div>
                      <div className="content-card">
                        <h4> {dataFistar.status_message}</h4>
                        <p>“{dataFistar.self_intro}”</p>
                      </div>
                      <div className="tab-mmfistar">
                        {dataFistar.keywords &&
                          dataFistar.keywords.map((keyword, key) => (
                            <a href="javascript:void(0)" key={key}>
                              {keyword.code.cd_label}
                            </a>
                          ))}
                      </div>
                      <div className="card-footer">
                        <ul className="list-item">
                          <li className="item">
                            <a
                              href="javascript:void(0)"
                              className="disabled-hover"
                            >
                              <img src={imageRead} alt="" />
                            </a>
                            <span>{nFormatter(this.state.fime, 0)}</span>
                          </li>
                          <li className="item">
                            <a
                              href="javascript:void(0)"
                              className="disabled-hover"
                            >
                              <img src={imageFb} alt="" />
                            </a>
                            <span>{nFormatter(this.state.facebook, 0)}</span>
                          </li>
                          <li className="item">
                            <a
                              href="javascript:void(0)"
                              className="disabled-hover"
                            >
                              <img src={imageIstar} alt="" />
                            </a>
                            <span>{nFormatter(this.state.instagram, 0)}</span>
                          </li>
                          <li className="item">
                            <a
                              href="javascript:void(0)"
                              className="disabled-hover"
                            >
                              <img src={imageYt} alt="" />
                            </a>
                            <span>{nFormatter(this.state.youtube, 0)}</span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    code: state.code
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Campaign));
