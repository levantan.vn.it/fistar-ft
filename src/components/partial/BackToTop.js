import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Header from "./Header";
import Footer from "./Footer";


function scrollToTopAnimated(scrollDuration) {
  if (typeof window !== 'undefined' && window.document && window.document.createElement) {
    var scrollStep = -window.pageYOffset / (scrollDuration / 15),
        scrollInterval = setInterval(function() {
            if (window.pageYOffset != 0) {
                window.scrollBy(0, scrollStep);
            } else clearInterval(scrollInterval);
        }, 15);
  }
}
var showBackHome = false;
if (typeof window !== 'undefined' && window.document && window.document.createElement) {

    window.onscroll = function() {scrollFunction()};


    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            showBackHome = showBackHome == false ? true : showBackHome;
        } else {
            showBackHome = false;
        }
    }

}




class BackToTop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showBacktoTop:false,
            prevScrollpos: (typeof window !== 'undefined' && window.document && window.document.createElement) ? window.pageYOffset : 0,
            visible: true
        };
        if (typeof window !== 'undefined' && window.document && window.document.createElement) {
          window.scrollTo(0, 0);
        }
    }
  // scrollStep = () => {
  //   if (window.pageYOffset === 0) {
  //     clearInterval(this.state.intervalId);
  //   }
  //   window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
  // };

  scrollToTop = () => {
    // let intervalId = setInterval(this.scrollStep, 16.66);
    // this.setState({ intervalId: intervalId });
    scrollToTopAnimated(700);
  };


    componentDidMount() {
        if (typeof window !== 'undefined' && window.document && window.document.createElement) {window.addEventListener("scroll", this.handleScroll);}

    }

    componentWillUnmount() {
        if (typeof window !== 'undefined' && window.document && window.document.createElement) { window.removeEventListener("scroll", this.handleScroll)};
    }

    handleScroll = () => {
        const { prevScrollpos } = this.state;
        if (typeof window !== 'undefined' && window.document && window.document.createElement) {
            const currentScrollPos = window.pageYOffset;
            const visible = prevScrollpos > currentScrollPos;
            this.setState({
                prevScrollpos: currentScrollPos,
                visible
            });
        }
    };

  render() {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
          showBackHome = showBackHome == false ? true : showBackHome;
      } else {
          showBackHome = false;
      }
    }

    return (
      <section id="back-to-top" className={showBackHome ? "d-block" : "d-none"}>
        <div className="drop-up btn" onClick={this.scrollToTop}>
          <i className="fas fa-long-arrow-alt-up" />
        </div>
      </section>
    );
  }
}

export default BackToTop;
