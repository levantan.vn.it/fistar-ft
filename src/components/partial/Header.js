import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import * as routeName from "./../../routes/routeName";
import { logoutAction } from "./../../store/actions/auth";
import logo from "./../../images/logo-03.png";
import "./styles.scss";
import { Navbar, Nav, NavDropdown, Dropdown } from "react-bootstrap";
import { withRouter } from "react-router";
import { getImageLink } from "./../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../constants/index";
import imageExample from "./../../images/example.jpg";

class MasterHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isToggle: false
    };
  }

  changeLanguage = lng => {
    this.props.i18n.changeLanguage(lng);
  };

  clickToggleMenu = () => {
    this.setState({
      isToggle: !this.state.isToggle
    });
  };

  onLogout = () => {
    const { auth } = this.props;
    this.props.logout();
    if (auth.type == "partner") {
      this.props.history.push(`${routeName.PARTNER_LOGIN}`);
    } else {
      this.props.history.push(`${routeName.FISTAR_LOGIN}`);
    }
  };

  render() {
    const { t, auth, i18n } = this.props;
    const { isToggle } = this.state;
    return (
      <header className="header-container">
        <div className="row ml-0 mr-0">
          <div className="col-md-12 p-0">
            <div className="header-top">
            <div className="breadcrumd-main">
              <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                  <li className="breadcrumb-item breadcrumb-home">
                    <Link to={routeName.HOME}>Home</Link>
                  </li>
                  <li className="breadcrumb-item active" aria-current="page">
                    <Link to={routeName.FISTAR_DASHBOARD}>Mypage</Link>
                  </li>
                </ol>
              </nav>
            </div>
              <ul className="list-language">
                <li className={`language-item${i18n.language === 'ko' ? ' active' : ''}`}>
                  <a
                    href="javascript:;"
                    className="language-link"
                    onClick={() => this.changeLanguage("ko")}
                  >
                    KO
                  </a>
                </li>
                <li className={`language-item${i18n.language === 'en' ? ' active' : ''}`}>
                  <a
                    href="javascript:;"
                    className="language-link"
                    onClick={() => this.changeLanguage("en")}
                  >
                    EN
                  </a>
                </li>
                <li className={`language-item${i18n.language === 'vi' ? ' active' : ''}`}>
                  <a
                    href="javascript:;"
                    className="language-link"
                    onClick={() => this.changeLanguage("vi")}
                  >
                    VN
                  </a>
                </li>
              </ul>
            </div>
            
            <div className="menu-top">
              <div className="logo">
                <h1>
                  <Link
                    to={
                      auth.type == "partner"
                        ? routeName.PARTNER_DASHBOARD
                        : auth.type == 'fistar'
                          ? routeName.FISTAR_DASHBOARD
                          : routeName.HOME
                    }
                  >
                    <img src={logo} alt="fistar" />
                  </Link>
                </h1>
              </div>
              <Navbar bg="light" expand="lg">
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                  <Nav className="mr-auto">
                    <NavLink
                      className="nav-item nav-link"
                      to={routeName.SERVICE}
                    >
                      {t("HEADER.HED_service")}
                    </NavLink>
                    <NavLink
                      className="nav-item nav-link"
                      to={routeName.CAMPAIGN}
                    >
                      {t("HEADER.HED_campaign")}
                    </NavLink>
                    <NavLink
                      className="nav-item nav-link"
                      to={routeName.FISTAR_FRONT}
                    >
                      {t("HEADER.HED_fistar")}
                    </NavLink>
                    <NavLink className="nav-item nav-link" to={routeName.FAQ}>
                      {t("HEADER.HED_faq")}
                    </NavLink>
                    <NavLink
                      className="nav-item nav-link"
                      to={routeName.CONTACTUS}
                    >
                      {t("HEADER.HED_contact_us")}
                    </NavLink>
                  </Nav>
                </Navbar.Collapse>
              </Navbar>
              <div className="login-info">
                {auth.isAuthenticated ? (
                  <Fragment>
                    <div className="fistar-user" style={{ display: "flex" }}>
                      <div className="thumnail-users">
                        <img
                          src={
                            auth.avatar
                              ? auth.type == "partner"
                                ? getImageLink(
                                    auth.avatar,
                                    IMAGE_TYPE.PARTNERS,
                                    IMAGE_SIZE.ORIGINAL
                                  )
                                : getImageLink(
                                    auth.avatar,
                                    IMAGE_TYPE.FISTARS,
                                    IMAGE_SIZE.ORIGINAL
                                  )
                              : auth.user && auth.user.p_image
                                ? auth.type == "partner"
                                  ? getImageLink(
                                      auth.user.p_image,
                                      IMAGE_TYPE.PARTNERS,
                                      IMAGE_SIZE.ORIGINAL
                                    )
                                  : getImageLink(
                                      auth.user.p_image,
                                      IMAGE_TYPE.FISTARS,
                                      IMAGE_SIZE.ORIGINAL
                                    )
                                : imageExample
                          }
                          alt={auth.name}
                        />
                      </div>

                      <Dropdown className="droppdow-name name">
                        <Dropdown.Toggle
                          // variant="success"
                          id="dropdown-basic"
                          className="btn dropdown-toggle"
                        >
                          {auth.user && auth.user.pm_name
                            ? auth.user.pm_name
                            : auth.name}
                        </Dropdown.Toggle>

                        {auth.type == "partner" ? (
                          <Dropdown.Menu className="dropdowns dropdown-profile">
                            <span>
                              <NavLink
                                to={routeName.PARTNER_DASHBOARD}
                                activeClassName="active"
                              >
                                {t("MENU_LEFT_PARTNER.MLP_dashboard")}
                              </NavLink>
                            </span>
                            <span>
                              <NavLink
                                to={routeName.PARTNER_CREATE_CAMPAIN}
                                activeClassName="active"
                              >
                                {t("MENU_LEFT_PARTNER.MLP_create_campaign")}
                              </NavLink>
                            </span>
                            <span>
                              <NavLink
                                to={routeName.PARTNER_SEARCH_FISTAR}
                                activeClassName="active"
                              >
                                {t("MENU_LEFT_PARTNER.MLP_search_fistar")}
                              </NavLink>
                            </span>
                            <span>
                              <NavLink
                                to={routeName.PARTNER_MY_FISTAR}
                                activeClassName="active"
                              >
                                {t("MENU_LEFT_PARTNER.MLP_myfistar")}
                              </NavLink>
                            </span>
                            <span>
                              <NavLink
                                to={routeName.PARTNER_CAMPAIGN_TRACKING}
                                activeClassName="active"
                              >
                                {t("MENU_LEFT_PARTNER.MLP_cp_tracking")}
                              </NavLink>
                            </span>
                            <span>
                              <NavLink
                                to={routeName.PARTNER_MANAGER_INFORMATION}
                                activeClassName="active"
                              >
                                {t("MENU_LEFT_PARTNER.MLP_setting")}
                              </NavLink>
                            </span>
                            <span>
                              <NavLink
                                to={routeName.PARTNER_QA}
                                activeClassName="active"
                              >
                                {t("MENU_LEFT_PARTNER.MLP_qa")}
                              </NavLink>
                            </span>

                            <button onClick={this.onLogout}>
                              {t("HEADER.HEA_button_logout")}
                            </button>
                          </Dropdown.Menu>
                        ) : (
                          <Dropdown.Menu className="dropdowns dropdown-profile">
                            <span>
                              <NavLink
                                to={routeName.FISTAR_DASHBOARD}
                                activeClassName="active"
                              >
                                {t("MENU_LEFT_PARTNER.MLP_dashboard")}
                              </NavLink>
                            </span>
                            <span>
                              <NavLink
                                to={routeName.FISTAR_PROFILE_GANERAL}
                                activeClassName="active"
                              >
                                {t("HEADER.HEA_profile")}
                              </NavLink>
                            </span>
                            <span>
                              <NavLink
                                to={routeName.FISTAR_MY_CAMPAIGN}
                                activeClassName="active"
                              >
                                {t("HEADER.HEA_my_campaign")}
                              </NavLink>
                            </span>
                            <span>
                              <NavLink
                                to={routeName.FISTAR_SEARCH_CAMPAIGN}
                                activeClassName="active"
                              >
                                {t("HEADER.HEA_campaign_search")}
                              </NavLink>
                            </span>
                            <span>
                              <NavLink
                                to={routeName.FISTAR_QA}
                                activeClassName="active"
                              >
                                {t("HEADER.HEA_qa")}
                              </NavLink>
                            </span>

                            <button onClick={this.onLogout}>
                              {t("HEADER.HEA_button_logout")}
                            </button>
                          </Dropdown.Menu>
                        )}
                      </Dropdown>
                    </div>
                  </Fragment>
                ) : (
                  <Fragment>
                    <ul
                      className="list-item"
                      style={{ display: "-webkit-box" }}
                    >
                      <li className="item">
                        <a className="link-item">{t("HEADER.HED_login")}</a>
                        <div className="droppdown-login">
                          <Link
                            to={routeName.FISTAR_LOGIN}
                            className="droppdown-item"
                          >
                            {t("SERVICE.SER_fistar")}
                          </Link>
                          <Link
                            to={routeName.PARTNER_LOGIN}
                            className="droppdown-item"
                          >
                            {t("SERVICE.SER_partner")}
                          </Link>
                        </div>
                      </li>
                      <li className="item">
                        <a className="link-item">{t("HEADER.HEA_join")}</a>
                        <div className="droppdown-login">
                          <Link
                            to={routeName.FISTAR_JOIN}
                            className="droppdown-item"
                          >
                            {t("HEADER.HED_fistar")}
                          </Link>
                          <Link
                            to={routeName.PARTNER_REGISTER}
                            className="droppdown-item"
                          >
                            {t("SERVICE.SER_partner")}
                          </Link>
                        </div>
                      </li>
                    </ul>
                  </Fragment>
                )}
              </div>
            </div>
          
          </div>
        </div>
      </header>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: data => dispatch(logoutAction())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(withRouter(MasterHeader)));
