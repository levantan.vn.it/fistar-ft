import React, { Component, Fragment } from 'react';
import { withTranslation, Trans } from 'react-i18next';
import { connect } from 'react-redux'
import './keyword.css'

class FistarKeyword extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      data: {
        keyword: [],
      },
    }
  }

  static getDerivedStateFromProps(nextProps, prevState){
    // if(nextProps.keywordSelected !== prevState.data.keyword){
      // console.log(nextProps.keywordSelected, 'asdsad');
      return {
        data: {
          keyword: nextProps.keywordSelected
        }
      }
    // }
    // else return null;
  }

  onClickKeyword = (id) => {
    // console.log(id);
    let { data } = this.state
    const index = data.keyword.indexOf(id);
    if (index !== -1) {
      data.keyword.splice(index, 1);
      this.setState({
        data
      }, () => {
        // console.log(this.state.data, 'data asd');
        this.props.onSelectedKeyword(this.state.data.keyword)
      })
    }else {
      // data = {
      //   ...this.state.data,
      //   keyword: [
      //     ...this.state.data.keyword,
      //     id,
      //   ],
      // }
      // console.log(data, 'data');
      // this.setState({
      //   data
      // }, () => {
      //   console.log(this.state.data, 'data asdddd');
        this.props.onSelectedKeyword([
          ...this.state.data.keyword,
          id,
        ])
      // })
    }
  }

  renderKeyword = () => {
    const { data, errors } = this.state
    const { code: { data : { keyword } } } = this.props

    if (!keyword) return

    return (
      <Fragment>
        {(keyword.code || []).map((item, key) => (
          <span
            href="#"
            className={data.keyword.includes(item.cd_id) ? 'keyword selected' : 'keyword'}
            key={key}
            onClick={() => this.onClickKeyword(item.cd_id)}
          >
            {item.cd_label}
          </span>
        ))}
      </Fragment>
    )
  }

  render() {
    return (
      <Fragment>
        {this.renderKeyword()}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    code: state.code
  }
}

const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation('translations')(FistarKeyword))
