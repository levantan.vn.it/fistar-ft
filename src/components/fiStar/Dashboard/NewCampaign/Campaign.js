import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import imgaeIdol from "./../../../../images/dash-02.png";
import imageFace from "./../../../../images/dashboard/face.png";
import imageIntar from "./../../../../images/dashboard/intar.png";
import imageToutube from "./../../../../images/toutube.png";
import imageRead from "./../../../../images/mk.png";
import ModalStatusCampaign from "./../../../fiStar/Dashboard/MyCampaign/ModalStatusCampaign";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import { getImageLink } from './../../../../common/helper'
import { IMAGE_SIZE, IMAGE_TYPE } from './../../../../constants/index'

class Campaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }

  renderStatus = () => {
    const {
      t,
      campaign: { last_matching_status_of_influencer }
    } = this.props;
    if (!last_matching_status_of_influencer) {
      return null;
    }

    return (
      <div className="process-campaign">
        <span className="icon-start" />
        {last_matching_status_of_influencer.label_status.fistar_status
          .split("→")
          .map((status, key) => {
            return (
              <Fragment key={`${key}`}>
                <p className="text">{status}</p>
                {last_matching_status_of_influencer.label_status.fistar_status.split(
                  "→"
                ).length -
                  1 !==
                  key && <span />}
              </Fragment>
            );
          })}
        <i className="fas fa-long-arrow-alt-right" />
      </div>
    );
  };

  OpenModal = e => {
    e.preventDefault();
    this.setState({ show: true });
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  onCloseModal = () => {
    console.log(this.state.show);
    this.setState({ show: !this.state.show });
  };

  render() {
    const { t, campaign } = this.props;
    console.log(campaign);
    let status = "";
    switch (campaign.cp_status) {
      case 59: {
        status = `${t("DASHBOARD_FISTAR.DFR_status_matching")}`;
        break;
      }
      case 60: {
        status = `${t("DASHBOARD_FISTAR.DFR_status_ready")}`;
        break;
      }
      case 61: {
        status = `${t("DASHBOARD_FISTAR.DFR_status_ongoing")}`;
        break;
      }
      case 62: {
        status = `${t("CAMPAIGN.CAN_close")}`;
        break;
      }
      default: {
        break;
      }
    }

    return (
      <div className="item">
        <div className="img-matching">
          <Link to="/fi-star/my-campaign-detail">
            <img
              src={getImageLink(campaign.cp_image, IMAGE_TYPE.CAMPAIGNS, IMAGE_SIZE.ORIGINAL)}
              alt={campaign.cp_name}
              title={campaign.cp_image_title}
            />
          </Link>
          <div className="btn over-flow">
            <i className="fas fa-heart" /> <span>{campaign.scraps_count}</span>
          </div>
        </div>
        <div className="status-campaign">
          <div className="name">
            <div className="left">
              <span className="wrap-matching">{status}</span>
              <h4>{campaign.cp_name}</h4>
            </div>
            <p>
              <a href="#">
                Open
                <span>{campaign.cp_period_start.split(" ")[0]}</span>
              </a>
            </p>
          </div>
          {this.renderStatus()}
          <div className="fistar-apply">
            <h4>fiStar {campaign.cp_total_influencer}</h4>
            <div className="user-apply" onClick={this.OpenModal}>
              <p>
                <span>{campaign.total_applied_count}</span> applied
              </p>
              {campaign.total_applied_count > 0 && (
                <div className="gr-user">
                  <div className="user">
                    <a href="/fi-star/my-campaign-detail">
                      <img
                        src=""
                        alt=""
                      />
                    </a>
                  </div>
                </div>
              )}
            </div>
          </div>
          <div className="price-matching">
            <div className="item-price">
              <div className="icon-solical">
                <a href="">
                  <img src={imageRead} alt="" />
                </a>
              </div>
              <button>Ready to Write</button>
            </div>
            <div className="item-price">
              <div className="icon-solical">
                <a href="">
                  <img src={imageFace} alt="" />
                </a>
              </div>
              <h6 className="texts text-center">-</h6>
            </div>
            <div className="item-price">
              <div className="icon-solical">
                <a href="">
                  <img src={imageIntar} alt="" />
                </a>
              </div>
              <button>Preparing</button>
            </div>
            <div className="item-price">
              <div className="icon-solical">
                <a href="">
                  <img src={imageToutube} alt="" />
                </a>
              </div>
              <button>Open Review</button>
            </div>
          </div>
        </div>

        <Modal
          size="lg"
          show={this.state.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="popup-history"
        >
          <ModalStatusCampaign onClose={this.onCloseModal} />
        </Modal>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(Campaign)
);
