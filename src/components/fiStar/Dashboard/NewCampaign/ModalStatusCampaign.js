import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";

// import calendar style
// You can customize style by copying asset folder.
import "@y0c/react-datepicker/assets/styles/calendar.scss";
import { format } from "date-fns";
// import PushMessageComponent from './../../../components/PushMessage'

import imgaeIdol from "../../../../images/dash-02.png";
import imageClose from "../../../../images/close-popup.svg";
import imageFb from "../../../../images/face.png";
import imageYt from "../../../../images/toutube.png";
import imageInstar from "../../../../images/intar.png";
import imageMk from "../../../../images/mk.png";
import imageThum from "../../../../images/thumnaildetail.png";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";

class ModalStatusCampaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }

  handleClose = () => {
    // console.log(this.props.onClose())
    this.setState({ show: !this.props.onClose() });
  };

  render() {
    const { t } = this.props;
    const { height } = this.state;

    return (
      <Fragment>
        <div className="popup-fistar-status">
          <div className="modal-header">
            <h4 id="exampleModalLabel">{t("POPUP_FISTAR_STATUS.PFS_title")}</h4>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
              onClick={this.handleClose}
            >
              <img src={imageClose} alt="" />
            </button>
          </div>
          <div className="body-popup">
            <div className="content-proceed">
              <div className="tab-content">
                <ul className="list-tab">
                  <li className="item">
                    <a href="" className="link-tab recruiting ">
                      {t("POPUP_FISTAR_STATUS.PFS_tab_recruiting")}
                    </a>
                    <p className="amount recruiting">4</p>
                  </li>
                  <li className="item">
                    <a href="" className="link-tab active">
                      {t("POPUP_FISTAR_STATUS.PFS_tab_fistar_applied")}
                    </a>
                    <p className="amount active">5</p>
                  </li>
                  <li className="item">
                    <a href="" className="link-tab ">
                      <i className="fas fa-star" />
                      {t("POPUP_FISTAR_STATUS.PFS_tab_scrap_fistar")}
                    </a>
                    <p className="amount">6</p>
                  </li>
                </ul>
              </div>
            </div>
            <div className="information-user">
              <div className="card-groups">
                <div className="card">
                  <div className="top">
                    <div className="left-card">
                      <div className="male">
                        <i className="fas fa-mars" />
                      </div>
                      <div className="address-old">
                        <h4>
                          <a href="">Gianna.Jun</a>
                        </h4>
                        <span>1987</span>
                        <span className="address">HaNoi</span>
                      </div>
                    </div>
                  </div>
                  <div className="img-partner">
                    <a href="">
                      {" "}
                      <img src={imageThum} alt="..." />
                    </a>
                    <div className="btn over-flow">
                      <i className="fas fa-star" />
                      <span>1,300</span>
                    </div>
                  </div>
                  <div className="card-footer">
                    <ul className="list-footer">
                      <li className="item">
                        <a href="">
                          <i className="fab fa-facebook-f" />
                        </a>
                        <span>186K</span>
                      </li>
                      <li className="item">
                        <a href="">
                          <i className="fab fa-instagram" />
                        </a>
                        <span>186K</span>
                      </li>
                      <li className="item">
                        <a href="">
                          <i className="fab fa-youtube-square" />
                        </a>
                        <span>226K</span>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="card">
                  <div className="top">
                    <div className="left-card">
                      <div className="male">
                        <i className="fas fa-mars" />
                      </div>
                      <div className="address-old">
                        <h4>
                          <a href="">Gianna.Jun</a>
                        </h4>
                        <span>1987</span>
                        <span className="address">HaNoi</span>
                      </div>
                    </div>
                  </div>
                  <div className="img-partner">
                    <a href="">
                      {" "}
                      <img src={imageThum} alt="..." />
                    </a>
                    <div className="btn over-flow">
                      <i className="fas fa-star" />
                      <span>1,300</span>
                    </div>
                  </div>
                  <div className="card-footer">
                    <ul className="list-footer">
                      <li className="item">
                        <a href="">
                          <i className="fab fa-facebook-f" />
                        </a>
                        <span>186K</span>
                      </li>
                      <li className="item">
                        <a href="">
                          <i className="fab fa-instagram" />
                        </a>
                        <span>186K</span>
                      </li>
                      <li className="item">
                        <a href="">
                          <i className="fab fa-youtube-square" />
                        </a>
                        <span>226K</span>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="card">
                  <div className="top">
                    <div className="left-card">
                      <div className="male">
                        <i className="fas fa-mars" />
                      </div>
                      <div className="address-old">
                        <h4>
                          <a href="">Gianna.Jun</a>
                        </h4>
                        <span>1987</span>
                        <span className="address">HaNoi</span>
                      </div>
                    </div>
                  </div>
                  <div className="img-partner">
                    <a href="">
                      {" "}
                      <img src={imageThum} alt="..." />
                    </a>
                    <div className="btn over-flow">
                      <i className="fas fa-star" />
                      <span>1,300</span>
                    </div>
                  </div>
                  <div className="card-footer">
                    <ul className="list-footer">
                      <li className="item">
                        <a href="">
                          <i className="fab fa-facebook-f" />
                        </a>
                        <span>186K</span>
                      </li>
                      <li className="item">
                        <a href="">
                          <i className="fab fa-instagram" />
                        </a>
                        <span>186K</span>
                      </li>
                      <li className="item">
                        <a href="">
                          <i className="fab fa-youtube-square" />
                        </a>
                        <span>226K</span>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="card">
                  <div className="top">
                    <div className="left-card">
                      <div className="male">
                        <i className="fas fa-mars" />
                      </div>
                      <div className="address-old">
                        <h4>
                          <a href="">Gianna.Jun</a>
                        </h4>
                        <span>1987</span>
                        <span className="address">HaNoi</span>
                      </div>
                    </div>
                  </div>
                  <div className="img-partner">
                    <a href="">
                      {" "}
                      <img src={imageThum} alt="..." />
                    </a>
                    <div className="btn over-flow">
                      <i className="fas fa-star" />
                      <span>1,300</span>
                    </div>
                  </div>
                  <div className="card-footer">
                    <ul className="list-footer">
                      <li className="item">
                        <a href="">
                          <i className="fab fa-facebook-f" />
                        </a>
                        <span>186K</span>
                      </li>
                      <li className="item">
                        <a href="">
                          <i className="fab fa-instagram" />
                        </a>
                        <span>186K</span>
                      </li>
                      <li className="item">
                        <a href="">
                          <i className="fab fa-youtube-square" />
                        </a>
                        <span>226K</span>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="card">
                  <div className="top">
                    <div className="left-card">
                      <div className="male">
                        <i className="fas fa-mars" />
                      </div>
                      <div className="address-old">
                        <h4>
                          <a href="">Gianna.Jun</a>
                        </h4>
                        <span>1987</span>
                        <span className="address">HaNoi</span>
                      </div>
                    </div>
                  </div>
                  <div className="img-partner">
                    <a href="">
                      {" "}
                      <img src={imageThum} alt="..." />
                    </a>
                    <div className="btn over-flow">
                      <i className="fas fa-star" />
                      <span>1,300</span>
                    </div>
                  </div>
                  <div className="card-footer">
                    <ul className="list-footer">
                      <li className="item">
                        <a href="">
                          <i className="fab fa-facebook-f" />
                        </a>
                        <span>186K</span>
                      </li>
                      <li className="item">
                        <a href="">
                          <i className="fab fa-instagram" />
                        </a>
                        <span>186K</span>
                      </li>
                      <li className="item">
                        <a href="">
                          <i className="fab fa-youtube-square" />
                        </a>
                        <span>226K</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(ModalStatusCampaign));
