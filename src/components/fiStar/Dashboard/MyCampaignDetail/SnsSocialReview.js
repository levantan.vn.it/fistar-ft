import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import * as routeName from "./../../../../routes/routeName";
import { withRouter } from "react-router";

import imgaeIdol from "./../../../../images/dash-02.png";
import imgaeLeft from "./../../../../images/left.svg";
import imageFime from "./../../../../images/mk.png";
import imageFace from "./../../../../images/dashboard/face.png";
import imageIntar from "./../../../../images/dashboard/intar.png";
import imageToutube from "./../../../../images/toutube.png";
import imageRead from "./../../../../images/mk.png";
import imageLogo from "./../../../../images/dashboard/Layer 6.png";
import { getImageLink } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";

const SNS = {
  FIME: 1,
  FACEBOOK: 2,
  YOUTUBE: 3,
  INSTAGRAM: 4
};

const SNS_STATE = {
  READY: 101,
  REQUEST: 102,
  CHECKING: 103,
  MODIFY: 104,
  COMPLETE: 105
};

const MATCHING_STATUS = {
    MARCHED: [8, 16],
    RECOMMENDEDED: [],
    CONSIDERING: [1, 9],
    REJECTED: [3, 11, 13],
    CANCEL: [2, 5, 7, 10, 15]
};

class Review extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: { ...props.sns },
      loading: false
    };
  }

  updateStatus = (m_id, data) => {
    this.setState({
      loading: true
    }, () => {
      console.log(m_id, data);
      this.props.updateStatus(m_id, data)
        .then(() => {
          this.setState({
            loading: false
          });
        });
    });
  };

  handleChangeInput = e => {
    this.setState({
      data: {
        ...this.state.data,
        m_ch_url: e.target.value
      }
    });
  };

  renderNoSns = (show, text) => {
    if (!show) {
      return null;
    }
    return (
      <div className="toptip-evaluate">
        <p>{text}</p>
      </div>
    );
  };

  renderSNSText = sns => {
    if (
      sns &&
      sns.review_status &&
      sns.review_status.rv_status &&
      sns.review_status.rv_status === SNS_STATE.COMPLETE &&
      !sns.m_ch_url
    ) {
      return (
        <textarea
          className="form-control"
          id="exampleFoghrmControlTextarvcea1"
          rows="6"
          placeholder="Please input (copy) the final review URL registered in SNS."
          onChange={this.handleChangeInput}
        />
      );
    }
    return (
      <textarea
        className="form-control"
        id="exampleFoghrmControlTextarvcea1"
        rows="6"
        defaultValue={
          sns && sns.m_ch_selected
            ? sns.m_ch_title + "\n" + (sns.m_ch_url ? sns.m_ch_url : "")
            : ""
        }
        readOnly
      />
    );
  };

  renderSNSButton = sns => {
    const { mstatus } = this.props;
    console.log(mstatus);
    if (!sns || !sns.review_status) {
      return null;
    }
    const { t } = this.props;
   
    switch (sns.review_status.rv_status) {
      case SNS_STATE.READY: {
        
          return (
            <button
              type="button"
              onClick={() => this.props.onClickRedirectToWrite(sns.sns_id)}
            >
              {sns.m_ch_title ? 'Writing' : t("MY_CAMPAIGN_DETAIL.MCD_button_write_review")}
            </button>
          );
        
        
      }
      case SNS_STATE.REQUEST: {
        return (
          <Fragment>
            <button
              type="button"
              className="mr-2"
              onClick={() => this.props.onClickRedirectToWrite(sns.sns_id)}
            >
              {this.state.loading ? (
                <div className="spinner-border" role="status">
                  <span className="sr-only">{t("LOADING.LOADING")}</span>
                </div>
              ) : (
                `${t("WRITE_REVIEW.WRW_button_edit")}`
              )}
            </button>
            <button
              type="button"
              onClick={() =>
                this.updateStatus(sns.m_id, {
                  ...sns,
                  rv_status: SNS_STATE.CHECKING
                })
              }
            >
              {this.state.loading ? (
                <div className="spinner-border" role="status">
                  <span className="sr-only">{t("LOADING.LOADING")}</span>
                </div>
              ) : (
                `${t("WRITE_REVIEW.WRW_button_request")}`
              )}
            </button>
          </Fragment>
        );
      }
      case SNS_STATE.CHECKING: {
        return (
          <button className="considering" type="button">
            {t("MY_CAMPAIGN_DETAIL.MCD_button_considering")}
          </button>
        );
      }
      case SNS_STATE.MODIFY: {
        return (
          <button
            type="button"
            onClick={() => this.props.onClickRedirectToWrite(sns.sns_id)}
          >
            {t("WRITE_REVIEW.WRW_button_edit")}
          </button>
        );
      }
      case SNS_STATE.COMPLETE: {
        if (sns.m_ch_url) {
          if (sns.m_ch_active_url) {
            return <button type="button">{t("FISTAR_JOIN.FJN_completed")}</button>;
          } else {
            return <button className="considering" type="button">
              {t("MY_CAMPAIGN_DETAIL.MCD_button_considering")}
            </button>
          }
        }
        return (
          <button
            type="button"
            onClick={() =>
              this.updateStatus(this.state.data.m_id, {
                ...this.state.data,
                rv_status: SNS_STATE.COMPLETE
              })
            }
          >
            {this.state.loading ? (
              <div className="spinner-border" role="status">
                <span className="sr-only">{t("LOADING.LOADING")}</span>
              </div>
            ) : (
              `${t("MY_CAMPAIGN_DETAIL.save_SNS_URL")}`
            )}
          </button>
        );
      }
      default:
        return null;
    }
    return (
      <button className="considering" type="button">
        {t("MY_CAMPAIGN_DETAIL.MCD_button_considering")}
      </button>
    );
  };

  render() {
    const { sns, cp_image, logo, text, showImage, t, mstatus } = this.props;
    let textWaring = "";
    let show = false;
    console.log(sns, 'snsssssssssssssssssssssss');
    if (sns && sns.review_status && sns.m_ch_selected) {
      switch (sns.review_status.rv_status) {
        case SNS_STATE.READY: {
          textWaring = `${t("MY_CAMPAIGN_DETAIL.MCD_toptip_text")}`;
          if (!sns.m_ch_title) {
            show = true;
          }
          break;
        }
        case SNS_STATE.REQUEST: {
          show = false;
          break;
        }
        case SNS_STATE.CHECKING: {
          textWaring = `${t("MY_CAMPAIGN_DETAIL.unable_to_modify")}`;
          show = true;
          break;
        }
        case SNS_STATE.MODIFY: {
          show = false;
          break;
        }
        case SNS_STATE.COMPLETE: {
          show = false;
          break;
        }
        default:
          break;
      }
    }
    console.log(this.props)

    return (
      <Fragment>
        <div className="card">
          {showImage && (
            <div className="image-card">
              <a href="javascript:void(0)">
                <img
                  src={getImageLink(
                    cp_image,
                    IMAGE_TYPE.CAMPAIGNS,
                    IMAGE_SIZE.ORIGINAL
                  )}
                  alt=""
                />
              </a>
            </div>
          )}
          <div className="top-card">
            <div className="left">
              <div className="icon-solical">
                <a href="javascript:void(0)">
                  <img src={logo} alt={text} />
                </a>
              </div>
              <div className="name">
                <h4>{text}</h4>
              </div>
            </div>
            {!!sns &&
              sns.m_ch_selected === 1 &&
              sns.m_ch_title && (
                <div className="right">
                  <p>{(sns.updated_at || "").split(" ")[0]}</p>
                </div>
              )}
            {this.renderNoSns(show, textWaring)}
          </div>
          <div className="form-card">{this.renderSNSText(sns)}</div>
          <div className="btn-submib">
            {this.renderSNSButton(!!sns && sns.m_ch_selected ? sns : "")}
          </div>
        </div>
      </Fragment>
    );
  }
}

class SnsSocialReview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRedirectToWrite: false,
      sns_id: "",
      loading: false
    };
  }

  onClickRedirectToWrite = sns_id => {
    this.setState({
      isRedirectToWrite: true,
      sns_id
    });
  };

  render() {
    const { t, sns, campaign, match , mstatus } = this.props;
    const { isRedirectToWrite, sns_id } = this.state;

    if (isRedirectToWrite) {
      return (
        <Redirect
          to={`/fi-star/my-campaign-detail/${match.params.id}${
            routeName.WRITE_REVIEW
          }/${sns_id}`}
        />
      );
    }

    let fime = null;
    let facebook = null;
    let youtube = null;
    let instagram = null;
    sns.map(s => {
      switch (s.sns_id) {
        case SNS.FIME: {
          fime = s;
          break;
        }
        case SNS.FACEBOOK: {
          facebook = s;
          break;
        }
        case SNS.YOUTUBE: {
          youtube = s;
          break;
        }
        case SNS.INSTAGRAM: {
          instagram = s;
          break;
        }
        default: {
          break;
        }
      }
    });
    MATCHING_STATUS.MARCHED.includes(mstatus)
    return (
      <Fragment>
        {MATCHING_STATUS.MARCHED.includes(mstatus) ? <div className="solical-review">
        <div className="title">
          <h4>
            {t("MY_CAMPAIGN_DETAIL.MCD_title_left")}{" "}
            <span> {t("MY_CAMPAIGN_DETAIL.MCD_title_red")} </span>{" "}
            {t("MY_CAMPAIGN_DETAIL.MCD_title_right")}.
          </h4>
        </div>
        <div className="card-solical">
          <div className="row">
            <div className="col-lg-6 col-md-6 col-sm-12">
              <Review
                {...this.props}
                sns={fime}
                cp_image={campaign.cp_main_image}
                logo={imageFime}
                text="fi :me Review"
                showImage={true}
                onClickRedirectToWrite={this.onClickRedirectToWrite}
                updateStatus={this.props.updateStatus}
              />
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12">
              <Review
                {...this.props}
                sns={facebook}
                cp_image={campaign.cp_main_image}
                logo={imageFace}
                text="Facebook Review"
                showImage={true}
                onClickRedirectToWrite={this.onClickRedirectToWrite}
                updateStatus={this.props.updateStatus}
              />
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12">
              <Review
                {...this.props}
                sns={youtube}
                cp_image={campaign.cp_main_image}
                logo={imageToutube}
                text="Youtube Review"
                showImage={true}
                onClickRedirectToWrite={this.onClickRedirectToWrite}
                updateStatus={this.props.updateStatus}
              />
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12">
              <Review
                {...this.props}
                sns={instagram}
                cp_image={campaign.cp_main_image}
                logo={imageIntar}
                text="Instagram Review"
                showImage={true}
                onClickRedirectToWrite={this.onClickRedirectToWrite}
                updateStatus={this.props.updateStatus}
              />
            </div>
          </div>
        </div>
      </div> : ""}
      
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(withRouter(SnsSocialReview))
);
