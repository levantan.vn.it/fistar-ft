import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import imgaeIdol from "../../../../images/dashboard/dash-02.png";
import imageFace from "../../../../images/dashboard/face.png";
import imageIntar from "../../../../images/dashboard/intar.png";
import imageToutube from "../../../../images/dashboard/toutube.png";
import imageFime from "../../../../images/dashboard/fime.png";
import {
  getNewCampaignAction,
  applyNewCampaignAction
} from "./../../../../store/actions/campaign";
import InfiniteScroll from "react-infinite-scroll-component";
import * as routeName from "./../../../../routes/routeName";
import { getImageLink } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";
import Scrap from "./../../../../components/Scrap";
import CampaignMainImage from './../../../../components/CampaignMainImage'

class Campaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      isApplied: false
    };
  }

  applyNewCampaign = campaign => {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props
          .applyNewCampaign(campaign.cp_id)
          .then(response => {
            this.setState({
              isApplied: true,
              loading: false
            });
            this.props.applyCallback();
          })
          .catch(e => {
            alert(`${this.props.t("CAMPAIGN.already_applied")}`);
          });
      }
    );
  };

  callback = () => {
    this.props.callback();
  };

  render() {
    const { t, campaign } = this.props;
    let status = "";
    let is_matching = false;
    let totalFistarMatching = 0
    switch (campaign.cp_status) {
      case 59: {
        status = `${t("DASHBOARD_FISTAR.DBF_status_matching")}`;
        campaign.matchings.map(matching => {
          if (
            matching.influencer &&
            matching.influencer.email === this.props.auth.email
          ) {
            is_matching = true;
          }
        });

          totalFistarMatching = campaign.matchings && campaign.matchings.length > 0 ? campaign.matchings.length : 0

        break;
      }
      case 60: {
        status = `${t("DASHBOARD_FISTAR.DBF_status_ready")}`;
        break;
      }
      case 61: {
        status = `${t("DASHBOARD_FISTAR.DBF_status_on_going")}`;
        break;
      }
      case 62: {
        status = `${t("DASHBOARD_FISTAR.DBF_close")}`;
        break;
      }
      default: {
        break;
      }
    }
    console.log(campaign);
    console.log(totalFistarMatching);

    const {
      campaign: { matchings }
    } = this.props;
    // if (!matchings || !matchings[0]) {
    //     return null;
    // }
    //
    //
      console.log(matchings)

    let channel =
      matchings && matchings[0] ? matchings[0].matching_channel : "";
    var fime = "";
    var facebook = "";
    var youtube = "";
    var instagram = "";

    channel &&
      channel.map(channel => {
        if (
          channel.sns_id == 1 &&
          channel.m_ch_selected == 1 &&
          channel.review_status
        ) {
          fime = channel.review_status.label.fistar_status;
        }
        if (
          channel.sns_id == 2 &&
          channel.m_ch_selected == 1 &&
          channel.review_status
        ) {
          facebook = channel.review_status.label.fistar_status;
        }
        if (
          channel.sns_id == 3 &&
          channel.m_ch_selected == 1 &&
          channel.review_status
        ) {
          youtube = channel.review_status.label.fistar_status;
        }
        if (
          channel.sns_id == 4 &&
          channel.m_ch_selected == 1 &&
          channel.review_status
        ) {
          instagram = channel.review_status.label.fistar_status;
        }
      });

    return (
      <Fragment>
        <div className="item wf-item-fistar">
          <div className="img-fistar">
            <Link to={"/fi-star/my-campaign-detail/" + campaign.cp_slug}>
              <CampaignMainImage campaign={campaign} />
            </Link>
            <Scrap
              scraps_count={campaign.scraps_count}
              is_scrap={campaign.is_scrap}
              id={campaign.cp_id}
              className="btn over-flow"
              iconClassName={"fa-heart"}
              callback={this.callback}
            />
            <span className="overlay-fistar">{status}</span>
          </div>
          <div className="title">
            <Link to={"/fi-star/my-campaign-detail/" + campaign.cp_slug}>
              <h4>{campaign.cp_name}</h4>
            </Link>
            <p>
              <Link to={"/fi-star/my-campaign-detail/" + campaign.cp_slug}>
                {campaign.cp_period_end.split(" ")[0]}{" "}
                {t("DASHBOARD_FISTAR.DFR_open")}
              </Link>
            </p>
          </div>
          <div className="amount-myfistar">
            <div className="list-item">
              <div className="item-amount">
                <p>
                  <span>{campaign.cp_total_influencer}</span>
                </p>
                <span>
                  <a href="javascript:void(0)">
                    {t("DASHBOARD_FISTAR.DFR_recriuting")}
                  </a>
                </span>
              </div>
              <div className="item-amount">
                <div className="wf-item">
                  <div className="left">
                    <p>
                      <span>
                        {/*{!this.state.isApplied*/}
                          {/*? campaign.total_applied_count*/}
                          {/*: campaign.total_applied_count + 1}*/}

                          {totalFistarMatching}

                      </span>
                    </p>
                    <span>
                      <a href="javascript:void(0)">
                        {t("DASHBOARD_FISTAR.DFR_apply")}
                      </a>
                    </span>
                  </div>
                  {!this.state.isApplied &&
                    campaign.cp_status === 59 &&
                    !is_matching && (
                      <div className="right">
                        <button
                          type={"button"}
                          onClick={() => this.applyNewCampaign(campaign)}
                        >
                          {this.state.loading ? (
                            <div className="spinner-border" role="status">
                              <span className="sr-only">
                                {t("LOADING.LOADING")}
                              </span>
                            </div>
                          ) : (
                            `${t("DASHBOARD_FISTAR.DFR_status_apply")}`
                          )}
                        </button>
                      </div>
                    )}
                </div>
              </div>
            </div>
          </div>
          <div className="footer-card">
            <div className="solical-myfistar">
              <div className="item-solical">
                <div className="icon-solical">
                  <a href="javascript:void(0)">
                    <img src={imageFime} alt="Fime" />
                  </a>
                </div>
                {fime ? (
                  <button className="status-channel">{fime}</button>
                ) : (
                  <h6 className="texts text-center">-</h6>
                )}
              </div>
              <div className="item-solical">
                <div className="icon-solical">
                  <a href="javascript:void(0)">
                    <img src={imageFace} alt="Facebook" />
                  </a>
                </div>
                {facebook ? (
                  <button className="status-channel">{facebook}</button>
                ) : (
                  <h6 className="texts text-center">-</h6>
                )}
              </div>
              <div className="item-solical">
                <div className="icon-solical">
                  <a href="javascript:void(0)">
                    <img src={imageIntar} alt="Instagram" />
                  </a>
                </div>
                {instagram ? (
                  <button className="status-channel">{instagram}</button>
                ) : (
                  <h6 className="texts text-center">-</h6>
                )}
                {/*<h6 className="texts text-center">-</h6>*/}
              </div>
              <div className="item-solical">
                <div className="icon-solical">
                  <a href="javascript:void(0)">
                    <img src={imageToutube} alt="Youtube" />
                  </a>
                </div>
                {youtube ? (
                  <button className="status-channel">{youtube}</button>
                ) : (
                  <h6 className="texts text-center">-</h6>
                )}
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

class PartnerNewCampaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      campaigns: [],
      scrap: 0,
      request: 0,
      apply: 0,
      recommended: 0,
      rejected: 0,
      total: 0,
      isLoadingCampaign: false,
      last_page: 1,
      page: 1,
      hasMore: true,
      moreNewCampaign: false,
      isApplied: false
    };
  }

  componentDidMount() {
    console.log(12312312313);
    this.setState(
      {
        isLoadingCampaign: true
      },
      () => {
        this.props.getNewCampaign(this.state.page).then(response => {
          this.setState({
            campaigns: response.data,
            scrap: response.scrap,
            request: response.request,
            apply: response.apply,
            recommended: response.recommended,
            rejected: response.rejected,
            total: response.total,
            isLoadingCampaign: false,
            last_page: response.last_page
          });
        });
      }
    );
  }

  callback = () => {
    this.props.callback();
  };

  applyCallback = () => {
    this.props.applyCallback();
    this.props.getNewCampaign(1).then(response => {
      this.setState({
        campaigns: response.data,
        scrap: response.scrap,
        request: response.request,
        apply: response.apply,
        recommended: response.recommended,
        rejected: response.rejected,
        total: response.total,
        isLoadingCampaign: false,
        last_page: response.last_page
      });
    });
  };

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }
    this.props.getNewCampaign(this.state.page + 1).then(response => {
      this.setState({
        campaigns: [...this.state.campaigns, ...response.data],
        page: response.current_page
      });
    });
  };

  renderCampaign = () => {
    const { campaigns } = this.state;
    console.log(campaigns);
    return (campaigns.slice(0, 5) || []).map((campaign, key) => (
      <Fragment key={key}>
        <Campaign
          {...this.props}
          campaign={campaign}
          applyNewCampaign={this.props.applyNewCampaign}
          auth={this.props.auth}
          callback={this.callback}
          applyCallback={this.applyCallback}
        />
      </Fragment>
    ));
  };
  moreRedirect = () => {
    this.setState({
      moreNewCampaign: true
    });
  };

  render() {
    const { t, height, count } = this.props;
    const {
      campaigns,
      isLoadingCampaign,
      scrap,
      request,
      apply,
      recommended,
      rejected,
      total,
      moreNewCampaign
    } = this.state;
    console.log(campaigns);
    if (moreNewCampaign) {
      return <Redirect to={routeName.FISTAR_SEARCH_CAMPAIGN} />;
    }
    return (
      <div className="my-fistar ">
        <div className="header-my-fistar">
          <div className="top">
            <h4>{t("DASHBOARD_FISTAR.DFR_new_campaign")}</h4>
            {!(campaigns.length === 0 && !isLoadingCampaign) && (
              <button onClick={this.moreRedirect}>
                {t("DASHBOARD_FISTAR.DFR_button_more")}
              </button>
            )}
          </div>
          <div className="amount-notification">
            <h2>{total}</h2>
          </div>
          <div className="list-amount">
            <div className="item">
              <p>
                <span>
                  <i className="fas fa-star" />
                  {count ? count.scrap : ""}
                </span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DFR_status_scrap")}</span>
            </div>
            <div className="item">
              <p>
                <span>{count ? count.recommended : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DFR_status_recommend")}</span>
            </div>
            <div className="item">
              <p>
                <span>{count ? count.request : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DFR_status_request")}</span>
            </div>
            <div className="item">
              <p>
                <span>{count ? count.apply : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DFR_status_apply")}</span>
            </div>
            <div className="item closed">
              <p>
                <span>{count ? count.rejected : ""}</span>
              </p>
              <span>Rejected</span>
            </div>
          </div>
        </div>

        {campaigns.length === 0 && !isLoadingCampaign ? (
          <Fragment>
            <div className={"text-center mt-5"}>No campaign</div>
          </Fragment>
        ) : (
          <div className="card-fistar">
            {this.renderCampaign()}
            {/*<InfiniteScroll*/}
            {/*dataLength={this.state.campaigns.length}*/}
            {/*next={this.fetchMoreData}*/}
            {/*loader={<h4>{t("LOADING.LOADING")}</h4>}*/}
            {/*hasMore={this.state.hasMore}*/}
            {/*height={height}*/}
            {/*className="card-fistar"*/}
            {/*>*/}
            {/*{this.renderCampaign()}*/}
            {/*</InfiniteScroll>*/}
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getNewCampaign: (page = 1) => dispatch(getNewCampaignAction(page)),
    applyNewCampaign: id => dispatch(applyNewCampaignAction(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerNewCampaign));
