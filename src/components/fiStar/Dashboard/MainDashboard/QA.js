import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { getQAAction } from "./../../../../store/actions/campaign";
import * as routeName from "./../../../../routes/routeName";

class PartnerDashboardQA extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      type: 1,
      qa_state: 1,
      qa: null,
      matching: 0,
      scrap: 0,
      request: 0,
      apply: 0,
      recommended: 0,
      total: 0,
      isLoadingFistar: false,
      moreContact: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingFistar: true
      },
      () => {
        this.props
          .getQAAction(this.state.page, this.state.type)
          .then(response => {
            console.log(response);
            // let dataMap = [];
            // let dataComplete = response.data.map((data, key) => {
            //   if (data.qa_state == 1) {
            //     dataMap.push(data);
            //   }
            //     return dataMap;
            // });

            this.setState({
              qa: response.data[0],
              matching: response.matching,
              scrap: response.scrap,
              request: response.request,
              apply: response.apply,
              recommended: response.recommended,
              total: response.total,
              isLoadingFistar: false
            });
          })
          .catch(() => {
            this.setState({
              isLoadingFistar: false
            });
          });
      }
    );
  }
  moreContactUs = () => {
    this.setState({
      moreContact: true
    });
  };

  render() {
    const { t } = this.props;
    const { qa, isLoadingFistar, moreContact } = this.state;

    if (isLoadingFistar) {
      return (
        <div className="campaign-track">
          <h1>{t("LOADING.LOADING")}</h1>
        </div>
      );
    }
    if (moreContact) {
      return <Redirect to={routeName.FISTAR_QA} />;
    }

    return (
      <Fragment>
        <div className="q-a-header">
          <div className="top">
            <h4>{t("DASHBOARD_FISTAR.DFR_qa")}</h4>
            <button onClick={this.moreContactUs}>
              {t("DASHBOARD_FISTAR.DFR_button_contact_us")}
            </button>
          </div>
          {qa ? (
            <div className="message-q-a">
              <div className="item item-q">
                <div className="thumnail-user">
                  <span className="user-name">Q</span>
                </div>
                <div className="message">
                  <span>{qa.qa_title}</span>
                </div>
              </div>
              <div className="item item-a">
                <div className="message-a">
                  {/*<span>*/}
                  {/*{qa.qa_answer ? qa.qa_answer : "Answer is in preparation."}*/}
                  {/*</span>*/}
                  <Link to={`/fi-star/qa/detail/${qa.qa_id}`}>
                    {qa.qa_state && qa.qa_state == 1
                      ? "Completed"
                      : "Preparing"}
                  </Link>
                </div>
                <div className="thumnail-user">
                  <span className="user-name">A</span>
                </div>
              </div>
            </div>
          ) : (
            <div className={"text-center mt-5"}>
              {t("DASHBOARD_FISTAR.DFR_text_no_question")}
            </div>
          )}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    getQAAction: (page = "", type = "", state = "") =>
      dispatch(getQAAction(page, type, state))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerDashboardQA));
