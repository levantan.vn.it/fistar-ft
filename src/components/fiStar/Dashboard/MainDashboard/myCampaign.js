import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import imgaeIdol from "../../../../images/dashboard/dash-02.png";
import { getMyCampaignDashBoardAction } from "./../../../../store/actions/campaign";
import InfiniteScroll from "react-infinite-scroll-component";
import * as routeName from "./../../../../routes/routeName";
import { getImageLink } from "./../../../../common/helper";
import {
  IMAGE_SIZE,
  IMAGE_TYPE,
  CAMPAIGN_STATUS
} from "./../../../../constants/index";
import Scrap from "./../../../../components/Scrap";
import CampaignMainImage from "./../../../../components/CampaignMainImage";
import {FISTER_FINISH_STEP} from "../../../../constants";

const MATCHING_STATUS = {
  MARCHED: [8, 16],
  RECOMMENDEDED: [],
  CONSIDERING: [1, 9],
  REJECTED: [3, 11, 13],
  CANCEL: [2, 5, 7, 10, 15]
};

class FistarMyCampaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      campaigns: [],
      matching: 0,
      ready: 0,
      ongoing: 0,
      closed: 0,
      total: 0,
      isLoadingCampaign: false,
      last_page: 1,
      page: 1,
      hasMore: true,
      isRedirectMyCamPaign: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingCampaign: true
      },
      () => {
        this.props.getMyCampaignDashBoard(this.state.page).then(response => {
          this.setState({
            campaigns: response.data,
            matching: response.matching,
            ready: response.ready,
            ongoing: response.ongoing,
            closed: response.closed,
            total: response.total,
            isLoadingCampaign: false,
            last_page: response.last_page
          });
        });
      }
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.reset) {
      this.props.getMyCampaignDashBoard(1).then(response => {
        this.setState({
          campaigns: response.data,
          matching: response.matching,
          ready: response.ready,
          ongoing: response.ongoing,
          closed: response.closed,
          total: response.total,
          isLoadingCampaign: false,
          last_page: response.last_page
        });
      });
      this.props.changeReset();
    }
  }

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }
    this.props.getMyCampaignDashBoard(this.state.page + 1).then(response => {
      this.setState({
        campaigns: [...this.state.campaigns, ...response.data],
        page: response.current_page
      });
    });
  };

  clickMore = () => {
    this.setState({
      isRedirectMyCamPaign: true
    });
  };

  renderStatus = matching => {
    const {auth} = this.props;
    console.log(matching, "4444444444444");
    if (!matching || !matching[0]) {
      return null;
    }
    
    console.log(auth)
    console.log(matching)
      let matchingFistar = matching && matching.length > 0 && matching.filter(matching => matching.influencer.email == auth.email)
      const { label, m_id } = matchingFistar[0].matching_status;
      let { stt_id, fistar_status } = label;
      console.log(matchingFistar)
      if (!matchingFistar || !matchingFistar[0]) {
          return null;
      }
    matching = matchingFistar && matchingFistar.length > 0 ? matchingFistar[0] : matching[0];

    console.log(matching)
    return (
      <div className="process-myfistar">
        <span className="icon-start" />
        {matching.matching_status.label.fistar_status
          .split("→")
          .map((status, key) => {
              if (matching.matching_status.label.fistar_status.split("→").length === key + 1) {
                  return (
                      <Fragment key={key}>
                          <p className="font-weight-bold color-bold-blue">{status}</p>
                      </Fragment>
                  );
              }
            return (
              <Fragment key={`${key}`}>
                <p className="text">{status}</p>
                {matching.matching_status && matching.matching_status.label && matching.matching_status.label.fistar_status.split("→")
                  .length -
                  1 !==
                  key && <span />}
              </Fragment>
            );
          })}
          {!FISTER_FINISH_STEP.includes(stt_id) && this.state.campaigns.cp_status !== CAMPAIGN_STATUS.CLOSED && (
              <i className="fas fa-long-arrow-alt-right" />
          )}
        {/*<i className="fas fa-long-arrow-alt-right" />*/}
      </div>
    );
  };

  renderCampaign = () => {
    const { campaigns } = this.state;
    const { t } = this.props;
    console.log(campaigns)
    return (campaigns.slice(0, 5) || []).map((campaign, key) => {
      let status = "";
      let totalFistar = 0;
      switch (campaign.cp_status) {
        case CAMPAIGN_STATUS.MATCHING: {
          status = `${t("DASHBOARD_FISTAR.DFR_status_matching")}`;
          // totalFistar = campaign.length;
            totalFistar = campaign.statitics.all
          break;
        }
        case CAMPAIGN_STATUS.READY: {
          status = `${t("DASHBOARD_FISTAR.DFR_status_ready")}`;
          totalFistar = campaign.statitics.matched
          break;
        }
        case CAMPAIGN_STATUS.ONGOING: {
          status = `${t("DASHBOARD_FISTAR.DFR_status_ongoing")}`;
            totalFistar = campaign.statitics.matched
          break;
        }
        case CAMPAIGN_STATUS.CLOSED: {
          status = `${t("CAMPAIGN.CAN_close")}`;
            totalFistar = campaign.statitics.matched
          break;
        }
        default: {
          break;
        }
      }
      let matching = [];
      console.log(campaign.matchings )
      let applyMarched = (campaign.matchings || []).map(fistar => {
        let status = fistar.matching_status.m_status;

        matching.push(fistar);
        if (MATCHING_STATUS.MARCHED.includes(status)) {
          return fistar;
        }
        return null;
      });
      applyMarched = applyMarched.filter(e => e !== null);
      console.log(applyMarched)
      let dataFistar = "";
      if (campaign.cp_status == 59) {
        dataFistar = campaign.matchings;
      } else {
        dataFistar = applyMarched;
      }

      // if (campaign.cp_status === CAMPAIGN_STATUS.READY) {
      //   console.log(campaign, 'aaaaaaaaaaa');
      // }
      return (
        <Fragment key={key}>
          <div className="item wf-item-matching">
            <div className="img-matching">
              <Link to={"/fi-star/my-campaign-detail/" + campaign.cp_slug}>
                <CampaignMainImage campaign={campaign} />
              </Link>
              <Scrap
                scraps_count={campaign.scraps_count}
                is_scrap={campaign.is_scrap}
                id={campaign.cp_id}
                className="btn over-flow"
                iconClassName={"fa-heart"}
                callback={() => this.props.callback()}
              />
              <span className="overlay-matching">{status}</span>
            </div>
            <div className="title">
              <Link to={"/fi-star/my-campaign-detail/" + campaign.cp_slug}>
                <h4>{campaign.cp_name}</h4>
              </Link>
              {/*<p><a href="#">{campaign.cp_period_end.split(' ')[0]} Open</a></p>*/}
            </div>
            {campaign.cp_status === CAMPAIGN_STATUS.MATCHING &&
              this.renderStatus(campaign.matchings)}

            <div className="footer-card">
              <div className="list-item">
                <div className="item">
                  <p>
                    <span>{campaign.cp_total_influencer}</span>
                  </p>
                  <span>
                    <a href="javascript:void(0)">
                      {t("DASHBOARD_FISTAR.DFR_recriuting")}
                    </a>
                  </span>
                </div>
                <div className="item">
                  <p>
                    <span>{totalFistar} </span>
                  </p>
                  <span>
                    <a href="javascript:void(0)">
                      {t("DASHBOARD_FISTAR.DFR_apply")}
                    </a>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </Fragment>
      );
    });
  };

  render() {
    const { t, height, count } = this.props;
    const {
      campaigns,
      isLoadingCampaign,
      matching,
      ready,
      ongoing,
      closed,
      total,
      isRedirectMyCamPaign
    } = this.state;
    const {auth} = this.props;
    console.log(auth);
    if (isRedirectMyCamPaign) {
      return <Redirect to={routeName.FISTAR_MY_CAMPAIGN} />;
    }

    return (
      <Fragment>
        <div className="header-track">
          <div className="top">
            <h4>fistar {auth.name}'s {t("DASHBOARD_FISTAR.DFR_campaign")}</h4>
            {!(campaigns.length === 0 && !isLoadingCampaign) && (
              <button onClick={this.clickMore}>
                {t("DASHBOARD_FISTAR.DFR_button_more")}
              </button>
            )}
          </div>
          <div className="amount-notification">
            <h2>
              {count
                ? count.matching + count.ready + count.ongoing + count.closed
                : ""}
            </h2>
          </div>
          <div className="list-amount">
            <div className="item">
              <p>
                <span>{count ? count.matching : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DFR_status_matching")}</span>
            </div>
            <div className="item">
              <p>
                <span>{count ? count.ready : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DFR_status_ready")}</span>
            </div>
            <div className="item">
              <p>
                <span>{count ? count.ongoing : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DFR_status_ongoing")}</span>
            </div>
            <div className="item closed">
              <p>
                <span>{count ? count.closed : ""}</span>
              </p>
              <span>{t("DASHBOARD_FISTAR.DFR_status_close")}</span>
            </div>
          </div>
        </div>
        <div className="">
          {campaigns.length === 0 && !isLoadingCampaign ? (
            <Fragment>
              <div className={"text-center mt-5"}>No campaign</div>
            </Fragment>
          ) : (
            <InfiniteScroll
              dataLength={this.state.campaigns.length}
              next={this.fetchMoreData}
              loader={<h4>{t("LOADING.LOADING")}</h4>}
              hasMore={this.state.hasMore}
              height={height}
              className="card-matching"
            >
              {this.renderCampaign()}
            </InfiniteScroll>
          )}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth:state.auth
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getMyCampaignDashBoard: (page = 1) =>
      dispatch(getMyCampaignDashBoardAction(page))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarMyCampaign));
