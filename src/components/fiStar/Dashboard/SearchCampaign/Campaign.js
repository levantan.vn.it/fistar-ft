import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import imgaeIdol from "./../../../../images/dash-02.png";
import imageFace from "./../../../../images/dashboard/face.png";
import imageIntar from "./../../../../images/dashboard/intar.png";
import imageToutube from "./../../../../images/toutube.png";
import imageRead from "./../../../../images/mk.png";

class Campaign extends Component {
  render() {
    const { t } = this.props;
    return (
      <div className="list-campaign-track">
        <div className="title">
          <h4>{t("MY_CAMPAIGN.MCN_tab_allcampaign")} 8</h4>
        </div>
        <div className="box-item">
          <div className="item">
            <div className="img-matching">
              <a href="/fi-star/my-campaign-detail">
                <img
                  src="https://images.unsplash.com/photo-1464863979621-258859e62245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                  alt=""
                />
              </a>
              <div className="btn over-flow">
                <i className="fas fa-heart" />
                <span>1,300</span>
              </div>
            </div>
            <div className="status-campaign">
              <div className="name">
                <div className="left">
                  <span className="wrap-matching">Matching</span>
                  <h4>Etude House</h4>
                </div>
                <p>
                  <a href="">
                    Open
                    <span>07/21</span>
                  </a>
                </p>
              </div>
              <div className="process-campaign">
                <span className="icon-start" />
                <p className="text">fiStar Apply</p>
                <span />
                <p className="active">Reject</p>
                <i className="fas fa-long-arrow-alt-right" />
                <button>Re-request</button>
              </div>
              <div className="fistar-apply">
                <h4>fiStar 4</h4>
                <div className="user-apply">
                  <p>
                    <span>2</span> applied
                  </p>
                  <div className="gr-user">
                    <div className="user">
                      <a href="">
                        <img
                          src="https://images.unsplash.com/photo-1464863979621-258859e62245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                          alt=""
                        />
                      </a>
                    </div>
                    <div className="user">
                      <a href="">
                        <img
                          src="https://images.unsplash.com/photo-1464863979621-258859e62245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                          alt=""
                        />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="price-matching">
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageRead} alt="" />
                    </a>
                  </div>
                  <button>Ready to Write</button>
                </div>
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageFace} alt="" />
                    </a>
                  </div>
                  <h6 className="texts text-center">-</h6>
                </div>
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageIntar} alt="" />
                    </a>
                  </div>
                  <button>Preparing</button>
                </div>
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageToutube} alt="" />
                    </a>
                  </div>
                  <button>Open Review</button>
                </div>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="img-matching">
              <a href="/fi-star/my-campaign-detail">
                <img
                  src="https://images.unsplash.com/photo-1464863979621-258859e62245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                  alt=""
                />
              </a>
              <div className="btn over-flow">
                <i className="fas fa-heart" />
                <span>1,300</span>
              </div>
            </div>
            <div className="status-campaign">
              <div className="name">
                <div className="left">
                  <span className="wrap-matching">Matching</span>
                  <h4>Etude House</h4>
                </div>
                <p>
                  <a href="">
                    Open
                    <span>07/21</span>
                  </a>
                </p>
              </div>
              <div className="process-campaign">
                <span className="icon-start" />
                <p className="text">fiStar Apply</p>
                <span />
                <p className="active">Reject</p>
                <i className="fas fa-long-arrow-alt-right" />
                <button>Re-request</button>
              </div>
              <div className="fistar-apply">
                <h4>fiStar 4</h4>
                <div className="user-apply">
                  <p>
                    <span>2</span> applied
                  </p>
                  <div className="gr-user">
                    <div className="user">
                      <a href="">
                        <img
                          src="https://images.unsplash.com/photo-1464863979621-258859e62245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                          alt=""
                        />
                      </a>
                    </div>
                    <div className="user">
                      <a href="">
                        <img
                          src="https://images.unsplash.com/photo-1464863979621-258859e62245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                          alt=""
                        />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="price-matching">
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageRead} alt="" />
                    </a>
                  </div>
                  <button>Ready to Write</button>
                </div>
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageFace} alt="" />
                    </a>
                  </div>
                  <h6 className="texts text-center">-</h6>
                </div>
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageIntar} alt="" />
                    </a>
                  </div>
                  <button>Preparing</button>
                </div>
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageToutube} alt="" />
                    </a>
                  </div>
                  <button>Open Review</button>
                </div>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="img-matching">
              <a href="/fi-star/my-campaign-detail">
                <img
                  src="https://images.unsplash.com/photo-1464863979621-258859e62245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                  alt=""
                />
              </a>
              <div className="btn over-flow">
                <i className="fas fa-heart" />
                <span>1,300</span>
              </div>
            </div>
            <div className="status-campaign">
              <div className="name">
                <div className="left">
                  <span className="wrap-matching">Matching</span>
                  <h4>Etude House</h4>
                </div>
                <p>
                  <a href="">
                    Open
                    <span>07/21</span>
                  </a>
                </p>
              </div>
              <div className="process-campaign">
                <span className="icon-start" />
                <p className="text">fiStar Apply</p>
                <span />
                <p className="active">Reject</p>
                <i className="fas fa-long-arrow-alt-right" />
                <button>Re-request</button>
              </div>
              <div className="fistar-apply">
                <h4>fiStar 4</h4>
                <div className="user-apply">
                  <p>
                    <span>2</span> applied
                  </p>
                  <div className="gr-user">
                    <div className="user">
                      <a href="">
                        <img
                          src="https://images.unsplash.com/photo-1464863979621-258859e62245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                          alt=""
                        />
                      </a>
                    </div>
                    <div className="user">
                      <a href="">
                        <img
                          src="https://images.unsplash.com/photo-1464863979621-258859e62245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                          alt=""
                        />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="price-matching">
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageRead} alt="" />
                    </a>
                  </div>
                  <button>Ready to Write</button>
                </div>
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageFace} alt="" />
                    </a>
                  </div>
                  <h6 className="texts text-center">-</h6>
                </div>
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageIntar} alt="" />
                    </a>
                  </div>
                  <button>Preparing</button>
                </div>
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageToutube} alt="" />
                    </a>
                  </div>
                  <button>Open Review</button>
                </div>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="img-matching">
              <a href="/fi-star/my-campaign-detail">
                <img src={imgaeIdol} alt="" />
              </a>
              <div className="btn over-flow">
                <i className="fas fa-heart" />
                <span>1,300</span>
              </div>
            </div>
            <div className="status-campaign">
              <div className="name">
                <div className="left">
                  <span className="wrap-matching">Matching</span>
                  <h4>Etude House</h4>
                </div>
                <p>
                  <a href="">
                    Open
                    <span>07/21</span>
                  </a>
                </p>
              </div>
              <div className="process-campaign">
                <span className="icon-start" />
                <p className="text">fiStar Apply</p>
                <span />
                <p className="active">Reject</p>
                <i className="fas fa-long-arrow-alt-right" />
                <button>Re-request</button>
              </div>
              <div className="fistar-apply">
                <h4>fiStar 4</h4>
                <div className="user-apply">
                  <p>
                    <span>2</span> applied
                  </p>
                  <div className="gr-user">
                    <div className="user">
                      <a href="">
                        <img src={imgaeIdol} alt="" />
                      </a>
                    </div>
                    <div className="user">
                      <a href="">
                        <img src={imgaeIdol} alt="" />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="price-matching">
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageIntar} alt="" />
                    </a>
                  </div>
                  <button>Ready to Write</button>
                </div>
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageFace} alt="" />
                    </a>
                  </div>
                  <h6 className="texts text-center">-</h6>
                </div>
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageIntar} alt="" />
                    </a>
                  </div>
                  <button>Preparing</button>
                </div>
                <div className="item-price">
                  <div className="icon-solical">
                    <a href="">
                      <img src={imageToutube} alt="" />
                    </a>
                  </div>
                  <button>Open Review</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(Campaign)
);
