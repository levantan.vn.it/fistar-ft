import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as routeName from "./../../../../routes/routeName";

import FiStarAuthHead from "./Head";

class FistarAuthRegisterHead extends Component {
  render() {
    const { t } = this.props;
    return (
      <div className="header-join-fistar">
        <div className="bg-color">
          <div className="row mt-0 mr-0">
            <div className="col-md-12">
              <div className="title-join-account">
                <h2 className="text-center">
                  {t("FISTAR_LOGIN.FL_fistar_account")}
                </h2>
              </div>
            </div>
            <div className="col-md-12  col-join">
              <div className="menu-join">
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                  <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                  >
                    <span className="toggler-icon">
                      <i className="fas fa-bars" />
                    </span>
                  </button>
                  <div
                    className="collapse navbar-collapse"
                    id="navbarNavAltMarkup"
                  >
                    <div className="navbar-nav">
                      <Link
                        className="nav-item nav-link "
                        to={routeName.FISTAR_LOGIN}
                      >
                        {t("FISTAR_LOGIN.FL_login")}
                        <span className="sr-only">(current)</span>
                      </Link>
                      <Link
                        className="nav-item nav-link"
                        to={routeName.FISTAR_FIND_EMAIL}
                      >
                        {t("FISTAR_LOGIN.FL_find_id")}
                      </Link>
                      <Link
                        className="nav-item nav-link"
                        to={routeName.FISTAR_FORGOT_PASSWORD}
                      >
                        {t("FISTAR_LOGIN.FL_reset_pw")}
                      </Link>
                      <Link
                        className="nav-item nav-link active"
                        to={routeName.FISTAR_REGISTER}
                      >
                        {t("FISTAR_LOGIN.FL_join")}
                      </Link>
                    </div>
                  </div>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(FistarAuthRegisterHead)
);
