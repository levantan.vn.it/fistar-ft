import React, { Component } from 'react';
import { withTranslation, Trans } from 'react-i18next';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import FistarAuthRegisterHead from './Head'
import BackToTop from './../../../partial/BackToTop'

class FistarAuthRegisterWrapper extends Component {
  render() {
    const { t } = this.props;
    return (
      <main className="main-container">
        <section className="form-join-account">
          <FistarAuthRegisterHead />
          <div className="body-form">
            <div className="container-form-join">
              {this.props.children}
            </div>
          </div>
        </section>
        <BackToTop />
      </main>
    );
  }
}

function mapStateToProps (state) {
  return {}
}

export default connect(mapStateToProps)(withTranslation('translations')(FistarAuthRegisterWrapper))
