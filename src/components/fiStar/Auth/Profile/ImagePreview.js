import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  loginAction,
  getCodeAction,
  joinAction
} from "./../../../../store/actions/auth";

class FistarProfileImagePreview extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      previewImage: "",
      imageAlt: ""
    };
  }

  onClickFindImage = () => {
    this.inputImage.click();
  };

  handleChangeFile = e => {
    let files = e.target.files;
    let file = files[0];
    this.setState(
      {
        previewImage: URL.createObjectURL(file)
      },
      () => {
        console.log(this.state, "dasd");
        this.props.onChangeImage(file);
      }
    );
  };

  render() {
    const { t, imageSrc, imageAlt } = this.props;
    const { previewImage } = this.state;

    return (
      <Fragment>
        <img src={previewImage ? previewImage : imageSrc} alt={imageAlt} />
        <input
          type="file"
          id="selectedFile"
          name="image"
          accept="image/gif,image/jpeg,image/png"
          onChange={this.handleChangeFile}
          onFocus={this.onFocusInput}
          ref={input => (this.inputImage = input)}
        />
        <button className="icon-add" onClick={this.onClickFindImage}>
          <i className="fas fa-camera" />
        </button>
      </Fragment>
    );
  }
}
export default withTranslation("translations")(FistarProfileImagePreview);
