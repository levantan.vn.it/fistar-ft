import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import * as routeName from "./../../../routes/routeName";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";

class FiStarAuthHead extends Component {
  render() {
    const { t } = this.props;
    return (
      <Fragment>
        <div className="header-form">
          <div className="logo">
            <h1>
              <Link to="/">{t("FISTAR_LOGIN.FL_fistar_account")}</Link>
            </h1>
          </div>
          <Navbar bg="light" expand="lg" className=" navbar-light bg-light">
            <Navbar.Toggle aria-controls="basic-navbar-nav">
              <span className="toggler-icon">
                <i className="fas fa-bars" />
              </span>
            </Navbar.Toggle>
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav>
                <NavLink
                  className="nav-item nav-link"
                  to={routeName.FISTAR_LOGIN}
                  activeClassName="active"
                >
                  {t("FISTAR_LOGIN.FL_login")}
                </NavLink>
                <NavLink
                  className="nav-item nav-link"
                  to={routeName.FISTAR_FIND_EMAIL}
                  activeClassName="active"
                >
                  {t("FISTAR_LOGIN.FL_find_id")}
                </NavLink>
                <NavLink
                  className="nav-item nav-link"
                  to={routeName.FISTAR_FORGOT_PASSWORD}
                  activeClassName="active"
                >
                  {t("FISTAR_LOGIN.FL_reset_pw")}
                </NavLink>
                <NavLink
                  className="nav-item nav-link"
                  to={routeName.FISTAR_JOIN}
                  activeClassName="active"
                >
                  {t("FISTAR_LOGIN.FL_join")}
                </NavLink>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          {/*<nav className="navbar navbar-expand-lg navbar-light bg-light">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span className="toggler-icon"><i className="fas fa-bars"></i></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav">
                <NavLink className="nav-item nav-link" to={routeName.FISTAR_LOGIN} activeClassName="active">Login</NavLink>
                <NavLink className="nav-item nav-link" to={routeName.FISTAR_FIND_EMAIL} activeClassName="active">Find ID</NavLink>
                <NavLink className="nav-item nav-link" to={routeName.FISTAR_FORGOT_PASSWORD} activeClassName="active">Reset PW</NavLink>
                <NavLink className="nav-item nav-link" to={routeName.FISTAR_JOIN} activeClassName="active">Join</NavLink>
              </div>
            </div>
          </nav>*/}
        </div>
      </Fragment>
    );
  }
  activeClassName = "active";
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(FiStarAuthHead)
);
