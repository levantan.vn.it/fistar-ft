import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import imageLoginFooter from "./../../../images/card-01.png";
import * as routeName from "./../../../routes/routeName";

class FiStarAuthFooter extends Component {
  render() {
    const { t } = this.props;
    return (
      <div className="card-content">
        <div className="row">
          <div className="col-md-5">
            <div className="card">
              <Link to={routeName.FISTAR_JOIN}>
                <img src={imageLoginFooter} alt="" />
              </Link>
            </div>
          </div>
          <div className="col-md-7 text-card">
            <div className="text">
              <p className="quoite text-center">❝</p>
              <h3 className="text-center mb-2" style={{ fontSize: "27px" }}>
                {t("FISTAR_LOGIN.FL_text01_content")}
              </h3>
              <p className="text-center">
                {t("FISTAR_LOGIN.FL_text02_content")}
              </p>
              <p className="quoite text-center">❠</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(FiStarAuthFooter)
);
