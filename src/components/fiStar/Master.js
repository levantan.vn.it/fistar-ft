import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Header from "./../partial/Header";
import Footer from "./../partial/Footer.js";
import "./master.scss";
import PushMessageComponent from "./../PushMessage";

import BackToTop from "./../partial/BackToTop";

class FiStarMaster extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    const { t, auth } = this.props;
    return (
      <div className="wrapper fistar-page">
        <Header />

        {this.props.children}
        <BackToTop />
        <Footer />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

export default connect(mapStateToProps)(FiStarMaster);
