import { SNS_CHANNEL } from "../constants";

if (typeof window !== 'undefined' && window.document && window.document.createElement) {

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

  function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

  var File = window.File;

  try {
    new File([], '');
  } catch (e) {
    File = function (_Blob) {
      _inherits(File, _Blob);

      function File(chunks, filename) {
        var opts = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

        _classCallCheck(this, File);

        var _this = _possibleConstructorReturn(this, _Blob.call(this, chunks, opts));

        _this.lastModifiedDate = new Date();
        _this.lastModified = +_this.lastModifiedDate;
        _this.name = filename;
        return _this;
      }

      return File;
    }(Blob);
  }
}

export function nFormatter(num, digits) {
  var si = [
    { value: 1, symbol: "" },
    { value: 1e3, symbol: "k" },
    { value: 1e6, symbol: "M" },
    { value: 1e9, symbol: "G" },
    { value: 1e12, symbol: "T" },
    { value: 1e15, symbol: "P" },
    { value: 1e18, symbol: "E" }
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
}

export function convertVi(alias) {
  var str = alias;
  str = str.toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
  str = str.replace(/Đ/g, "D");
  str = str.replace(
    /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,
    " "
  );
  str = str.replace(/ + /g, " ");

  str = str.trim();

  return str;
}

export function calcAge(dateString) {
  var birthday = +new Date(dateString);
  return ~~((Date.now() - birthday) / 31557600000);
}
export function DateFormatYMDDatePicker(dateString, t = "-") {
  dateString = dateString.replace(/-/g, "/")
  var date = new Date(dateString);
  let year = date.getFullYear();
  let month = date.getMonth(); //months from 1-12
  let day = date.getDate();
  day = day < 10 ? "0" + day : day;
  month = month < 10 ? "0" + month : month;

  return year + t + month + t + day;
}
export function DateFormatYMDDatePickerPlus7Day(dateString, t = "-") {
   dateString = dateString.replace(/-/g, "/")
  var date = new Date(dateString);
  let year = date.getFullYear();
  let month = date.getMonth(); //months from 1-12
  let day = date.getDate();
  day = day + 7
  day = day < 10 ? "0" + day : day;

  month = month < 10 ? "0" + month : month;

  return year + t + month + t + day;
}
export function DateFormatYMD(dateString, t = '/') {
  dateString = dateString.replace(/-/g, "/")
  var date = new Date(dateString);
  let year = date.getFullYear();
  let month = ("0" + (date.getMonth() + 1)).slice(-2); //months from 1-12
  let day = ("0" + (date.getDate() + 1)).slice(-2);
  console.log(dateString, year + t + month + t + day);
  return year + t + month + t + day;
}

// function checkImageExists(imageUrl, callBack) {
//     var imageData = new Image();
//     imageData.onload = function() {
//         callBack(true);
//     };
//     imageData.onerror = function() {
//         callBack(false);
//     };
//     imageData.src = imageUrl;
// }

export function getImageLink(imageName, folder, size) {
  let imageDefault = `https://x.kinja-static.com/assets/images/logos/placeholders/default.png`;
  if (!imageName || typeof imageName !== "string") {
    return imageDefault;
  }
  if (imageName.includes("http")) {
    return imageName;
  }
    let urlImage=""
  if(process.env.HTTP_SECURE == true) {
       urlImage = process.env.REACT_APP_BASE.replace("https", "http") + "/storage/" + folder + "/";
  }else {
       urlImage = process.env.REACT_APP_BASE + "/storage/" + folder + "/";
  }

  if (size) {
    urlImage += size + "/";
  }
  urlImage += imageName;
  // return checkImageExists(urlImage, function(existsImage) {
  //   if(existsImage == true) {
  return urlImage;
  //   }
  //   else {
  //     return `https://x.kinja-static.com/assets/images/logos/placeholders/default.png`;
  //   }
  // });
  // var image = new Image();
  // var urlImage = process.env.REACT_APP_BASE + '/storage/' + folder + '/' + size + '/' + imageName
  // image.src = urlImage;
  // console.log(image, 'image');
  // if (image.width == 0) {
  //   return `https://x.kinja-static.com/assets/images/logos/placeholders/default.png`;
  // } else {
  //   return urlImage;
  // }
}

export function getChannelFollower(channels, channel_type) {
  let follower =
    channels[channel_type] && channels[channel_type].usn_follower
      ? channels[channel_type].usn_follower
      : 0;
  return nFormatter(follower, 0);
}

export function getId(url) {
  if (typeof url !== "string") {
    return null;
  }
  var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
  var match = url.match(regExp);

  if (match && match[2].length == 11) {
    return match[2];
  } else {
    return "error";
  }
}

export function convertLocationSearchToObject(search = '') {
  if (!search) {
    return {}
  }
  search = search.substring(1);
  search = JSON.parse('{"' 
    + decodeURI(search)
    .replace(/"/g, '\\"')
    .replace(/&/g, '","')
    .replace(/=\./g, 'vvvvvvvvvvvvvvvv.')
    .replace(/==/g, '-------------')
    .replace(/===/g, '------------------------')
    .replace(/=/g,'":"') 
    .replace(/vvvvvvvvvvvvvvvv./g,'=.') 
    .replace(/-------------/g,'==') 
    .replace(/------------------------/g,'===') 
    .replace(/ttttttttttttttt/g,'=') 
    + '"}')
  return search
}

export function convertObjectToLocationSearch(params = {}) {
  return Object.keys(params).map(key => key + '=' + params[key]).join('&');
}

export function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type:mime});
}
function buildFormData(formData, data, excepts, parentKey) {
  if (data && typeof data === 'object' && !excepts.includes(parentKey)) {
    Object.keys(data).forEach(key => {
      buildFormData(formData, data[key], excepts, parentKey ? `${parentKey}[${key}]` : key);
    });
  } else {
    const value = data == null ? '' : data;

    formData.append(parentKey, value);
  }
}

export function createFormData(data, excepts) {
  const formData = new FormData();

  buildFormData(formData, data, excepts);

  return formData;
}
// export function validateEmail(email) {
//     var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//     return re.test(String(email).toLowerCase());
// }

export const validUrl = (url, type) => {
    switch(type){
        case 'facebook': return /facebook\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
        case '2': return /facebook\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
        case 'instagram': return /instagram\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
        case '4': return /instagram\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
        case 'youtube': return /youtube\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
        case '3': return /youtube\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
        default: return /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm.test(url);
    }
}

export const getLastNestedObject = (object, name) => {
  if (!object[name]){
    return object;
  }
  return getLastNestedObject(object[name], name)
}
