import { actionTypes } from "./actionTypes";
import api from "../../api";
import Cookies from "js-cookie";
import { setAuthorizationHeader } from "./../../api";

export const getProfileLoggedInAction = () => dispatch => {
  let token = Cookies.get("token");
  if (!token) {
    dispatch(logoutSuccess());
    return Promise.reject(false);
  }
  let type = Cookies.get("type");
  let name = null;
  let avatar = null;
  let pc_name = null;
  let email = null;
  let phone = null;
  let id = null;
  let first_login = null;
  if (!!type) {
    if (type === "fistar") {
      return api.fiStar.auth
        .getProfile()
        .then(response => {
          let status = response.data.data.status;
          if (status) {
            let influencer = status.influencer;
            name = influencer.name;
            avatar = influencer.avatar;
            pc_name = influencer.id;
            email = influencer.email;
            phone = influencer.phone;
            first_login = influencer.first_login;
          }
          dispatch(
            loginSuccess(
              token,
              null,
              type,
              name,
              avatar,
              pc_name,
              email,
              phone,
              first_login
            )
          );
          return Promise.resolve(response.data);
        })
        .catch(e => {
          dispatch(logoutSuccess());
          return Promise.reject(e.response);
        });
    } else {
      return api.partner.auth
        .inforProfile()
        .then(response => {
          console.log(response);
          let status = response.data.partner;
          if (status) {
            let partner = status;
            name = partner.pm_name;
            avatar = partner.p_image;
            pc_name = partner.pc_name;
            email = partner.email;
            phone = partner.pc_phone;
            id = partner.pid;
            first_login = partner.first_login;
          }
          dispatch(
            loginSuccess(
              token,
              null,
              type,
              name,
              avatar,
              pc_name,
              email,
              phone,
              id,
              first_login
            )
          );
          return Promise.resolve(status);
        })
        .catch(e => {
          dispatch(logoutSuccess());
          return Promise.reject(e.response);
        });
    }
  } else {
    return api.fiStar.auth
      .getProfile()
      .then(response => {
        let status = response.data.data.status;
        if (status) {
          let influencer = status.influencer;
          name = influencer.name;
          avatar = influencer.avatar;
          pc_name = influencer.id;
          email = influencer.email;
          phone = influencer.phone;
          first_login = influencer.first_login;
        }
        dispatch(
          loginSuccess(
            token,
            null,
            type,
            name,
            avatar,
            pc_name,
            email,
            phone,
            first_login
          )
        );
        return Promise.resolve(response.data);
      })
      .catch(e => {
        return api.partner.auth
          .inforProfile()
          .then(response => {
            console.log(response);
            let status = response.data.partner;
            if (status) {
              let partner = status;
              name = partner.pm_name;
              avatar = partner.p_image;
              pc_name = partner.pc_name;
              email = partner.email;
              phone = partner.pc_phone;
              id = partner.pid;
              first_login = partner.first_login;
            }
            dispatch(
              loginSuccess(
                token,
                null,
                type,
                name,
                avatar,
                pc_name,
                email,
                phone,
                id,
                first_login
              )
            );
            return Promise.resolve(status);
          })
          .catch(e => {
            dispatch(logoutSuccess());
            return Promise.reject(e.response);
          });
      });
  }
};

export const logoutSuccess = () => {
  Cookies.remove("token");
  Cookies.remove("type");
  return {
    type: actionTypes.LOGOUT_SUCCESS
  };
};

export const logoutAction = () => dispatch => {
  dispatch(logoutSuccess());
};

export const loginSuccess = (
  token,
  user,
  type,
  name = null,
  avatar = null,
  pc_name = null,
  email = null,
  phone = null,
  id = null,
  first_login = null
) => {
  Cookies.set("token", token);
  Cookies.set("type", type);
  setAuthorizationHeader(token);
  return {
    type: actionTypes.LOGIN_SUCCESS,
    payload: {
      token: token,
      user: user,
      name: name,
      avatar: avatar,
      pc_name: pc_name,
      email: email,
      phone: phone,
      id: id,
      first_login: first_login,
      type: type
    }
  };
};

export const loginAction = data => dispatch => {
  return api.auth
    .login(data)
    .then(response => {
      console.log(response);
      const { access_token, data: user } = response.data;
      // if (!!user.sns_token_facebook) {
        dispatch(
          loginSuccess(
            access_token,
            user,
            "fistar",
            user.name,
            user.avatar,
            user.id,
            user.email,
            user.phone,
            user.id,
            user.first_login
          )
        );
      // }

      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const createAccessTokenFBAction = (
  uid,
  accessToken,
  isRegister,
  isNew
) => dispatch => {
  return api.auth
    .createAccessTokenFB(uid, accessToken, 2, isRegister, isNew)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const dispatchLoginSuccessAction = data => dispatch => {
  console.log(data);
  const { access_token, data: user } = data;
  dispatch(
    loginSuccess(
      access_token,
      user,
      "fistar",
      user.name,
      user.avatar,
      user.id,
      user.email,
      user.phone,
      user.id,
      user.first_login
    )
  );
};

export const loginFacebookAction = data => dispatch => {
  return api.fiStar.auth
    .loginFacebook(data)
    .then(response => {
      const { access_token, data: user } = response.data;
      dispatch(
        loginSuccess(
          access_token,
          user,
          "fistar",
          user.name,
          user.avatar,
          user.id,
          user.email,
          user.phone,
          user.id,
          user.first_login
        )
      );
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const findEmailAction = data => dispatch => {
  return api.fiStar.auth
    .findEmail(data)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const forgotPasswordAction = data => dispatch => {
  return api.fiStar.auth
    .forgotPassword(data)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const resetPasswordAction = data => dispatch => {
  return api.fiStar.auth
    .resetPassword(data)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const changePasswordAction = data => dispatch => {
  return api.fiStar.auth
    .changePassword(data)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const joinInfo = (
  data,
  type,
  isChangeID = true,
  isChangeEmail = true,
  isNewPass = false,
  isSkipStep = false
) => ({
  type: actionTypes.REGISTER,
  payload: {
    data: data,
    isChangeID: isChangeID,
    isChangeEmail: isChangeEmail,
    isNewPass: isNewPass,
    isSkipStep: isSkipStep,
    type: type
  }
});
export const joinAction = (
  data,
  type,
  isChangeID = true,
  isChangeEmail = true,
  isNewPass = false,
  isSkipStep = false
) => dispatch => {
  dispatch(
    joinInfo(data, type, isChangeID, isChangeEmail, isNewPass, isSkipStep)
  );
};

export const checkExistAction = (name, value) => dispatch => {
  return api.fiStar.auth
    .checkExist({ name, value })
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const fistarJoinAction = data => dispatch => {
  return api.fiStar.auth
    .fistarJoin(data)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

// getCodeSuccess
export const getCodeSuccess = (type, data) => ({
  type: actionTypes.FETCH_CODE_SUCCESS,
  payload: {
    data: data,
    type: type
  }
});
export const getCodeAction = type => dispatch => {
  return api.code
    .getCode(type)
    .then(response => {
      dispatch(getCodeSuccess(type, response.data));
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const registerAction = data => dispatch => {
  return api.fiStar.auth
    .register(data)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

// profile

export const getProfileFistarAction = () => dispatch => {
  return api.fiStar.auth
    .getProfile()
    .then(response => {
      dispatch(
        loginSuccess(Cookies.get("token"), response.data.data.status, "fistar")
      );
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const updateProfile = (
  // token,
  user,
  type,
  name = null,
  avatar = null,
  pc_name = null,
  email = null,
  phone = null,
  id = null,
  first_login = null
) => {
  return {
    type: actionTypes.UPDATE_PROFILE,
    payload: {
      // token: token,
      user: user,
      name: name,
      avatar: avatar,
      pc_name: pc_name,
      email: email,
      phone: phone,
      id: id,
      first_login: first_login,
      type: type
    }
  };
};

export const updateProfileFistarAction = (type, data) => dispatch => {
  console.log(data);
  if (type === "general") {
    return api.fiStar.auth
      .updateProfileGeneral(data)
      .then(response => {
        console.log(response);

        console.log(response.data.data.data);
        const { data } = response.data.data;

        dispatch(
          updateProfile(
            // access_token,
            response.data.data.data,
            "fistar",
            response.data.data.data.fullname,
            response.data.data.data.picture,
            response.data.data.data.email,
            response.data.data.data.phone,
            response.data.data.data.id,
            response.data.data.data.first_login
          )
        );

        return Promise.resolve(response.data);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }
  if (type === "information") {
    return api.fiStar.auth
      .updateProfileInformation(data)
      .then(response => {
        return Promise.resolve(response.data);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }
  if (type === "password") {
    return api.fiStar.auth
      .updateProfilePassword(data)
      .then(response => {
        return Promise.resolve(response.data);
      })
      .catch(e => {
        return Promise.reject(e.response);
      });
  }
};

//qa
export const getQAFistarAction = (
  page = 1,
  type = "",
  search = "",
  state = ""
) => dispatch => {
  return api.fiStar
    .getQa(page, type, search, state)
    .then(response => {
      console.log(response);
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};
export const getQADetailAction = id => dispatch => {
  return api.fiStar
    .getQaDetail(id)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};
export const createQAAction = data => dispatch => {
  return api.fiStar
    .createQa(data)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

//partner

export const loginActionPartner = data => dispatch => {
  return api.partner.auth
    .login(data)
    .then(response => {
      // if (localStorage.getItem('token')) {
      //   localStorage.removeItem('token');
      // }
      // setAuthorizationHeader(response.data.token)
      // localStorage.setItem('token', response.data.token);
      const { access_token, user } = response.data;
      dispatch(
        loginSuccess(
          access_token,
          user,
          "partner",
          user.pm_name,
          user.avatar,
          user.pc_name,
          user.email,
          user.pc_phone,
          user.pid,
          user.first_login
        )
      );
      return Promise.resolve(response.data);
    })
    .catch(e => {
      console.log(e.response);
      return Promise.reject(e.response);
    });
};
export const findEmailActionPartner = data => dispatch => {
  return api.partner.auth
    .findEmail(data)
    .then(response => {
      // if (localStorage.getItem('token')) {
      //   localStorage.removeItem('token');
      // }
      // setAuthorizationHeader(response.data.token)
      // localStorage.setItem('token', response.data.token);
      // dispatch(loginSuccess(response.data.access_token, response.data.user))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};
export const forgotPasswordActionPartner = data => dispatch => {
  return api.partner.auth
    .forgotPassword(data)
    .then(response => {
      // if (localStorage.getItem('token')) {
      //   localStorage.removeItem('token');
      // }
      // setAuthorizationHeader(response.data.token)
      // localStorage.setItem('token', response.data.token);
      // dispatch(loginSuccess(response.data.access_token, response.data.user))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const resetPasswordActionPartner = data => dispatch => {
  return api.partner.auth
    .resetPassword(data)
    .then(response => {
      // if (localStorage.getItem('token')) {
      //   localStorage.removeItem('token');
      // }
      // setAuthorizationHeader(response.data.token)
      // localStorage.setItem('token', response.data.token);
      // dispatch(loginSuccess(response.data.access_token, response.data.user))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const changePasswordActionPartner = data => dispatch => {
  return api.partner.auth
    .changePassword(data)
    .then(response => {
      // if (localStorage.getItem('token')) {
      //   localStorage.removeItem('token');
      // }
      // setAuthorizationHeader(response.data.token)
      // localStorage.setItem('token', response.data.token);
      // dispatch(loginSuccess(response.data.access_token, response.data.user))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const registerActionPartner = data => dispatch => {
  return api.partner.auth
    .register(data)
    .then(response => {
      // if (localStorage.getItem('token')) {
      //   localStorage.removeItem('token');
      // }
      // setAuthorizationHeader(response.data.token)
      // localStorage.setItem('token', response.data.token);
      // dispatch(loginSuccess(response.data.access_token, response.data.user))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const checkExistActionPartner = (name, value) => dispatch => {
  return api.partner.auth
    .checkExist({ name, value })
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const createCampainActionPartner = data => dispatch => {
  return api.partner.auth
    .createCampain(data)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const updateCampainActionPartner = (cp_id, data) => dispatch => {
  return api.partner.auth
    .updateCampain(cp_id, data)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      console.log(e, "rrrrrrrrrr");
      return Promise.reject(e.response);
    });
};

export const listProfileActionPartner = data => dispatch => {
  return api.partner.auth
    .inforProfile(data)
    .then(response => {
      let profile = response.data.partner;
      profile.channels = response.data.channel;
      profile.keywords = response.data.keyword;
      return Promise.resolve(profile);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const updateManagerInformationActionPartner = (
  pid,
  data
) => dispatch => {
  return api.partner.auth
    .updateManager(pid, data)
    .then(response => {
      console.log(response.data.status);
      const { data } = response.data.status;

      dispatch(
        updateProfile(
          // access_token,
          response.data.status,
          "partner",
          response.data.status.pm_name,
          response.data.status.p_image,
          response.data.status.email,
          response.data.status.pm_phone,
          response.data.status.pid,
          response.data.status.first_login
        )
      );

      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const changePasswordProfileActionPartner = (pid, data) => dispatch => {
  return api.partner.auth
    .changePasswowrdProfile(pid, data)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const updateCompanyInfomationActionPartner = (pid, data) => dispatch => {
  return api.partner.auth
    .updateCompanyInformation(pid, data)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

const clearStateRegister = () => {
  return {
    type: "CLEAR_STATE_REGISTER",
    payload: []
  };
};
export const clearRegisterInfo = () => dispatch => {
  dispatch(clearStateRegister());
};

export const FaqMainMenu = (type, page = 1) => dispatch => {
  return api.mainMenu
    .faq(type, page)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const FaqFistarMainMenu = (page = 1) => dispatch => {
  return api.mainMenu
    .faqFistar(page)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const FaqPartnerMainMenu = (page = 1) => dispatch => {
  return api.mainMenu
    .faqPartner(page)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

//front campaign

export const getCampaignFrontAction = (page = 1, status = "") => dispatch => {
  return api.front
    .getMyCampaignFront(page, status)
    .then(response => {
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getCampaignAction = (
  page = 1,
  type = "",
  status = []
) => dispatch => {
  return api.front
    .getCampaign(page, type, status)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};
//front fistar
export const getFistarAction = (
  page = 1,
  sort = "",
  order = "",
  state = "",
  active = ""
) => dispatch => {
  return api.front
    .getFistar(page, sort, order, state, active)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

//front banner
export const getBannerAction = by_period => dispatch => {
  return api.mainMenu
    .banners(by_period)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

//front countMainMenu

export const getCountMainMenuAction = () => dispatch => {
  return api.mainMenu
    .getCounMainMenu()
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

//front Fistar Campaign

export const getDataFistarCampaign = () => dispatch => {
  return api.front
    .getDataFistarCampaign()
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};


//checkSnsVerify

export const checkSnsVerify = (data) => dispatch => {
    return api.checkSnsVerify
        .verifySns(data)
        .then(response => {
            return Promise.resolve(response.data);
        })
        .catch(e => {
            return Promise.reject(e.response);
        });
};

//getBrands

export const getBrand = () => dispatch => {
    return api.brands
        .getBrand()
        .then(response => {
            return Promise.resolve(response.data);
        })
        .catch(e => {
            return Promise.reject(e.response);
        });
};