import { actionTypes } from "./actionTypes";
import api from "../../api";
// import setAuthorizationHeader from './../../utils/setAuthorizationHeader'

export const getCodeSuccess = (type, data) => ({
  type: actionTypes.FETCH_CODE_SUCCESS,
  payload: {
    data: data,
    type: type
  }
});
export const searchFistarAction = (search, page, state, active) => dispatch => {
  return api.fiStar
    .searchFistar(search, page, state, active)
    .then(response => {
      //   dispatch(getCodeSuccess(type, response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};
export const getFistarAction = (uid, search = "") => dispatch => {
  return api.fiStar
    .getFistar(uid, search)
    .then(response => {
      //   dispatch(getCodeSuccess(type, response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};
export const getHistoryAction = uid => dispatch => {
  return api.fiStar
    .getHistory(uid)
    .then(response => {
      //   dispatch(getCodeSuccess(type, response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};
