import { actionTypes } from "./actionTypes";
import api from "../../api";
// import setAuthorizationHeader from './../../utils/setAuthorizationHeader'

// export const getCampaignSuccess = (data) => ({
//   type: actionTypes.FETCH_MESSAGE_SUCCESS,
//   payload: {
//     data: data,
//   }
// })
export const getCampaignAction = (
  page = 1,
  type = "",
  is_relate = "",
  fime_like = "",
) => dispatch => {
  return api.campaign
    .getCampaign(page, type, is_relate, fime_like)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

// export const getFistarAction = () => dispatch => {
//   return api.campaign
//     .getFistar()
//     .then(response => {
//       // dispatch(getPushMessageSuccess(response.data))
//       return Promise.resolve(response.data);
//     })
//     .catch(e => {
//       return Promise.reject(e.response);
//     });

export const countScrapAction = (uid, cp_id) => dispatch => {
  return api.campaign
    .countScrap(uid, cp_id)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getMyFistarAction = (page = 1, filter = "", pid) => dispatch => {
  return api.campaign
    .myFistarPartner(page, filter, pid)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getTotalStatusAction = pid => dispatch => {
  return api.campaign
    .totalStatus(pid)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getFistarAction = (page = 1, type = "") => dispatch => {
  return api.campaign
    .getFistar(page, type)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getNewFistarAction = () => dispatch => {
  return api.fiStar
    .getNewFistar()
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getQAAction = (page = "", type = "", state = "") => dispatch => {
  console.log(page, type, state);
  return api.fiStar
    .getQa(page, type, state)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getMyCampaignAction = (
  page = 1,
  type = "",
  search = "",
  campaign = "",
  keyword = ""
) => dispatch => {
  return api.campaign
    .getMyCampaign(page, type, search, campaign, keyword)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getMyCampaignDashBoardAction = (
  page = 1,
  type = "",
  search = ""
) => dispatch => {
  return api.campaign
    .getMyCampaignDashBoard(page, type, search)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getNewCampaignAction = (page = 1, type = "") => dispatch => {
  return api.campaign
    .getNewCampaign(page, type)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getCountCampaignAction = () => dispatch => {
  return api.campaign
    .getCountCampaign()
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getCampaignReviewAction = () => dispatch => {
  return api.campaign
    .getCampaignReview()
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getSearchCampaignAction = (
  page = 1,
  type = "",
  search = "",
  campaign = "",
  keyword = "",
  by_relate = ""
) => dispatch => {
  return api.campaign
    .searchCampaign(page, type, search, campaign, keyword, by_relate)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const applyNewCampaignAction = id => dispatch => {
  return api.campaign
    .applyCampaign(id)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getCampaignSuccess = data => ({
  type: actionTypes.GET_CAMPAIGN_SUCCESS,
  payload: {
    data: data
  }
});

export const getCampaignDetailFistarAction = id => dispatch => {
  console.log(123213333);
  return api.campaign
    .getCampaignDetailFistar(id)
    .then(response => {
      console.log(response);
      dispatch(getCampaignSuccess(response.data));
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getCampaignDetailAction = id => dispatch => {
  console.log(123213333);
  return api.campaign
    .getCampaignDetail(id)
    .then(response => {
      console.log(response);
      dispatch(getCampaignSuccess(response.data));
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const getFistarOfCampaignDetailAction = (id, type) => dispatch => {
  console.log(123213333);
  return api.campaign
    .getFistarOfCampaignDetail(id, type)
    .then(response => {
      console.log(response);
      dispatch(getCampaignSuccess(response.data));
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const nextStepAction = (m_id, stt_id) => dispatch => {
  return api.campaign
    .nextStep(m_id, stt_id)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const updateReviewAction = (m_id, data) => dispatch => {
  return api.campaign
    .updateReview(m_id, data)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const updateApproveReviewAdminAction = (m_id, rv_status) => dispatch => {
  return api.campaign
    .updateApproveReviewAdmin(m_id, rv_status)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const updateModifyReviewAdminAction = (m_id, rv_status) => dispatch => {
  return api.campaign
    .updateModifyReviewAdmin(m_id, rv_status)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const changeStatusAction = (id, status) => dispatch => {
  return api.campaign
    .changeStatus(id, status)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};

export const clickSelectSNSAction = (m_id, sns_id) => dispatch => {
  return api.campaign
    .clickSelectSNS(m_id, sns_id)
    .then(response => {
      // dispatch(getPushMessageSuccess(response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};
