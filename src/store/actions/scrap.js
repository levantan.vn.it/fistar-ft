import { actionTypes } from "./actionTypes";
import api from "../../api";

// import setAuthorizationHeader from './../../utils/setAuthorizationHeader'

// export const getCodeSuccess = (type, data) => ({
//   type: actionTypes.FETCH_CODE_SUCCESS,
//   payload: {
//     data: data,
//     type: type,
//   }
// })
export const scrapAction = id => dispatch => {
  return api
    .scrap(id)
    .then(response => {
      // dispatch(getCodeSuccess(type, response.data))
      return Promise.resolve(response.data);
    })
    .catch(e => {
      return Promise.reject(e.response);
    });
};
