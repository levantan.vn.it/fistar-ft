import { actionTypes } from './../actions/actionTypes'

const initialState = {
  data: {
    campaigns: {},
    fistars: {},
    notices: {},
  },
  loading: false,
}
// const count = (state = initialState, action) => {
export default function message(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_MESSAGE_CAMPAIGN_SUCCESS: {
      if (!action.payload.reset) {
        return {
          ...state,
          data: {
            ...state.data,
            campaigns: {
              ...action.payload.data,
              data: [
                ...state.data.campaigns.data,
                ...action.payload.data.data
              ]
            }
          }
        }
      }
      return {
        ...state,
        data: {
          ...state.data,
          campaigns: action.payload.data
        }
      }
    };
    case actionTypes.FETCH_MESSAGE_FISTAR_SUCCESS: {
      if (!action.payload.reset) {
        return {
          ...state,
          data: {
            ...state.data,
            fistars: {
              ...action.payload.data,
              data: [
                ...state.data.fistars.data,
                ...action.payload.data.data
              ]
            }
          }
        }
      }
      return {
        ...state,
        data: {
          ...state.data,
          fistars: action.payload.data
        }
      }
    };
    case actionTypes.FETCH_MESSAGE_NOTICE_SUCCESS: {
      if (!action.payload.reset) {
        return {
          ...state,
          data: {
            ...state.data,
            notices: {
              ...action.payload.data,
              data: [
                ...state.data.notices.data,
                ...action.payload.data.data
              ]
            }
          }
        }
      }
      return {
        ...state,
        data: {
          ...state.data,
          notices: action.payload.data
        }
      }
    };
    default:
      return state
  }
}

// export default message;
