import { actionTypes } from "./../actions/actionTypes";

const initialState = {
  data: null,
  type: null,
  error: null,
  loading: false,
  isChangeID: true,
  isChangeEmail: true
};
// const count = (state = initialState, action) => {
export default function register(state = initialState, action) {
  switch (action.type) {
    case actionTypes.REGISTER:
      {
        return {
          ...state,
          loading: false,
          data: {
            ...state.data,
            ...action.payload.data
          },
          type: action.payload.type,
          isChangeID:
            action.payload.isChangeID !== null
              ? action.payload.isChangeID
              : state.isChangeID,
          isChangeEmail:
            action.payload.isChangeEmail !== null
              ? action.payload.isChangeEmail
              : state.isChangeEmail,
          isNewPass: action.payload.isNewPass,
          isSkipStep: action.payload.isSkipStep,
        };
      }
      break;
    case "CLEAR_STATE_REGISTER": {
      return {};
    }
    default:
      return state;
  }
}

// export default register;
