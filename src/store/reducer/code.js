import { actionTypes } from './../actions/actionTypes'

const initialState = {
  data: {},
  loading: false,
}
// const count = (state = initialState, action) => {
export default function code(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_CODE_SUCCESS: {
      let { type: types } = action.payload
      types = types.split(',')
      if (types.length === 1) {
        return {
          ...state,
          data: {
            ...state.data,
            [action.payload.type]: action.payload.data
          },
        }
      }
      let { data } = state
      types.map((type) => {
        let name = type.split('_').join(' ')
        action.payload.data.map((group) => {
          if (group.cdg_name.toLowerCase().includes(name.toLowerCase())) {
            data = {
              ...data,
              [type]: group
            }
          } else if (
            group.cdg_name.toLowerCase() === 'fi:star type'
            && name.toLowerCase() === 'fistar type'
          ) {
            data = {
              ...data,
              [type]: group
            }
          }
        })
      })
      return {
        ...state,
        data,
      }
    };
    default:
      return state
  }
}

// export default code;
