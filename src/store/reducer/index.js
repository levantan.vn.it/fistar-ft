import { combineReducers } from 'redux'
import auth from './auth'
import count from './count'
import register from './register'
import code from './code'
import channel from './channel'
import message from './message'
import campaign from './campaign'

export default combineReducers({
  auth,
  count,
  register,
  code,
  channel,
  message,
  campaign,
});
