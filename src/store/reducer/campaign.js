import { actionTypes } from './../actions/actionTypes'

const initialState = {
  data: {},
  loading: false,
}
// const count = (state = initialState, action) => {
export default function campaign(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_CAMPAIGN_SUCCESS: {
      if (!action.payload.reset) {
        return {
          ...state,
          data: {
            ...state.data,
            campaigns: {
              ...action.payload.data,
              data: [
                ...state.data.campaigns.data,
                ...action.payload.data.data
              ]
            }
          }
        }
      }
      return {
        ...state,
        data: {
          ...state.data,
          campaigns: action.payload.data
        }
      }
    };
    case actionTypes.GET_CAMPAIGN_SUCCESS: {
      return {
        ...state,
        data: action.payload.data
      }
    };
    default:
      return state
  }
}

// export default campaign;
