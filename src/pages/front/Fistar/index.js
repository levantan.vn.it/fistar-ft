import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import imageIdol from "./../../../images/dash-02.png";
import { getFistarAction } from "../../../store/actions/auth";
import InfiniteScroll from "react-infinite-scroll-component";
import FistarComponent from "./../../../components/partial/front/fistar/Fistar";
import * as routeName from "../../../routes/routeName";
import PageHead from './../../../components/PageHead'

import { Navbar, Nav, NavDropdown, Dropdown } from "react-bootstrap";

const TYPE = {
  ALL: "",
  NEW: "created_at",
  NEWORDER: "desc",
  HOT: "rate_scrap",
  HOTORDER: "desc"
};

class Fistar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: TYPE.ALL,
      sort: "",
      order: "",
      fistars: [],
      total: 0,
      ready: 0,
      onGoing: 0,
      closed: 0,
      textTitle: `${this.props.t("MY_CAMPAIGN.MCN_tab_allcampaign")}`,
      last_page: 1,
      page: 1,
      hasMore: true,
      isLoadingCampaign: false,
      search: "",
      reset: true,
      stateStatus: 2,
      activeStatus: 1
    };
    this.triggerClick = React.createRef();
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      window.scrollTo(0, 0);
    }
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingCampaign: true,
        reset: true
      },
      () => {
        this.initData();
      }
    );
  }

  initData = () => {
    this.props
      .getFistar(
        this.state.page,
        this.state.sort,
        this.state.order,
        this.state.stateStatus,
        this.state.activeStatus
      )
      .then(response => {
        console.log(response);
        this.setState({
          fistars: response.data,
          total: !this.state.reset ? this.state.total : response.total,
          ready: response.ready,
          onGoing: response.ongoing,
          closed: response.closed,
          isLoadingCampaign: false,
          last_page: response.last_page,
          reset: false
        });
      })
      .catch(() => {
        this.setState({
          isLoadingCampaign: false,
          reset: false
        });
      });
  };

  clickChangeTab = (sort, order) => {
    console.log(sort, order);

    this.setState(
      {
        sort: sort,
        order: order,
        page: 1,
        hasMore: true,
        isLoadingCampaign: true
      },
      () => {
        this.triggerClick.current.click();
        this.initData();
      }
    );
  };

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }

    this.props
      .getFistar(
        this.state.page + 1,
        this.state.sort,
        this.state.order,
        this.state.stateStatus,
        this.state.activeStatus
      )
      .then(response => {
        this.setState({
          fistars: [...this.state.fistars, ...response.data],
          page: this.state.page + 1
        });
      });
  };

  render() {
    const {
      sort,
      order,
      fistars,
      total,
      ready,
      onGoing,
      closed,
      textTitle,
      hasMore,
      isLoadingCampaign,
      search,
      reset
    } = this.state;
    const { t } = this.props;
    return (
      <React.Fragment>
        <PageHead title="FiStar | Fistar" />
        <div className="form-site">
          <div className="bg-overlay">
            <div className="color-overlay">
              <div className="header-form header-mm-campaign container-mmfaq">
                <div className="logo logo-faq">
                  <h1>
                    <a href="">{t("FISTAR.FSR_fistar")}</a>
                  </h1>
                </div>

                <Navbar
                  bg="light"
                  expand="lg"
                  className=" navbar-light bg-light"
                >
                  <Navbar.Toggle
                    aria-controls="basic-navbar-nav-register"
                    ref={this.triggerClick}
                  >
                    <span className="toggler-icon">
                      <i className="fas fa-bars" />
                    </span>
                  </Navbar.Toggle>
                  <Navbar.Collapse id="basic-navbar-nav-register">
                    <Nav>
                      <div className="navbar-nav">
                        <span
                          className={`nav-item nav-link${
                            sort === TYPE.ALL && order === TYPE.ALL
                              ? " active"
                              : ""
                          }`}
                          onClick={() =>
                            this.clickChangeTab(TYPE.ALL, TYPE.ALL)
                          }
                        >
                          {t("FISTAR.FSR_all")}
                        </span>
                        <span
                          className={`nav-item nav-link${
                            sort === TYPE.NEW && order === TYPE.NEWORDER
                              ? " active"
                              : ""
                          }`}
                          onClick={() =>
                            this.clickChangeTab(TYPE.NEW, TYPE.NEWORDER)
                          }
                        >
                          {t("FISTAR.FSR_new")}
                        </span>
                        <span
                          className={`nav-item nav-link${
                            sort === TYPE.HOT && order === TYPE.HOTORDER
                              ? " active"
                              : ""
                          }`}
                          onClick={() =>
                            this.clickChangeTab(TYPE.HOT, TYPE.HOTORDER)
                          }
                        >
                          {t("FISTAR.FSR_hot")}
                        </span>
                      </div>
                    </Nav>
                  </Navbar.Collapse>
                </Navbar>
              </div>
            </div>
          </div>
          <div className="body-form  container-mmfaq">
            {isLoadingCampaign ? (
              <div id="loading" />
            ) : (
              <div className="card-mmfistar">
                <InfiniteScroll
                  dataLength={this.state.fistars.length}
                  next={this.fetchMoreData}
                  loader={<div id="loading" />}
                  hasMore={this.state.hasMore}
                  className="card-mmfistar"
                >
                  {fistars.map((fistar, key) => (
                    <Fragment key={key}>
                      <FistarComponent fistar={fistar} />
                    </Fragment>
                  ))}
                </InfiniteScroll>
              </div>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    getFistar: (page = 1, sort = "", order = "", state = "", active = "") =>
      dispatch(getFistarAction(page, sort, order, state, active))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Fistar));
