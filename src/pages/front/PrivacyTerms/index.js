import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import imageIdol from "./../../../images/dash-02.png";
import { getCampaignAction } from "../../../store/actions/auth";
import InfiniteScroll from "react-infinite-scroll-component";
import CampaignCard from "./../../../components/partial/front/campaign/Campaign";
import "./privacyTerms.scss";
import Privacy from "./../../../components/partial/front/privacy-terms/privacy";
import Terms from "./../../../components/partial/front/privacy-terms/terms";
import * as routeName from "./../../../routes/routeName";
import { Tab, Nav } from "react-bootstrap";
import PageHead from './../../../components/PageHead'

class PrivacyTerms extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data } = this.state;
    return (
      <React.Fragment>
        <PageHead title="Privacy Policy | Fistar" />
        <main className="main-container">
          <section className="privacy-policy">
            <Tab.Container defaultActiveKey={"tabPrivacy"}>
              <div className="top-policy">
                <div className="container">
                  <Nav variant="pills" className="tab">
                    <Nav.Item>
                      <ul className="list-policy">
                        <li className="item-policy">
                          {/*<Nav.Link*/}
                          {/*eventKey="tabPrivacy"*/}
                          {/*className="link-policy"*/}
                          {/*>*/}
                          {/*Privacy Policy*/}
                          {/*</Nav.Link>*/}
                          <NavLink
                            className="link-policy"
                            activeClassName="active"
                            act
                            to={routeName.PRIVACY_TERMS}
                          >
                            Privacy Policy
                          </NavLink>
                        </li>
                        <li className="item-policy">
                          {/*<Nav.Link*/}
                          {/*eventKey="tabTermsService"*/}
                          {/*className="link-policy"*/}
                          {/*>*/}
                          {/*Terms of Service*/}
                          {/*</Nav.Link>*/}
                          <NavLink
                            className="link-policy"
                            activeClassName="active"
                            to={routeName.TERMS}
                          >
                            Terms of Service
                          </NavLink>
                        </li>
                      </ul>
                    </Nav.Item>
                  </Nav>
                </div>
              </div>
              <div className="contents-policy">
                <Tab.Content>
                  <Tab.Pane eventKey="tabPrivacy">
                    <div className="privacy-policy-term">
                      <Privacy />
                    </div>
                  </Tab.Pane>
                  <Tab.Pane eventKey="tabTermsService">
                    <div className="privacy-policy-term">
                      <Terms />
                    </div>
                  </Tab.Pane>
                </Tab.Content>
              </div>
            </Tab.Container>
          </section>
        </main>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PrivacyTerms));
