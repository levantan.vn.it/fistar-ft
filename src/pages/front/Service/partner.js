import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import * as routeName from "./../../../routes/routeName";

import { Tab, Nav, Navbar } from "react-bootstrap";

import example from "./../../../images/thumnaildetail.png";
import PartnerComponent from "./../../../components/partial/front/service/Partner";
import PageHead from './../../../components/PageHead'

import "./service.scss";
class Partner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {}
    };
  }

  render() {
    const { data } = this.state;
      const { t } = this.props;
    return (
      <React.Fragment>
        <PageHead title="Partner Service | Fistar" />
        <div className="form-site">
          <Tab.Container defaultActiveKey={"tabPlatform"}>
            <div className="bg-overlay">
              <div className="color-overlay">
                <div className="header-form header-mm-campaign container-mmfaq">
                  <div className="logo logo-faq">
                    <h1>
                      <a href="">{t("CAMPAIGN.CAN_campaign")}</a>
                    </h1>
                  </div>

                  <Navbar
                    bg="light"
                    expand="lg"
                    className=" navbar navbar-expand-lg navbar-light bg-light"
                  >
                    <Navbar.Toggle
                      aria-controls="basic-navbar-nav-register"
                      ref={this.triggerClick}
                    >
                      <span className="toggler-icon">
                        <i className="fas fa-bars" />
                      </span>
                    </Navbar.Toggle>
                    <Navbar.Collapse id="basic-navbar-nav-register">
                      <Nav>
                        <div className="platform ">
                          <ul className="navbar-nav">
                            <li className="nav-item nav-link">
                              <NavLink
                                className="nav-link"
                                to={routeName.SERVICE}
                              >
                                  {" "}
                                  {t("SERVICE.SER_platform")}
                              </NavLink>
                            </li>
                            <li className="nav-item nav-link">
                              <NavLink
                                className="nav-link"
                                to={routeName.SERVICE_FISTAR}
                              >
                                  {" "}
                                  {t("SERVICE.SER_fistar")}
                              </NavLink>
                            </li>
                            <li className="nav-item nav-link">
                              <NavLink
                                className="nav-link active"
                                to={routeName.SERVICE_PARTNER}
                              >
                                  {" "}
                                  {t("SERVICE.SER_partner")}
                              </NavLink>
                            </li>
                          </ul>
                        </div>
                      </Nav>
                    </Navbar.Collapse>
                  </Navbar>
                </div>
              </div>
            </div>
            <div className="body-form  container-mmfaq service-bg">
              <PartnerComponent />
            </div>
          </Tab.Container>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Partner));
