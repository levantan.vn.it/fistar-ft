import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { FaqPartnerMainMenu } from "../../../store/actions/auth";
import InfiniteScroll from "react-infinite-scroll-component";
import FaqComponent from "../../../components/partial/front/faq/faqPartner";
import * as routeName from "../../../routes/routeName";
import PageHead from './../../../components/PageHead'

import "./faq.scss";

import {
  Panel,
  PanelGroup,
  Accordion,
  Card,
  CustomToggle,
  Button
} from "react-bootstrap";

const TYPE = {
  ALL: "",
  PARTNER: 0,
  FISTAR: 1
};

class Faq extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      faqs: [],
      isLoadingFaq: false,
      type: TYPE.ALL,
      page: 1,
      total: 0,
      hasMore: true,
      accordion: ""
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingFaq: true
      },
      () => {
        this.initData();
      }
    );
  }

  initData = () => {
    this.props
      .faqPartner(this.state.page)
      .then(response => {
        this.setState({
          faqs: response.data,
          total: this.state.total ? this.state.total : response.total,
          isLoadingFaq: false,
          last_page: response.last_page
        });
      })
      .catch(() => {
        this.setState({
          isLoadingFaq: false
        });
      });
  };

  clickChangeTab = type => {
    let textTitle = "";
    switch (type) {
      case TYPE.PARTNER: {
        textTitle = "Partner";
        break;
      }
      case TYPE.FISTAR: {
        textTitle = "Fistar";
        break;
      }
      default: {
        textTitle = "All";
        break;
      }
    }
    this.setState(
      {
        type,
        page: 1,
        isLoadingFaq: true,
        textTitle
      },
      () => {
        this.initData();
      }
    );
  };

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }

    this.props.faqPartner(this.state.page + 1).then(response => {
      this.setState({
        faqs: [...this.state.faqs, ...response.data],
        page: this.state.page + 1
      });
    });
  };

  clickToggle = e => {
    if (e === this.state.accordion) {
      this.setState({
        accordion: ""
      });
    } else {
      this.setState({
        accordion: e
      });
    }
  };

  render() {
    const { t } = this.props;
    const {
      data,
      errors,
      successm,
      type,
      total,
      isLoadingFaq,
      hasMore,
      faqs
    } = this.state;


    return (
      <Fragment>
        <PageHead title="FiStar FAQ | Fistar" />
        <div className="form-site">
          <div className="bg-overlay">
            <div className="color-overlay">
              <div className="header-form header-mm-campaign container-mmfaq">
                <div className="logo logo-faq">
                  <h1>
                    <a href="">FAQ</a>
                  </h1>
                </div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                  <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarNavAltMarkup2"
                    aria-controls="navbarNavAltMarkup2"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                  >
                    <span className="toggler-icon">
                      <i className="fas fa-bars" />
                    </span>
                  </button>
                  <div
                    className="collapse navbar-collapse"
                    id="navbarNavAltMarkup2"
                  >
                    <div className="navbar-nav">
                      <NavLink to={routeName.FAQ} className="nav-item nav-link ">
                        All
                      </NavLink>
                      <NavLink
                        to={routeName.FAQ_FISTAR}
                        className="nav-item nav-link active"
                      >
                        fiStar
                      </NavLink>
                      <NavLink
                        to={routeName.FAQ_PARTNER}
                        className="nav-item nav-link"
                      >
                        Partner
                      </NavLink>
                    </div>
                  </div>
                </nav>
              </div>
            </div>
          </div>

          <div className="body-form container-mmfaq">
            <div className="content">
              <div className="top-mmfaq">
                <p>
                  Total <span>{this.state.total}</span>
                </p>
              </div>
              <div className="collapse-page">
                <div
                  className="accordion accordion-main-content"
                  id="accordionExample"
                >
                  {isLoadingFaq ? (
                    <div id="loading" />
                  ) : (
                    <InfiniteScroll
                      dataLength={faqs.length}
                      next={this.fetchMoreData}
                      loader={<div id="loading" />}
                      hasMore={this.state.hasMore}
                      className="box-item"
                    >
                      <Accordion defaultActiveKey="0">
                        {faqs.map((faq, key) => (
                          <Fragment key={key}>
                            {/*<FaqComponent keyTo={key} faq={faq} />*/}
                            <Card>
                              <Card.Header
                                className={
                                  this.state.accordion === key ? "selected" : ""
                                }
                              >
                                <Accordion.Toggle
                                  as={Button}
                                  variant="link"
                                  onClick={() => this.clickToggle(key)}
                                  eventKey={key}
                                >
                                  <p className="collapsed">{faq.faq_title}</p>
                                  <i
                                    className={
                                      this.state.accordion === key
                                        ? "fas fa-chevron-up"
                                        : "fas fa-chevron-down"
                                    }
                                  />
                                </Accordion.Toggle>
                              </Card.Header>
                              <Accordion.Collapse
                                eventKey={key}
                                className="collapse-faq"
                              >
                                <Card.Body>
                                  <span>A</span>
                                  <p>{faq.faq_content}</p>
                                </Card.Body>
                              </Accordion.Collapse>
                            </Card>
                          </Fragment>
                        ))}
                      </Accordion>
                    </InfiniteScroll>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    faqPartner: (page = 1) => dispatch(FaqPartnerMainMenu(page))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Faq));
