import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { FaqMainMenu } from "../../../store/actions/auth";
import InfiniteScroll from "react-infinite-scroll-component";
import FaqComponent from "../../../components/partial/front/faq";
import * as routeName from "./../../../routes/routeName";

import { Navbar, Nav, NavDropdown, Dropdown } from "react-bootstrap";
import PageHead from './../../../components/PageHead'

import {
  Panel,
  PanelGroup,
  Accordion,
  Card,
  CustomToggle,
  Button
} from "react-bootstrap";

import "./faq.scss";

const TYPE = {
  ALL: "",
  PARTNER_FRONT: 0,
  FISTAR_FRONT: 1
};

class Faq extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      faqs: [],
      isLoadingFaq: false,
      type: TYPE.ALL,
      page: 1,
      total: 0,
      hasMore: true,
      accordion: "",
      accordionSet: false,
      reset: true
    };
    this.triggerClick = React.createRef();
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingFaq: true,
        reset: true
      },
      () => {
        this.initData();
      }
    );
  }

  initData = () => {
    this.props
      .faq(this.state.type, this.state.page)
      .then(response => {
        this.setState({
          faqs: response.data,
          total: response.total,
          isLoadingFaq: false,
          last_page: response.last_page,
          reset: false
        });
      })
      .catch(() => {
        this.setState({
          isLoadingFaq: false,
          reset: false
        });
      });
  };

  clickChangeTab = type => {
    this.setState(
      {
        type,
        page: 1,
        isLoadingFaq: true,
        accordion: "",
        hasMore: true
      },
      () => {
        this.triggerClick.current.click();
        this.initData();
      }
    );
  };

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }

    this.props.faq(this.state.type, this.state.page + 1).then(response => {
      this.setState({
        faqs: [...this.state.faqs, ...response.data],
        page: this.state.page + 1
      });
    });
  };

  clickToggle = (key, e) => {

    if (key === this.state.accordion) {
      this.setState({
        accordion: ""
      });
    } else {
      this.setState({
        accordion: key
      });
    }
  };

  render() {
    const { t } = this.props;
    const {
      data,
      errors,
      successm,
      type,
      total,
      isLoadingFaq,
      hasMore,
      faqs
    } = this.state;

    return (
      <Fragment>
        <PageHead title="FAQ | Fistar" />
        <div className="form-site">
          <div className="bg-overlay">
            <div className="color-overlay">
              <div className="header-form header-mm-campaign container-mmfaq">
                <div className="logo logo-faq">
                  <h1>
                    <a href="">{t("HEADER.HED_faq")}</a>
                  </h1>
                </div>

                <Navbar bg="light" expand="lg" className=" navbar-light bg-light">
                  <Navbar.Toggle
                    aria-controls="basic-navbar-nav-register"
                    ref={this.triggerClick}
                  >
                    <span className="toggler-icon">
                      <i className="fas fa-bars" />
                    </span>
                  </Navbar.Toggle>
                  <Navbar.Collapse id="basic-navbar-nav-register">
                    <Nav>
                      <div className="navbar-nav">
                        <span
                          className={`nav-item nav-link${
                            type === TYPE.ALL ? " active" : ""
                          }`}
                          onClick={() => this.clickChangeTab(TYPE.ALL)}
                        >
                          {t("FAQ.FAQ_all")}
                        </span>
                        <span
                          className={`nav-item nav-link${
                            type === TYPE.FISTAR_FRONT ? " active" : ""
                          }`}
                          onClick={() => this.clickChangeTab(TYPE.FISTAR_FRONT)}
                        >
                          {t("FAQ.FAQ_fistar")}
                        </span>
                        <span
                          className={`nav-item nav-link${
                            type === TYPE.PARTNER_FRONT ? " active" : ""
                          }`}
                          onClick={() => this.clickChangeTab(TYPE.PARTNER_FRONT)}
                        >
                          {t("FAQ.FAQ_partner")}
                        </span>
                      </div>
                    </Nav>
                  </Navbar.Collapse>
                </Navbar>
              </div>
            </div>
          </div>

          <div className="body-form container-mmfaq">
            <div className="content">
              <div className="top-mmfaq">
                <p>
                  Total <span>{this.state.total}</span>
                </p>
              </div>
              <div className="collapse-page">
                <div
                  className="accordion accordion-main-content"
                  id="accordionExample"
                >
                  {isLoadingFaq ? (
                    <div id="loading" />
                  ) : faqs.length > 0 ? (
                    <InfiniteScroll
                      dataLength={faqs.length}
                      next={this.fetchMoreData}
                      loader={faqs.length < 10 ? "" : <div id="loading" />}
                      hasMore={this.state.hasMore}
                      className="box-item box-scroll"
                    >
                      <Accordion defaultActiveKey={this.state.type}>
                        {faqs.map((faq, key) => (
                          <Fragment key={key}>
                            {/*<FaqComponent keyTo={key} faq={faq} />*/}
                            <Card className={key}>
                              <Card.Header
                                className={
                                  this.state.accordion === key ? "selected" : ""
                                }
                              >
                                <Accordion.Toggle
                                  as={Button}
                                  variant="link"
                                  onClick={e => this.clickToggle(key, e)}
                                  eventKey={key + 100000}
                                >
                                  <p className="collapsed">{faq.faq_title}</p>
                                  <i
                                    className={
                                      this.state.accordion === key
                                        ? "fas fa-chevron-up"
                                        : "fas fa-chevron-down"
                                    }
                                  />
                                </Accordion.Toggle>
                              </Card.Header>
                              <Accordion.Collapse
                                eventKey={key + 100000}
                                className="collapse-faq"
                              >
                                <Card.Body>
                                  <span>A</span>
                                  <p>{faq.faq_content}</p>
                                </Card.Body>
                              </Accordion.Collapse>
                            </Card>
                          </Fragment>
                        ))}
                      </Accordion>
                    </InfiniteScroll>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    faq: (type = "", page = 1) => dispatch(FaqMainMenu(type, page))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Faq));
