import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import imageIdol from "./../../../images/dash-02.png";
import { getCampaignFrontAction } from "../../../store/actions/auth";
import InfiniteScroll from "react-infinite-scroll-component";
import CampaignCard from "./../../../components/partial/front/campaign/Campaign";
import * as routeName from "../../../routes/routeName";

import { Navbar, Nav, NavDropdown, Dropdown } from "react-bootstrap";
import { getCodeAction } from "../../../store/actions/code";
import PageHead from './../../../components/PageHead'

const TYPE = {
  ALL: "",
  READY: "60",
  ONGOING: "61",
  CLOSED: "62"
};

class Campaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: TYPE.ALL,
      campaigns: [],
      total: 0,
      ready: 0,
      onGoing: 0,
      closed: 0,
      textTitle: `${this.props.t("MY_CAMPAIGN.MCN_tab_allcampaign")}`,
      last_page: 1,
      page: 1,
      hasMore: true,
      isLoadingCampaign: false,
      search: "",
      reset: true,
      status: ""
    };
    this.triggerClick = React.createRef();
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingCampaign: true,
        reset: true
      },
      () => {
        this.initData();

        this.props.getCode("location");
      }
    );
  }

  initData = () => {
    this.props
      .getCampaign(this.state.page, this.state.status)
      .then(response => {
        console.log(response);
        this.setState({
          campaigns: response.data,
          total: !this.state.reset ? this.state.total : response.total,
          ready: response.ready,
          onGoing: response.ongoing,
          closed: response.closed,
          isLoadingCampaign: false,
          last_page: response.last_page,
          reset: false
        });
      })
      .catch(() => {
        this.setState({
          isLoadingCampaign: false,
          reset: false
        });
      });
  };

  clickChangeTab = status => {
    let textTitle = "";
    const { t } = this.props;
    console.log(status);
    switch (status) {
      case TYPE.READY: {
        textTitle = `${t("MY_CAMPAIGN.MCN_tab_ready_campaign")}`;
        break;
      }
      case TYPE.ONGOING: {
        textTitle = `${t("MY_CAMPAIGN.MCN_tab_ongoing")}`;
        break;
      }
      case TYPE.CLOSED: {
        textTitle = `${t("MY_CAMPAIGN.MCN_tab_close")}`;
        break;
      }
      default: {
        textTitle = `${t("MY_CAMPAIGN.MCN_text_all")}`;
        break;
      }
    }
    this.setState(
      {
        status: status,
        page: 1,
        isLoadingCampaign: true,
        textTitle,
        hasMore: true
      },
      () => {
        this.triggerClick.current.click();
        this.initData();
      }
    );
  };

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }

    this.props
      .getCampaign(this.state.page + 1, this.state.status)
      .then(response => {
        this.setState({
          campaigns: [...this.state.campaigns, ...response.data],
          page: this.state.page + 1
        });
      });
  };

  render() {
    const {
      type,
      campaigns,
      total,
      ready,
      onGoing,
      closed,
      textTitle,
      hasMore,
      isLoadingCampaign,
      search,
      reset,
      status
    } = this.state;
    const { t } = this.props;
    console.log(campaigns);
    // const campaignNotMatching =
    //   campaigns.length > 0 &&
    //   campaigns.filter(campaign => campaign.cp_status !== 59);
    // console.log(campaignNotMatching);
    let campaignNotMatching = campaigns;
    return (
      <React.Fragment>
        <PageHead title="Campaign | Fistar" />
        <div className="form-site">
          <div className="bg-overlay">
            <div className="color-overlay">
              <div className="header-form header-mm-campaign container-mmfaq">
                <div className="logo logo-faq">
                  <h1>
                    <a href="">{t("CAMPAIGN.CAN_campaign")}</a>
                  </h1>
                </div>

                <Navbar
                  bg="light"
                  expand="lg"
                  className=" navbar navbar-expand-lg navbar-light bg-light"
                >
                  <Navbar.Toggle
                    aria-controls="basic-navbar-nav-register"
                    ref={this.triggerClick}
                  >
                    <span className="toggler-icon">
                      <i className="fas fa-bars" />
                    </span>
                  </Navbar.Toggle>
                  <Navbar.Collapse id="basic-navbar-nav-register">
                    <Nav>
                      <div className="navbar-nav">
                        <span
                          className={`nav-item nav-link${
                            status === TYPE.ALL ? " active" : ""
                          }`}
                          onClick={() => this.clickChangeTab(TYPE.ALL)}
                        >
                          {t("CAMPAIGN.CAN_all")}
                        </span>
                        <span
                          className={`nav-item nav-link${
                            status === TYPE.ONGOING ? " active" : ""
                          }`}
                          onClick={() => this.clickChangeTab(TYPE.ONGOING)}
                        >
                          {t("CAMPAIGN.CAN_on_going")}
                        </span>
                        <span
                          className={`nav-item nav-link${
                            status === TYPE.READY ? " active" : ""
                          }`}
                          onClick={() => this.clickChangeTab(TYPE.READY)}
                        >
                          {t("CAMPAIGN.CAN_be_open")}
                        </span>
                        <span
                          className={`nav-item nav-link${
                            status === TYPE.CLOSED ? " active" : ""
                          }`}
                          onClick={() => this.clickChangeTab(TYPE.CLOSED)}
                        >
                          {t("CAMPAIGN.CAN_close")}
                        </span>
                      </div>
                    </Nav>
                  </Navbar.Collapse>
                </Navbar>
              </div>
            </div>
          </div>
          <div className="body-form  container-mmfaq">
            <div className="content">
              {/*<div className="gr-card">*/}

              {isLoadingCampaign ? (
                <div id="loading" />
              ) : (
                <div className="gr-card">
                  <InfiniteScroll
                    dataLength={this.state.campaigns.length}
                    next={this.fetchMoreData}
                    loader={<div id="loading" />}
                    hasMore={this.state.hasMore}
                    className="gr-card"
                  >
                    {campaignNotMatching.length > 0 &&
                      campaignNotMatching.map((campaign, key) => (
                        <Fragment key={key}>
                          <CampaignCard campaign={campaign} />
                        </Fragment>
                      ))}
                  </InfiniteScroll>
                </div>
              )}

              {/*</div>*/}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    getCampaign: (page = 1, status = "") =>
      dispatch(getCampaignFrontAction(page, status)),
    getCode: type => dispatch(getCodeAction(type))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Campaign));
