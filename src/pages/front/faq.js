import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { FaqMainMenu } from "../../store/actions/auth";
import InfiniteScroll from "react-infinite-scroll-component";
import FaqComponent from "../../components/partial/front/faq";

const TYPE = {
  ALL: "",
  PARTNER: 0,
  FISTAR: 1
};

class Faq extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      faqs: [],
      isLoadingFaq: false,
      type: TYPE.ALL,
      page: 1,
      total: 0,
      hasMore: true
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingFaq: true
      },
      () => {
        this.initData();
      }
    );
  }

  initData = () => {
    this.props
      .faq(this.state.type, this.state.page)
      .then(response => {
        console.log(response);
        this.setState({
          faqs: response.data,
          total: this.state.total ? this.state.total : response.total,
          isLoadingFaq: false,
          last_page: response.last_page
        });
      })
      .catch(() => {
        this.setState({
          isLoadingFaq: false
        });
      });
  };

  clickChangeTab = type => {
    console.log(type);
    let textTitle = "";
    switch (type) {
      case TYPE.PARTNER: {
        textTitle = "Partner";
        break;
      }
      case TYPE.FISTAR: {
        textTitle = "Fistar";
        break;
      }
      default: {
        textTitle = "All";
        break;
      }
    }
    this.setState(
      {
        type,
        page: 1,
        isLoadingFaq: true,
        textTitle
      },
      () => {
        this.initData();
      }
    );
  };

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }

    this.props.faq(this.state.type, this.state.page + 1).then(response => {
      this.setState({
        faqs: [...this.state.faqs, ...response.data],
        page: this.state.page + 1
      });
    });
  };

  render() {
    const { t } = this.props;
    const {
      data,
      errors,
      successm,
      type,
      total,
      isLoadingFaq,
      hasMore,
      faqs
    } = this.state;

    console.log(this.state);

    return (
      <div className="form-site">
        <div className="header-form">
          <div className="logo logo-faq">
            <h1>
              <a href="">FAQ</a>
            </h1>
          </div>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarNavAltMarkup2"
              aria-controls="navbarNavAltMarkup2"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="toggler-icon">
                <i className="fas fa-bars" />
              </span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup2">
              <div className="navbar-nav">
                <span
                  className={`nav-item nav-link${
                    type === TYPE.ALL ? " active" : ""
                  }`}
                  onClick={() => this.clickChangeTab(TYPE.ALL)}
                >
                  All{" "}
                </span>
                <span
                  className={`nav-item nav-link${
                    type === TYPE.FISTAR ? " active" : ""
                  }`}
                  onClick={() => this.clickChangeTab(TYPE.FISTAR)}
                >
                  fiStar
                </span>
                <span
                  className={`nav-item nav-link${
                    type === TYPE.PARTNER ? " active" : ""
                  }`}
                  onClick={() => this.clickChangeTab(TYPE.PARTNER)}
                >
                  Partner
                </span>
              </div>
            </div>
          </nav>
        </div>
        <div className="body-form">
          <div className="content">
            <div className="top-mmfaq">
              <p>
                Total <span>{this.state.total}</span>
              </p>
            </div>
            <div className="collapse-page">
              <div className="accordion" id="accordionExample">
                {isLoadingFaq ? (
                  <div className="list-campaign-track text-center">
                    <div className="spinner-border" role="status">
                      <span className="sr-only">{t("LOADING.LOADING")}</span>
                    </div>
                  </div>
                ) : (
                  <InfiniteScroll
                    dataLength={faqs.length}
                    next={this.fetchMoreData}
                    loader={<h4>{t("LOADING.LOADING")}</h4>}
                    hasMore={this.state.hasMore}
                    className="box-item"
                  >
                    {faqs.map((faq, key) => (
                      <Fragment key={key}>
                        <FaqComponent faq={faq} />
                      </Fragment>
                    ))}
                  </InfiniteScroll>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    faq: (type = "", page = 1) => dispatch(FaqMainMenu(type, page))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(Faq));
