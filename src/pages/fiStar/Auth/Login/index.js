import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import {
  loginAction,
  loginFacebookAction,
  getCodeAction,
  joinAction,
  createAccessTokenFBAction,
  dispatchLoginSuccessAction
} from "./../../../../store/actions/auth";
import FiStarAuthFooter from "./../../../../components/fiStar/Auth/Footer";
import * as routeName from "./../../../../routes/routeName";
import { Modal, Button } from "react-bootstrap";
import cancelImage from "./../../../../images/cancel.svg";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import ModalGetAccessToken from "./../../../../components/fiStar/Auth/ModalGetAccessToken";
import "./login.scss";
import {
  REDIRECT_LOGIN,
  REDIRECT_LOGIN_FISTAR
} from "../../../../constants/index";
import api from "../../../../api";
import {
  convertLocationSearchToObject,
  convertObjectToLocationSearch
} from "./../../../../common/helper";

class FiStarLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        email: "",
        password: ""
      },
      errors: {
        email: "",
        password: "",
        message: ""
      },
      response: null,
      genders: [],
      isLoading: false,
      isShowModal: false,
      isShowModalFalse: false,
      isLoadingFB: false,
      isLoadingModal: false,
      url: REDIRECT_LOGIN_FISTAR,
    };
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      if (window.FB) {
        window.FB.init({
          appId: process.env.REACT_APP_FACEBOOK_FIME_ID,
          status: true,
          xfbml: true,
          version: "v2.3" // or v2.6, v2.5, v2.4, v2.3
        });
      }
    }
  }

  componentDidMount() {
    let v = this.props.location.search.match(new RegExp("(?:[?&]code=)([^&]+)"));
    let code = v ? v[1] : null;
    let search = convertLocationSearchToObject(this.props.location.search);

    if (search.code) {
      if (!search.t) {
        this.setState({ isLoadingFB: true });
        api
          .getAccessToken("fime", "/login", code)
          .then(res => {
            this.props
              .loginFacebook({ accessToken: res.data.access_token })
              .then((response) => {
                if (response.data.first_login == 1) {
                  this.props.history.push(routeName.FISTAR_APPROVE_REGISTER);
                  return;
                }
              })
              .catch(error => {
                this.clearCode();
                if (error.status == 402) {
                  this.setState({
                    errors: {
                      ...this.state.errors,
                      facebook: this.props.t(
                        "FISTAR_JOIN.FJN_erro_facebook_register_lock"
                      )
                    },
                    isLoadingFB: false
                  });
                  return;
                }
                if (error.status == 400) {
                  let fb = "";
                  let channels = error.data.data.channel;
                  channels.map(channel => {
                    if (channel.sns_id == 2) {
                      fb = channel.sns_url;
                    }
                  });
                  if (typeof window !== 'undefined' && window.document && window.document.createElement) {
                    window.localStorage.fb = fb;
                    window.localStorage.info = JSON.stringify(error.data);
                  }
                  this.setState({
                    isShowModal: true,
                    url: REDIRECT_LOGIN_FISTAR
                  });
                  return;
                }
                this.setState({
                  errors: {
                    ...this.state.errors,
                    facebook: this.props.t(
                      "FISTAR_JOIN.FJN_erro_facebook_register"
                    )
                  },
                  isLoadingFB: false
                });
              });
          })
          .catch(() => {
            this.setState({
              errors: {
                ...this.state.errors,
                facebook: this.props.t("FISTAR_JOIN.FJN_erro_facebook_register")
              },
              isLoadingFB: false
            });
          });
      } else {
        this.setState({
          isShowModal: true,
          isLoadingModal: true,
          url: REDIRECT_LOGIN_FISTAR
        });
        api
          .getAccessToken("fistar", "/login?t=1", search.code)
          .then(res => {
            let info = {}
            if (typeof window !== 'undefined' && window.document && window.document.createElement) {
              info = JSON.parse(window.localStorage.info);
            }
            api
              .ProvideAccessToken({
                url: (typeof window !== 'undefined' && window.document && window.document.createElement) ? window.localStorage.fb : '',
                token: res.data.access_token,
                uid: info.data.uid
              })
              .then(res => {
                if (res.data.data) {
                  this.props.dispatchLoginSuccess(info);
                } else {
                  this.clearCode();
                  this.setState({
                    isShowModalFalse: true,
                    isLoadingModal: false
                  });
                }
              })
              .catch(() => {
                this.clearCode();
                this.setState({
                  isShowModalFalse: true,
                  isLoadingModal: false
                });
              });
          })
          .catch(() => {
            this.clearCode();
            this.setState({
              isShowModalFalse: true,
              isLoadingModal: false
            });
          });
      }
    }
  }

  clearCode = () => {
    const { location } = this.props;
    let path = location.path;
    let search = location.search;
    search = convertLocationSearchToObject(search);
    search.code = "";
    search = convertObjectToLocationSearch(search);
    this.props.history.push({
      pathname: path,
      search: "?" + search
    });
  };

  responseFacebook = e => {
    this.props.loginFacebook({ accessToken: e.accessToken }).catch(error => {
      this.setState({
        errors: {
          ...this.state.errors,
          facebook: this.props.t("FISTAR_JOIN.FJN_erro_facebook_register")
        }
      });
    });
  };

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        email: "",
        password: "",
        message: ""
      }
    }));
  };

  validateEmail = email => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  validateForm = () => {
    const errors = {
      email: "",
      password: "",
      message: ""
    };
    const { data, validateEmail } = this.state;
    const { t } = this.props;

    if (!data.password) {
      errors.password = t("FISTAR_LOGIN.FL_erro_pw");
    } else if (data.password && data.password.length < 6) {
      errors.password = t("FISTAR_LOGIN.FL_erro_pw_limit_6");
    }

    if (!data.email) {
      errors.email = "The email field is required.";
    } else if (data.email && !this.validateEmail(data.email)) {
      errors.email = "The email must be a valid email address.";
    }
    return errors;
  };

  submitForm = e => {
    e.preventDefault();
    const { data } = this.state;
    const { t } = this.props;
    const validate = this.validateForm();
    if (!validate.email && !validate.password) {
      this.setState(
        {
          isLoading: true
        },
        () => {
          this.props
            .login(data)
            .then(response => {
              if (!response.data.sns_token_facebook) {
                let fb = "";
                let channels = response.data.channel;
                channels.map(channel => {
                  if (channel.sns_id == 2) {
                    fb = channel.sns_url;
                  }
                });
                if (typeof window !== 'undefined' && window.document && window.document.createElement) {
                  window.localStorage.fb = fb;
                  window.localStorage.info = JSON.stringify(response);
                }
                this.setState({
                  response: response,
                  isLoading: false,
                  isShowModal: true,
                  url: REDIRECT_LOGIN_FISTAR,
                });
                return;
              }

              if (response.data.first_login == 1) {
                this.props.history.push(routeName.FISTAR_APPROVE_REGISTER);
                return;
              }
            })
            .catch(e => {
              const errors = {
                email: "",
                password: "",
                message: ""
              };
              if (e.status === 402) {
                errors.message = this.props.t(
                  "FISTAR_JOIN.FJN_erro_facebook_register_lock"
                );
                this.setState({
                  errors,
                  isLoading: false
                });
                return;
              }
              if (e.data.errors) {
                errors.email = e.data.errors.email
                  ? t("FISTAR_RESETPW.FRP_email_incorrect")
                  : "";
                errors.password = e.data.errors.password
                  ? e.data.errors.password[0]
                  : "";
              }
              errors.message =
                e.data.message && e.data.message === "Unauthorized"
                  ? e.data.message === "Unauthorized"
                    ? t("FISTAR_LOGIN.FL_erro_email_pass")
                    : e.data.message
                  : e.data.message && e.data.message
                    ? e.data.message
                    : "";
              this.setState({
                errors,
                isLoading: false
              });
            });
        }
      );
    } else {
      this.setState(
        {
          errors: validate
        },
        () => {}
      );
    }
  };

  callback = () => {
    this.props.dispatchLoginSuccess(this.state.response);
  };

  handleClose = () => {
    this.setState({ isShowModal: false });
  };

  renderLoginFB = () => (
    <a href={REDIRECT_LOGIN}>
      <button className="btn btn-submit-facebook text-center">
        <i className="fab fa-facebook-f" />
        {this.props.t("FISTAR_LOGIN.FLN_sign_facebook")}
      </button>
    </a>
  );

  renderLoginFBLoading = () => (
    <button className="btn btn-submit-facebook text-center">
      <i className="fab fa-facebook-f" />
      <div className="spinner-border" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </button>
  );

  render() {
    const { t } = this.props;
    const { data, errors, genders, isLoading, isLoadingFB } = this.state;

    let uid = "";
    if (this.state.response && this.state.response.data) {
      uid = this.state.response.data.uid;
    }

    return (
      <Fragment>
        <h3 className="partner-login">{t("FISTAR_LOGIN.FL_fistar_login")}</h3>
        <div className="submit-face">
          {isLoadingFB && this.renderLoginFBLoading()}
          {!isLoadingFB && this.renderLoginFB()}
          {errors.facebook && <p className="text-danger">{errors.facebook}</p>}
        </div>
        <form className="wf-form-content" onSubmit={this.submitForm}>
          <div className="row row-eq-height">
            <div className="col-md-9">
              <div className="form-input">
                <div
                  className={`input-group mb-3${
                    errors.email || errors.message ? " invalid" : ""
                  }`}
                >
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-envelope" />
                    </span>
                  </div>
                  <input
                    placeholder={t("FISTAR_LOGIN.FL_input_email")}
                    type="text"
                    id="email"
                    name="email"
                    className="form-input empty is-invalid"
                    value={data.email}
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                  />
                  {errors.email && (
                    <div
                      className="tooptip-text-error d-flex align-items-center"
                      style={{ width: "260px" }}
                    >
                      {/*<p>이메일 주소와 비밀번호가 정확하지 않습니다. 다시 한번 확인해주세요!!</p>*/}
                      <p>{errors.email}</p>
                    </div>
                  )}
                </div>
                <div
                  className={`input-group${
                    errors.password || errors.message ? " invalid" : ""
                  }`}
                >
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-lock-open" />
                    </span>
                  </div>
                  <input
                    placeholder={t("FISTAR_LOGIN.FL_password")}
                    type="password"
                    id="password"
                    name="password"
                    className="form-input empty mb-0"
                    value={data.password}
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                  />
                  {(errors.password || errors.message) && (
                    <div
                      className="tooptip-text-error  d-flex align-items-center"
                      style={{ width: "260px" }}
                    >
                      {/*<p>이메일 주소와 비밀번호가 정확하지 않습니다. 다시 한번 확인해주세요!!</p>*/}
                      <p>
                        {errors.password ? errors.password : errors.message}
                      </p>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="col-md-3">
              <button className="btn btn-submit" onClick={this.submitForm}>
                {isLoading ? (
                  <div className="spinner-border " role="status">
                    <span className="sr-only">{t("LOADING.LOADING")}</span>
                  </div>
                ) : (
                  t("FISTAR_LOGIN.FL_login")
                )}
              </button>
            </div>
          </div>
          <div className="row">
            <div className="col-md-9">
              <div className="text">
                <p className="text-left mt-4 mb-4">
                  <Link to={routeName.FISTAR_FIND_EMAIL}>
                    {t("FISTAR_LOGIN.FL_lost_account")}
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </form>
        <FiStarAuthFooter />
        {this.state.isShowModal && (
          <ModalGetAccessToken
            isShowModal={this.state.isShowModal}
            handleClose={this.handleClose}
            uid={uid}
            callback={this.callback}
            isShowModalFalse={this.state.isShowModalFalse}
            isLoadingModal={this.state.isLoadingModal}
            url={this.state.url}
          />
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    login: data => dispatch(loginAction(data)),
    loginFacebook: data => dispatch(loginFacebookAction(data)),
    getCode: type => dispatch(getCodeAction(type)),
    join: (data, type, isChangeID = null, isChangeEmail = null) =>
      dispatch(joinAction(data, type, isChangeID, isChangeEmail)),
    createAccessTokenFB: (data, accessToken) =>
      dispatch(createAccessTokenFBAction(data, accessToken)),
    dispatchLoginSuccess: data => dispatch(dispatchLoginSuccessAction(data))
  };
};

// const loadData = (store) => {
//   // store.dispatch(getCodeAction('gender'))
//   // For the connect tag we need Provider component but on the server at this moment app is not rendered yet
//   // So we need to use store itself to load data
//   // return store.dispatch(fetchArticles(param)); // Manually dispatch a network request
// };
// FiStarLogin.loadData = loadData
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FiStarLogin));
