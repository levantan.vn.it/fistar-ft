import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import {
  updateProfileFistarAction,
  logoutAction
} from "./../../../../store/actions/auth";
import { Modal, Button } from "react-bootstrap";
import * as routeName from "./../../../../routes/routeName";
import cancelImage from "./../../../../images/cancel.svg";

class FistarProfileResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        old_password: "",
        password: "",
        password_confirmation: ""
      },
      errors: {
        old_password: "",
        password: "",
        password_confirmation: "",
        message: ""
      },
      genders: [],
      isLoading: false,
      success: false,
      isRedirect: false
    };
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        old_password: "",
        password: "",
        password_confirmation: "",
        message: ""
      }
    }));
  };

  validateForm = () => {
    const errors = {
      old_password: "",
      password: "",
      password_confirmation: "",
      message: ""
    };
    const { data } = this.state;
    const { t } = this.props;
    if (!data.old_password) {
      errors.old_password = `${t("PROFILE_FISTAR.PFR_toptip_pass_old")}`;
    }
    if (data.password && data.old_password == data.password) {
      errors.password = t("PROFILE_FISTAR.PFR_toptip_pass_new_same_pw_old");
    }
    if (data.password && data.password !== data.password_confirmation) {
      errors.password_confirmation = `${t(
        "PROFILE_FISTAR.PFR_erro_confirm_pass_not_match"
      )}`;
    }
    if (!data.password) {
      errors.password = `${t("PROFILE_FISTAR.PFR_toptip_pass_new")}`;
    } else if (data.password.length < 6) {
      errors.password = t("PROFILE_FISTAR.PFR_toptip_pass_limit_6");
    }

    if (!data.password_confirmation) {
      errors.password_confirmation = `${t(
        "PROFILE_FISTAR.PFR_toptip_confirm_pass_new"
      )}`;
    } else if (data.password_confirmation.length < 6) {
      errors.password_confirmation = t(
        "PROFILE_FISTAR.PFR_erro_confirm_pass_limit_6"
      );
    }

    return errors;
  };

  submitForm = e => {
    e.preventDefault();
    const { data, errors } = this.state;
    const validate = this.validateForm();
    if (
      validate.old_password === "" &&
      validate.password === "" &&
      validate.password_confirmation === ""
    ) {
      this.setState(
        {
          isLoading: true
        },
        () => {
          this.props.updateProfile(data).then(response => {
            if (response.code === 401) {
              this.setState({
                isLoading: false,
                errors: {
                  ...this.state.errors,
                  old_password: response.data.errors.current
                    ? response.data.errors.current[0]
                    : "",
                  password: response.data.errors.password
                    ? response.data.errors.password[0]
                    : ""
                }
              });
            } else {
              this.setState({
                isLoading: false,
                success: true
              });
            }
          });
        }
      );
    } else {
      this.setState({
        errors: validate
      });
    }
  };

  handleClose = () => {
    this.setState({ success: false });
  };

  handleShow = () => {
    this.setState({ success: true });
  };

  onExited = () => {
    this.props.logout();
    this.props.history.push("/login");
    this.setState({
      isRedirect: true
    });
  };

  render() {
    const { t } = this.props;
    const { data, errors, genders, isLoading, isRedirect } = this.state;

    // if (isRedirect) {
    //   return <Redirect to={routeName.FISTAR_LOGIN}/>
    // }

    return (
      <Fragment>
        <div className="content-mypage-partner">
          <div className="container-change">
            <div className="title-changepass">
              <h4>{t("PROFILE_FISTAR.PFR_title")}</h4>
            </div>
            <form className="myfistar-form" onSubmit={this.submitForm}>
              <div className="pass">
                <div className="form-group row">
                  <div className="group-icon-pass">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fas fa-unlock" />
                      </span>
                    </div>
                    <input
                      placeholder={t("PROFILE_FISTAR.PFR_input_password")}
                      type="password"
                      className="form-input empty"
                      name="old_password"
                      id="old_password"
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                      value={data.old_password}
                    />
                    {errors.old_password && (
                      <div className="tooptip-pass">
                        <p>{errors.old_password}</p>
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className="pass new-pass">
                <div className="form-group row">
                  <div className="group-icon-pass">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fas fa-unlock" />
                      </span>
                    </div>
                    <input
                      placeholder={t("PROFILE_FISTAR.PFR_input_new_password")}
                      type="password"
                      className="form-input empty"
                      name="password"
                      id="password"
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                      value={data.password}
                    />
                    {errors.password && (
                      <div className="tooptip-pass">
                        <p>{errors.password}</p>
                      </div>
                    )}
                  </div>
                </div>
                <div className="form-group row">
                  <div className="group-icon-pass">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fas fa-unlock" />
                      </span>
                    </div>
                    <input
                      placeholder={t(
                        "PROFILE_FISTAR.PFR_input_confirm_password"
                      )}
                      type="password"
                      className="form-input empty"
                      name="password_confirmation"
                      id="password_confirmation"
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                      value={data.password_confirmation}
                    />
                    {errors.password_confirmation && (
                      <div className="tooptip-pass">
                        <p>{errors.password_confirmation}</p>
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className="btn-ok">
                <button onClick={this.submitForm} type="submit">
                  {isLoading ? (
                    <div className="spinner-border" role="status">
                      <span className="sr-only">{t("LOADING.LOADING")}</span>
                    </div>
                  ) : (
                    t("PROFILE_FISTAR.PFR_button_ok")
                  )}
                </button>
              </div>
            </form>
          </div>
        </div>
        <Modal
          show={this.state.success}
          onHide={this.handleClose}
          onExited={this.onExited}
          dialogClassName="howto-register modal-dialog-centered"
        >
          <div className="modal-content">
            <div className="register">
              <div className="top modal-header">
                <h4>{t("PROFILE_FISTAR.PFR_popup_fistar")}</h4>
                <button
                  type="button"
                  className="btn btn-close close"
                  onClick={this.handleClose}
                >
                  <img src={cancelImage} alt="close" />
                </button>
              </div>
              <div className="body-popup">
                <p className="mb-3 text-center">
                  {t("POPUP_CHANGE_PASSWORD.PCP_text_1")}
                </p>
                <p className="text-center">
                  <small>{t("POPUP_CHANGE_PASSWORD.PCP_text_2")}</small>
                </p>
                <button type="button" onClick={this.onExited}>
                  {t("PROFILE_FISTAR.PFR_button_ok")}
                </button>
              </div>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    updateProfile: data =>
      dispatch(updateProfileFistarAction("password", data)),
    logout: () => dispatch(logoutAction())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarProfileResetPassword));
