import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import {
  getProfileFistarAction,
  updateProfileFistarAction,
  checkExistAction
} from "./../../../../store/actions/auth";
import FistarProfileImagePreview from "./../../../../components/fiStar/form/ImagePreview";
import * as routeName from "./../../../../routes/routeName";
import "./profile.css";
import { Modal, Button } from "react-bootstrap";
import cancelImage from "./../../../../images/cancel.svg";
import { getImageLink } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";

class FistarProfileGaneral extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        status_message: "",
        contact: "",
        email: "",
        address: "",
        image: null,
        name: null
      },
      email_backup: "",
      errors: {
        status_message: "",
        contact: "",
        email: "",
        address: "",
        image: "",
        message: ""
      },
      isActiveSubmit: false,
      isLoading: false,
      isBack: false,
      isChangeEmail: false,
      isLoadingCheckExistEmail: false,
      existEmail: 0,
      success: false
    };
  }

  componentDidMount() {
    this.props.getProfile().then(response => {
      const {
        data: {
          status: { influencer }
        }
      } = response;
      this.setState({
        data: {
          status_message: influencer.status_message,
          contact: influencer.phone,
          email: influencer.email,
          address: influencer.address,
          image: influencer.avatar,
          name: influencer.name,
          id: influencer.id
        },
        email_backup: influencer.email
      });
    });
  }

  handleChangeFile = file => {
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        image: file
      },
      isActiveSubmit: true,
      errors: {
        ...prevState.errors,
        image:
          file.size > process.env.REACT_APP_MAX_IMAGE_SIZE * 1000000
            ? this.props.t("VALIDATE.VLD_img_size_big") + ("")+
              process.env.REACT_APP_MAX_IMAGE_SIZE +
              "MB."
            : ""
      }
    }));
  };

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      },
      isActiveSubmit: true
    }));
  };

  handleChangeInputEmail = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        email: value
      },
      isActiveSubmit: true,
      isChangeEmail: true
    }));
  };

  validateNumber = event => {
    const { data } = this.state;

    var invalidChars = ["-", "+", "e", "."];

    if (invalidChars.includes(event.key)) {
      event.preventDefault();
    }
  };

  onFocusInput = e => {
    this.setState({
      errors: {
        status_message: "",
        contact: "",
        email: "",
        address: "",
        image: "",
        message: ""
      }
    });
  };

  checkExistEmail = () => {
    const { data } = this.state;
    const { t } = this.props;
    if (data.email) {
      if (!this.validateEmail(data.email)) {
        this.setState(prevState => ({
          existID: 1,
          errors: {
            ...prevState.errors,
            email: t("PROFILE_FISTAR.PFR_erro_email_valid")
          }
        }));
        return;
      }
      if (data.email == this.state.email_backup) {
        this.setState(prevState => ({
          existEmail: 2
        }));
      } else {
        this.setState(
          {
            isLoadingCheckExistEmail: true
          },
          () => {
            this.props
              .checkExist("email", data.email)
              .then(() => {
                this.setState(prevState => ({
                  existEmail: 1,
                  isLoadingCheckExistEmail: false,
                  errors: {
                    ...prevState.errors,
                    email: t("PROFILE_FISTAR.PFR_erro_email_existed")
                  }
                }));
              })
              .catch(() => {
                this.setState(prevState => ({
                  existEmail: 2,
                  isLoadingCheckExistEmail: false
                }));
              });
          }
        );
      }
    } else {
      this.setState(prevState => ({
        existID: 1,
        errors: {
          ...prevState.errors,
          email: this.props.t("PROFILE_FISTAR.PFR_erro_email")
        }
      }));
    }
  };

  validateEmail = email => {
    // var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //   var re = /^[a-zA-Z0-9\.]{2,}@[a-zA-Z0-9]{2}(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])?)(?=.*[\.])*$/;
      var re = /([a-z]+[a-z0-9]*[_\.]?[a-z0-9]+)@(([a-z0-9]{2,}\.)*[a-z0-9]{2,}\.)+[a-z]{2,}/;
    return re.test(String(email).toLowerCase());
  };

    validateEmailCustomer = email => {
        var re = /^[a-zA-Z0-9]{2,}@[a-zA-Z0-9]{2,}.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])*$/;
        return re.test(String(email).toLowerCase());
    };

  validateForm = () => {
    const errors = {
      status_message: "",
      contact: "",
      email: "",
      address: ""
    };
    const { t } = this.props;
    const { data } = this.state;
    if (!data.status_message) {
      errors.status_message = t("PROFILE_FISTAR.PFR_erro_status");
    }
    if (!data.contact) {
      errors.contact = t("PROFILE_FISTAR.PFR_erro_contact");
    } else if (isNaN(data.contact) == true) {
      errors.contact = t("PROFILE_FISTAR.PFR_erro_contact_is_number");
    }else if (data.contact.length < 9) {
        errors.contact = `${t("PARTNER_JOIN.PJN_erro_phone_limit_9")}`;
    } else if (data.contact.length > 15) {
        errors.contact = `${t("PARTNER_JOIN.PJN_erro_phone_limit_15")}`;
    }
    if (!data.email) {
      errors.email = t("PROFILE_FISTAR.PFR_erro_email");
    } else if (data.email && !this.validateEmail(data.email)) {
      errors.email = t("PROFILE_FISTAR.PFR_erro_email_valid");
    }
    else if (this.state.errors.email) {
      errors.email = this.state.errors.email;
    }
    if (!data.address) {
      errors.address = t("PROFILE_FISTAR.PFR_erro_address");
    }

    return errors;
  };

  submitForm = e => {
    e.preventDefault();
    const { data, errors } = this.state;
    const validate = { ...this.state.errors, ...this.validateForm() };
    const { t } = this.props;
    if (
      !validate.status_message &&
      !validate.contact &&
      !validate.email &&
      !validate.address &&
      !validate.image
    ) {
      if (this.state.existEmail === 1 && this.state.isChangeEmail) {
        const { data } = this.state;
        if (data.email) {
          if (!this.validateEmail(data.email)) {
            this.setState(prevState => ({
              existID: 1,
              errors: {
                ...prevState.errors,
                email: t("PROFILE_FISTAR.PFR_erro_email_valid")
              }
            }));
            return;
          }

          if (data.email !== this.state.email_backup) {
            this.setState(
              {
                isLoadingCheckExistEmail: true
              },
              () => {
                this.props
                  .checkExist("email", data.email)
                  .then(() => {
                    this.setState(prevState => ({
                      existEmail: 1,
                      isLoadingCheckExistEmail: false,
                      errors: {
                        ...prevState.errors,
                        email: t("PROFILE_FISTAR.PFR_erro_email_existed")
                      }
                    }));
                  })
                  .catch(() => {
                    this.setState(prevState => ({
                      existEmail: 2,
                      isLoadingCheckExistEmail: false
                    }));

                    let formData = new FormData();
                    Object.keys(data).map(key => {
                      formData.append(key, data[key]);
                    });
                    this.setState(
                      {
                        isLoading: true
                      },
                      () => {
                        this.props
                          .updateProfile(formData)
                          .then(response => {
                            this.setState({
                              isLoading: false,
                              success: true,
                              email_backup: data.email
                            });
                          })
                          .catch((er, code) => {
                            this.setState({
                              isLoading: false,
                              existEmail: 2,
                              errors: {
                                ...this.state.errors,
                                image:
                                  t("VALIDATE.VLD_img_size_big") +
                                  process.env.REACT_APP_MAX_IMAGE_SIZE +
                                  "MB."
                              }
                            });
                          });
                      }
                    );
                  });
              }
            );
          } else {
            this.setState(prevState => ({
              existEmail: 2,
              isLoadingCheckExistEmail: false
            }));
            let formData = new FormData();

            Object.keys(data).map(key => {
              formData.append(key, data[key]);
            });
            this.setState(
              {
                isLoading: true
              },
              () => {
                this.props
                  .updateProfile(formData)
                  .then(response => {
                    this.setState({
                      isLoading: false,
                      success: true,
                      email_backup: data.email
                    });
                  })
                  .catch((er, code) => {
                    this.setState({
                      isLoading: false,
                      existEmail: 2,
                      errors: {
                        ...this.state.errors,
                        image:
                          t("VALIDATE.VLD_img_size_big") +
                          process.env.REACT_APP_MAX_IMAGE_SIZE +
                          "MB."
                      }
                    });
                  });
              }
            );
          }
        } else {
          this.setState(prevState => ({
            existID: 1,
            errors: {
              ...prevState.errors,
              email: t("PROFILE_FISTAR.PFR_erro_email")
            }
          }));
        }
      } else {
        let formData = new FormData();
        Object.keys(data).map(key => {
          formData.append(key, data[key]);
        });
        this.setState(
          {
            isLoading: true
          },
          () => {
            this.props
              .updateProfile(formData)
              .then(response => {
                this.setState({
                  isLoading: false,
                  success: true
                });
              })
              .catch((er, code) => {
                this.setState({
                  isLoading: false,
                  errors: {
                    ...this.state.errors,
                    image:
                      t("VALIDATE.VLD_img_size_big") +
                      process.env.REACT_APP_MAX_IMAGE_SIZE +
                      "MB."
                  }
                });
              });
          }
        );
      }
    } else {
      this.setState({
        errors: validate
      });
    }
  };

  onClickCancel = () => {
    this.setState({
      isBack: true
    });
  };

  handleClose = () => {
    this.setState({ success: false });
  };

  handleShow = () => {
    this.setState({ success: true });
  };

  onExited = () => {
    this.setState({
      isRedirect: true
    });
  };

  render() {
    const { t } = this.props;
    const {
      data,
      errors,
      previewImage,
      isActiveSubmit,
      isLoading,
      isBack,
      isChangeEmail,
      isLoadingCheckExistEmail,
      existEmail
    } = this.state;

    if (isBack) {
      return <Redirect to={routeName.FISTAR_DASHBOARD} />;
    }

    return (
      <Fragment>
        <div className="content-mypage-partner">
          <div className="container">
            <div className="wf-row">
              <div className="left image-page">
                <FistarProfileImagePreview
                  imageSrc={getImageLink(
                    data.image,
                    IMAGE_TYPE.FISTARS,
                    IMAGE_SIZE.ORIGINAL
                  )}
                  imageAlt={data.name}
                  onChangeImage={this.handleChangeFile}
                />
                {errors.image && <p className="text-danger">{errors.image}</p>}
              </div>
              <div className="right">
                <div className="title-right">
                  <h4>
                    {data.name} <span>{data.id}</span>
                  </h4>
                </div>
                <form className="myfistar-form" onSubmit={this.submitForm}>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm"
                    >
                      {t("PROFILE_FISTAR.PFR_label_status_message")}
                    </label>
                    <div className="col-sm-9">
                      <textarea
                        className="form-control"
                        rows="5"
                        id="status_message"
                        name="status_message"
                        onChange={this.handleChangeInput}
                        onFocus={this.onFocusInput}
                        value={data.status_message}
                      />
                      {errors.status_message && (
                        <p className="text-danger">{errors.status_message}</p>
                      )}
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="inputPassword"
                      className="col-sm-3 col-form-label"
                    >
                      {t("PROFILE_FISTAR.PFR_label_contact")}
                      <span className="form-obligatory">*</span>
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        className="form-control"
                        id="contact"
                        name="contact"
                        onChange={this.handleChangeInput}
                        onFocus={this.onFocusInput}
                        value={data.contact}
                        onKeyPress={this.validateNumber}
                      />
                      {errors.contact && (
                        <p className="text-danger">{errors.contact}</p>
                      )}
                    </div>
                  </div>
                  <div
                    className={`form-group row${errors.email ? " mb-0" : ""}`}
                  >
                    <label
                      htmlFor="inputPassword"
                      className="col-sm-3 col-form-label"
                    >
                      {t("PROFILE_FISTAR.PFR_label_email")}
                      <span className="form-obligatory">*</span>
                    </label>
                    <div className="col-sm-9 group-check">
                      <input
                        type="text"
                        className={`form-control${
                          errors.email ? " is-invalid" : ""
                        }${existEmail == 2 ? " is-valid" : ""}`}
                        id="email"
                        name="email"
                        onChange={this.handleChangeInputEmail}
                        onFocus={this.onFocusInput}
                        value={data.email}
                      />
                      {isChangeEmail && (
                        <button
                          className="btn"
                          type="button"
                          onClick={this.checkExistEmail}
                        >
                          {isLoadingCheckExistEmail ? (
                            <div className="spinner-border" role="status">
                              <span className="sr-only">
                                {t("LOADING.LOADING")}
                              </span>
                            </div>
                          ) : (
                            `${t("PROFILE_FISTAR.PFR_button_check_email")}`
                          )}
                        </button>
                      )}
                    </div>
                  </div>
                  {errors.email && (
                    <div className="form-group row">
                      <label
                        htmlFor="inputPassword"
                        className="col-sm-3 col-form-label"
                      />
                      <div className="col-sm-9 group-check">
                        <p className="text-danger">{errors.email}</p>
                      </div>
                    </div>
                  )}
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm"
                    >
                      {t("PROFILE_FISTAR.PFR_label_address")}
                      <span className="form-obligatory">*</span>
                    </label>
                    <div className="col-sm-9">
                      <textarea
                        className="form-control"
                        rows="5"
                        id="address"
                        name="address"
                        onChange={this.handleChangeInput}
                        onFocus={this.onFocusInput}
                        value={data.address}
                      />
                      {errors.address && (
                        <p className="text-danger">{errors.address}</p>
                      )}
                    </div>
                  </div>
                </form>
                <div className="group-button">
                  <div className="row">
                    <div className="col-md-9 offset-md-3 col-12">
                      <div className="group">
                        <button type="button" onClick={this.onClickCancel}>
                          {t("PROFILE_FISTAR.PFR_button_cancel")}
                        </button>
                        {isActiveSubmit ? (
                          <button
                            className="btn apply-button active-hover"
                            onClick={this.submitForm}
                          >
                            {isLoading ? (
                              <div className="spinner-border" role="status">
                                <span className="sr-only">
                                  {t("LOADING.LOADING")}
                                </span>
                              </div>
                            ) : (
                              `${t("PROFILE_FISTAR.PFR_button_apply")}`
                            )}
                          </button>
                        ) : (
                          <button
                            type={"button"}
                            className={!isActiveSubmit ? `disable-hover` : ""}
                          >
                            {t("PROFILE_FISTAR.PFR_button_apply")}
                          </button>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          show={this.state.success}
          onHide={this.handleClose}
          onExited={this.onExited}
          dialogClassName="howto-register modal-dialog-centered"
        >
          <div className="modal-content">
            <div className="register">
              <div className="top modal-header">
                <h4>{t("PROFILE_FISTAR.PFR_popup_fistar")}</h4>
                <button
                  type="button"
                  className="btn btn-close close"
                  onClick={this.handleClose}
                >
                  <img src={cancelImage} alt="close" />
                </button>
              </div>
              <div className="body-popup">
                <p>{t("PROFILE_FISTAR.PFR_popup_description")}</p>
                <button type="button" onClick={this.handleClose}>
                  {t("PROFILE_FISTAR.PFR_button_ok")}
                </button>
              </div>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getProfile: () => dispatch(getProfileFistarAction()),
    updateProfile: data => dispatch(updateProfileFistarAction("general", data)),
    checkExist: (name, value) => dispatch(checkExistAction(name, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarProfileGaneral));
