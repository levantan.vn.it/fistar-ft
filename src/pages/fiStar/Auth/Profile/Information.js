import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import {
  updateProfileFistarAction,
    checkSnsVerify,
  getProfileFistarAction
} from "./../../../../store/actions/auth";
import { getCodeAction } from "./../../../../store/actions/code";
import FistarProfileImagePreview from "./../../../../components/fiStar/form/ImagePreview";
import FistarKeyword from "./../../../../components/fiStar/form/Keyword";
import FistarGender from "./../../../../components/fiStar/form/Gender";
import FistarLocation from "./../../../../components/fiStar/form/Location";
import { format } from "date-fns";
import { DatePicker } from "@y0c/react-datepicker";
// import calendar style
// You can customize style by copying asset folder.
import "@y0c/react-datepicker/assets/styles/calendar.scss";
import "./profile.css";
import * as routeName from "./../../../../routes/routeName";
import { Modal, Button } from "react-bootstrap";
import cancelImage from "./../../../../images/cancel.svg";
import ModalGetAccessToken from "./../../../../components/fiStar/Auth/ModalGetAccessToken";
import {
  getImageLink,
  convertLocationSearchToObject,
  convertObjectToLocationSearch,
  dataURLtoFile
} from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE, REDIRECT_UPDATE_FISTAR, VALIDATION } from "./../../../../constants/index";
import Api from "../../../../api";
import Flatpickr from 'react-flatpickr'
import 'flatpickr/dist/themes/material_blue.css'
import { DATE_FORMAT } from "../../../../constants";

class FistarProfileInformartion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        image: "",
        gender: "",
        dob: "",
        location: "",
        bank_name: "",
        bank_branch: "",
        bank_account: "",
        bank_account_number: "",
        swift_code: "",
        keyword: [],
        introduction: "",
        facebook: "",
        youtube: "",
        instagram: "",
        name: ""
      },
      errors: {
        image: "",
        gender: "",
        dob: "",
        location: "",
        bank_name: "",
        bank_branch: "",
        bank_account: "",
        bank_account_number: "",
        swift_code: "",
        keyword: "",
        introduction: "",
        facebook: "",
        youtube: "",
        instagram: "",
          verifySnsInstagram: "",
          verifySnsYoutube: "",
          verifySnsFacebook: "",
        message: ""
      },
        verifySnsInstagram: false,
        verifySnsYoutube: false,
        verifySnsFacebook: false,
        validSnsInstagram: false,
        validSnsYoutube: false,
        validSnsFacebook: false,
      isActiveSubmit: false,
      isLoading: false,
      isBack: false,
      success: false,
      isShowModal: false,
      oldFBLink: "",
      uid: "",
      isLoadingModal: false,
      isShowModalFalse: false,
      imageBase64: '',
      hasFacebook: false,
      hasYoutube: false,
      hasInstagram: false,
    };
  }

  componentDidMount() {
    this.props.getCode("gender,location,keyword");
    // console.log(window.localStorage.data);
    // let dataLocal = window.localStorage.data
    // console.log(dataLocal);
    // window.localStorage.removeItem('data')
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      if (window.localStorage.data) {
        this.setState({ isLoadingModal: true })
        let data = JSON.parse(window.localStorage.data)
        let imageName = window.localStorage.imageName
        let imageBase64 = data.image
        if (imageName) {
          data.image = dataURLtoFile(data.image, imageName)
        }
        this.setState({
          data: data,
          isShowModal: true,
          imageBase64: imageBase64,
        }, () => {
          let v = window.location.search.match(new RegExp("(?:[?&]code=)([^&]+)"));
          let code = v ? v[1] : null;
          if (code) {
            window.localStorage.removeItem("data");
            window.localStorage.removeItem("imageName");
            return Api.getAccessToken("fistar", "/fi-star/profile/information", code).then(response => {
              return Api.ProvideAccessToken({
                url: this.state.data.facebook,
                token: response.data.access_token
              })
                .then(res => {
                  this.clearCode()
                  console.log(res, '==============');
                  this.setState({
                    isLoading: true
                  },
                  () => {
                    if (res.data.data) {
                      let formData = new FormData();
                      Object.keys(data).map(key => {
                        if (data[key] && data[key].constructor === Array) {
                          data[key].map(item => {
                            formData.append(key + "[]", item);
                          });
                        } else {
                          formData.append(key, data[key]);
                        }
                      });
                      console.log(this.state, '-------------------');
                      return this.props
                        .updateProfile(formData)
                        .then(response => {
                          console.log(response, '/////////////////////////////');
                          this.setState({
                            isShowModal: false,
                            isLoading: false,
                            success: true,
                            oldFBLink: data.facebook,
                          });
                        })
                        .catch((er, code) => {
                          this.setState({
                            isLoading: false,
                            errors: {
                              ...this.state.errors,
                              image:
                                this.props.t("VALIDATE.VLD_img_size_big") +
                                process.env.REACT_APP_MAX_IMAGE_SIZE +
                                "MB."
                            }
                          });
                        });
                    }
                    console.log(111111111111);
                    this.setState({
                      isShowModalFalse: true,
                      isLoadingModal: false
                    })
                  });
                })
                .catch(e => {
                  console.log(234234234);
                  this.setState({
                    isShowModalFalse: true,
                    isLoadingModal: false
                  })
                  return Promise.reject(e);
                });
            });
          }
        })
        return
      }
    }

    this.props.getProfile().then(response => {
      const {
        data: {
          status: { influencer }
        }
      } = response;
      let facebook = "";
      let youtube = "";
      let instagram = "";
      influencer.channel.map(ch => {
        if (ch.sns.sns_name === "facebook") {
          facebook = ch.sns_url;
        }
        if (ch.sns.sns_name === "youtube") {
          youtube = ch.sns_url;
        }
        if (ch.sns.sns_name === "instagram") {
          instagram = ch.sns_url;
        }
      });

      this.setState({
        data: {
          image: influencer && influencer.avatar ? influencer.avatar : "",
          gender: influencer.gender,
          dob: influencer.dob,
          location: influencer.location,
          bank_name:
            !influencer.bank_name || influencer.bank_name === "null"
              ? ""
              : influencer.bank_name,
          bank_branch:
            !influencer.bank_branch || influencer.bank_branch === "null"
              ? ""
              : influencer.bank_branch,
          bank_account:
            !influencer.bank_account_name ||
            influencer.bank_account_name === "null"
              ? ""
              : influencer.bank_account_name,
          bank_account_number:
            !influencer.bank_account_number ||
            influencer.bank_account_number === "null"
              ? ""
              : influencer.bank_account_number,
          swift_code:
            !influencer.bank_swift_code || influencer.bank_swift_code === "null"
              ? ""
              : influencer.bank_swift_code,
          keyword: influencer.keywords.map(e => e.cd_id),
          introduction: influencer.self_intro,
          facebook,
          youtube,
          instagram,
          name: influencer.name,
          id: influencer.id
        },
        oldFBLink: facebook,
        uid: influencer.uid,
        hasFacebook: !!facebook,
        hasYoutube: !!youtube,
        hasInstagram: !!instagram,
      });
    });

    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      console.log(312313123, document.getElementsByClassName('fistar-content')[0]);
      document.getElementsByClassName('fistar-content')[0].classList.add("fistar-content-information")
    }

  }

  clearCode = () => {
    const { location } = this.props
    let path = location.path
    let search = location.search
    search = convertLocationSearchToObject(search)
    search.code = ''
    search = convertObjectToLocationSearch(search)
    this.props.history.push({
      pathname: path,
      search: '?' + search,
    })
  }

  handleChangeFile = file => {
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        image: file
      },
      isActiveSubmit: true,
      errors: {
        ...prevState.errors,
        image:
          file.size > process.env.REACT_APP_MAX_IMAGE_SIZE * 1000000
            ? this.props.t("VALIDATE.VLD_img_size_big") +
              process.env.REACT_APP_MAX_IMAGE_SIZE +
              "MB."
            : ""
      }
    }));
  };

  handleChange = date => {
    console.log(date, '444444444444');
    const { t } = this.props;
    if (date == "") {
      this.setState({
        errors: {
          ...this.state.errors,
          dob: "Birthday field is required!"
        },
        isActiveSubmit: true
      });
    } else {
      console.log((new Date()).getTime(), (new Date(date)).getTime());
      if ((new Date()).getTime() > (new Date(date)).getTime()) {
        this.setState(prevState => ({
          startDate: date,
          data: {
            ...prevState.data,
            dob: format(new Date(date), "YYYY-MM-DD")
          },
          errors: {
            ...this.state.errors,
            dob: ""
          },
          isActiveSubmit: true
        }));
      } else {
        this.setState({
          errors: {
            ...this.state.errors,
            dob: `${t("FISTAR_JOIN.FJN_smaller_than")}`
          },
          isActiveSubmit: true
        });
      }
    }
  };

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      },
      isActiveSubmit: true
    }));
  };

    validUrl(url, type){
        switch(type){
            case 'facebook': return /facebook\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 2: return /facebook\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 'instagram': return /instagram\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 4: return /instagram\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 'youtube': return /youtube\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 3: return /youtube\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            default: return /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm.test(url);
        }

    }

    handleChangeInputSns = (e, type) => {

        console.log(e.currentTarget.name)
        console.log(type)
        if(e.currentTarget.value) {
            var snsValid = this.validUrl(e.currentTarget.value, type);
        }else {
            snsValid=true
        }

        switch (e.currentTarget.name) {
            case "facebook":
                var facebook = e.currentTarget.value;
                if(snsValid == true) {



                    this.setState(prevState => ({
                        data: {
                            ...prevState.data,
                            facebook: facebook
                        },
                        validSnsFacebook: false,

                    }));
                    var data = {};
                    data.sns_id = 2;
                    data.url = facebook
                    if(facebook) {

                        this.props
                            .verifySns(data)
                            .then(response => {
                                console.log(response)


                                if (response.data == false) {
                                    this.setState(prevState => ({
                                        verifySnsFacebook: true,
                                        errors: {
                                            verifySnsFacebook: "Url Facebook is exist"
                                        },

                                    }))
                                }
                                if(response.data == true) {

                                    this.setState(prevState => ({
                                        verifySnsFacebook: false,
                                        isActiveSubmit: true,
                                        errors: {
                                            verifySnsFacebook: ""
                                        },

                                    }))

                                }

                            }).catch(e => {
                            console.log(e)
                        });

                    }else {

                        this.setState(prevState => ({
                            verifySnsFacebook: false,
                        }))

                    }



                }else {
                    console.log('zozozozoz else')
                    this.setState(prevState => ({
                        validSnsFacebook: true,
                        data: {
                            ...prevState.data,
                            facebook: facebook
                        },
                        errors: {
                            verifySnsFacebook: "Url Facebook is invalid"
                        }
                    }))

                }



                break;
            case "youtube":
                var youtube = e.currentTarget.value;

                if(snsValid == true) {

                    this.setState(prevState => ({
                        data: {
                            ...prevState.data,
                            youtube: youtube
                        },
                        validSnsYoutube: false,

                    }));

                    var data = {};
                    data.sns_id = 3;
                    data.url = youtube

                    if(youtube) {

                        this.props
                            .verifySns(data)
                            .then(response => {
                                console.log(response)

                                if (response.data == false) {
                                    this.setState(prevState => ({
                                        verifySnsYoutube: true,

                                        errors: {
                                            verifySnsYoutube: "Url Youtube is exist"
                                        }
                                    }))
                                }
                                if (response.data == true) {
                                    this.setState(prevState => ({
                                        verifySnsYoutube: false,
                                        isActiveSubmit: true,
                                        errors: {
                                            verifySnsYoutube: ""
                                        }
                                    }))
                                }

                            }).catch(e => {
                            console.log(e)
                        });

                    }else {

                        this.setState(prevState => ({
                            verifySnsYoutube: false,
                        }))

                    }


                }else {
                    console.log('zozozozoz else')
                    this.setState(prevState => ({
                        validSnsYoutube: true,
                        data: {
                            ...prevState.data,
                            youtube: youtube
                        },
                        errors: {
                            verifySnsYoutube: "Url Youtube is invalid"
                        }
                    }))

                }



                break;
            case "instagram":
                var instagram = e.currentTarget.value;
                if(snsValid == true) {

                    this.setState(prevState => ({
                        data: {
                            ...prevState.data,
                            instagram: instagram
                        },
                        validSnsInstagram: false,
                        onChange: true
                    }));

                    var data = {};
                    data.sns_id = 4;
                    data.url = instagram

                    if(instagram) {

                        this.props
                            .verifySns(data)
                            .then(response => {
                                console.log(response)

                                if (response.data == false) {
                                    this.setState(prevState => ({
                                        verifySnsInstagram: true,
                                        onChange: false,
                                        errors: {
                                            verifySnsInstagram: "Url instagram is exist"
                                        }
                                    }))
                                }
                                if (response.data == true) {
                                    this.setState(prevState => ({
                                        verifySnsInstagram: false,
                                        isActiveSubmit: true,
                                        errors: {
                                            verifySnsInstagram: ""
                                        }
                                    }))
                                }
                                console.log(this.state)

                            }).catch(e => {
                            console.log(e)
                        });

                    }else {

                        this.setState(prevState => ({
                            verifySnsInstagram: false,
                        }))

                    }

                }else {
                    console.log('zozozozoz else')
                    this.setState(prevState => ({
                        validSnsInstagram: true,
                        data: {
                            ...prevState.data,
                            instagram: instagram
                        },
                        errors: {
                            verifySnsInstagram: "Url Instagram is invalid"
                        }
                    }))

                }



                break;
        }

    };


  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        image: "",
        gender: "",
        dob: "",
        location: "",
        bank_name: "",
        bank_branch: "",
        bank_account: "",
        bank_account_number: "",
        swift_code: "",
        keyword: "",
        introduction: "",
        facebook: "",
        youtube: "",
        instagram: "",
        message: ""
      }
    }));
  };

  onSelectedKeyword = keyword => {
    this.setState({
      data: {
        ...this.state.data,
        keyword
      },
      isActiveSubmit: true
    });
  };

  validateForm = () => {
    const errors = {
      gender: "",
      dob: "",
      location: "",
      bank_name: "",
      bank_branch: "",
      bank_account: "",
      bank_account_number: "",
      swift_code: "",
      keyword: "",
      introduction: "",
      facebook: "",
      youtube: "",
      instagram: "",
        verifySnsInstagram: "",
        verifySnsFacebook: "",
        verifySnsYoutube: "",
      message: ""
    };
    const { data } = this.state;
    const { t } = this.props;
    if (!data.gender) {
      errors.gender = `${t("FISTAR_JOIN.FJN_gender_required")}`;
    }
    if (!data.dob) {
      errors.dob = `${t("FISTAR_JOIN.FJN_erro_date_birth")}`;
    }
    if (this.state.errors.dob) {
      errors.dob = this.state.errors.dob;
    }
    if (!data.location) {
      errors.location = `${t("FISTAR_JOIN.FJN_location_required.")}`;
    }

      if (this.state.verifySnsInstagram) {
          errors.verifySnsInstagram = "Url instagram is exist";
      }
      if (this.state.verifySnsYoutube) {
          errors.verifySnsYoutube = "Url Youtube is exist";
      }
      if (this.state.verifySnsFacebook) {
          errors.verifySnsFacebook = "Url Facebook is exist";
      }

      if (this.state.validSnsInstagram) {
          errors.verifySnsInstagram = "Url instagram is exist";
      }
      if (this.state.validSnsYoutube) {
          errors.verifySnsYoutube = "Url Youtube is exist";
      }
      if (this.state.validSnsFacebook) {
          errors.verifySnsFacebook = "Url Facebook is exist";
      }

    if (data.keyword.length <= 0) {
      errors.keyword = `${t("FISTAR_JOIN.FJN_erro_key_word")}`;
    }
    if (!data.introduction) {
      errors.introduction = `${t("FISTAR_JOIN.FJN_erro_introduction")}`;
    } else if (data.introduction.length > VALIDATION.MAX_LENGTH) {
      errors.introduction = `${t("FISTAR_JOIN.FJN_than_200_characters")}`;
    }
    if (!data.facebook) {
      errors.facebook = `${t("FISTAR_JOIN.FJN_facebook")}`;
    }

    return errors;
  };

  submitFormData = e => {
    e.preventDefault();
    const { data, errors } = this.state;
    const validate = { ...this.state.errors, ...this.validateForm() };
    if (
      validate.image === "" &&
      validate.gender === "" &&
      validate.dob === "" &&
      validate.location === "" &&
      validate.keyword === "" &&
      validate.introduction === "" &&
      validate.facebook === "" &&
      validate.verifySnsFacebook === "" &&
      validate.verifySnsYoutube === "" &&
      validate.verifySnsInstagram === ""
    ) {
      let formData = new FormData();
      Object.keys(data).map(key => {
        if (data[key] && data[key].constructor === Array) {
          data[key].map(item => {
            formData.append(key + "[]", item);
          });
        } else {
          formData.append(key, data[key]);
        }
      });
      console.log(this.state, '-------------------');
      if (this.state.data.facebook !== this.state.oldFBLink) {
        console.log(this.state.data, this.state.oldFBLink);

        this.setState({
          isShowModal: true
        })
        return;
      }
      this.setState(
        {
          isLoading: true
        },
        () => {
          this.props
            .updateProfile(formData)
            .then(response => {
              this.setState({
                isLoading: false,
                success: true,
                hasFacebook: !!this.state.data.facebook,
                hasYoutube: !!this.state.data.youtube,
                hasInstagram: !!this.state.data.instagram,
              });
            })
            .catch((er, code) => {
              this.setState({
                isLoading: false,
                errors: {
                  ...this.state.errors,
                  image:
                    this.props.t("VALIDATE.VLD_img_size_big") +
                    process.env.REACT_APP_MAX_IMAGE_SIZE +
                    "MB."
                }
              });
            });
        }
      );
    } else {
      this.setState({
        errors: validate
      });
    }
  };

  submitForm = e => {
    e.preventDefault();
  };

  onClickBack = () => {
    this.setState({
      isBack: true
    });
  };

  handleClose = () => {
    this.setState({
      success: false,
      // isShowModal: this.state.data.facebook !== this.state.oldFBLink
    });
  };

  handleCloseModalGetToken = () => {
    this.setState({
      isShowModal: false
    });
  };

  callback = () => {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      const { data } = this.state
      const { info } = this.props;
      let userInfo = {...data}
      if (typeof data.image === 'string') {
        window.localStorage.data = JSON.stringify(userInfo)
        window.location.href = REDIRECT_UPDATE_FISTAR
      } else {
        const reader = new FileReader();
        reader.readAsDataURL(userInfo.image);
        reader.onload = () => {
          window.localStorage.imageName = userInfo.image.name
          userInfo.image = reader.result
          window.localStorage.data = JSON.stringify(userInfo)
          window.location.href = REDIRECT_UPDATE_FISTAR
      }

      }
    }
    // this.handleCloseModalGetToken();
  };

  handleShow = () => {
    this.setState({ success: true });
  };

  onExited = () => {
    this.setState({
      isRedirect: true
    });
  };

  render() {
    const {
      t,
      code: {
        data: { keyword }
      }
    } = this.props;
    const { data, errors, isActiveSubmit, isLoading, isBack } = this.state;

    if (isBack) {
      return <Redirect to={routeName.FISTAR_DASHBOARD} />;
    }
    console.log(this.state, 'qqqqqqqqqqqqqqqqqqqqqq');
    return (
      <Fragment>
        <div className="content-mypage-partner">
          <div className="container">
            <div className="wf-row">
              <div className="left image-page">
                <FistarProfileImagePreview
                  imageSrc={this.state.imageBase64 ? this.state.imageBase64 : getImageLink(
                    data.image,
                    IMAGE_TYPE.FISTARS,
                    IMAGE_SIZE.ORIGINAL
                  )}
                  imageAlt={data.name}
                  onChangeImage={this.handleChangeFile}
                />
                {errors.image && <p className="text-danger">{errors.image}</p>}
              </div>
              <div className="right">
                <div className="title-right">
                  <h4>
                  </h4>
                </div>
                <form className="myfistar-form" onSubmit={this.submitForm}>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm"
                    >
                      {t("PROFILE_FISTAR.PFR_label_gender")}
                      <span className="form-obligatory">*</span>
                    </label>
                    <div className="col-sm-9">
                      <FistarGender
                        value={data.gender}
                        error={errors.gender}
                        handleChange={this.handleChangeInput}
                      />
                      {errors.gender && (
                        <p className="text-danger">{errors.gender}</p>
                      )}
                    </div>
                  </div>
                  <div className="form-group row date-of-birth">
                    <label
                      htmlFor="inputPassword"
                      className="col-sm-3 col-form-label"
                    >
                      {t("PROFILE_FISTAR.PFR_label_date_birth")}
                      <span className="form-obligatory">*</span>
                    </label>
                    <div className="col-sm-9 input-date">
                      <Flatpickr
                          options={{
                            wrap: true,
                            disableMobile: "true"
                          }}
                          className="wp-date-icon"
                          value={data.dob}
                          onChange={date => { this.handleChange(format(new Date(date), DATE_FORMAT)) }}
                        >
                          <React.Fragment>
                            <input type="text" placeholder="" data-input />
                            <i class="far fa-calendar i-calendar"></i>
                          </React.Fragment>
                        </Flatpickr>
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm"
                    >
                      {t("PROFILE_FISTAR.PFR_label_country")}
                      <span className="form-obligatory">*</span>
                    </label>
                    <div className="col-sm-9">
                      <FistarLocation
                        value={data.location}
                        error={errors.location}
                        handleChange={this.handleChangeInput}
                      />
                      {errors.location && (
                        <p className="text-danger">{errors.location}</p>
                      )}
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm"
                    >
                      {t("PROFILE_FISTAR.PFR_label_bank_account")}
                    </label>
                    <div className="col-sm-9 check-sns">
                      <div className="col-md-12 col-sns">
                        <div className="input-group">
                          <span className="input-group-text">
                            {t("PROFILE_FISTAR.PFR_bank_name")}
                          </span>
                          <input
                            placeholder="User name(URL)"
                            type="text"
                            className="form-control"
                            id="bank_name"
                            name="bank_name"
                            placeholder=""
                            onChange={this.handleChangeInput}
                            onFocus={this.onFocusInput}
                            value={data.bank_name}
                          />
                        </div>
                      </div>
                      <div className="col-md-12 col-sns">
                        <div className="input-group">
                          <span className="input-group-text">
                            {t("PROFILE_FISTAR.PFR_bank_brankch")}
                          </span>
                          <input
                            placeholder="User name(URL)"
                            type="text"
                            className="form-control"
                            id="bank_branch"
                            name="bank_branch"
                            placeholder=""
                            onChange={this.handleChangeInput}
                            onFocus={this.onFocusInput}
                            value={data.bank_branch}
                          />
                        </div>
                      </div>
                      <div className="col-md-12 col-sns">
                        <div className="input-group">
                          <span className="input-group-text">
                            {t("PROFILE_FISTAR.PFR_account_name")}
                          </span>
                          <input
                            placeholder="User name(URL)"
                            type="text"
                            className="form-control"
                            id="bank_account"
                            name="bank_account"
                            placeholder=""
                            onChange={this.handleChangeInput}
                            onFocus={this.onFocusInput}
                            value={data.bank_account}
                          />
                        </div>
                      </div>
                      <div className="col-md-12 col-sns">
                        <div className="input-group">
                          <span className="input-group-text">
                            {t("PROFILE_FISTAR.PFR_account_number")}
                          </span>
                          <input
                            placeholder="User name(URL)"
                            type="text"
                            className="form-control"
                            id="bank_account_number"
                            name="bank_account_number"
                            placeholder=""
                            onChange={this.handleChangeInput}
                            onFocus={this.onFocusInput}
                            value={data.bank_account_number}
                          />
                        </div>
                      </div>
                      <div className="col-md-12 col-sns">
                        <div className="input-group">
                          <span className="input-group-text">
                            {t("PROFILE_FISTAR.PFR_swift_code")}
                          </span>
                          <input
                            placeholder="User name(URL)"
                            type="text"
                            className="form-control"
                            id="swift_code"
                            name="swift_code"
                            placeholder=""
                            onChange={this.handleChangeInput}
                            onFocus={this.onFocusInput}
                            value={data.swift_code}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm"
                    >
                      {t("PROFILE_FISTAR.PFR_label_keyword")}
                      <span className="form-obligatory">*</span>
                    </label>
                    <div className="col-sm-9 tag">
                      <div className="text-tag col-md-12 col-sns">
                        <p className="text-blue text-red">
                          {t("PROFILE_FISTAR.PFR_title_keyword")}
                        </p>
                        <p>{t("PROFILE_FISTAR.PFR_text_keyword")}</p>
                      </div>
                      <div className="col-md-12 col-sns group-selected group-select-myfistar">
                        <FistarKeyword
                          keywordSelected={data.keyword}
                          onSelectedKeyword={this.onSelectedKeyword}
                        />
                        {errors.keyword && (
                          <p className="text-danger">{errors.keyword}</p>
                        )}
                      </div>
                      <div className="col-md-12 col-sns">
                        <p className="select-of text-center">
                          <span className="text-select-of">
                            {t("PROFILE_FISTAR.PFR_select ")}{" "}
                            {data.keyword.length}
                          </span>{" "}
                          {t("PROFILE_FISTAR.PFR_out_of")}{" "}
                          {keyword ? keyword.code.length : 0}
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm"
                    >
                      {t("PROFILE_FISTAR.PFR_label_introduction")}
                      <span className="form-obligatory">*</span>
                    </label>
                    <div className="col-sm-9 form-introduction">
                      <textarea
                        className="form-control"
                        id="examplejFormdfdgContrfolTextgarea2"
                        rows="5"
                        placeholder={t("PROFILE_FISTAR.PFR_input_introduction")}
                        id="introduction"
                        name="introduction"
                        onChange={this.handleChangeInput}
                        onFocus={this.onFocusInput}
                        value={data.introduction}
                      />
                      {errors.introduction && (
                        <p className="text-danger">{errors.introduction}</p>
                      )}
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm"
                    >
                      {t("PROFILE_FISTAR.PFR_label_sns")}
                    </label>
                    <div className="col-sm-9 check-sns">
                      <div className="col-md-12 col-sns">
                        <div className="input-group" >
                          <span className="input-group-text">
                            Facebook : facebook.com <b> *</b>
                          </span>
                          {this.state.hasFacebook ? (
                            <div className="form-control disable-input">{data.facebook}</div>
                          ) : (
                            <input
                              placeholder={t("PROFILE_FISTAR.PFR_input_sns")}
                              type="text"
                              className="form-control"
                              id="facebook"
                              name="facebook"
                              onChange={(e) =>this.handleChangeInputSns(e, 2)}
                              onFocus={this.onFocusInput}
                              value={data.facebook}
                            />
                          )}


                            {this.state.verifySnsFacebook && (
                                <div className="tooptip-text ">
                                    <p className="text-danger">Url Facebook is exist</p>
                                </div>
                            )}
                            {this.state.validSnsFacebook && (
                                <div className="tooptip-text ">
                                    <p className="text-danger">Url Facebook is invalid</p>
                                </div>
                            )}
                          
                          {errors.facebook && (
                            <p className="text-danger">{errors.facebook}</p>
                          )}
                        </div>
                        <div class="reconnect">
                          <button class="btn btn-primary re-btn">Reconnect Facebook</button>
                        </div>
                      </div>
                      <div className="col-md-12 col-sns">
                        <div className="input-group">
                          <span className="input-group-text">
                            YouTube : youtube.com
                          </span>
                          {this.state.hasYoutube ? (
                            <div className="form-control disable-input">{data.youtube}</div>
                          ) : (
                            <input
                              placeholder={t("PROFILE_FISTAR.PFR_input_sns")}
                              type="text"
                              className="form-control"
                              id="youtube"
                              name="youtube"
                              onChange={(e)=>this.handleChangeInputSns(e, 3)}
                              onFocus={this.onFocusInput}
                              value={data.youtube}
                            />
                          )}

                            {this.state.verifySnsYoutube && (
                                <div className="tooptip-text ">
                                    <p className="text-danger">Url Youtube is exist</p>
                                </div>
                            )}
                            {this.state.validSnsYoutube && (
                                <div className="tooptip-text ">
                                    <p className="text-danger">Url Youtube is invalid</p>
                                </div>
                            )}
                        </div>
                      </div>
                      <div className="col-md-12 col-sns">
                        <div className="input-group">
                          <span className="input-group-text">
                            Instagram : instagram.com
                          </span>
                          {this.state.hasInstagram ? (
                            <div className="form-control disable-input">{data.instagram}</div>
                          ) : (
                            <input
                              placeholder={t("PROFILE_FISTAR.PFR_input_sns")}
                              type="text"
                              className="form-control"
                              id="instagram"
                              name="instagram"
                              onChange={(e)=>this.handleChangeInputSns(e, 4)}
                              onFocus={this.onFocusInput}
                              value={data.instagram}
                            />
                          )}

                            {this.state.verifySnsInstagram && (
                                <div className="tooptip-text ">
                                    <p className="text-danger">Url Instagram is exist</p>
                                </div>
                            )}
                            {this.state.validSnsInstagram && (
                                <div className="tooptip-text ">
                                    <p className="text-danger">Url Instagram is invalid</p>
                                </div>
                            )}
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
                <div className="group-button">
                  <div className="row">
                    <div className="col-md-9 offset-md-3 col-12">
                      <div className="group">
                        <button type="button" onClick={this.onClickBack}>
                          {t("PROFILE_FISTAR.PFR_button_cancel")}
                        </button>
                        {isActiveSubmit ? (
                          <button
                            className="btn apply-button active-hover"
                            onClick={this.submitFormData}
                            type="submit"
                          >
                            {isLoading ? (
                              <div className="spinner-border" role="status">
                                <span className="sr-only">
                                  {t("LOADING.LOADING")}
                                </span>
                              </div>
                            ) : (
                              <Fragment>
                                {t("PROFILE_FISTAR.PFR_button_apply")}
                              </Fragment>
                            )}
                          </button>
                        ) : (
                          <button
                            type={"button"}
                            className={!isActiveSubmit ? `disable-hover` : ""}
                          >
                            {t("PROFILE_FISTAR.PFR_button_apply")}
                          </button>
                        )}
                      </div>
                    </div>
                    {/*<div className="col-md-9 offset-md-3 col-12">
                      <div className="apply">
                        <button className="btn" onClick={this.submitFormData}>Apply</button>
                      </div>
                    </div>*/}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          show={this.state.success}
          onHide={this.handleClose}
          onExited={this.onExited}
          dialogClassName="howto-register modal-dialog-centered"
        >
          <div className="modal-content">
            <div className="register">
              <div className="top modal-header">
                <h4>{t("PROFILE_FISTAR.PFR_popup_fistar")}</h4>
                <button
                  type="button"
                  className="btn btn-close close"
                  onClick={this.handleClose}
                >
                  <img src={cancelImage} alt="close" />
                </button>
              </div>
              <div className="body-popup">
                <p>{t("PROFILE_FISTAR.PFR_popup_description")}</p>
                <button type="button" onClick={this.handleClose}>
                  {t("PROFILE_FISTAR.PFR_button_ok")}
                </button>
              </div>
            </div>
          </div>
        </Modal>
        {this.state.isShowModal && (
          <ModalGetAccessToken
            isShowModal={this.state.isShowModal}
            handleClose={this.handleCloseModalGetToken}
            uid={this.state.uid}
            callback={this.callback}
            isCallback={true}
            isShowModalFalse={this.state.isShowModalFalse}
            isLoadingModal={this.state.isLoadingModal}
          />
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    code: state.code
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCode: type => dispatch(getCodeAction(type)),
    getProfile: () => dispatch(getProfileFistarAction()),
    updateProfile: data =>
      dispatch(updateProfileFistarAction("information", data)),
      verifySns: data => dispatch(checkSnsVerify(data))
  };
};

const loadData = store => {
  // store.dispatch(getCodeAction('gender'))
  // For the connect tag we need Provider component but on the server at this moment app is not rendered yet
  // So we need to use store itself to load data
  // return store.dispatch(fetchArticles(param)); // Manually dispatch a network request
};
FistarProfileInformartion.loadData = loadData;
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarProfileInformartion));
