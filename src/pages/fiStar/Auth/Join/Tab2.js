import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { withRouter } from "react-router";
// import { joinAction } from './../../../../store/actions/auth'
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import {
  checkExistAction,
  fistarJoinAction
} from "./../../../../store/actions/auth";
import { Modal, Button } from "react-bootstrap";
import cancelImage from "./../../../../images/close-popup.svg";
import api from "../../../../api";
import { REDIRECT_JOIN_STEP_2 } from "../../../../constants/index";

class FistarJoinTab2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        isChangeEmail: true,
        email: "",
        password: "",
        password_confirmation: "",

      },
        successProcess:false,
      errors: {
        email: "",
        password: "",
        password_confirmation: "",
        message: ""
      },
      show: false,
      join: false,
      success: false,
      loading: false,
      isAgree: false,
      notice_content: "email",
      isLoadingFB: false
    };

    // if (window.FB) {
    //   window.FB.init({
    //     appId: process.env.REACT_APP_FACEBOOK_FIME_ID,
    //     status: true,
    //     xfbml: true,
    //     version: "v2.3" // or v2.6, v2.5, v2.4, v2.3
    //   });
    // }
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };
    handleChangeInputConfirmPW = e => {
        const { password } = this.state.data;
        const name = e.target.name;
        const value = e.target.value;
      if(password && e.target.value !== password) {
          this.setState(prevState => ({
              data: {
                  ...prevState.data,
                  [name]: value
              },
              show: false,
              errors: {
                  password_confirmation: this.props.t("FISTAR_JOIN.FJN_erro")
              }
          }));
      }else {

          this.setState(prevState => ({
              data: {
                  ...prevState.data,
                  [name]: value
              },

              errors: {
                  email: "",
                  password: "",
                  password_confirmation: "",
                  message: ""
              },
          }));
      }

    };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        id: "",
        email: "",
        message: ""
      }
    }));
  };

  validateEmail = email => {
    // var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //   var re = /^[a-zA-Z0-9\.]{2,}@[a-zA-Z0-9]{2}(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])?)(?=.*[\.])*$/;
      var re = /([a-z]+[a-z0-9]*[_\.]?[a-z0-9]+)@(([a-z0-9]{2,}\.)*[a-z0-9]{2,}\.)+[a-z]{2,}/;
    return re.test(String(email).toLowerCase());
  };

    validateEmailCustomer = email => {
        var re = /^[a-zA-Z0-9]{2,}@[a-zA-Z0-9]{2,}.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])*$/;
        return re.test(String(email).toLowerCase());
    };

  validateForm = () => {
    const errors = {
      email: "",
      password: "",
      password_confirmation: ""
    };
    const { data } = this.state;
    const { t } = this.props;
    if (!data.email) {
      errors.email = `${t("FISTAR_FINDID.FFI_toptip_email")}`;
    } else if (data.email && !this.validateEmail(data.email)) {
      errors.email = `${t("FISTAR_LOGIN.FL_toptip_email")}`;
    }
    // else if (data.email && !this.validateEmailCustomer(data.email)) {
    //     errors.email = `${t("FISTAR_LOGIN.FL_toptip_email")}`;
    // }
    if (!data.password) {
      errors.password = `${t("PARTNER_RESETPW.PRW_erro_forget_new_pw")}`;
    } else if (data.password.length < 6) {
      errors.password = t("FISTAR_JOIN.FJN_erro_pw_limit_6");
    }
    if (!data.password_confirmation) {
      errors.password_confirmation = `${t(
        "PARTNER_RESETPW.PRW_erro_forget_confirm_new_pw"
      )}`;
    } else if (data.password  && data.password !== data.password_confirmation) {
      errors.password_confirmation = `${t("FISTAR_JOIN.FJN_erro")}`;
    }
    return errors;
  };

  submitForm = e => {
    e.preventDefault();
    const validate = this.validateForm();
    const { t } = this.props;
    if (
      validate.email == "" &&
      validate.password == "" &&
      validate.password_confirmation == ""
    ) {
      this.setState(
        {
          loading: true
        },
        () => {
          this.props
            .fistarJoin(this.state.data)
            .then(() => {
              this.setState({
                show: true
              });
            })
            .catch(e => {
              const errors = {
                email: "",
                password: "",
                message: ""
              };
              if (e.data.status == false) {
                errors.email = t("FISTAR_JOIN.FJN_erro_existed_email");
              }
              this.setState(prevState => ({
                errors: errors,
                loading: false
              }));
            });
        }
      );
    } else {
      this.setState({
        errors: validate
      });
    }
  };

  handleChangeCheckbox = e => {
    const name = e.target.name;
    const checked = e.target.checked;
    this.setState({
      isAgree: checked
    });
  };

  componentDidMount() {
    let v = this.props.location.search.match(new RegExp("(?:[?&]code=)([^&]+)"));
    let code = v ? v[1] : null;
    if (code) {
      this.setState({isLoadingFB: true, isAgree: true})
      api.getAccessToken("fime", "/fi-star/join?tab=2", code).then(res => {
        this.props.responseFacebook(res.data.access_token).catch(error => {
          if (error.status === 403) {
            if (error.data.message != "Not exist fi:star account") {
              if (error.data.data === false) {
                let data = {
                  name: res.data.name,
                  sns_id: res.data.id
                };
                this.props.join(data, "fistar", true, true, true);
                return;
              } else {
                let result = error.data.data;
                let data = {
                  name: result.REG_NAME,
                  id: result.ID,
                  uid: result.USER_NO
                };
                this.props.join(data, "fistar", !result.ID, true, true);
              }
            } else {
              this.props.clearCode()
              this.setState({
                notice_content: "facebook",
                show: true,
                isLoadingFB: false
              });
            }
          }
          if (error.status == 402) {
            this.props.clearCode()
            this.setState({
              errors: {
                ...this.state.errors,
                facebook: this.props.t("FISTAR_JOIN.FJN_erro_facebook_register_lock")
              },
              isLoadingFB: false
            });
          }
        });
      });
    }
  }

  responseFacebook = e => {
    // this.props.responseFacebook(e.accessToken).catch(error => {
    //   if (error.status === 403) {
    //     if (error.data.message != "Not exist fi:star account") {
    //       if (error.data.data === false) {
    //         let data = {
    //           name: e.name,
    //           sns_id: e.id
    //         };
    //         this.props.join(data, "fistar", true, true, true);
    //         return;
    //       } else {
    //         let result = error.data.data;
    //         let data = {
    //           name: result.REG_NAME,
    //           id: result.ID,
    //           uid: result.USER_NO
    //         };
    //         this.props.join(data, "fistar", !result.ID, true, true);
    //       }
    //     } else {
    //       this.setState({
    //         notice_content: "facebook",
    //         show: true
    //       });
    //     }
    //   }
    // });
  };

  handleClose = () => {
    this.setState({ show: false, loading: false, notice_content: "email" });
  };

  handleShow = () => {
    this.setState({ show: true });
  };
  handleJoin = () => {
    if (this.state.join) {
      this.props.join(this.state.data, "fistar", true, true);
    }
  };

  renderLoginFB = () => (
    <a
      href={REDIRECT_JOIN_STEP_2}
    >
      <button className="btn btn-submit-facebook text-center">
        <i className="fab fa-facebook-f" />
        {this.props.t("FISTAR_JOIN.FJN_sign_facebook")}
      </button>
    </a>
  );

  renderLoginFBLoading = () => (
    <button className="btn btn-submit-facebook text-center">
      <i className="fab fa-facebook-f" />
      <div className="spinner-border" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </button>
  );

  render() {
    const { t } = this.props;
    const { data, errors, success, loading, isAgree, isLoadingFB } = this.state;
    return (
      <Fragment>
        <div className="submit-face">
          {isAgree ? (
            <Fragment>
              {isLoadingFB && this.renderLoginFBLoading()}
              {!isLoadingFB && this.renderLoginFB()}
              {errors.facebook && <p className="text-danger">{errors.facebook}</p>}
            </Fragment>
          ) : (
            <button
              className="btn btn-submit-facebook text-center"
              type="button"
              style={{ background: "#b5b5b5" }}
            >
              <i className="fab fa-facebook-f" />
              {t("FISTAR_JOIN.FJN_button_join_id")}
            </button>
          )}
          <div className="form-check-face">
            <div className="check-detail-custom mb-3">
              <input
                type="checkbox"
                name="Sass"
                id="agree"
                checked={isAgree}
                onChange={this.handleChangeCheckbox}
              />
              <label htmlFor="agree" className="label-all">
                {t("FISTAR_JOIN.FJN_checked")}
              </label>
            </div>
          </div>
        </div>
        <div className="wf-form-content form-sub-register mt-4">
          <div className="row">
            <div className="col-md-9">
              <div className="text-register">
                <p className="text-left mt-2 mb-4">
                  <a href="javascript:void(0)">
                    {t("FISTAR_JOIN.FJN_join_via_email")}
                  </a>
                </p>
              </div>
            </div>
          </div>
          <form onSubmit={this.submitForm}>
            <div className="row">
              <div className="col-md-9">
                <div className="form-input form-comfirm-register">
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fas fa-envelope" />
                      </span>
                    </div>
                    <input
                      placeholder={t("FISTAR_JOIN.FJN_input_email")}
                      type="text"
                      className={`form-input empty${
                        errors.email ? " is-invalid" : ""
                      }`}
                      id="email"
                      name="email"
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                    />
                  </div>
                  {errors.email && (
                    <p className="text-danger">{errors.email}</p>
                  )}
                  <div className="input-group mt-3">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fas fa-lock-open" />
                      </span>
                    </div>
                    <input
                      placeholder={t("FISTAR_JOIN.FJN_input_password")}
                      type="password"
                      className={`form-input empty mb-0${
                        errors.password ? " is-invalid" : ""
                      }`}
                      id="password"
                      name="password"
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                    />
                  </div>
                  {errors.password && (
                    <p className="text-danger">{errors.password}</p>
                  )}
                  <div className="input-group mt-3">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fas fa-lock-open" />
                      </span>
                    </div>
                    <input
                      placeholder={t("FISTAR_JOIN.FJN_confirm_password")}
                      type="password"
                      className={`form-input empty mb-0${
                        errors.password_confirmation ? " is-invalid" : ""
                      }`}
                      id="password_confirmation"
                      name="password_confirmation"
                      onChange={this.handleChangeInputConfirmPW}
                      onFocus={this.onFocusInput}
                    />
                  </div>
                  {errors.password_confirmation && (
                    <p class="text-danger">{errors.password_confirmation}</p>
                  )}
                </div>
              </div>
              <div className="col-md-3 sub-face">
                <button
                  className="btn btn-submit btn-submit-confirm"
                  onClick={this.submitForm}
                >
                  {loading ? (
                    <div className="spinner-border" role="status">
                      <span className="sr-only">{t("LOADING.LOADING")}</span>
                    </div>
                  ) : (
                    t("FISTAR_JOIN.FJN_button_join")
                  )}
                </button>
              </div>
            </div>
          </form>
        </div>
        <Modal
          show={this.state.show}
          onHide={this.handleClose}
          dialogClassName="howto-register modal-dialog-centered"
        >
          <div className="modal-content">
            <div className="register">
              <div className="top modal-header">
                <h4>{t("FISTAR_JOIN.PJN_fistar")}</h4>
                <button
                  type="button"
                  className="btn btn-close close"
                  onClick={this.handleClose}
                >
                  <img src={cancelImage} alt="close" />
                </button>
              </div>
              <div className="body-popup">
                {this.state.notice_content == "email" ? (
                  <React.Fragment>
                    <p className="mb-3">
                      {t("FISTAR_JOIN.FJN_notice_cofirm_mail")}
                    </p>
                    <p>{t("FISTAR_JOIN.FJN_notice_please_check")}</p>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <p className="mb-3">
                      {t("FISTAR_JOIN.FJN_notice_already_account")}
                    </p>
                  </React.Fragment>
                )}
                <button type="button" onClick={this.handleClose}>
                  {t("FISTAR_JOIN.PJN_ok")}
                </button>
              </div>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    checkExist: (name, value) => dispatch(checkExistAction(name, value)),
    fistarJoin: data => dispatch(fistarJoinAction(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(withRouter(FistarJoinTab2)));
