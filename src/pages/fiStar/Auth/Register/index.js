import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import {
  clearRegisterInfo,
  registerAction,
  joinAction
} from "./../../../../store/actions/auth";
import FiStarAuthFooter from "./../../../../components/fiStar/Auth/Footer";
import * as routeName from "./../../../../routes/routeName";
import FiStarRegisterStep1 from "./Step1";
import FiStarRegisterStep2 from "./Step2";
import FiStarRegisterStep3 from "./Step3";
import ModalGetAccessToken from "./../../../../components/fiStar/Auth/ModalGetAccessToken";
import Api from "../../../../api";
import {
  dataURLtoFile,
  convertLocationSearchToObject,
  convertObjectToLocationSearch,
  createFormData
} from "../../../../common/helper";
import {
  REDIRECT_REGISTER_FISTAR,
} from "../../../../constants/index";

class FiStarRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
      },
      errors: {
        email: "",
        password: "",
        message: ""
      },
      step: 1,
      isHasInfo: true,
      uid: "",
      isNew: "",
      isChangeName: false,
      isShowModal: false,
      isShowModalFalse: false,
      allowRegister: false,
      registerFormData: {},
      isLoadingModal: false,
    };
  }

  clearCode = () => {
    const { location } = this.props
    let path = location.path
    let search = location.search
    search = convertLocationSearchToObject(search)
    search.code = ''
    search = convertObjectToLocationSearch(search)
    this.props.history.push({
      pathname: path,
      search: '?' + search,
    })
  }

  componentDidMount() {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      if (window.localStorage.register && window.localStorage.data) {
        this.setState({ isLoadingModal: true })
        // get value from local storage
        let register = JSON.parse(window.localStorage.register)
        let data = JSON.parse(window.localStorage.data)
        let imageName = window.localStorage.imageName
        // delete localstorage
        window.localStorage.removeItem("register");
        window.localStorage.removeItem("data");
        window.localStorage.removeItem("imageName");
        register.data = data
        register.data.image = dataURLtoFile(register.data.image, imageName)
        this.props.join(
          register.data,
          register.type,
          register.isChangeID,
          register.isChangeEmail,
          register.isNewPass,
          register.isSkipStep
        )
        this.setState({
          registerFormData: register.data,
          isShowModal: true,
          step: 2,
          isHasInfo: true,
        }, () => {
          let v = window.location.search.match(new RegExp("(?:[?&]code=)([^&]+)"));
          let code = v ? v[1] : null;
          if (code) {
            return Api.getAccessToken("fistar", "/fi-star/register", code).then(response => {
              return Api.ProvideAccessToken({
                url: this.state.registerFormData.channels.facebook.value,
                token: response.data.access_token
              })
                .then(res => {
                  this.clearCode()
                  if (res.data.data) {
                    // Submit register neu trung khop
                    let userInfo = this.state.registerFormData;
                    if (Object.keys(userInfo).length > 0) {
                      userInfo.access_token = response.data.access_token
                      let formData = createFormData(userInfo, ['image'])
                      // new FormData();
                      // Object.keys(userInfo).map(key => {
                      //   if (userInfo[key].constructor === Array) {
                      //     userInfo[key].map(item => {
                      //       formData.append(key + "[]", item);
                      //     });
                      //   } else {
                      //     formData.append(key, userInfo[key]);
                      //   }
                      // });
                      // formData.append("access_token", response.data.access_token);
                      return this.props.register(formData).then(res => {
                        this.setState({
                          step: 3,
                          isShowModal: false
                        });
                        return Promise.resolve(res);
                      });
                    }
                  }
                  this.setState({
                    isShowModalFalse: true,
                    isLoadingModal: false
                  })
                  return Promise.reject();
                })
                .catch(e => {
                  this.setState({
                    isShowModalFalse: true,
                    isLoadingModal: false
                  })
                  return Promise.reject(e);
                });
            });
          }
        })
        return
      }
      // delete localstorage
      window.localStorage.removeItem("register");
      window.localStorage.removeItem("data");
      window.localStorage.removeItem("imageName");
    }
    const { info, location } = this.props;
    let search = location.search;
    search = convertLocationSearchToObject(search)
    let req = "";
    if (search.req) {
      req = search.req;
      req = req.split("&")[0];
    }
    if (!info.data && !req) {
      this.setState({
        isHasInfo: false
      });
    } else {
      this.setState(
        {
          step: info.isSkipStep ? 2 : this.state.step
        },
        () => {}
      );
    }
    if (info.data && !info.data.uid) {
      this.setState({ isNew: 1 });
    }
    if (req) {
      this.setState(
        {
          data: {
            isChangeEmail: true,
            email: atob(req.split(".")[0]),
            password: atob(req.split(".")[1]),
            password_confirmation: atob(req.split(".")[1])
          },
          isChangeName: true
        },
        () => {
          let isChangeID = true
          let isChangeEmail = false
          let isNewPass = false
          let isSkipStep = false
          this.props.join(this.state.data, "fistar", isChangeID, isChangeEmail, isNewPass, isSkipStep);
        }
      );
    }
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        email: "",
        password: "",
        message: ""
      }
    }));
  };

  submitForm = e => {
    e.preventDefault();
    const { data } = this.state;
    this.props.login(data).catch(e => {
      const errors = {
        email: "",
        password: "",
        message: ""
      };
      // if (e.data.errors) {
      //   e.data.errors.email ? errors.email = e.data.errors.email[0] : ''
      //   e.data.errors.password ? errors.password = e.data.errors.password[0] : ''
      // }
      // e.data.message ? errors.message = e.data.message : ''
      this.setState({
        errors
      });
    });
  };

  clickNextStep = (step, data) => {
    if (step == 1) {
      this.setState({
        step: step
      });
    } else if (step == 2) {
      // return this.props.register({...this.state.data, ...data})
      //   .then(() => {
      this.setState(prevState => ({
        step: step,
        data: {
          ...prevState.data,
          ...data
        }
      }));
      // })
    } else if (step == 3) {
      //XU LY REGISTER
      const { info } = this.props;
      let allowSubmit = false;
      let userInfo = { ...info.data, ...this.state.data, ...data };
      console.log(userInfo, '--------------------');

      // if (window && typeof window !== undefined) {

      //   const reader = new FileReader();
      //   reader.readAsDataURL(userInfo.image);
      //   reader.onload = () => {
      //     window.localStorage.imageName = userInfo.image.name
      //     userInfo.image = reader.result
      //     let register = {
      //       data: {},
      //       ...info
      //     }
      //     window.localStorage.data = JSON.stringify(userInfo)
      //     window.localStorage.register = JSON.stringify(register)
      //   };
      // }

      //1-Goi Modal cap accesstoken
      this.setState({
        isShowModal: true,
        registerFormData: userInfo
      });

      // return this.props.register(formData).then(response => {
      //   this.setState({
      //     uid: response.data.data.uid,
      //     isShowModal: true,
      //     data: {
      //       ...this.state.data,
      //       ...data
      //     }
      //   });
      // });
    }
  };

  renderStep = () => {
    const { step } = this.state;
    return (
      <Fragment>
        <div className="title">
          <p>
            {" "}
            <span>fi :Star JOIN</span> Show your special talent on fi :Star!
          </p>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="content-step">
              <div className={step < 1 ? "item-circle" : "item-circle active"}>
                <label>Basic</label>
              </div>
              <span />
              <div className={step < 2 ? "item-circle" : "item-circle active"}>
                <label>Details</label>
              </div>
              <span />
              <div className={step < 3 ? "item-circle" : "item-circle active"}>
                <label>Completed</label>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  };

  completeStep = () => {
    this.props.clearRegisterData();
  };

  callback = e => {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
      const { registerFormData } = this.state
      const { info } = this.props;
      let userInfo = {...registerFormData}
      const reader = new FileReader();
      reader.readAsDataURL(userInfo.image);
      reader.onload = () => {
        window.localStorage.imageName = userInfo.image.name
        userInfo.image = reader.result
        let register = {
          data: {},
          ...info
        }
        window.localStorage.data = JSON.stringify(userInfo)
        window.localStorage.register = JSON.stringify(register)

        window.location.href = REDIRECT_REGISTER_FISTAR
      }
    }
    // let sns = new FormData();
    // sns.append("url", this.state.registerFormData.facebook);
    // sns.append("token", e.accessToken);
    // // Kiem tra trung khop access_token va URL
    // return Api.ProvideAccessToken({
    //   url: this.state.registerFormData.facebook,
    //   token: e.accessToken
    // })
    //   .then(res => {
    //     if (res.data.data) {
    //       //Submit register neu trung khop
    //       let formData = new FormData();
    //       let userInfo = this.state.registerFormData;
    //       if (Object.keys(userInfo).length > 0) {
    //         Object.keys(userInfo).map(key => {
    //           if (userInfo[key].constructor === Array) {
    //             userInfo[key].map(item => {
    //               formData.append(key + "[]", item);
    //             });
    //           } else {
    //             formData.append(key, userInfo[key]);
    //           }
    //         });
    //         formData.append("access_token", e.accessToken);
    //         return this.props.register(formData).then(res => {
    //           this.setState({
    //             step: 3,
    //             isShowModal: false
    //           });
    //           return Promise.resolve(res);
    //         });
    //       }
    //     }
    //     return Promise.reject(e);
    //   })
    //   .catch(e => {
    //     return Promise.reject(e);
    //   });
  };

  handleClose = () => {
    this.setState({ isShowModal: false });
  };

  renderStepForm = () => {
    const { step, data, isChangeName } = this.state;
    const { isChangeID, isChangeEmail } = this.props;

    switch (step) {
      case 1:
        return (
          <FiStarRegisterStep1
            data={data}
            onNextStep={this.clickNextStep}
            isChangeEmail={isChangeEmail}
            isChangeID={isChangeID}
            isChangeName={isChangeName}
          />
        );
        break;
      case 2:
        return (
          <FiStarRegisterStep2 data={data} onNextStep={this.clickNextStep} />
        );
        break;
      case 3:
        return (
          <FiStarRegisterStep3
            data={data}
            onNextStep={this.clickNextStep}
            completeStep={this.completeStep}
          />
        );
        break;

      default:
        return null;
        break;
    }
  };

  render() {
    const { t } = this.props;
    const { data, errors, isHasInfo } = this.state;

    if (!isHasInfo) {
      return <Redirect to={routeName.FISTAR_JOIN} />;
    }

    return (
      <Fragment>
        {this.renderStep()}
        {this.renderStepForm()}
        {this.state.isShowModal && (
          <ModalGetAccessToken
            isShowModal={this.state.isShowModal}
            handleClose={this.handleClose}
            uid={this.state.uid}
            callback={this.callback}
            isRegister={1}
            isNew={this.state.isNew}
            isShowModalFalse={this.state.isShowModalFalse}
            isLoadingModal={this.state.isLoadingModal}
            url={REDIRECT_REGISTER_FISTAR}
            isCallback={true}
          />
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    info: state.register
  };
};

const mapDispatchToProps = dispatch => {
  return {
    register: data => dispatch(registerAction(data)),
    clearRegisterData: () => dispatch(clearRegisterInfo()),
    join: (
      data,
      type,
      isChangeID = null,
      isChangeEmail = null,
      isNewPass = false,
      isSkipStep = false
    ) =>
      dispatch(
        joinAction(data, type, isChangeID, isChangeEmail, isNewPass, isSkipStep)
      )
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FiStarRegister));
