import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { checkExistAction, joinAction } from "./../../../../store/actions/auth";
import FiStarAuthFooter from "./../../../../components/fiStar/Auth/Footer";
import closePopup from "./../../../../images/close-popup.svg";
import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import { VALIDATION } from './../../../../constants/index'

class FiStarRegisterStep1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        name:
          props.info.data && props.info.data.name ? props.info.data.name : "",
        id: props.info.data && props.info.data.id ? props.info.data.id : "",
        uid: props.info.data && props.info.data.uid ? props.info.data.uid : "",
        status_message:
          props.info.data && props.info.data.status_message
            ? props.info.data.status_message
            : "",
        contact:
          props.info.data && props.info.data.contact
            ? props.info.data.contact
            : "",
        email:
          props.info.data && props.info.data.email ? props.info.data.email : "",
        address:
          props.info.data && props.info.data.address
            ? props.info.data.address
            : ""
      },
      show: false,
      redirectHome: false,
      errors: {
        name: "",
        id: "",
        status_message: "",
        contact: "",
        email: "",
        address: "",
        message: ""
      },
      existID: !props.info.isChangeID ? 2 : 0,
      isLoadingCheckExistID: false,
      existEmail: !props.info.isChangeEmail ? 2 : 0,
      isLoadingCheckExistEmail: false,
      isRedirectToJoin: false,
      isLoadingNextStep: false,
      isNewPass: props.info.isNewPass
    };
  }

  // componentDidMount() {
  //   window.addEventListener("beforeunload", this.handleLeavePage);
  // }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.info.data &&
      this.props.info.data.email &&
      !prevProps.info.data
    ) {
      this.setState({
        data: {
          ...this.state.data,
          ...this.props.info.data
        }
      });
    }
  }

  handleLeavePage(e) {
    e.preventDefault();
    const confirmationMessage = "load page ";

    e.returnValue = confirmationMessage;
    return confirmationMessage;
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  handleChangeInputNumberOnly = e => {
    const name = e.target.name;
    const value = e.target.value;
    if (/^\d*$/.test(value)) {
      this.setState(prevState => ({
        data: {
          ...prevState.data,
          [name]: value
        }
      }));
    }
  };

  validateNumber = event => {
    const { data } = this.state;

    var invalidChars = ["-", "+", "e", "."];

    if (invalidChars.includes(event.key)) {
      event.preventDefault();
    }
  };

  onFocusInput = e => {
    const name = e.target.name;
    const existID = this.state.existID !== 2 || name === "id" ? 0 : 2;
    const existEmail = this.state.existEmail !== 2 || name === "email" ? 0 : 2;
    this.setState(prevState => ({
      errors: {
        name: "",
        id: "",
        status_message: "",
        contact: "",
        email: "",
        address: "",
        message: ""
      },
      existID,
      existEmail
    }));
  };

  checkExistID = () => {
    const { data } = this.state;
    if (data.id) {

        if (!this.validateId(data.id)) {
            this.setState(prevState => ({
                existID: 1,
                errors: {
                    ...prevState.errors,
                    id: this.props.t("FISTAR_JOIN.FJN_erro_id_symbol_special")
                }
            }));
            return;
        }else if (data.id && /[\!\#\$\%\{\}\^\&\*\(\)\<\>\=\+\/\.\,\`\~\"\?\[\]\|]/.test(data.id)
        ) {
            this.setState(prevState => ({
                existID: 1,
                errors: {
                    ...prevState.errors,
                    id: this.props.t("FISTAR_JOIN.FJN_erro_id_symbol_special")
                }
            }));
            return;
        }

      this.setState(
        {
          isLoadingCheckExistID: true
        },
        () => {
          if (data.id.length > 50) {
            this.setState(prevState => ({
              existID: 1,
              isLoadingCheckExistID: false,
              errors: {
                ...prevState.errors,
                id: this.props.t("FISTAR_FINDID.FFI_toptip_id_limit")
              }
            }));
          } else {
            this.props
              .checkExist("id", data.id)
              .then(() => {
                this.setState(prevState => ({
                  existID: 1,
                  isLoadingCheckExistID: false,
                  errors: {
                    ...prevState.errors,
                    id: this.props.t("FISTAR_JOIN.FJN_erro_ID_existed")
                  }
                }));
              })
              .catch(() => {
                this.setState(prevState => ({
                  existID: 2,
                  isLoadingCheckExistID: false
                }));
              });
          }
        }
      );
    } else {
      this.setState(prevState => ({
        existID: 1,
        errors: {
          ...prevState.errors,
          id: this.props.t("FISTAR_JOIN.FJN_erro_ID")
        }
      }));
    }
  };

  checkExistEmail = () => {
    const { data } = this.state;
    if (data.email) {
      if (!this.validateEmail(data.email)) {
        this.setState(prevState => ({
          existID: 1,
          errors: {
            ...prevState.errors,
            email: this.props.t("FISTAR_JOIN.FJN_erro_email_valid")
          }
        }));
        return;
      }


      this.setState(
        {
          isLoadingCheckExistEmail: true
        },
        () => {
          this.props
            .checkExist("email", data.email)
            .then(() => {
              this.setState(prevState => ({
                existEmail: 1,
                isLoadingCheckExistEmail: false,
                errors: {
                  ...prevState.errors,
                  email: this.props.t("FISTAR_JOIN.FJN_erro_existed_email")
                }
              }));
            })
            .catch(() => {
              this.setState(prevState => ({
                existEmail: 2,
                isLoadingCheckExistEmail: false
              }));
            });
        }
      );
    } else {
      this.setState(prevState => ({
        existID: 1,
        errors: {
          ...prevState.errors,
          email: this.props.t("FISTAR_JOIN.FJN_erro_email")
        }
      }));
    }
  };

  validateEmail = email => {
    // var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //   var re = /^[a-zA-Z0-9\.]{2,}@[a-zA-Z0-9]{2}(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])?)(?=.*[\.])*$/;
      var re = /([a-z]+[a-z0-9]*[_\.]?[a-z0-9]+)@(([a-z0-9]{2,}\.)*[a-z0-9]{2,}\.)+[a-z]{2,}/;
    return re.test(String(email).toLowerCase());
  };
    validateId = id => {
        var re = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/;
        return re.test(String(id).toLowerCase());
    };
    validateEmailCustomer = email => {
        var re = /^[a-zA-Z0-9]{2,}@[a-zA-Z0-9]{2,}.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])*$/;
        return re.test(String(email).toLowerCase());
    };

    handleChangeInputConfirmPW = e => {
        const {password} = this.state.data;
        const name = e.target.name;
        const value = e.target.value;
        if(password && e.target.value !== password) {
            this.setState(prevState => ({
                data: {
                    ...prevState.data,
                    [name]: value
                },
                errors: {
                    ...prevState.errors,
                    password_confirmation: this.props.t("FISTAR_JOIN.FJN_erro")
                }
            }));
        }else {

            this.setState(prevState => ({
                data: {
                    ...prevState.data,
                    [name]: value
                },
                errors: {
                    email: "",
                    password: "",
                    password_confirmation: "",
                    message: ""
                },
            }));
        }

    };

  validateForm = () => {
    const errors = {
      name: "",
      id: "",
      status_message: "",
      contact: "",
      email: "",
      address: "",
      message: "",
      password: "",
      password_confirmation: ""
    };
    const { data } = this.state;
    const { t } = this.props;
    if (!data.name) {
      errors.name = t("FISTAR_JOIN.FJN_erro_name");
    } else if (data.name && data.name.length > 100) {
      errors.name = t("FISTAR_JOIN.FJN_erro_name_limit_100");
    } else if (data.name && /[\!\@\#\$\%\{\}\^\&\*\(\)\<\>\_\-\=\+\/\.\,\`\~\"\?\[\]\|]/.test(data.name)
    ) {
        errors.name = t("FISTAR_JOIN.FJN_erro_name_symbol_special");
    }
    if (data.status_message && data.status_message.length > VALIDATION.MAX_LENGTH) {
      errors.status_message = `${t("FISTAR_JOIN.FJN_status_message_limit")}`;
    }
    if (!data.id) {
      errors.id = `${t("FISTAR_FINDID.FFI_toptip_id")}`;
    } else if (data.id && data.id.length > 50) {
      errors.id = `${t("FISTAR_FINDID.FFI_toptip_id_limit")}`;
    }else if (data.id && /[\!\#\$\%\{\}\^\&\*\(\)\<\>\=\+\/\.\,\`\~\"\?\[\]\|]/.test(data.id)
    ) {
        errors.id = t("FISTAR_JOIN.FJN_erro_id_symbol_special");
    }else if (data.id && !this.validateId(data.id)
    ) {
        errors.id = t("FISTAR_JOIN.FJN_erro_id_symbol_special");
    }


    if (!data.contact) {
      errors.contact = t("FISTAR_JOIN.FJN_erro_contact");
    } else if (data.contact && data.contact.length < 9) {
      errors.contact = t("FISTAR_JOIN.FJN_erro_contact_limit_9");
    } else if (data.contact && data.contact.length > 15) {
      errors.contact = t("FISTAR_JOIN.FJN_erro_contact_limit_15");
    }
    if (!data.email) {
      errors.email = t("FISTAR_JOIN.FJN_erro_email");
    } else if (data.email && !this.validateEmail(data.email)) {
      errors.email = `${t("FISTAR_LOGIN.FL_toptip_email")}`;
    }
    if (!data.address) {
      errors.address = t("FISTAR_JOIN.FJN_erro_address");
    } else if (data.address && data.address.length > VALIDATION.MAX_LENGTH) {
      errors.address = t("FISTAR_JOIN.FJN_erro_address_limit");
    } else if (
      data.address &&
      /[\!\@\#\$\%\{\}\^\&\*\(\)]/.test(data.address)
    ) {
      errors.address = t("FISTAR_JOIN.FJN_erro_address_symbol_special");
    }
    if (this.state.isNewPass) {
      if (!data.password) {
        errors.password = t("FISTAR_JOIN.FJN_erro_pw");
      } else if (data.password.length < 6) {
        errors.password = t("FISTAR_JOIN.FJN_erro_pw_limit_6");
      }
      if (!data.password_confirmation) {
        errors.password_confirmation = t(
          "FISTAR_JOIN.FJN_erro_confirm-password"
        );
      } else if (data.password_confirmation && data.password !== data.password_confirmation) {
        errors.password_confirmation = t(
          "FISTAR_JOIN.FJN_erro_confirm-password-match"
        );
      }
    }
    return errors;
  };

  onNextStep = () => {
    const { data, errors, existID, existEmail } = this.state;
    const validate = this.validateForm();
    if (
      !validate.id &&
      !validate.name &&
      !validate.contact &&
      !validate.email &&
      !validate.address &&
      !validate.password &&
      !validate.password_confirmation &&
      !validate.status_message
      // && ((this.props.info.data && this.props.info.data.id) || existID === 2)
      // && ((this.props.info.data && this.props.info.data.email) || existEmail === 2)
    ) {
      if (existID === 2 && existEmail == 2) {
        this.props.join(data, "fistar");
        this.props.onNextStep(2, data);
        return;
      }
      this.setState({
        isLoadingNextStep: true
      });
      let promise = [];
      if (existID !== 2) {
        promise.push(this.props.checkExist("id", data.id));
        this.setState(
          {
            isLoadingCheckExistID: true
          },
          () => {
            this.props
              .checkExist("id", data.id)
              .then(() => {
                this.setState(prevState => ({
                  existID: 1,
                  isLoadingCheckExistID: false,
                  isLoadingNextStep: false,
                  errors: {
                    ...prevState.errors,
                    id: this.props.t("FISTAR_JOIN.FJN_erro_ID_existed")
                  }
                }));
              })
              .catch(() => {
                this.setState(
                  {
                    existID: 2,
                    isLoadingCheckExistID: false,
                    isLoadingNextStep: false
                  },
                  () => {
                    if (
                      this.state.existEmail === 2 &&
                      this.state.existID === 2
                    ) {
                      this.props.join(data, "fistar");
                      this.props.onNextStep(2, data);
                      return;
                    }
                  }
                );
              });
          }
        );
      }
      if (existEmail !== 2) {
        promise.push(this.props.checkExist("email", data.email));

          if (!this.validateEmail(data.email)) {
              this.setState(prevState => ({
                  existID: 1,
                  errors: {
                      ...prevState.errors,
                      email: this.props.t("FISTAR_JOIN.FJN_erro_email_valid")
                  }
              }));
              return;
          }



        this.setState(
          {
            isLoadingCheckExistEmail: true
          },
          () => {
            this.props
              .checkExist("email", data.email)
              .then(res => {
                this.setState(prevState => ({
                  existEmail: 1,
                  isLoadingCheckExistEmail: false,
                  isLoadingNextStep: false,
                  errors: {
                    ...prevState.errors,
                    email: this.props.t("FISTAR_JOIN.FJN_erro_existed_email")
                  }
                }));
              })
              .catch(e => {
                this.setState(
                  {
                    existEmail: 2,
                    isLoadingCheckExistEmail: false,
                    isLoadingNextStep: false
                  },
                  () => {
                    if (
                      this.state.existEmail === 2 &&
                      this.state.existID === 2
                    ) {
                      this.props.join(data, "fistar");
                      this.props.onNextStep(2, data);
                      return;
                    }
                  }
                );
              });
          }
        );
      }
    } else {
      this.setState({
        errors: validate,
        isLoadingNextStep: false
      });
    }
  };

  onPreviousStep = () => {
    // this.setState({
    //   isRedirectToJoin: true
    // });
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  handleShow = () => {
    this.setState({ show: true });
  };
  redirectHome = () => {
    this.setState({ redirectHome: true });
  };

  render() {
    const { t, info } = this.props;
    const {
      data,
      errors,
      existID,
      existEmail,
      isLoadingCheckExistID,
      isLoadingCheckExistEmail,
      isRedirectToJoin,
      isLoadingNextStep,
      redirectHome,
      isNewPass
    } = this.state;

    if (isRedirectToJoin) {
      return <Redirect to={"/fi-star/join"} />;
    }
    if (redirectHome) {
      return <Redirect to={"/"} />;
    }
    return (
      <Fragment>
        <div className="row">
          <div className="col-md-12">
            <div className="note-form">
              <p>
                {t("FISTAR_JOIN.FJN_title_step")}{" "}
                <span>{t("FISTAR_JOIN.FJN_title_fime")}</span>
              </p>
              <span className="right">{t("FISTAR_JOIN.FJN_required")}</span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <form>
              <div className="form-group row">
                <label
                  htmlFor="colFormLabelSm"
                  className="col-sm-3 col-form-label col-form-label-sm label-join"
                >
                  {t("FISTAR_JOIN.FJN_label_name")}
                  <span>*</span>
                </label>
                <div className="col-sm-9">
                  {/*{!info.data || !info.data.name ? (*/}
                  {this.props.isChangeName ? (
                    <input
                      type="text"
                      className={`form-control form-control-sm${
                        errors.name ? " is-invalid" : ""
                      }`}
                      id="name"
                      name="name"
                      placeholder=""
                      value={data.name}
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                    />
                  ) : (
                    <input
                      type="text"
                      className={`form-control form-control-sm`}
                      id="name"
                      name="name"
                      placeholder=""
                      value={data.name}
                      readOnly
                    />
                  )}
                  {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
                </div>
              </div>
              <div className={`form-group row${errors.id ? " mb-0" : ""}`}>
                <label
                  htmlFor="colFormLabelSm"
                  className="col-sm-3 col-form-label col-form-label-sm label-join"
                >
                  {t("FISTAR_JOIN.FJN_label_id")}
                  <span>*</span>
                </label>
                <div className="col-sm-9 group-check">
                  {info.isChangeID ? (
                    <input
                      type="text"
                      className={`form-control form-control-sm${
                        errors.id ? " is-invalid" : ""
                      }${existID == 2 ? " is-valid" : ""}`}
                      id="id"
                      name="id"
                      placeholder=""
                      value={data.id}
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                    />
                  ) : (
                    <input
                      type="text"
                      className={`form-control form-control-sm`}
                      id="id"
                      name="id"
                      placeholder=""
                      value={data.id}
                      readOnly
                    />
                  )}

                  {info.isChangeID && (
                    <button
                      className="btn"
                      type="button"
                      onClick={this.checkExistID}
                    >
                      {isLoadingCheckExistID ? (
                        <div className="spinner-border" role="status">
                          <span className="sr-only">
                            {t("LOADING.LOADING")}
                          </span>
                        </div>
                      ) : (
                        `${t("FISTAR_JOIN.FJN_button_check ID")}`
                      )}
                    </button>
                  )}
                </div>
              </div>
              {errors.id && (
                <div className="form-group row">
                  <label for="colFormLabelSm" className="col-sm-3" />
                  <div className="col-sm-9">
                    <p className="text-danger">{errors.id}</p>
                  </div>
                </div>
              )}

              <div className="form-group row">
                <label
                  htmlFor="colFormLabelSm"
                  className="col-sm-3 col-form-label col-form-label-sm label-join"
                >
                  {t("FISTAR_JOIN.FJN_status_mess")}
                </label>
                <div className="col-sm-9">
                  <textarea
                    className={`form-control${
                      errors.status_message ? " is-invalid" : ""
                    }`}
                    id="status_message"
                    name="status_message"
                    placeholder=""
                    value={data.status_message}
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                  />
                  {errors.status_message && (
                    <div className="invalid-feedback">
                      {errors.status_message}
                    </div>
                  )}
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="colFormLabelSm"
                  className="col-sm-3 col-form-label col-form-label-sm label-join"
                >
                  {t("FISTAR_JOIN.FJN_label_contact")}
                  <span>*</span>
                </label>
                <div className="col-sm-9">
                  <input
                    type="text"
                    className={`form-control form-control-sm${
                      errors.contact ? " is-invalid" : ""
                    }`}
                    id="contact"
                    name="contact"
                    value={data.contact}
                    onChange={this.handleChangeInputNumberOnly}
                    onFocus={this.onFocusInput}
                  />
                  {errors.contact && (
                    <div className="invalid-feedback">{errors.contact}</div>
                  )}
                </div>
              </div>
              <div className={`form-group row${errors.email ? " mb-0" : ""}`}>
                <label
                  htmlFor="colFormLabelSm"
                  className="col-sm-3 col-form-label col-form-label-sm label-join"
                >
                  {t("FISTAR_JOIN.FJN_label_email")}
                  <span>*</span>
                </label>
                <div className="col-sm-9 group-check">
                  {info.isChangeEmail ? (
                    <input
                      type="email"
                      className={`form-control form-control-sm${
                        errors.email ? " is-invalid" : ""
                      }${existEmail == 2 ? " is-valid" : ""}`}
                      id="email"
                      name="email"
                      value={data.email}
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                    />
                  ) : (
                    <input
                      type="email"
                      className={`form-control form-control-sm${
                        errors.email ? " is-invalid" : ""
                      }`}
                      id="email"
                      name="email"
                      value={data.email}
                      readOnly
                    />
                  )}
                  {info.isChangeEmail && (
                    <button
                      className="btn"
                      type="button"
                      onClick={this.checkExistEmail}
                    >
                      {isLoadingCheckExistEmail ? (
                        <div className="spinner-border" role="status">
                          <span className="sr-only">
                            {t("LOADING.LOADING")}
                          </span>
                        </div>
                      ) : (
                        `${t("FISTAR_JOIN.FJN_check_email")}`
                      )}
                    </button>
                  )}
                </div>
              </div>
              {errors.email && (
                <div className="form-group row">
                  <label for="colFormLabelSm" className="col-sm-3" />
                  <div className="col-sm-9">
                    <p className="text-danger">{errors.email}</p>
                  </div>
                </div>
              )}
              <div className="form-group row align-items-baseline">
                <label
                  htmlFor="colFormLabelSm"
                  className="col-sm-3 col-form-label col-form-label-sm label-join"
                >
                  {t("FISTAR_JOIN.FJN_label_address")}
                  <span>*</span>
                </label>
                <div className="col-sm-9">
                  <textarea
                    className={`form-control${
                      errors.address ? " is-invalid" : ""
                    }`}
                    id="address"
                    name="address"
                    value={data.address}
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                  />
                  {errors.address && (
                    <div className="invalid-feedback">{errors.address}</div>
                  )}
                </div>
              </div>
              {isNewPass && (
                <Fragment>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm label-join"
                    >
                      {t("FISTAR_JOIN.FJN_input_password")}
                      <span>*</span>
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="password"
                        className={`form-control form-control-sm${
                          errors.password ? " is-invalid" : ""
                        }`}
                        id="password"
                        name="password"
                        value={data.password}
                        onChange={this.handleChangeInput}
                        onFocus={this.onFocusInput}
                      />
                      {errors.password && (
                        <div className="invalid-feedback">
                          {errors.password}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="colFormLabelSm"
                      className="col-sm-3 col-form-label col-form-label-sm label-join"
                    >
                      {t("FISTAR_JOIN.FJN_confirm_password")}
                      <span>*</span>
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="password"
                        className={`form-control form-control-sm${
                          errors.password_confirmation ? " is-invalid" : ""
                        }`}
                        id="password_confirmation"
                        name="password_confirmation"
                        value={data.password_confirmation}
                        onChange={this.handleChangeInputConfirmPW}
                        onFocus={this.onFocusInput}
                      />
                      {errors.password_confirmation && (
                        <div className="invalid-feedback">
                          {errors.password_confirmation}
                        </div>
                      )}
                    </div>
                  </div>
                </Fragment>
              )}
            </form>
            <div className="row">
              <div className="col-md-3" />
              <div className="col-md-9">
                <div className="button-group">
                  <button onClick={this.onPreviousStep}>
                    {t("FISTAR_JOIN.FJN_button_previous")}
                  </button>
                  <button onClick={this.onNextStep}>
                    {isLoadingNextStep ? (
                      <div className="spinner-border" role="status">
                        <span className="sr-only">{t("LOADING.LOADING")}</span>
                      </div>
                    ) : (
                      `${t("FISTAR_JOIN.FJN_button_next")}`
                    )}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Modal
          size="lg"
          show={this.state.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="howto-register modal-dialog-centered"
        >
          <div className="register">
            <div className="top modal-header">
              <h4>{t("FISTAR_JOIN.notication")}</h4>
              <button
                type="button"
                className="btn btn-close close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.handleClose}
              >
                <img src={closePopup} alt="" />
              </button>
            </div>
            <div className="body-popup">
              <p className="text-center">{t("FISTAR_JOIN.modify_campaign")}</p>
              <button onClick={this.redirectHome}>
                {t("POPUP_RESET_PASS.PRP_button_ok")}
              </button>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    info: state.register
  };
};

const mapDispatchToProps = dispatch => {
  return {
    checkExist: (name, value) => dispatch(checkExistAction(name, value)),
    join: (data, type, isChangeID = null, isChangeEmail = null) =>
      dispatch(joinAction(data, type, isChangeID, isChangeEmail))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FiStarRegisterStep1));
