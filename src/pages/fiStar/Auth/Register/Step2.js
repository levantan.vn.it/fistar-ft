import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { DatePicker } from "@y0c/react-datepicker";
// import calendar style
// You can customize style by copying asset folder.
import "@y0c/react-datepicker/assets/styles/calendar.scss";

// import DatePicker from 'react-date-picker';
// import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";
// import moment from 'moment'
// import * as startOfDay from "date-fns"
import { format } from "date-fns";
import "./register.css";
import { loginAction, joinAction,checkSnsVerify } from "./../../../../store/actions/auth";
import { getCodeAction } from "./../../../../store/actions/code";
import { getChannelAction } from "./../../../../store/actions/channel";
import { VALIDATION } from "./../../../../constants/index";
import Flatpickr from 'react-flatpickr'
import 'flatpickr/dist/themes/material_blue.css'
import { DATE_FORMAT } from "../../../../constants";


class FiStarRegisterStep2 extends Component {
  constructor(props) {
    super(props);
    console.log(props, '-000000000');
    var x = Date.now();
    var y = x - 18 * 86400 * 365 * 1000;
    var z = new Date(y);
    // var DateYear = z.toISOString();
    var DateOld = z.toISOString().split("T");
    this.state = {
      dateOld: DateOld[0],
      data: {
        image:
          props.info.data && props.info.data.image ? props.info.data.image : "",
        gender:
          props.info.data && props.info.data.gender
            ? props.info.data.gender
            : "",
        dob:
          props.info.data && props.info.data.dob
            ? props.info.data.dob
            : DateOld[0],
        location:
          props.info.data && props.info.data.location
            ? props.info.data.location
            : "",
        bank_name:
          props.info.data && props.info.data.bank_branch
            ? props.info.data.bank_branch
            : "",
        bank_branch:
          props.info.data && props.info.data.bank_branch
            ? props.info.data.bank_branch
            : "",
        bank_account:
          props.info.data && props.info.data.bank_account
            ? props.info.data.bank_account
            : "",
        bank_account_number:
          props.info.data && props.info.data.bank_account_number
            ? props.info.data.bank_account_number
            : "",
        swift_code:
          props.info.data && props.info.data.swift_code
            ? props.info.data.swift_code
            : "",
        keyword:
          props.info.data && props.info.data.keyword
            ? props.info.data.keyword
            : "",
        introduction:
          props.info.data && props.info.data.introduction
            ? props.info.data.introduction
            : "",
        // facebook:
        //   props.info.data && props.info.data.facebook
        //     ? props.info.data.facebook
        //     : "",
        // youtube:
        //   props.info.data && props.info.data.youtube
        //     ? props.info.data.youtube
        //     : "",
        // instagram:
        //   props.info.data && props.info.data.instagram
        //     ? props.info.data.instagram
        //     : "",
        agree_term:
          props.info.data && props.info.data.agree_privacy
            ? props.info.data.agree_privacy
            : false,
        agree_privacy:
          props.info.data && props.info.data.agree_privacy
            ? props.info.data.agree_privacy
            : false,
        channels: props.info.data && props.info.data.channels
            ? props.info.data.channels
            : {},
      },
        verifySnsInstagram: false,
        verifySnsYoutube: false,
        verifySnsFacebook: false,
        validSnsInstagram: false,
        validSnsYoutube: false,
        validSnsFacebook: false,
      errors: {
        image: "",
        gender: "",
        dob: "",
        location: "",
        bank_name: "",
        bank_branch: "",
        bank_account: "",
        bank_account_number: "",
        swift_code: "",
        keyword: "",
        introduction: "",
        facebook: "",
        youtube: "",
        instagram: "",
        agree_term: "",
        agree_privacy: "",
          verifySnsInstagram: "",
          verifySnsYoutube: "",
          verifySnsFacebook: "",
        message: ""
      },
      genders: [],
      locations: [],
      keywords: [],
      channels: [],
      isLoading: false,
      startDate: new Date(),
      isLoadingForm: false
    };
  }

  componentDidMount() {
    this.props.getCode("gender,location,keyword");
    this.props.getChannel()
      .then((response) => {
        this.setState({
          channels: response
        })
      })
  }

  static getDerivedStateFromProps(props, state) {
    if (props.code.data.gender && !state.data.gender) {
      return {
        data: {
          ...state.data,
          gender: props.code.data.gender.code[0].cd_id
        }
      };
    }
    if (props.code.data.location && !state.data.location) {
      return {
        data: {
          ...state.data,
          location: props.code.data.location.code[0].cd_id
        }
      };
    }
    return null
  }

  handleChange = date => {
    if (new Date() > new Date(date)) {
      this.setState(prevState => ({
        startDate: date,
        data: {
          ...prevState.data,
          dob: format(new Date(date), "YYYY-MM-DD")
        },
        dateOldOnchane: date,
        errors: {
          image: "",
          gender: "",
          dob: "",
          location: "",
          bank_name: "",
          bank_branch: "",
          bank_account: "",
          bank_account_number: "",
          swift_code: "",
          keyword: "",
          introduction: "",
          facebook: "",
          youtube: "",
          instagram: "",
          agree_term: "",
          agree_privacy: "",
          message: ""
        }
      }));
    } else {
      if (date == "") {
        this.setState({
          errors: {
            ...this.state.errors,
            dob: "Birthday field is required!"
          }
        });
      } else {
        this.setState({
          errors: {
            ...this.state.errors,
            dob: "The date selected must be smaller than the current date!"
          }
        });
      }
    }
  };

  handleChangeFile = e => {
    let files = e.target.files;
    let file = files[0];
    if (file) {
      if (
        file.type == "image/jpeg" ||
        file.type == "image/gif" ||
        file.type == "image/png" ||
        file.type == "image/jpg"
      ) {
        this.setState(prevState => ({
          data: {
            ...prevState.data,
            image: files[0]
          },
          errors: {
            image: "",
            gender: "",
            dob: "",
            location: "",
            bank_name: "",
            bank_branch: "",
            bank_account: "",
            bank_account_number: "",
            swift_code: "",
            keyword: "",
            introduction: "",
            facebook: "",
            youtube: "",
            instagram: "",
            agree_term: "",
            agree_privacy: "",
            message: ""
          }
        }));
      } else {
        this.setState({
          errors: {
            ...this.state.errors,
            image: "The file is not image!"
          }
        });
      }
    }
  };

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

    handleChangeInputNumberOnly = e => {
        const name = e.target.name;
        const value = e.target.value;
        if (/^\d*$/.test(value)) {
            this.setState(prevState => ({
                data: {
                    ...prevState.data,
                    [name]: value
                }
            }));
        }
    };

    validUrl(url, type){
        switch(type){
            case 'facebook': return /facebook\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 2: return /facebook\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 'instagram': return /instagram\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 4: return /instagram\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 'youtube': return /youtube\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 3: return /youtube\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            default: return /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm.test(url);
        }

    }


    handleChangeInputChannel = (sns, value) => {

        var snsValid="";
        if(value) {
            console.log(value)
            snsValid = this.validUrl(value, sns.sns_id);
        }else {
            console.log(value)
            snsValid=true
        }


        switch (sns.sns_id) {
            case 2:
                var facebook = value;

                if(snsValid == true) {

                    this.setState(prevState => ({
                        validSnsFacebook: false,
                        data: {
                            ...prevState.data,
                            channels: {
                                ...this.state.data.channels,
                                [sns.sns_name]: {
                                    id: sns.sns_id,
                                    value: value,
                                    disable: false,
                                }
                            }
                        }
                    }));
                    var data = {};
                    data.sns_id = 2;
                    data.url = facebook

                    if(facebook) {
                        this.props
                            .verifySns(data)
                            .then(response => {


                                if (response.data == false) {
                                    this.setState(prevState => ({
                                        verifySnsFacebook: true,
                                        errors: {
                                            verifySnsFacebook: "Url Facebook is exist"
                                        },

                                    }))
                                }
                                if(response.data == true) {

                                    this.setState(prevState => ({
                                        verifySnsFacebook: false,
                                        errors: {
                                            verifySnsFacebook: ""
                                        },

                                    }))

                                }

                            }).catch(e => {
                        });
                    }else {

                        this.setState(prevState => ({
                            verifySnsFacebook: false,
                        }))

                    }

                }else {
                    console.log('zozozozoz else')
                    this.setState(prevState => ({
                        validSnsFacebook: true,
                        data: {
                            ...prevState.data,
                            channels: {
                                ...this.state.data.channels,
                                [sns.sns_name]: {
                                    id: sns.sns_id,
                                    value: value,
                                    disable: false,
                                }
                            }
                        },
                        errors: {
                            verifySnsFacebook: "Url Facebook is invalid"
                        }
                    }))

                }

                break;
            case 3:
                var youtube = value;

                if(snsValid == true) {

                    this.setState(prevState => ({
                        validSnsYoutube: facebook,
                        data: {
                            ...prevState.data,
                            channels: {
                                ...this.state.data.channels,
                                [sns.sns_name]: {
                                    id: sns.sns_id,
                                    value: value,
                                    disable: false,
                                }
                            }
                        }
                    }));

                    var data = {};
                    data.sns_id = 3;
                    data.url = youtube

                    if(youtube) {
                        this.props
                            .verifySns(data)
                            .then(response => {

                                if (response.data == false) {
                                    this.setState(prevState => ({
                                        verifySnsYoutube: true,

                                        errors: {
                                            verifySnsYoutube: "Url Youtube is exist"
                                        }
                                    }))
                                }
                                if (response.data == true) {
                                    this.setState(prevState => ({
                                        verifySnsYoutube: false,

                                        errors: {
                                            verifySnsYoutube: ""
                                        }
                                    }))
                                }

                            }).catch(e => {
                        });
                    }else {

                        this.setState(prevState => ({
                            verifySnsYoutube: false,
                        }))

                    }

                }else {
                    console.log('zozozozoz else')
                    this.setState(prevState => ({
                        validSnsYoutube: true,
                        data: {
                            ...prevState.data,
                            channels: {
                                ...this.state.data.channels,
                                [sns.sns_name]: {
                                    id: sns.sns_id,
                                    value: value,
                                    disable: false,
                                }
                            }
                        },
                        errors: {
                            verifySnsYoutube: "Url Youtube is invalid"
                        }
                    }))

                }

                break;
            case 4:
                var instagram = value;
                if(snsValid == true) {


                    this.setState(prevState => ({
                        validSnsInstagram: false,
                        data: {
                            ...prevState.data,
                            channels: {
                                ...this.state.data.channels,
                                [sns.sns_name]: {
                                    id: sns.sns_id,
                                    value: value,
                                    disable: false,
                                }
                            }
                        }
                    }));

                    var data = {};
                    data.sns_id = 4;
                    data.url = instagram

                    if(instagram) {

                        this.props
                            .verifySns(data)
                            .then(response => {

                                if (response.data == false) {
                                    this.setState(prevState => ({
                                        verifySnsInstagram: true,
                                        onChange: false,
                                        errors: {
                                            verifySnsInstagram: "Url instagram is exist"
                                        }
                                    }))
                                }
                                if (response.data == true) {
                                    this.setState(prevState => ({
                                        verifySnsInstagram: false,
                                        onChange: false,
                                        errors: {
                                            verifySnsInstagram: ""
                                        }
                                    }))
                                }

                            }).catch(e => {
                        });

                    }else {

                        this.setState(prevState => ({
                            verifySnsInstagram: false,
                        }))

                    }

                }else {
                    console.log('zozozozoz else')
                    this.setState(prevState => ({
                        validSnsInstagram: true,
                        data: {
                            ...prevState.data,
                            channels: {
                                ...this.state.data.channels,
                                [sns.sns_name]: {
                                    id: sns.sns_id,
                                    value: value,
                                    disable: false,
                                }
                            }
                        },
                        errors: {
                            verifySnsInstagram: "Url Instagram is invalid"
                        }
                    }))

                }

                break;
        }


    };


  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        image: "",
        gender: "",
        dob: "",
        location: "",
        bank_name: "",
        bank_branch: "",
        bank_account: "",
        bank_account_number: "",
        swift_code: "",
        keyword: "",
        introduction: "",
        facebook: "",
        youtube: "",
        instagram: "",
        agree_term: "",
        agree_privacy: "",
        message: ""
      }
    }));
  };

  handleCheckboxChange = e => {
    const name = e.target.name;
    const checked = e.target.checked;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: checked
      },
      errors: {
        image: "",
        gender: "",
        dob: "",
        location: "",
        bank_name: "",
        bank_branch: "",
        bank_account: "",
        bank_account_number: "",
        swift_code: "",
        keyword: "",
        introduction: "",
        facebook: "",
        youtube: "",
        instagram: "",
        agree_term: "",
        agree_privacy: "",
        message: ""
      }
    }));
  };

  handleCheckboxAllChange = e => {
    const name = e.target.name;
    const checked = e.target.checked;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        agree_term: checked,
        agree_privacy: checked
      },
      errors: {
        image: "",
        gender: "",
        dob: "",
        location: "",
        bank_name: "",
        bank_branch: "",
        bank_account: "",
        bank_account_number: "",
        swift_code: "",
        keyword: "",
        introduction: "",
        facebook: "",
        youtube: "",
        instagram: "",
        agree_term: "",
        agree_privacy: "",
        message: ""
      }
    }));
  };

  submitForm = e => {
    e.preventDefault();
    const { data } = this.state;
    // this.props.login(data)
    //   .catch((e) => {
    //     const errors = {
    //       email: '',
    //       password: '',
    //       message: '',
    //     };
    //     if (e.data.errors) {
    //       e.data.errors.email ? errors.email = e.data.errors.email[0] : ''
    //       e.data.errors.password ? errors.password = e.data.errors.password[0] : ''
    //     }
    //     e.data.message ? errors.message = e.data.message : ''
    //     this.setState({
    //       errors
    //     })
    //   })
  };

  validateForm = () => {
    const errors = {
      image: "",
      gender: "",
      dob: "",
      location: "",
      bank_name: "",
      bank_branch: "",
      bank_account: "",
      bank_account_number: "",
      swift_code: "",
      keyword: "",
      introduction: "",
      facebook: "",
      youtube: "",
      instagram: "",
      agree_term: "",
      agree_privacy: "",
        verifySnsInstagram: "",
        verifySnsFacebook: "",
        verifySnsYoutube: "",
      message: ""
    };
    const { data } = this.state;

    data.dob =
      data.dob === "" || data.dob === "undefined"
        ? this.state.dateOld
        : data.dob;

    const { t } = this.props;
    if (!data.image) {
      errors.image = `${t("FISTAR_JOIN.FJN_erro_image")}`;
    } else if (data.image.size > process.env.REACT_APP_MAX_IMAGE_SIZE * 1000000) {
      errors.image = `${t("FISTAR_JOIN.FJN_erro_image_max_"+process.env.REACT_APP_MAX_IMAGE_SIZE)}`;
    }
    if (!data.gender) {
      errors.gender = `${t("FISTAR_JOIN.FJN_gender_required")}`;
    }
    if (!data.dob) {
      errors.dob = `${t("FISTAR_JOIN.FJN_erro_date_birth")}`;
    }
    if (!data.location) {
      errors.location = `${t("FISTAR_JOIN.FJN_location_required")}`;
    }
    if (data.keyword.length <= 0) {
      errors.keyword = `${t("FISTAR_JOIN.FJN_erro_key_word")}`;
    }
      if (this.state.verifySnsInstagram) {
          errors.verifySnsInstagram = "Url instagram is exist";
      }
      if (this.state.verifySnsYoutube) {
          errors.verifySnsYoutube = "Url Youtube is exist";
      }
      if (this.state.verifySnsFacebook) {
          errors.verifySnsFacebook = "Url Facebook is exist";
      }

      if (this.state.validSnsInstagram) {
          errors.verifySnsInstagram = "Url instagram is exist";
      }
      if (this.state.validSnsYoutube) {
          errors.verifySnsYoutube = "Url Youtube is exist";
      }
      if (this.state.validSnsFacebook) {
          errors.verifySnsFacebook = "Url Facebook is exist";
      }

    if (!data.introduction) {
      errors.introduction = `${t("FISTAR_JOIN.FJN_erro_introduction")}`;
    } else if (data.introduction.length > VALIDATION.MAX_LENGTH) {
      errors.introduction = `${t("FISTAR_JOIN.FJN_than_200_characters")}`;
    }
    if (!data.channels.facebook || (data.channels.facebook && !data.channels.facebook.value)) {
      errors.facebook = `${t("FISTAR_JOIN.FJN_facebook")}`;
    }
    if (!data.agree_term) {
      errors.agree_term = `${t("FISTAR_JOIN.FJN_agree_term_required.")}`;
    }
    if (!data.agree_privacy) {
      errors.agree_privacy = `${t("FISTAR_JOIN.FJN_agree_privacy_required.")}`;
    }

    return errors;
  };

  onNextStep = () => {
    const { data, errors } = this.state;
    let validate = this.validateForm();
    validate = { ...this.state.errors, ...validate };
    if (
      validate.image === "" &&
      validate.gender === "" &&
      validate.dob === "" &&
      validate.location === "" &&
      validate.keyword === "" &&
      validate.introduction === "" &&
      validate.facebook === "" &&
      validate.agree_term === "" &&
      validate.agree_privacy === "" &&
      validate.verifySnsFacebook === "" &&
      validate.verifySnsYoutube === "" &&
      validate.verifySnsInstagram === ""
    ) {
      this.setState(
        {
          isLoadingForm: false
        },
        () => {
          this.props.onNextStep(3, data);
        }
      );
    } else {
      this.setState({
        errors: validate
      });
    }
  };

  onPreviousStep = () => {
    const { data } = this.state;
    this.props.join(data, "fistar");
    this.props.onNextStep(1);
  };

  onClickFindImage = () => {
    this.inputImage.click();
  };

  onClickKeyword = id => {
    const { data } = this.state;
    const index = data.keyword.indexOf(id);
    if (index !== -1) {
      data.keyword.splice(index, 1);
      this.setState({
        data
      });
    } else {
      this.setState(prevState => ({
        data: {
          ...prevState.data,
          keyword: [...prevState.data.keyword, id]
        }
      }));
    }
  };

  renderGender = () => {
    const { data, errors } = this.state;
    const {
      t,
      code: {
        data: { gender }
      }
    } = this.props;

    if (!gender) return;

    return (
      <div className="form-group row">
        <label
          htmlFor="colFormLabelSm"
          className="col-sm-3 col-form-label col-form-label-sm"
        >
          {t("FISTAR_JOIN.FJN_label_gender")}
          <span>*</span>
        </label>
        <div className="col-sm-9 main-select">
          <select
            className={`form-control main-content-select form-control-sm${
              errors.gender ? " is-invalid" : ""
            }`}
            value={data.gender}
            onChange={this.handleChangeInput}
            name="gender"
            id="gender"
          >
            {(gender.code || []).map((item, key) => (
              <option value={item.cd_id} key={key}>
                {item.cd_label}
              </option>
            ))}
          </select>
          {errors.gender && (
            <div className="invalid-feedback">{errors.gender}</div>
          )}
        </div>
      </div>
    );
  };

  renderLocation = () => {
    const { data, errors } = this.state;
    const {
      t,
      code: {
        data: { location }
      }
    } = this.props;

    if (!location) return;

    return (
      <div className="form-group row">
        <label
          htmlFor="colFormLabelSm"
          className="col-sm-3 col-form-label col-form-label-sm"
        >
          {t("FISTAR_JOIN.FJN_label_country")}
          <span>*</span>
        </label>
        <div className="col-sm-9 main-select">
          <select
            className={`form-control form-control-sm${
              errors.location ? " is-invalid" : ""
            }`}
            value={data.location}
            onChange={this.handleChangeInput}
            name="location"
            id="location"
          >
            {(location.code || []).map((item, key) => (
              <option value={item.cd_id} key={key}>
                {item.cd_label}
              </option>
            ))}
          </select>
          {errors.location && (
            <div className="invalid-feedback">{errors.location}</div>
          )}
        </div>
      </div>
    );
  };

  renderKeyword = () => {
    const { data, errors } = this.state;
    const {
      code: {
        data: { keyword }
      }
    } = this.props;

    if (!keyword) return;

    return (
      <div className="row">
        <div className="col-md-3" />
        <div className="tag col-md-9">
          {(keyword.code || []).map((item, key) => (
            <span
              href="#"
              className={
                data.keyword.includes(item.cd_id)
                  ? "keyword selected"
                  : "keyword"
              }
              key={key}
              onClick={() => this.onClickKeyword(item.cd_id)}
            >
              {item.cd_label}
            </span>
          ))}
          {errors.keyword && <p className="text-danger">{errors.keyword}</p>}
        </div>
      </div>
    );
  };

  renderChannel = () => {
    return this.state.channels.map((channel, key) => {
      let value = this.state.data.channels[channel.sns_name] ? this.state.data.channels[channel.sns_name].value : ''
      let disable = this.state.data.channels[channel.sns_name] ? this.state.data.channels[channel.sns_name].disable : ''
      console.log(channel, 'fsdf');
      return <Fragment key={key}>
        <div className="form-group row">
          <label
            htmlFor="colFormLabelSm"
            className="col-sm-3 col-form-label col-form-label-sm"
          >
            {key == 0 && (
              <Fragment>
                {this.props.t("FISTAR_JOIN.FJN_sns")}
                <span>*</span>
              </Fragment>
            )}
          </label>
          <div className="col-sm-9 group-check-detail">
            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">
                  {channel.sns_name} : {channel.sns_url.replace("https://", "")}
                  {channel.sns_id == 2 && (<b> *</b>)}
                </span>
              </div>
              {disable && <input
                placeholder={this.props.t("FISTAR_JOIN.FJN_input_sns")}
                type="text"
                className="form-input empty"
                id={channel.sns_name}
                name={channel.sns_name}
                value={value}
                readOnly
                disable
              />}
              {!disable && <input
                placeholder={this.props.t("FISTAR_JOIN.FJN_input_sns")}
                type="text"
                className="form-input empty"
                id={channel.sns_name}
                name={channel.sns_name}
                onChange={(e) => this.handleChangeInputChannel(channel, e.target.value)}
                onFocus={this.onFocusInput}
                value={value}
              />}
                {channel.sns_id == 2 && this.state.verifySnsFacebook && (
                    <div className="tooptip-text">
                        <p className="text-danger">Url Facebook is exist</p>
                    </div>
                )}
                {channel.sns_id == 2 && this.state.validSnsFacebook && (
                    <div className="tooptip-text">
                        <p className="text-danger">Url Facebook is invalid</p>
                    </div>
                )}
                {channel.sns_id == 3 && this.state.verifySnsYoutube && (
                    <div className="tooptip-text">
                        <p className="text-danger">Url Youtube is exist</p>
                    </div>
                )}
                {channel.sns_id == 3 && this.state.validSnsYoutube && (
                    <div className="tooptip-text">
                        <p className="text-danger">Url Youtube is invalid</p>
                    </div>
                )}
                {channel.sns_id == 4 && this.state.verifySnsInstagram && (
                    <div className="tooptip-text">
                        <p className="text-danger">Url Instagram is exist</p>
                    </div>
                )}
                {channel.sns_id == 4 && this.state.validSnsInstagram && (
                    <div className="tooptip-text">
                        <p className="text-danger">Url Instagram is invalid</p>
                    </div>
                )}
              {this.state.errors[channel.sns_name] && (
                <p className="text-danger">{this.state.errors[channel.sns_name]}</p>
              )}
            </div>
          </div>
        </div>
      </Fragment>
    })
  }

  render() {
    const {
      t,
      code: {
        data: { keyword }
      }
    } = this.props;
    const { data, errors, isLoadingForm } = this.state;
    return (
      <Fragment>
        <div className="row">
          <div className="col-md-12">
            <div className="note-form">
              <p>
                {/*Please check the basic information written on the{" "}*/}
                {/*<span>fi :me.</span>*/}
              </p>
              <span className="right">Required</span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <form className="form-join-detail" onSubmit={this.submitForm}>
              <div className={`form-group row${errors.image ? " mb-0" : ""}`}>
                <label
                  htmlFor="colFormLabelSm"
                  className="col-sm-3 col-form-label col-form-label-sm"
                >
                  {t("FISTAR_JOIN.FJN_label_profile_image")}
                  <span>*</span>
                </label>
                <div className="col-sm-9 group-check">
                  <input
                    type="file"
                    id="selectedFile"
                    name="image"
                    onChange={this.handleChangeFile}
                    onFocus={this.onFocusInput}
                    ref={input => (this.inputImage = input)}
                    accept="image/*"
                  />
                  <span
                    type="text"
                    className={`form-control form-control-sm${
                      errors.image ? " is-invalid" : ""
                    }`}
                    id="fakeInput"
                  >
                    {data.image.name}
                  </span>
                  {/*errors.image && (
                    <p className="text-danger">{errors.image}</p>
                  )*/}
                  <button
                    className="btn"
                    type="button"
                    onClick={this.onClickFindImage}
                  >
                    {t("FISTAR_JOIN.FJN_button_find")}
                  </button>
                  {/*<input
                    ref={input => this.inputImage = input}
                    type="file"
                    className="input-file"
                    id="image"
                    name="image"
                    placeholder=""
                    onChange={this.handleChangeFile}
                    onFocus={this.onFocusInput}
                  />
                  {errors.image && (
                    <p className="text-danger">{errors.image}</p>
                  )}
                  <button className="btn" type="button" onClick={this.onClickFindImage}>Find</button>*/}
                </div>
              </div>
              {errors.image && (
                <div className="form-group row">
                  <label htmlFor="colFormLabelSm" className="col-sm-3" />
                  <div className="col-sm-9">
                    <p className="text-danger">{errors.image}</p>
                  </div>
                </div>
              )}
              {this.renderGender()}
              <div className="form-group row">
                <label
                  htmlFor="colFormLabelSm"
                  className="col-sm-3 col-form-label col-form-label-sm"
                >
                  {t("FISTAR_JOIN.FJN_label_date_birth")}
                  <span>*</span>
                </label>
                <div className="col-sm-9">
                  <div className="input-date">
                    {/*<DatePicker
                      onChange={this.handleChange}
                      clear={true}
                      showDefaultIcon
                      showToday={false}
                      initialDate={
                        this.state.data.dob
                          ? this.state.data.dob
                          : this.state.dateOld
                      }
                    />*/}
                    <Flatpickr
                      options={{
                        wrap: true,
                        disableMobile: "true"
                      }}
                      className="wp-date-icon"
                      value={this.state.data.dob
                          ? this.state.data.dob
                          : this.state.dateOld}
                      onChange={date => { this.handleChange(format(new Date(date), DATE_FORMAT)) }}
                    >
                      <React.Fragment>
                        <input type="text" placeholder="" data-input />
                        <i class="far fa-calendar i-calendar"></i>
                        <a className="input-button" title="clear" data-clear>
                            <i className="fa fa-times" aria-hidden="true"></i>
                        </a>
                      </React.Fragment>
                    </Flatpickr>
                  </div>
                  {/*<DatePicker
                    onChange={this.handleChange}
                    value={this.state.startDate}
                    // className="form-control input-date"
                    calendarIcon={null}
                    clearIcon={null}
                  />*/}
                  {/*<i className="far fa-calendar-alt"></i>
                  <input
                    type="date"
                    className={`form-control form-control-sm${errors.dob ? ' is-invalid' : ''}`}
                    id="dob"
                    name="dob"
                    placeholder=""
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                    value={data.dob}
                  />*/}
                  {errors.dob && <p className="text-danger">{errors.dob}</p>}
                </div>
              </div>
              {this.renderLocation()}
              <div className="group-bank-account">
                <div className="form-group row">
                  <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm"
                  >
                    {t("FISTAR_JOIN.FJN_label_bank_account")}
                  </label>
                  <div className="col-sm-9 group-check-detail">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          {t("FISTAR_JOIN.FJN_bank_name")}
                        </span>
                      </div>
                      <input
                        type="text"
                        className="form-input empty"
                        id="bank_name"
                        name="bank_name"
                        placeholder=""
                        onChange={this.handleChangeInput}
                        onFocus={this.onFocusInput}
                        value={data.bank_name}
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-sm-3" />
                  <div className="col-sm-9 group-check-detail">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          {t("FISTAR_JOIN.FJN_bank_branch")}
                        </span>
                      </div>
                      <input
                        type="text"
                        className="form-input empty"
                        id="bank_branch"
                        name="bank_branch"
                        placeholder=""
                        onChange={this.handleChangeInput}
                        onFocus={this.onFocusInput}
                        value={data.bank_branch}
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-sm-3" />
                  <div className="col-sm-9 group-check-detail">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          {t("FISTAR_JOIN.FJN_account_name")}
                        </span>
                      </div>
                      <input
                        type="text"
                        className="form-input empty"
                        id="bank_account"
                        name="bank_account"
                        placeholder=""
                        onChange={this.handleChangeInput}
                        onFocus={this.onFocusInput}
                        value={data.bank_account}
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-sm-3" />
                  <div className="col-sm-9 group-check-detail">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          {t("FISTAR_JOIN.FJN_account_number")}
                        </span>
                      </div>
                      <input
                        placeholder=""
                        type="text"
                        className="form-input empty"
                        id="bank_account_number"
                        name="bank_account_number"
                        onChange={this.handleChangeInputNumberOnly}
                        onFocus={this.onFocusInput}
                        value={data.bank_account_number}
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-sm-3" />
                  <div className="col-sm-9 group-check-detail">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          {t("FISTAR_JOIN.FJN_swift_code")}
                        </span>
                      </div>
                      <input
                        type="text"
                        className="form-input empty"
                        id="swift_code"
                        name="swift_code"
                        placeholder=""
                        onChange={this.handleChangeInput}
                        onFocus={this.onFocusInput}
                        value={data.swift_code}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="tag-join-detail">
                <div className="row">
                  <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm"
                  >
                    {t("FISTAR_JOIN.FJN_label_matching_keyword")}
                    <span>*</span>
                  </label>
                  <div className="text-tag col-md-9">
                    <p className="text-red">{t("FISTAR_JOIN.FJN_title_red")}</p>
                    <p>{t("FISTAR_JOIN.FJN_title")}</p>
                  </div>
                </div>
                {this.renderKeyword()}
              </div>
              <div className="select-four">
                <div className="row">
                  <div className="col-md-3" />
                  <div className="col-md-9 mt-2">
                    <h4 className="text-center">
                      <span className="color-red">
                        {t("FISTAR_JOIN.FJN_select")} {data.keyword.length}
                      </span>{" "}
                      {t("FISTAR_JOIN.FJN_out_of")}{" "}
                      {keyword ? keyword.code.length : ""}
                    </h4>
                  </div>
                </div>
                <div className="form-group row">
                  <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm"
                  >
                    {t("FISTAR_JOIN.FJN_label_introduction")}
                    <span>*</span>
                  </label>
                  <div className="col-sm-9 group-select">
                    <textarea
                      rows="4"
                      cols="50"
                      placeholder={t("FISTAR_JOIN.FJN_controduction")}
                      id="introduction"
                      name="introduction"
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                      value={data.introduction}
                    />
                    {errors.introduction && (
                      <p className="text-danger">{errors.introduction}</p>
                    )}
                  </div>
                </div>
                <div className="group-bank-account group-sns">
                {this.renderChannel()}
                </div>
                <div className="form-group row form-Terms">
                  <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm label-term-condition"
                  >
                    {t("FISTAR_JOIN.FJN_label_term_condition")}
                    <span>*</span>
                  </label>
                  <div className="col-sm-9 group-select">
                    <div className="row">
                      <div className="col-md-12">
                        <div className="check-detail-custom mb-3">
                          <input
                            type="checkbox"
                            name="Sass"
                            id="all"
                            checked={data.agree_term && data.agree_privacy}
                            onChange={this.handleCheckboxAllChange}
                          />
                          <label htmlFor="all" className="label-all">
                            {t("FISTAR_JOIN.FJN_check_all")}
                          </label>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-12">
                        <textarea
                          rows="4"
                          cols="50"
                          className="bg-transparent"
                          placeholder="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation "
                          readOnly
                          disabled={true}
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-12">
                        <div className="check-detail-custom check-terms">
                          <input
                            type="checkbox"
                            name="agree_term"
                            id="agree_term"
                            checked={data.agree_term}
                            onChange={this.handleCheckboxChange}
                          />
                          <label
                            htmlFor="agree_term"
                            className={errors.agree_term ? "text-danger" : ""}
                          >
                            {t("FISTAR_JOIN.FJN_check_condition")}
                          </label>
                        </div>
                      </div>
                    </div>
                    <div className="row mt-3">
                      <div className="col-md-12">
                        <textarea
                          rows="4"
                          cols="50"
                          className="bg-transparent"
                          placeholder="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation "
                          readOnly
                          disabled={true}
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-12">
                        <div className="check-detail-custom check-terms">
                          <input
                            type="checkbox"
                            name="agree_privacy"
                            id="agree_privacy"
                            checked={data.agree_privacy}
                            onChange={this.handleCheckboxChange}
                          />
                          <label
                            htmlFor="agree_privacy"
                            className={
                              errors.agree_privacy ? "text-danger" : ""
                            }
                          >
                            {t("FISTAR_JOIN.FJN_check_privacy")}
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            <div className="row">
              <div className="col-md-3" />
              <div className="col-md-9">
                <div className="button-group">
                  <button onClick={this.onPreviousStep} type="button">
                    {t("FISTAR_JOIN.FJN_button_previous")}
                  </button>
                  <button onClick={this.onNextStep} type="button">
                    {isLoadingForm ? (
                      <div className="spinner-border" role="status">
                        <span className="sr-only">{t("LOADING.LOADING")}</span>
                      </div>
                    ) : (
                      `${t("FISTAR_JOIN.FJN_button_apply")}`
                    )}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    code: state.code,
    info: state.register
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: data => dispatch(loginAction(data)),
    getCode: type => dispatch(getCodeAction(type)),
    getChannel: type => dispatch(getChannelAction(false)),
    join: (data, type, isChangeID = null, isChangeEmail = null) =>
      dispatch(joinAction(data, type, isChangeID, isChangeEmail)),
      verifySns: data => dispatch(checkSnsVerify(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FiStarRegisterStep2));
