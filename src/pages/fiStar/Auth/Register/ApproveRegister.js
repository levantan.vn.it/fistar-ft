import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { changePasswordAction } from "./../../../../store/actions/auth";
import { changePasswordActionPartner } from "../../../../store/actions/auth";
import * as routeName from "./../../../../routes/routeName";
import "./approveRegister.scss";

class ApproveRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectFistar: false
    };
  }
  redirectFistar = () => {
    this.setState({
      redirectFistar: true
    });
  };

  render() {
    const { t } = this.props;
    const { redirectFistar } = this.state;
    if (redirectFistar) {
      return <Redirect to={routeName.FISTAR_DASHBOARD} />;
    }
    return (
      <Fragment>
        <div className="body-form-thanks approve-fistar">
          <div className="container-form-join">
            <div className="row">
              <div className="col-md-12 thanks-for">
                <h2 className="text-center mb-4">
                  {t("FISTAR_JOIN_CONFIRM.FJC_title")}
                </h2>
                <p className=" text-center font-weight-bold">
                  {t("FISTAR_JOIN_CONFIRM.FJC_text_content")}{" "}
                  {/*<strong>fi :Star.</strong>*/}
                </p>
                <p className="text-center font-weight-bold">
                  {t("FISTAR_JOIN_CONFIRM.FJC_text_1")}
                </p>
                <p className="important-text text-center font-weight-bold">
                  {t("FISTAR_JOIN_CONFIRM.FJC_text_2")}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="time-approvals col-md-12">
                <p>{t("FISTAR_JOIN_CONFIRM.FJC_text_3")}</p>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12 p-0">
                <div className="button-group-thanks">
                  <Link
                    className="w-100 redirect-to-dashboard"
                    to={routeName.FISTAR_DASHBOARD}
                  >
                    {t("FISTAR_JOIN_CONFIRM.FJC_text_banner_3")}
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/*<section className="register-partner-form approve-register">*/}
        {/*<div className="container">*/}
        {/*<div className="content-form-register">*/}
        {/*<div className="row text-form">*/}
        {/*<div className="col-md-12">*/}
        {/*<h3>*/}
        {/*<strong>fi :Star JOIN</strong>*/}
        {/*</h3>*/}
        {/*</div>*/}
        {/*</div>*/}
        {/*<div className="row step-form">*/}
        {/*<div className="col-md-12 content-step-form">*/}
        {/*<div className="title-approve" />*/}
        {/*<div className="notification">*/}
        {/*<div className="row content-text">*/}
        {/*<div className="col-md-12 thanks-for text-title-approve">*/}
        {/*<h2 className="text-center">Congratulations!</h2>*/}
        {/*<p className="mt-2 text-center">*/}
        {/*We sincerely congratulate you on becoming a fi :Star.*/}
        {/*</p>*/}
        {/*</div>*/}
        {/*<div className="description-text col-md-12">*/}
        {/*<div className="content-text-description">*/}
        {/*<p>*/}
        {/*Challenge various campaigns after signing in with fi*/}
        {/*:Star.*/}
        {/*</p>*/}
        {/*<p>*/}
        {/*<strong>Show your special talent on fi :Star!</strong>*/}
        {/*</p>*/}
        {/*</div>*/}
        {/*<div className="content-text-description">*/}
        {/*<p>Please check your personal information once more</p>*/}
        {/*<p>to have a smooth campaign.</p>*/}
        {/*</div>*/}
        {/*</div>*/}
        {/*<div className="col-md-12 main-redirect">*/}
        {/*<div className="button-group-thanks">*/}
        {/*<NavLink*/}
        {/*className="text-center w-100"*/}
        {/*to={routeName.FISTAR_LOGIN}*/}
        {/*>*/}
        {/*fi :Star LOGIN*/}
        {/*</NavLink>*/}
        {/*</div>*/}
        {/*</div>*/}
        {/*</div>*/}
        {/*</div>*/}
        {/*</div>*/}
        {/*</div>*/}
        {/*</div>*/}
        {/*</div>*/}
        {/*</section>*/}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(ApproveRegister));
