import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { changePasswordAction } from "./../../../../store/actions/auth";
import { Modal, Button } from "react-bootstrap";
import * as routeName from "./../../../../routes/routeName";
import cancelImage from "./../../../../images/cancel.svg";

class FistarForgotPasswordStep3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        password: "",
        password_confirmation: ""
      },
      errors: {
        password: "",
        password_confirmation: "",
        message: ""
      },
      success: false,
      loading: false,
      isRedirect: false
    };
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        password: "",
        password_confirmation: "",
        message: ""
      }
    }));
  };

  validateForm = () => {
    const errors = {
      email: "",
      password: "",
      password_confirmation: ""
    };
    const { data } = this.state;
    const { t } = this.props;
    if (!data.password) {
      errors.password = t("FISTAR_RESETPW.FRP_erro_pw");
    } else if (data.password.length < 6) {
      errors.password = t("FISTAR_RESETPW.FRP_erro_pw_limit_6");
    }
    if (!data.password_confirmation) {
      errors.password_confirmation = t("FISTAR_RESETPW.FRP_erro_confirm_pw");
    } else if (data.password !== data.password_confirmation) {
      errors.password_confirmation = t(
        "FISTAR_RESETPW.FRP_erro_confirm_pw_not_match"
      );
    }

    return errors;
  };

  submitForm = e => {
    e.preventDefault();
    const validate = this.validateForm();
    if (
      !validate.email &&
      !validate.password &&
      !validate.password_confirmation
    ) {
      if (!this.state.loading) {
        const { data } = this.state;
        const { email, code } = this.props;
        this.setState(
          {
            loading: true
          },
          () => {
            data.email = email;
            data.code = code;
            this.props
              .changePassword(data)
              .then(response => {
                this.setState({
                  success: true,
                  loading: false
                });
                this.props.onNextStep(3);
              })
              .catch(e => {
                if (e.data.errors.email || e.data.errors.code) {
                  alert("Something went wrong");
                  this.setState({
                    loading: false
                  });
                  return;
                }
                const errors = {
                  password: "",
                  password_confirmation: "",
                  message: ""
                };
                if (e.data.errors) {
                  errors.password = e.data.errors.password
                    ? e.data.errors.password[0]
                    : "";
                }
                errors.message =
                  e.data.status == false
                    ? this.props.t("FISTAR_RESETPW.FRP_erro_code_wrong")
                    : "";
                this.setState({
                  errors,
                  loading: false
                });
              });
          }
        );
      }
    } else {
      this.setState({
        errors: validate
      });
    }
  };

  handleClose = () => {
    this.setState({ success: false });
  };

  handleShow = () => {
    this.setState({ success: true });
  };

  onExited = () => {
    this.setState({
      isRedirect: true
    });
  };

  render() {
    const { t } = this.props;
    const { data, errors, success, loading, isRedirect } = this.state;

    if (isRedirect) {
      return <Redirect to={routeName.FISTAR_LOGIN} />;
    }

    return (
      <Fragment>
        <form className="container-form" onSubmit={this.submitForm}>
          <div className="wf-form-content search-login">
            <div className="row">
              <div className="col-md-12 mb-3">
                <a href="javascript:viod(0)" className="text-login-form">
                  {t("FISTAR_RESETPW.FRP_fiStar_find_password")}
                </a>
              </div>
            </div>
            <div className="row row-eq-height">
              <div className="col-md-12">
                <div className="form-input">
                  <div className="input-group mb-3">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fas fa-lock-open" />
                      </span>
                    </div>
                    <input
                      placeholder={t("FISTAR_RESETPW.FRP_input_new_pw")}
                      type="password"
                      className="empty"
                      id="password"
                      name="password"
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                    />
                    {errors.password && (
                      <div className="tooptip-search-login">
                        <p>{errors.password}</p>
                      </div>
                    )}
                  </div>
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fas fa-lock-open" />
                      </span>
                    </div>
                    <input
                      placeholder={t("FISTAR_RESETPW.FRP_confirm_new_pw")}
                      type="password"
                      className="empty mb-0"
                      id="password_confirmation"
                      name="password_confirmation"
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                    />
                    {errors.password_confirmation && (
                      <div className="tooptip-search-login">
                        <p>{errors.password_confirmation}</p>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="row btn-submit-pass">
              <div className="col-md-12">
                <button
                  className="btn btn-submit-pass-rs"
                  onClick={this.submitForm}
                >
                  {loading ? (
                    <div className="spinner-border text-primary" role="status">
                      <span className="sr-only">{t("LOADING.LOADING")}</span>
                    </div>
                  ) : (
                    <Fragment>
                      {t("FISTAR_RESETPW.FRP_button_new_pw_ok")}
                    </Fragment>
                  )}
                </button>
              </div>
            </div>
          </div>
        </form>
        <Modal
          show={this.state.success}
          onHide={this.handleClose}
          onExited={this.onExited}
          dialogClassName="howto-register modal-dialog-centered"
        >
          <div className="modal-content">
            <div className="register">
              <div className="top modal-header">
                <h4>{t("FISTAR_RESETPW.FRP_title_fistar")}</h4>
                <button
                  type="button"
                  className="btn btn-close close"
                  onClick={this.handleClose}
                >
                  <img src={cancelImage} alt="close" />
                </button>
              </div>
              <div className="body-popup">
                <p className="mb-3">{t("RESET_PW.PW_reset")}</p>
                <p>
                  <small>{t("POPUP_RESET_PASS.PRP_text")}</small>
                </p>
                <button type="button" onClick={this.handleClose}>
                  {t("POPUP_RESET_PASS.PRP_button_ok")}
                </button>
              </div>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    changePassword: data => dispatch(changePasswordAction(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarForgotPasswordStep3));
