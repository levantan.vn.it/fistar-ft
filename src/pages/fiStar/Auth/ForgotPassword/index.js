import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { findEmailAction } from "./../../../../store/actions/auth";
import FistarForgotPasswordStep1 from "./Step1";
import FistarForgotPasswordStep2 from "./Step2";
import FistarForgotPasswordStep3 from "./Step3";

class FistarForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      email: "",
      code: ""
    };
  }

  clickNextStep = (step, data = {}) => {
    this.setState(prevState => ({
      step: step,
      ...data
    }));
  };

  renderStep = () => {
    const { step } = this.state;
    switch (step) {
      case 1:
        return <FistarForgotPasswordStep1 onNextStep={this.clickNextStep} />;
        break;
      case 2:
        return (
          <FistarForgotPasswordStep2
            onNextStep={this.clickNextStep}
            email={this.state.email}
            id={this.state.id}
          />
        );
        break;
      case 3:
        return (
          <FistarForgotPasswordStep3
            onNextStep={this.clickNextStep}
            email={this.state.email}
            code={this.state.code}
          />
        );
        break;

      default:
        return null;
        break;
    }
  };

  render() {
    return <Fragment>{this.renderStep()}</Fragment>;
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    findEmail: data => dispatch(findEmailAction(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarForgotPassword));
