import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { forgotPasswordAction } from "./../../../../store/actions/auth";
import { Modal, Button } from "react-bootstrap";
import * as routeName from "./../../../../routes/routeName";
import cancelImage from "./../../../../images/cancel.svg";

class FistarForgotPasswordStep1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        id: "",
        email: ""
      },
      errors: {
        id: "",
        email: "",
        message: ""
      },
      success: false,
      loading: false
    };
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        id: "",
        email: "",
        message: ""
      }
    }));
  };

  submitForm = e => {
    e.preventDefault();
    const { t } = this.props;
    if (!this.state.loading) {
      const { data } = this.state;
      console.log(data);
      this.setState(
        {
          loading: true
        },
        () => {
          this.props
            .forgotPassword(data)
            .then(response => {
              this.setState({
                success: true,
                loading: false
              });
              this.handleShow();
              // setTimeout(() => {
              // body...
              // this.props.onNextStep(2, {email: data.email})
              // }, 1000);
            })
            .catch(e => {
              const errors = {
                id: "",
                email: "",
                message: ""
              };
              if (e.data.errors) {
                errors.id = e.data.errors.id ? e.data.errors.id[0] : "";
                if (errors.id && errors.id == "The selected id is invalid.") {
                  errors.id = t("FISTAR_RESETPW.FRP_id_incorrect_wrong");
                }
                errors.email = e.data.errors.email
                  ? e.data.errors.email[0]
                  : "";
                if (
                  errors.email &&
                  errors.email == "The selected email is invalid."
                ) {
                  errors.email = t("FISTAR_RESETPW.FRP_email_incorrect_wrong");
                }
              }
              // errors.message =
              //   e.data.message == false
              //     ? t("FISTAR_RESETPW.FRP_email_incorrect")
              //     : "";
              this.setState({
                errors,
                loading: false
              });
            });
        }
      );
    }
  };

  handleClose = () => {
    this.setState({ success: false });
  };

  handleShow = () => {
    this.setState({ success: true });
  };

  onExited = () => {
    this.props.onNextStep(2, {
      email: this.state.data.email,
      id: this.state.data.id
    });
  };

  render() {
    const { t } = this.props;
    const { data, errors, success, loading } = this.state;

    return (
      <Fragment>
        <form
          className="wf-form-content search-login"
          onSubmit={this.submitForm}
        >
          <div className="row">
            <div className="col-md-12 mb-3">
              <a className="text-login-form" href="javascript:void(0)">
                {t("FISTAR_RESETPW.FRP_fiStar_find_password")}
              </a>
            </div>
          </div>
          <div className="row row-eq-height">
            <div className="col-md-12">
              <div className="form-input">
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-user-alt" />
                    </span>
                  </div>
                  <input
                    placeholder={t("FISTAR_RESETPW.FRP_id")}
                    type="text"
                    name="id"
                    id="id"
                    value={data.id}
                    className="empty"
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                  />
                  {errors.id && (
                    <div className="tooptip-search-login d-flex align-items-center">
                      {/*<p>가입아이디가 정확하지 않습니다. 다시 한번 확인해주세요!!</p>*/}
                      <p>{errors.id}</p>
                    </div>
                  )}
                </div>
                <div className="input-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-envelope" />
                    </span>
                  </div>
                  <input
                    placeholder={t("FISTAR_RESETPW.FRP_email")}
                    type="text"
                    name="email"
                    id="email"
                    value={data.email}
                    className="empty
                    mb-0"
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                  />
                  {(errors.email || errors.message) && (
                    <div className="tooptip-search-login d-flex align-items-center">
                      {/*<p>가입아이디가 정확하지 않습니다. 다시 한번 확인해주세요!!</p>*/}
                      <p>{errors.message ? errors.message : errors.email}</p>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <div className="text-search-login">
                <p className="text-right mt-2 mb-4">
                  {/*<a href="javascript:void(0)" style={{ cursor: "unset" }}>*/}
                  {/*{t("FISTAR_RESETPW.FRP_email_you_can")}*/}
                  {/*</a>*/}
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              {/*success && (
                <div className="alert alert-success" role="alert">
                  We send an email to your email.<br /> Thanks!
                </div>
              )*/}
              <button
                className="btn btn-submit-search mt-3"
                onClick={this.submitForm}
              >
                {loading ? (
                  <div className="spinner-border text-primary" role="status">
                    <span className="sr-only">{t("LOADING.LOADING")}</span>
                  </div>
                ) : (
                  <Fragment>
                    {t("FISTAR_RESETPW.FRP_button_very_next")}
                  </Fragment>
                )}
              </button>
            </div>
          </div>
        </form>
        <Modal
          show={this.state.success}
          onHide={this.handleClose}
          onExited={this.onExited}
          dialogClassName="howto-register modal-dialog-centered"
        >
          <div className="modal-content">
            <div className="register">
              <div className="top modal-header">
                <h4>fiStar</h4>
                <button
                  type="button"
                  className="btn btn-close close"
                  onClick={this.handleClose}
                >
                  <img src={cancelImage} alt="close" />
                </button>
              </div>
              <div className="body-popup">
                <p className="mb-3">{t("FISTAR_RESETPW.FRP_popup_title")}</p>
                <p>
                  <small>{t("FISTAR_RESETPW.FRP_popup_description")}</small>
                </p>
                <button type="button" onClick={this.handleClose}>
                  {t("FISTAR_RESETPW.FRP_popup_bt_ok")}
                </button>
              </div>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    forgotPassword: data => dispatch(forgotPasswordAction(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarForgotPasswordStep1));
