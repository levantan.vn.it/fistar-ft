import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import MyCampaign from "../../../components/fiStar/Dashboard/MainDashboard/myCampaign";
import NewCampaign from "../../../components/fiStar/Dashboard/MainDashboard/newCampaign";
import RighComponent from "../../../components/fiStar/Dashboard/MainDashboard/Right";
import "./Dashboard.css";
import { getCountCampaignAction } from "./../../../store/actions/campaign";
import * as routeName from './../../../routes/routeName'

class FistarDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: null,
      reset: false
    };
  }

  componentDidMount() {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
     window.scrollTo(0, 0);
    }
    this.props.getCountCampaign().then(response => {
      this.setState({
        count: response
      });
    });
  }

  callback = () => {
    this.props.getCountCampaign().then(response => {
      this.setState({
        count: response
      });
    });
  };

  applyCallback = () => {
    console.log("applyCallback");
    this.setState({
      reset: true
    });
  };

  changeReset = () => {
    console.log("changeReset");
    this.setState({
      reset: false
    });
  };

  render() {
    const { t } = this.props;
    const { count } = this.state;

    // if (this.props.auth.first_login === 1) {
    //   return <Redirect to={routeName.FISTAR_APPROVE_REGISTER}/>
    // }

    return (
      <div className="content mypage-fistar-dashboard fistar-dashbord">
        <div className="campaign-track">
          <MyCampaign
            height={"119vh"}
            count={count}
            callback={this.callback}
            reset={this.state.reset}
            changeReset={this.changeReset}
          />
        </div>
        <NewCampaign
          height={"119vh"}
          count={count}
          callback={this.callback}
          applyCallback={this.applyCallback}
        />

        <RighComponent />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getCountCampaign: () => dispatch(getCountCampaignAction())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarDashboard));
