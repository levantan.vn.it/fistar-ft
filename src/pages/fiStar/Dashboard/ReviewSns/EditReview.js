import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class EditReview extends Component {
  render() {
    const { t } = this.props;
    return (
      <div className="content  myfistar-mycampaign fime-review-write">
        <div className="container">
          <div className="top-fime-review">
            <div className="name left-fimereview">
              <span className="wrap-matching">Ready</span>
              <div className="text-campaign">
                <p>LILYBYRED</p>
                <h4>LILYBYRED MOOD CHEAT KIT</h4>
              </div>
            </div>
          </div>
          <div className="result-fimereview">
            <div className="category">
              <div className="key-name">
                <span className="key">Channel</span>
                <span className="name">fime</span>
              </div>
              <div className="key-name">
                <span className="key">Category</span>
                <span className="name">SKIN</span>
              </div>
            </div>
            <div className="etude-review">
              <h4>Etude House Review 1st</h4>
              <p>2019. 05. 01</p>
            </div>
          </div>
          <div className="content-review">
            <textarea
              className="form-control"
              id="exampleFormControlTextarjkea1"
              rows="15"
            />
          </div>
          <div className="gr-btn-review">
            <button>Cancel</button>
            <button>Save</button>
          </div>
          <div className="save-sns-url">
            <h4 className="title">
              Please input (copy) the final review URL registered in SNS.
            </h4>
            <div className="content-sns">
              <div className="left-save-sns">
                <textarea
                  className="form-control"
                  id="exampleFoghrmControlTextarvcea1"
                  rows="5"
                >
                  https://www.facebook.com/cheongrae1/?__tn__=kC-R&eid=ARBRbcLILqiqBB1Ff7wfbdMTOzWrraic5hxNsaocL8JztQBx43srKX_ugEduJ2NBFP3E_HWCCPk6QcC7&hc_ref=ARTic_HT-imrekxr4_lmAx3l683UeLtP5_kXu9YFW6ePqkllqf2ogCr_5bA6lE3_hZ4&fref=nf
                </textarea>
              </div>
              <div className="right-save-sns">
                <button className="btn-save-sns">Save SNS URL</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  withTranslation("translations")(EditReview)
);
