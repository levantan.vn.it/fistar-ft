import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import * as routeName from "./../../../../../routes/routeName";
import { getQADetailAction } from "../../../../../store/actions/auth";
import FistarAnswerQuestions from './../AnswerQuestions'
import { getLastNestedObject } from "../../../../../common/helper";

class FistarQADetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isLoadingHave: false,
      qaDetail: {},
      lastDetail: null,
    };
  }

  componentDidMount() {
    const {
      match: { params }
    } = this.props;
    const { id } = params;

    this.setState(
      {
        isLoading: true
      },
      () => {
        this.props
          .getQaDetail(id)
          .then(response => {
            this.setState({
              qaDetail: response,
              lastDetail: getLastNestedObject(response, 'children'),
              isLoading: false,
              isLoadingHave: true
            });
          })
          .catch(() => {
            this.setState({
              isLoading: false
            });
          });
      }
    );
  }

  renderQA = (qaDetail, isShowTilte = true) => {
    const { t } = this.props;
    const { data, errors, success, isLoadingHave } = this.state;

    return <React.Fragment>
      <div className="question-campaign">
        {isShowTilte && <div className="title">
          <h5>{qaDetail.qa_title}</h5>
          <p className="date">
            {qaDetail.created_at
              ? qaDetail.created_at.split(" ").shift()
              : ""}
          </p>
        </div>}
        <div className="question">
          <p>{qaDetail.qa_question} </p>
        </div>
      </div>
      <div className="replied-campaign">
        <div className="top top-replied-fistar">
          {isLoadingHave ? (
            <button
              className={
                qaDetail.qa_state == 0 ? "preparing-content" : "completed"
              }
            >
              {qaDetail.qa_state == 0
                ? `${t("QA_DETAIL.QDL_button_preparing")}`
                : `${t("QA_DETAIL.QDL_button_completed")}`}
            </button>
          ) : (
            ""
          )}
          <p className="date">
            {qaDetail.qa_answer
              ? qaDetail.created_at
                ? qaDetail.created_at.split(" ").shift()
                : ""
              : ""}
          </p>
        </div>
        <div className="replied">
          <p>
            {qaDetail.qa_answer
              ? qaDetail.qa_answer
              : "Answer is in preparation."}
          </p>
        </div>
      </div>
      {qaDetail.children && this.renderQA(qaDetail.children, false)}
    </React.Fragment>
  }

  render() {
    const { t } = this.props;
    const { data, errors, success, qaDetail, isLoadingHave, lastDetail } = this.state;
    return (
      <div className="content campaign-tracking-campaign-01">
        <div className="container">
          <div className="top-partner-qa">
            <div className="left">
              <h4>
                {t("QA_CONTACT_US.QCU_qa")}
                <span>{t("QA_CONTACT_US.QCU_text_check")}</span>
              </h4>
            </div>
            <div className="right trwf-fistar">
              <NavLink
                to={routeName.FISTAR_QA_ANSWER_QUESTION}
                className="contact-us"
              >
                {t("QA_CONTACT_US.QCU_button_contact")}
              </NavLink>
            </div>
          </div>
          {this.renderQA(qaDetail)}
          <div className={`btn-list ${lastDetail && lastDetail.qa_state != 0 ? 'mb-0' : ''}`}>
            <NavLink className="to-list" to={routeName.FISTAR_QA}>
              List{" "}
            </NavLink>
          </div>
          {lastDetail && lastDetail.qa_state != 0 && <FistarAnswerQuestions qaDetail={lastDetail} />}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    getQaDetail: id => dispatch(getQADetailAction(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarQADetail));
