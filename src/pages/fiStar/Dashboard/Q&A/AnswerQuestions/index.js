import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import * as routeName from "./../../../../../routes/routeName";
import { getCodeAction } from "../../../../../store/actions/code";
import { createQAAction } from "../../../../../store/actions/auth";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import closePopup from "./../../../../../images/close-popup.svg";

class FistarAnswerQuestions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        qa_category: "",
        qa_title: "",
        qa_question: "",
        qa_state: 0,
        qa_type: 1,
        parent_id: 0
      },
      show: false,
      success: false,
      redirectList: false,
      errors: {
        qa_category: "",
        qa_title: "",
        qa_question: "",
        message: ""
      },
      qaDetail: null,
    };
  }
  componentDidMount() {
    this.props.getCode("catalog");
    this.props.getCode("qa");
    if (this.props.qaDetail && this.props.qaDetail.qa_id) {
      this.setState({
        qaDetail: this.props.qaDetail,
        data: {
          ...this.state.data,
          qa_category: this.props.qaDetail.qa_category,
          parent_id: this.props.qaDetail.qa_id,
          qa_title: this.props.qaDetail.qa_title,
        },
      })
    }
  }

  componentDidUpdate(preProps, preState) {
    if (
      this.props.qaDetail && this.props.qaDetail.qa_id 
      && preProps.qaDetail
      && this.props.qaDetail.qa_id != preProps.qaDetail.qa_id
    ) {
      this.setState({
        qaDetail: this.props.qaDetail,
        data: {
          ...this.state.data,
          qa_category: this.props.qaDetail.qa_category,
          parent_id: this.props.qaDetail.qa_id,
          qa_title: this.props.qaDetail.qa_title,
        },
      })
    }
  }
  
  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        qa_category: "",
        qa_title: "",
        qa_question: "",
        message: ""
      }
    }));
  };

  validateForm = () => {
    const errors = {
      qa_category: "",
      qa_title: "",
      qa_question: "",
      message: ""
    };
    const { data } = this.state;

    if (!data.qa_category) {
      errors.qa_category = "The category field is required.";
    }
    if (!data.qa_title) {
      errors.qa_title = "The title field is required.";
    }
    if (!data.qa_question) {
      errors.qa_question = "The content field is required.";
    }

    return errors;
  };

  onSubmit = e => {
    e.preventDefault();
    const { data, errors } = this.state;
    const validate = this.validateForm();
    if (
      validate.qa_question == "" &&
      validate.qa_title == "" &&
      validate.qa_category == ""
    ) {
      this.props
        .createQa(data)
        .then(response => {
          this.setState({
            success: true,
            loading: false,
            show: true
          });
        })
        .catch(e => {
          const errors = {
            qa_category: "",
            qa_title: "",
            qa_question: "",
            message: ""
          };
          if (e.data.errors) {
            errors.qa_question =
              e.data.errors && e.data.errors.qa_question
                ? e.data.errors.qa_question[0]
                : "";
            errors.qa_category =
              e.data.errors && e.data.errors.qa_category
                ? e.data.errors.qa_category[0]
                : "";
            errors.qa_title =
              e.data.errors && e.data.errors.qa_title
                ? e.data.errors.qa_title[0]
                : "";
          }
          this.setState({
            errors,
            loading: false
          });
        });
    } else {
      this.setState(
        {
          errors: validate
        },
        () => {
        }
      );
    }
  };

  renderCategory = () => {
    const { data, errors } = this.state;
    const {
      code: {
        data: { qa }
      }
    } = this.props;

    if (!qa) return;
    console.log(!this.state.qaDetail);

    return (
      <div className="left">
        {!this.state.qaDetail && <React.Fragment>
          <select
            className={`form-control form-control-sm dfgdf`}
            value={data.qa_category}
            onChange={this.handleChangeInput}
            name="qa_category"
            id="qa_category"
          >
            <option>{this.props.t("ANSWER_QUESTION.AQN_category")}</option>
            {(qa.code || []).map((qa, key) => (
              <option value={qa.cd_id} key={key}>
                {qa.cd_label}
              </option>
            ))}
          </select>
          <i className="fas fa-chevron-down" />
          {errors.qa_category && (
            <div className="tooptip-text">
              <p className="text-danger">{errors.qa_category}</p>
            </div>
          )}
        </React.Fragment>}
        {this.state.qaDetail && <select
          className={`form-control form-control-sm`}
          value={data.qa_category}
          name="qa_category"
          id="qa_category"
          readOnly
          disabled
        >
          <option>{this.props.t("ANSWER_QUESTION.AQN_category")}</option>
          {(qa.code || []).map((qa, key) => (
            <option value={qa.cd_id} key={key}>
              {qa.cd_label}
            </option>
          ))}
        </select>}
      </div>
    );
  };
  backList = () => {
    this.setState({
      redirectList: true
    });
  };

  CloseModal = () => {
    this.props.closeModal();
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  handleShow = () => {
    this.setState({ show: true });
  };

  render() {
    const { t } = this.props;
    const { data, errors, success, redirectList } = this.state;
    if (redirectList) {
      return <Redirect to={routeName.FISTAR_QA} />;
    }

    return (
      <div className={`content campaign-tracking-campaign-01 ${this.state.qaDetail ? 'w-100' : ''}`}>
        <div className="container">
          {!this.state.qaDetail && <div className="top-partner-qa">
            <div className="left">
              <h4>
                {t("QA_CONTACT_US.QCU_qa")}
                <span>{t("ANSWER_QUESTION.AQN_tittle_fistar")}</span>
              </h4>
            </div>
          </div>}
          <div className={`make-question ${this.state.qaDetail ? 'pt-0' : ''}`}>
            <form action="" onSubmit={this.onSubmit}>
              {!this.state.qaDetail && <div className="title-question">
                {this.renderCategory()}
                <div className="right">
                  {!this.state.qaDetail && <input
                    type="text"
                    className="form-control"
                    id="question01"
                    name="qa_title"
                    aria-describedby="emailHelp"
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                    placeholder={t("ANSWER_QUESTION.AQN_input_placeholder")}
                  />}
                  {this.state.qaDetail && <input
                    type="text"
                    className="form-control"
                    id="question01"
                    name="qa_title"
                    aria-describedby="emailHelp"
                    value={this.state.data.qa_title}
                    readOnly
                  />}
                  {errors.qa_title && (
                    <div className="tooptip-text">
                      <p className="text-danger">{errors.qa_title}</p>
                    </div>
                  )}
                </div>
              </div>}
              <div className="content-questipon">
                <textarea
                  className="form-control"
                  id="exampleFormContdfrolTextarea1"
                  rows="18"
                  name="qa_question"
                  onChange={this.handleChangeInput}
                  onFocus={this.onFocusInput}
                  placeholder="Please enter the content of your inquiry."
                />
                {errors.qa_question && (
                  <div className="tooptip-text">
                    <p className="text-danger">{errors.qa_question}</p>
                  </div>
                )}
              </div>

              <div className="gr-btn">
                <button onClick={this.backList}>Cancel</button>
                <button onClick={this.onSubmit}>OK</button>
              </div>
            </form>
          </div>
        </div>

        <Modal
          size="lg"
          show={this.state.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="modal-partner-pass modal-dialog-centered"
        >
          <div className="popup-partner-pass">
            <div className="top">
              <h4>Fistar</h4>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.handleClose}
              >
                <img src={closePopup} alt="" />
              </button>
            </div>
            <div className="content">
              <p>Create question success!</p>
              <button onClick={this.backList}>OK</button>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    code: state.code
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCode: type => dispatch(getCodeAction(type)),
    createQa: data => dispatch(createQAAction(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarAnswerQuestions));
