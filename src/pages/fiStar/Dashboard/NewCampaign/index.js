import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
import {
  getNewCampaignAction,
  getCountCampaignAction
} from "../../../../store/actions/campaign";

import NewCampaign from "./../../../../components/fiStar/Dashboard/NewCampaign/Campaign";

const TYPE = {
  ALL: "",
  SCRAP: "scrap",
  REQUEST: "request",
  APPLY: "apply",
  RECOMMENDED: "recommended"
};

class FistarNewCampaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: TYPE.ALL,
      campaigns: [],
      newCampaigns: [],
      total: 0,
      // kind:'',
      ready: 0,
      onGoing: 0,
      request: 0,
      scrap: 0,
      apply: 0,
      recommended: 0,
      closed: 0,
      textTitle: `${this.props.t("MY_CAMPAIGN.MCN_tab_allcampaign")}`,
      last_page: 1,
      page: 1,
      hasMore: true,
      isLoadingCampaign: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoadingCampaign: true
      },
      () => {
        this.initData();
      }
    );
  }

  initData = () => {
    this.props
      .getNewCampaign(this.state.page, this.state.type)
      .then(response => {
        console.log(response);
        this.setState({
          newCampaigns: response.data,
          total: this.state.total ? this.state.total : response.total,
          isLoadingCampaign: false,
          last_page: response.last_page
        });
      })
      .catch(() => {
        this.setState({
          isLoadingCampaign: false
        });
      });
    this.props
      .getCountCampaign()
      .then(response => {
        console.log(response);
        this.setState({
          request: response.request,
          scrap: response.scrap,
          apply: response.apply,
          recommended: response.recommended
        });
      })
      .catch(() => {
        this.setState({
          isLoadingCampaign: false,
          reset: false
        });
      });
  };

  clickChangeTab = type => {
    let textTitle = "";
    const { t } = this.props;
    switch (type) {
      case TYPE.SCRAP: {
        textTitle = `${t("DASHBOARD_FISTAR.DBF_status_scrap")}`;
        break;
      }
      case TYPE.REQUEST: {
        textTitle = `${t("DASHBOARD_FISTAR.DBF_status_request")}`;
        break;
      }
      case TYPE.APPLY: {
        textTitle = `${t("DASHBOARD_FISTAR.DBF_status_apply")}`;
        break;
      }
      case TYPE.RECOMMENDED: {
        textTitle = `${t("DASHBOARD_FISTAR.DBF_status_recommended")}`;
        break;
      }
      default: {
        textTitle = `${t("MY_CAMPAIGN.MCN_tab_allcampaign")}`;
        break;
      }
    }
    this.setState(
      {
        type,
        page: 1,
        isLoadingCampaign: true,
        textTitle
      },
      () => {
        this.initData();
      }
    );
  };

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }

    this.props
      .getNewCampaign(this.state.page + 1, this.state.type)
      .then(response => {
        this.setState({
          newCampaigns: [...this.state.newCampaigns, ...response.data],
          page: this.state.page + 1
        });
      });
  };

  render() {
    const { t } = this.props;
    const {
      type,
      campaigns,
      newCampaigns,
      total,
      kind,
      request,
      apply,
      scrap,
      recommended,
      textTitle,
      hasMore,
      isLoadingCampaign
    } = this.state;

    console.log(this.state);

    let totalCam = total;
    switch (type) {
      case TYPE.SCRAP: {
        totalCam = scrap;
        break;
      }
      case TYPE.REQUEST: {
        totalCam = request;
        break;
      }
      case TYPE.APPLY: {
        totalCam = apply;
        break;
      }
      case TYPE.RECOMMENDED: {
        totalCam = recommended;
        break;
      }
      default: {
        totalCam = total;
        break;
      }
    }

    return (
      <div className="content myfistar-campaign mypage-partner-campaign-tracking">
        <div className="top">
          <div className="title">
            <h4>
              <a href="javascript:void(0)">
                {t("DASHBOARD_FISTAR.DFR_new_campaign")}
              </a>
            </h4>
          </div>
        </div>
        <div className="information-tracking">
          <ul className="list-information list-myfistar-title">
            <li
              className={`item item-myfistar${
                type === TYPE.ALL ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(TYPE.ALL)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">
                  {isLoadingCampaign && !total ? "~" : total}
                </p>
                <p>{t("MY_CAMPAIGN.MCN_tab_allcampaign")}</p>
              </a>
            </li>
            <li
              className={`item item-myfistar${
                type === TYPE.SCRAP ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(TYPE.SCRAP)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">
                  {isLoadingCampaign && !scrap ? "~" : scrap}
                </p>
                <p>{t("DASHBOARD_FISTAR.DFR_status_scrap")}</p>
              </a>
            </li>
            <li
              className={`item item-myfistar${
                type === TYPE.REQUEST ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(TYPE.REQUEST)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">
                  {isLoadingCampaign && !request ? "~" : request}
                </p>
                <p>{t("DASHBOARD_FISTAR.DFR_status_request")}</p>
              </a>
            </li>
            <li
              className={`item item-myfistar${
                type === TYPE.APPLY ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(TYPE.APPLY)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">
                  {isLoadingCampaign && !apply ? "~" : apply}
                </p>
                <p>{t("DASHBOARD_FISTAR.DFR_button_apply")}</p>
              </a>
            </li>
            <li
              className={`item item-myfistar${
                type === TYPE.RECOMMENDED ? " active" : ""
              }`}
              onClick={() => this.clickChangeTab(TYPE.RECOMMENDED)}
            >
              <a href="javascript:void(0)" className="text">
                <p className="number">
                  {isLoadingCampaign && !recommended ? "~" : recommended}
                </p>
                <p>{t("DASHBOARD_FISTAR.DFR_status_recommend")}</p>
              </a>
            </li>
          </ul>
        </div>
        <div className="list-campaign-track">
          <div className="title">
            <h4>{`${textTitle} ${totalCam}`}</h4>
          </div>
          {isLoadingCampaign ? (
            <div className="list-campaign-track text-center">
              <div className="spinner-border" role="status">
                <span className="sr-only">{t("LOADING.LOADING")}</span>
              </div>
            </div>
          ) : (
            <InfiniteScroll
              dataLength={newCampaigns.length}
              next={this.fetchMoreData}
              loader={<h4>{t("LOADING.LOADING")}</h4>}
              hasMore={this.state.hasMore}
              className="box-item"
            >
              {newCampaigns.map((campaign, key) => (
                <Fragment key={key}>
                  <NewCampaign campaign={campaign} />
                </Fragment>
              ))}
            </InfiniteScroll>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    getNewCampaign: (page = 1, type = "") =>
      dispatch(getNewCampaignAction(page, type)),
    getCountCampaign: () => dispatch(getCountCampaignAction())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarNewCampaign));
