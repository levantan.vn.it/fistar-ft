import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import imgaeIdol from "./../../../../images/dash-02.png";
import imgaeLeft from "./../../../../images/left.svg";
import {
  getCampaignDetailFistarAction,
  updateReviewAction
} from "./../../../../store/actions/campaign";
import InfoCampaignDetail from "./../../../../components/fiStar/Dashboard/MyCampaignDetail/Campaign";
import SnsSocialReview from "./../../../../components/fiStar/Dashboard/MyCampaignDetail/SnsSocialReview";
import * as routeName from "./../../../../routes/routeName";

const SNS_STATE = {
  READY: 101,
  REQUEST: 102,
  CHECKING: 103,
  MODIFY: 104,
  COMPLETE: 105
};

class FistarMyCampaignDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      campaign: {},
      isLoadingCampaign: false,
      isBack: false,
        isBackSearchCampaign: false,
        locationLink:""
    };
  }

  componentDidMount() {
    this.state.locationLink = document.referrer
    const {
      match: { params }
    } = this.props;
    const { id } = params;
    this.setState(
      {
        isLoadingCampaign: true
      },
      () => {
        this.initData(id);
      }
    );
  }

  initData = id => {
    return this.props
      .getCampaign(id)
      .then(response => {
        this.setState({
          campaign: response,
          isLoadingCampaign: false
        });
      })
      .catch(() => {
        this.setState({
          isLoadingCampaign: false
        });
        this.props.history.push(routeName.FISTAR_DASHBOARD)
      });
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      this.initData(this.props.match.params.id);
    }
  }

  onClickBack = () => {
    this.setState({
      isBack: true
    });
  };
    onClickBackSearchCampaign = () => {
        this.setState({
            isBackSearchCampaign: true
        });
    };

  getSNSReview = () => {
    const { auth } = this.props;
    const { campaign, isLoadingCampaign } = this.state;
    if (!isLoadingCampaign && Object.keys(campaign).length > 0) {
      let sns = [];
      campaign.matchings.map(fistar => {
        if (fistar.influencer.email == auth.email) {
          sns = fistar.matching_channel;
        }
      });
      return sns;
      // let fistar = campaign.matchings.map((fistar) => {
      //   if (fistar.influencer.email === auth.email) {
      //     return fistar
      //   }
      //   return null
      // })
      // fistar = fistar.filter(e => e != null)
      // fistar = fistar[0]
      // if (!!fistar) {
      //   return fistar.v_review_status
      // }
    }
    return [];
  };
  getMStatus = () => {
    const { auth } = this.props;
    const { campaign, isLoadingCampaign } = this.state;
    if (!isLoadingCampaign && Object.keys(campaign).length > 0) {
      let matching_status = "";
      campaign.matchings.map(fistar => {
        if (fistar.influencer.email == auth.email) {
          matching_status=fistar.matching_status.m_status;
        }
      });
      return matching_status;
      // let fistar = campaign.matchings.map((fistar) => {
      //   if (fistar.influencer.email === auth.email) {
      //     return fistar
      //   }
      //   return null
      // })
      // fistar = fistar.filter(e => e != null)
      // fistar = fistar[0]
      // if (!!fistar) {
      //   return fistar.v_review_status
      // }
    }
    return [];
  };

  callrequest = (id, data) => {
    return this.props.updateReview(id, data).then(() => {
      return this.setState({}, () => {
        return this.initData(this.props.match.params.id);
      });
    });
  };

  render() {
    const { t, auth } = this.props;
    const { campaign, isLoadingCampaign, isBack, isBackSearchCampaign } = this.state;

    if (isBack) {
      return <Redirect to={routeName.FISTAR_MY_CAMPAIGN} />;
    }
    if(isBackSearchCampaign) {
        return <Redirect to={routeName.FISTAR_SEARCH_CAMPAIGN} />;
    }
      const {matchings}=campaign;
      let titleCampaign = "";
      let listCampaign = "";
        if(!isLoadingCampaign) {
            if(campaign.matchings && campaign.matchings.length > 0) {
                let uid = campaign.matchings.filter(uid => uid.influencer.email == this.props.auth.email)
                if(uid.length > 0) {
                    titleCampaign = t("MY_CAMPAIGN_DETAIL.MCD_my_campaign");
                    listCampaign = <button onClick={this.onClickBack}>
                        <img src={imgaeLeft} alt="back" />{" "}
                        {t("MY_CAMPAIGN_DETAIL.MCD_button_list")}
                    </button>
                }else {
                    titleCampaign = t("CAMPAIGN_SEARCH.CSH_campaign_search");
                    listCampaign = <button onClick={this.onClickBackSearchCampaign}>
                        <img src={imgaeLeft} alt="back" />{" "}
                        {t("MY_CAMPAIGN_DETAIL.MCD_button_list")}
                    </button>
                }
            }else {
                titleCampaign = t("CAMPAIGN_SEARCH.CSH_campaign_search");
                listCampaign = <button onClick={this.onClickBackSearchCampaign}>
                    <img src={imgaeLeft} alt="back" />{" "}
                    {t("MY_CAMPAIGN_DETAIL.MCD_button_list")}
                </button>
            }
        }





    return (
      <div className="content campaign-tracking-campaign-01 myfistar-mycampaign">
        <div className="container-campaign-tracking">
          <div className="top-camppaign-tracking">
            <div className="left">
              <h4>{titleCampaign}</h4>
            </div>
            <div className="right">
                {listCampaign}
            </div>
          </div>
          <div className="content-tracking w-100">
              {isLoadingCampaign ? <div id="loading"></div>  :  <InfoCampaignDetail
                  campaign={campaign}
                  reloadCampaign={this.initData}
              />}

            {/*<InfoCampaignDetail*/}
              {/*campaign={campaign}*/}
              {/*reloadCampaign={this.initData}*/}
            {/*/>*/}

            {!!campaign &&
              [60, 61].includes(campaign.cp_status) && (
                <SnsSocialReview
                  campaign={campaign}
                  sns={this.getSNSReview()}
                  mstatus={this.getMStatus()}
                  updateStatus={this.callrequest}
                  cpStatus={campaign.cp_status}
                />
              )}

            {!!campaign &&
              [62].includes(campaign.cp_status) && (
                <div className="campaign-complete">
                  Completed
                </div>
              )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth,
    campaign: state.campaign,
    code: state.code
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getCampaign: id => dispatch(getCampaignDetailFistarAction(id)),
    updateReview: (id, data) => dispatch(updateReviewAction(id, data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(FistarMyCampaignDetail));
