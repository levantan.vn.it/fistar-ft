import React, {Component, Fragment} from "react";
import ReactDOM from "react-dom";
import {withTranslation, Trans} from "react-i18next";
import {Link, Redirect, NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {getCodeAction} from "../../../../../store/actions/code";
// import calendar style
// You can customize style by copying asset folder.
import "@y0c/react-datepicker/assets/styles/calendar.scss";
import {
    listProfileActionPartner,
    checkSnsVerify,
    getBrand,
    updateCompanyInfomationActionPartner
} from "../../../../../store/actions/auth";
import "./company.scss";
import PartnerProfileImagePreview from "./../../../../../components/fiStar/form/ImagePreview";
import * as routeName from "./../../../../../routes/routeName";
import {Modal, Tabs, Tab, Nav} from "react-bootstrap";
import closePopup from "./../../../../../images/close-popup.svg";
import {getImageLink} from "./../../../../../common/helper";
import {IMAGE_SIZE, IMAGE_TYPE, VALIDATION} from "./../../../../../constants/index";

class CompanyInformation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                pc_image: "",
                pc_address: "",
                pc_name: "",
                pc_phone: "",
                pc_phone_backup: "",
                pc_tob: "",
                facebook: {
                    id: 2,
                    value: ""
                },
                youtube: {
                    id: 3,
                    value: ""
                },
                instagram: {
                    id: 4,
                    value: ""
                },
                pc_brand: "",
                pc_introduction: "",
                keyword: [],
                channel: [],
                channelValidate: []
            },
            // sns_name
            show: false,
            url: "",
            partner_type: {},
            keywords: {},
            catalogs: {},
            brands: [],
            onChange: false,
            onChangePImage: false,
            verifySnsInstagram: false,
            verifySnsYoutube: false,
            verifySnsFacebook: false,
            validSnsInstagram: false,
            validSnsYoutube: false,
            validSnsFacebook: false,
            errors: {
                pc_image: "",
                pc_address: "",
                pc_name: "",
                pc_phone: "",
                pc_tob: "",
                facebook: "",
                pc_brand: "",
                pc_introduction: "",
                keyword: "",
                channel: "",
                verifySnsInstagram: "",
                verifySnsYoutube: "",
                verifySnsFacebook: "",
                message: ""
            },
            loading: false,
            success: false
        };
        this.previewVideo = React.createRef();
    }

    onSearchFiStar = () => {
        this.setState({
            isSearchFiStar: true
        });
    };

    handleChangeInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState(prevState => ({
            data: {
                ...prevState.data,
                [name]: value
            },
            onChange: true,
            success: false
        }));
    };

    handleChangeInputSns = (e, type) => {
        console.log(e.currentTarget.name)
        console.log(type)
        if(e.currentTarget.value) {
            var snsValid = this.validUrl(e.currentTarget.value, type);
        }else {
            snsValid=true
        }

        switch (e.currentTarget.name) {
            case "facebook":
                var facebook = e.currentTarget.value;


                console.log(snsValid);
                if(snsValid == true) {

                this.setState(prevState => ({
                    data: {
                        ...prevState.data,
                        facebook: {
                            value: facebook
                        }
                    },
                    onChange: true,
                    validSnsFacebook:false
                }));
                var data = {};
                data.sns_id = 2;
                data.url = facebook

                    if(facebook) {

                        this.props
                            .verifySns(data)
                            .then(response => {
                                console.log(response)


                                if (response.data == false) {
                                    this.setState(prevState => ({
                                        verifySnsFacebook: true,
                                        errors: {
                                            verifySnsFacebook: "Url Facebook is exist"
                                        },

                                    }))
                                }
                                if(response.data == true) {

                                    this.setState(prevState => ({
                                        verifySnsFacebook: false,
                                        errors: {
                                            verifySnsFacebook: ""
                                        },

                                    }))

                                }

                            }).catch(e => {
                            console.log(e)
                        });

                    }else {

                        this.setState(prevState => ({
                            verifySnsFacebook: false,
                        }))

                    }

                }else {
                    console.log('zozozozoz else')
                    this.setState(prevState => ({
                        validSnsFacebook: true,
                        data: {
                            ...prevState.data,
                            facebook: {
                                value: facebook
                            }
                        },
                        errors: {
                            verifySnsFacebook: "Url Facebook is invalid"
                        }
                    }))

                }

                break;
            case "youtube":
                var youtube = e.currentTarget.value;

                console.log(snsValid);
                if(snsValid == true) {
                    this.setState(prevState => ({
                        data: {
                            ...prevState.data,
                            youtube: {
                                value: youtube
                            }
                        },
                        onChange: true,
                        validSnsYoutube: false
                    }));

                    var data = {};
                    data.sns_id = 3;
                    data.url = youtube
                    if(youtube) {
                        this.props
                            .verifySns(data)
                            .then(response => {
                                console.log(response)
                                console.log('zozozo if')
                                if (response.data == false) {
                                    this.setState(prevState => ({
                                        verifySnsYoutube: true,

                                        errors: {
                                            verifySnsYoutube: "Url Youtube is exist"
                                        }
                                    }))
                                }
                                if (response.data == true) {
                                    this.setState(prevState => ({
                                        verifySnsYoutube: false,

                                        errors: {
                                            verifySnsYoutube: ""
                                        }
                                    }))
                                }

                            }).catch(e => {
                            console.log(e)
                        });
                    }else {

                        this.setState(prevState => ({
                            verifySnsYoutube: false,
                        }))

                    }



                }else {
                    console.log('zozozozoz else')
                    this.setState(prevState => ({
                        validSnsYoutube: true,
                        data: {
                            ...prevState.data,
                            youtube: {
                                value: youtube
                            }
                        },
                        errors: {
                            verifySnsYoutube: "Url Youtube is invalid"
                        }
                    }))

                }



                break;
            case "instagram":
                var instagram = e.currentTarget.value;

                if(snsValid == true) {

                this.setState(prevState => ({
                    data: {
                        ...prevState.data,
                        instagram: {
                            value: instagram
                        }
                    },
                    validSnsInstagram: false,
                    onChange: true
                }));

                var data = {};
                data.sns_id = 4;
                data.url = instagram
                    if(instagram) {
                        this.props
                            .verifySns(data)
                            .then(response => {
                                console.log(response)

                                if (response.data == false) {
                                    this.setState(prevState => ({
                                        verifySnsInstagram: true,
                                        onChange: false,
                                        errors: {
                                            verifySnsInstagram: "Url instagram is exist"
                                        }
                                    }))
                                }
                                if (response.data == true) {
                                    this.setState(prevState => ({
                                        verifySnsInstagram: false,
                                        onChange: false,
                                        errors: {
                                            verifySnsInstagram: ""
                                        }
                                    }))
                                }
                                console.log(this.state)

                            }).catch(e => {
                            console.log(e)
                        });
                    }else {

                        this.setState(prevState => ({
                            verifySnsInstagram: false,
                        }))

                    }


                }else {
                    console.log('zozozozoz else')
                    this.setState(prevState => ({
                        validSnsInstagram: true,
                        data: {
                            ...prevState.data,
                            instagram: {
                                value: instagram
                            }
                        },
                        errors: {
                            verifySnsInstagram: "Url Instagram is invalid"
                        }
                    }))

                }

                break;
        }
        let currentTargetChannel = e.currentTarget.name;
        let currentTargetValue = e.currentTarget.value;
        let {
            data: {channels, channel}
        } = this.state;

        const target = e.target;
        const name = target.name;
        const value = target.value;
        channels = channels.map(
            e =>
                e.sns_name === name
                    ? {
                        ...e,
                        url: value
                    }
                    : e
        );
        channel = channels.map(e => `${e.sns_id},${e.url}`);
        let channel_sns_id = channels.map(e => `${e.sns_id}`);
        let channel_url = channels.map(e => `${e.url}`);
        this.setState(prevState => ({
            data: {
                ...prevState.data,
                channels,
                channel,
                currentTargetChannel: currentTargetValue
            },
            onChange: true
        }));
    };


    validUrl(url, type){
        switch(type){
            case 'facebook': return /facebook\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 2: return /facebook\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 'instagram': return /instagram\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 4: return /instagram\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 'youtube': return /youtube\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 3: return /youtube\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            default: return /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm.test(url);
        }

    }

    handleChangeInputNumberOnly = e => {
        const name = e.target.name;
        const value = e.target.value;
        if (/^\d*$/.test(value)) {
            this.setState(prevState => ({
                data: {
                    ...prevState.data,
                    [name]: value
                },
                onChange: true
            }));
        }
    };

    componentDidMount() {
        this.props.getCode("keyword,partner_type");
        this.props.inforProfile().then(response => {
            const keyword = response.keywords.map(e => e.cd_id);
            const channel = response.channels.map(e => `${e.sns_id},${e.url}`);
            let channels = response.channels;
            let facebook, youtube, instagram;
            if (channels) {
                channels.map(item => {
                    if (item.sns_name == "facebook") facebook = item.url;
                    if (item.sns_name == "youtube") youtube = item.url;
                    if (item.sns_name == "instagram") instagram = item.url;
                });
            }

            this.setState({
                data: {
                    ...this.state.data,
                    keyword,
                    channel,
                    ...response,
                    pc_phone_backup: response.pc_phone,
                    facebook: {
                        value: facebook ? facebook : ""
                    },
                    youtube: {
                        value: youtube ? youtube : ""
                    },
                    instagram: {
                        value: instagram ? instagram : ""
                    }
                }
            });
        });

        this.props.getBrand().then(response => {
            console.log(response)
            this.setState({
                brands: response
            });
        });

    }

    onFocusInput = e => {
        this.setState(prevState => ({
            errors: {
                pc_image: "",
                pc_address: "",
                pc_name: "",
                pc_phone: "",
                pc_tob: "",
                pc_brand: "",
                facebook: "",
                youtube: "",
                instagram: "",
                pc_introduction: "",
                keyword: [],
                channel: "",
                message: ""
            },
            success: false
        }));
    };

    validateForm = () => {
        const errors = {
            pc_image: "",
            pc_address: "",
            pc_name: "",
            pc_phone: "",
            pc_tob: "",
            facebook: "",
            pc_brand: "",
            pc_introduction: "",
            keyword: "",
            channel: "",
            verifySnsInstagram: "",
            verifySnsFacebook: "",
            verifySnsYoutube: "",
            message: ""
        };
        const {data, handleImageDescriptionChange} = this.state;
        const {t} = this.props;

        // if (!data.pc_image) {
        //   errors.pc_image = `${t("FISTAR_JOIN.FJN_erro_image")}`;
        // }
        if (this.state.errors.pc_image) {
            errors.pc_image = this.state.errors.pc_image;
        }
        if (!data.pc_address) {
            errors.pc_address = `${t("PARTNER_JOIN.PJN_erro_address")}`;
        } else if (data.pc_address && data.pc_address.length > VALIDATION.MAX_LENGTH) {
            errors.pc_address = `${t("PARTNER_SETTING.PSG_erro_address_limit")}`;
        }
        if (!data.pc_name) {
            errors.pc_name = `${t("PARTNER_SETTING.PSG_erro_company_name")}`;
        } else if (data.pc_name && data.pc_name.length > VALIDATION.MAX_NAME_LENGTH) {
            errors.pc_name = `${t("PARTNER_SETTING.PSG_erro_company_name_limit")}`;
        }
        if (this.state.verifySnsInstagram) {
            errors.verifySnsInstagram = "Url instagram is exist";
        }
        if (this.state.verifySnsYoutube) {
            errors.verifySnsYoutube = "Url Youtube is exist";
        }
        if (this.state.verifySnsFacebook) {
            errors.verifySnsFacebook = "Url Facebok is exist";
        }
        if (this.state.validSnsInstagram) {
            errors.verifySnsInstagram = "Url instagram is exist";
        }
        if (this.state.validSnsYoutube) {
            errors.verifySnsYoutube = "Url Youtube is exist";
        }
        if (this.state.validSnsFacebook) {
            errors.verifySnsFacebook = "Url Facebok is exist";
        }
        // if (data.facebook.value == "") {
        //   errors.facebook = `${t("PARTNER_SETTING.PSG_sns_facebook")}`;
        // }

        // if (data.facebook.value !== "" ) {
        //       errors.facebook = `${t("PARTNER_SETTING.PSG_sns_facebook")}`;
        //     }
        // if (!data.pc_phone) {
        //     errors.pc_phone = 'The phone fistar field is required.'
        // }
        if (data.pc_phone && isNaN(data.pc_phone) == true) {
            errors.pc_phone = `${t("PARTNER_SETTING.PSG_phone_number")}`;
        }
        if (data.pc_phone && data.pc_phone.length < 9) {
            errors.pc_phone = `${t("PARTNER_SETTING.PSG_phone_number_least_9")}`;
        } else if (data.pc_phone && data.pc_phone.length > 15) {
            errors.pc_phone = `${t("PARTNER_SETTING.PSG_phone_number_maximum_15")}`;
        }

        if (!data.pc_tob) {
            errors.pc_tob = `${t("PARTNER_SETTING.PSG_bussiness_required")}`;
        }
        if (!data.pc_brand) {
            errors.pc_brand = `${t("PARTNER_SETTING.PSG_erro_brand")}`;
        }
        // else if (data.pc_brand && data.pc_brand.length > VALIDATION.MAX_LENGTH) {
        //     errors.pc_brand = `${t("PARTNER_SETTING.PSG_erro_brand_limit")}`;
        // }
        if (data.keyword.length <= 0) {
            errors.keyword = `${t("PARTNER_SETTING.PSG_erro_keyword")}`;
        }
        if (data.keyword.channel <= 0) {
            errors.channel = `${t("PARTNER_SETTING.PSG_channel_required")}`;
        } else if (this.state.errors.channel) {
            errors.channel = `${t("PARTNER_SETTING.PSG_channel_required")}`;
        }
        if (!data.pc_introduction) {
            errors.pc_introduction = `${t("PARTNER_SETTING.PSG_erro_introduction")}`;
        } else if (data.pc_introduction && data.pc_introduction.length > VALIDATION.MAX_LENGTH) {
            errors.pc_introduction = `${t(
                "PARTNER_SETTING.PSG_erro_introduction_limit"
            )}`;
        }

        return errors;
    };

    submitForm = e => {
        e.preventDefault();
        const {data, errors} = this.state;
        const validate = this.validateForm();
        let dataSubmit = {};
        console.log(validate);
        if (typeof data.pc_image == "string") {
            dataSubmit.pid = data.pid;
            dataSubmit.pc_address = data.pc_address;
            dataSubmit.pc_name = data.pc_name;
            dataSubmit.pc_phone = data.pc_phone;
            dataSubmit.pc_tob = data.pc_tob;
            dataSubmit.pc_brand = data.pc_brand;
            dataSubmit.pc_introduction = data.pc_introduction;
            dataSubmit.keyword = data.keyword;
            dataSubmit.channel = [
                `2,${this.state.data.facebook.value}`,
                `3,${this.state.data.youtube.value}`,
                `4,${this.state.data.instagram.value}`
            ];
        } else {
            dataSubmit = data;
            dataSubmit.channel = [
                `2,${this.state.data.facebook.value}`,
                `3,${this.state.data.youtube.value}`,
                `4,${this.state.data.instagram.value}`
            ];
        }
        if (
            validate.pc_image === "" &&
            validate.pc_address === "" &&
            validate.pc_name === "" &&
            validate.pc_phone === "" &&
            validate.pc_tob === "" &&
            validate.pc_brand === "" &&
            validate.pc_introduction === "" &&
            validate.keyword === "" &&
            validate.facebook === "" &&
            validate.channel === "" &&
            validate.verifySnsInstagram === "" &&
            validate.verifySnsYoutube === "" &&
            validate.verifySnsFacebook === ""
        ) {
            console.log('zozoz sub')
            let formData = new FormData();
            Object.keys(dataSubmit).map(key => {
                if (dataSubmit[key]) {
                    if (dataSubmit && dataSubmit[key].constructor === Array) {
                        dataSubmit[key].map(item => {
                            formData.append(key + "[]", item);
                        });
                    } else {
                        formData.append(key, dataSubmit[key]);
                    }
                }
            });
            // formData.append("_method", "POST");
            if (!this.state.loading) {
                this.setState({loading: true}, () => {
                    this.props
                        .updateCompanyInformation(dataSubmit.pid, formData)
                        .then(response => {
                            this.setState({
                                success: true,
                                loading: false,
                                show: true
                            });
                        })
                        .catch(e => {
                            const errors = {
                                pc_image: "",
                                pc_address: "",
                                pc_name: "",
                                pc_phone: "",
                                pc_tob: "",
                                pc_brand: "",
                                pc_introduction: "",
                                keyword: [],
                                channel: "",
                                message: ""
                            };
                            if (e.data.errors) {
                                // e.data.errors.pc_image ? errors.pc_image = e.data.errors.pc_image[0] : ''
                                // e.data.errors.pc_address ? errors.pc_address = e.data.errors.pc_address[0] : ''
                                // e.data.errors.pc_name ? errors.pc_name = e.data.errors.pc_name[0] : ''
                                // e.data.errors.pc_phone ? errors.pc_phone = e.data.errors.pc_phone[0] : ''
                                // e.data.errors.pc_tob ? errors.pc_tob = e.data.errors.pc_tob[0] : ''
                                // e.data.errors.pc_brand ? errors.pc_brand = e.data.errors.pc_brand[0] : ''
                                // e.data.errors.pc_introduction ? errors.pc_introduction = e.data.errors.pc_introduction[0] : ''
                                // e.data.errors.keyword ? errors.keyword = e.data.errors.keyword[0] : ''
                                // e.data.errors.channel ? errors.channel = e.data.errors.channel[0] : ''
                            }
                            // (e.data.message == false) ? errors.message = 'Could not find this id' : ''
                            this.setState({
                                errors,
                                loading: false
                            });
                        });
                });
            }
        } else {
            this.setState({
                errors: validate
            });
        }
    };

    handleChangeFile = e => {
        // let files = e.target.files;
        this.setState(prevState => ({
            data: {
                ...prevState.data,
                pc_image: e
            },
            errors: {
                ...prevState.errors,
                pc_image:
                    e.size > process.env.REACT_APP_MAX_IMAGE_SIZE * 1000000
                        ? "The may not be greater than " +
                        process.env.REACT_APP_MAX_IMAGE_SIZE +
                        "MB."
                        : ""
            },
            onChange: true,
            onChangePImage: true
        }));
    };

    onClickKeyword = id => {
        const {data} = this.state;
        const index = data.keyword.indexOf(id);
        if (index !== -1) {
            data.keyword.splice(index, 1);
            this.setState({
                data,
                onChange: true,
            });
        } else {
            this.setState(prevState => ({
                data: {
                    ...prevState.data,
                    keyword: [...prevState.data.keyword, id]
                },
                onChange: true,
                errors: {
                    keyword: ""
                },
            }));
        }
    };

    renderKeyword = () => {
        const {data, errors} = this.state;
        const {
            code: {
                data: {keyword}
            }
        } = this.props;

        if (!keyword) return;

        return (
            <div className="col-md-12 col-sns group-selected">
                {(keyword.code || []).map((item, key) => (
                    <a
                        className={data.keyword.includes(item.cd_id) ? "selected" : ""}
                        key={key}
                        onClick={() => this.onClickKeyword(item.cd_id)}
                    >
                        {item.cd_label}
                    </a>
                ))}
                {errors.keyword && <p className="text-danger">{errors.keyword}</p>}
            </div>
        );
    };

    renderPartnerType = () => {
        const {keywords, data, errors} = this.state;
        const {
            code: {
                data: {partner_type}
            }
        } = this.props;

        if (!partner_type) return;

        return (
            <div className="col-sm-9 group-select main-select d-block select-catalog-main">
                <select
                    className={`form-control form-control-sm padding-lr-15  select-catalog${
                        errors.pc_tob ? " is-invalid" : ""
                        }`}
                    value={data.pc_tob}
                    onChange={this.handleChangeInput}
                    name="pc_tob"
                    id="pc_tob"
                >
                    <option/>
                    {(partner_type.code || []).map((partner_type, key) => (
                        <option value={partner_type.cd_id} key={key}>
                            {partner_type.cd_label}
                        </option>
                    ))}
                </select>
                {errors.pc_tob && <p className="text-danger">{errors.pc_tob}</p>}
            </div>
        );
    };

    renderBrand = () => {
        const {keywords,brands, data, errors} = this.state;
        const {
            code: {
                data: {partner_type}
            }
        } = this.props;

        if (!partner_type) return;

        return (
            <div className="col-sm-9 group-select main-select d-block">
                <select
                    className={`form-control form-control-sm padding-lr-15  ${
                        errors.pc_brand ? " is-invalid" : ""
                        }`}
                    value={data.pc_brand}
                    onChange={this.handleChangeInput}
                    name="pc_brand"
                    id="pc_brand"
                >
                    <option/>
                    {(brands || []).map((brand, key) => (
                        <option value={brand.CODE} key={key}>
                            {brand.CODE_NM}
                        </option>
                    ))}
                </select>
                {errors.pc_brand && <p className="text-danger">{errors.pc_brand}</p>}
            </div>
        );
    };

    renderSns = () => {
        const {data, errors} = this.state;
        console.log(this.state)
        return (
            <div className="col-sm-9 check-sns">
                <div className="col-md-12 col-sns">
                    <div className="input-group">
            <span className="input-group-text">
              Facebook
              <b> </b>
            </span>
                        <input
                            placeholder="User name(URL)"
                            type="text"
                            value={this.state.data.facebook.value}
                            name="facebook"
                            onFocus={this.onFocusInput}
                            onChange={(e) =>this.handleChangeInputSns(e, 2)}
                            className="form-control padding-lr-15 "
                        />
                    </div>
                    {this.state.verifySnsFacebook && (
                        <div className="tooptip-text">
                            <p className="text-danger">Url Facebook is exist</p>
                        </div>
                    )}
                    {this.state.validSnsFacebook && (
                        <div className="tooptip-text">
                            <p className="text-danger">Url Facebook is invalid</p>
                        </div>
                    )}
                </div>
                <div className="col-md-12 col-sns">
                    <div className="input-group">
                        <span className="input-group-text">Youtube</span>
                        <input
                            placeholder="User name(URL)"
                            type="text"
                            value={this.state.data.youtube.value}
                            name="youtube"
                            onFocus={this.onFocusInput}
                            onChange={(e) =>this.handleChangeInputSns(e, 3)}
                            className="form-control padding-lr-15 "
                        />

                        {this.state.verifySnsYoutube && (
                            <div className="tooptip-text">
                                <p className="text-danger">
                                    Url Youtube is exist
                                </p>
                            </div>
                        )}
                        {this.state.validSnsYoutube && (
                            <div className="tooptip-text">
                                <p className="text-danger">
                                    Url Youtube is valid
                                </p>
                            </div>
                        )}
                    </div>
                </div>
                <div className="col-md-12 col-sns">
                    <div className="input-group">
                        <span className="input-group-text">Instagram</span>
                        <input
                            placeholder="User name(URL)"
                            type="text"
                            value={this.state.data.instagram.value}
                            name="instagram"
                            onFocus={this.onFocusInput}
                            onChange={(e) =>this.handleChangeInputSns(e, 4)}
                            className="form-control padding-lr-15 "
                        />

                        {this.state.verifySnsInstagram && (
                            <div className="tooptip-text">
                                <p className="text-danger">
                                    Url Instagram is exist
                                </p>
                            </div>
                        )}
                        {this.state.validSnsInstagram && (
                            <div className="tooptip-text">
                                <p className="text-danger">
                                    Url Instagram is invalid
                                </p>
                            </div>
                        )}
                    </div>
                </div>

                {/*{(data.channels || []).map((item, key) => {*/}

                {/*if(item.sns_name == this.state.data.facebook) {*/}
                {/*return (*/}

                {/*<div className="col-md-12 col-sns" key={key}>*/}
                {/*<div className="input-group">*/}
                {/*<span className="input-group-text">{item.sns_name}*/}
                {/*<b> *</b></span>*/}
                {/*<input placeholder="User name(URL)" type="text"*/}
                {/*value={item.url}*/}
                {/*name={item.sns_name}*/}
                {/*onChange={this.handleChangeInputSns}*/}
                {/*className="form-control"/>*/}
                {/*</div>*/}
                {/*</div>*/}
                {/*)*/}

                {/*}*/}
                {/*}*/}
                {/*)}*/}
                {errors.channel && <p className="text-danger">{errors.channel}</p>}
            </div>
        );
    };

    validateNumber(event) {
        const {data} = this.state;

        var invalidChars = ["-", "+", "e", "."];

        if (invalidChars.includes(event.key)) {
            event.preventDefault();
        }
    }

    CloseModal = () => {
        this.props.closeModal();
    };

    handleClose = () => {
        this.setState({show: false});
    };
    handleShow = () => {
        this.setState({show: true});
    };

    render() {
        const {t} = this.props;
        const {data, errors, keywords, loading, success} = this.state;
        const {
            code: {
                data: {keyword}
            }
        } = this.props;
        console.log(this.state)
        return (
            <Fragment>
                <div className="content-mypage-partner">
                    <div className="container">
                        <div className="wf-row">
                            <div className="left image-page">
                                <PartnerProfileImagePreview
                                    imageSrc={getImageLink(
                                        data.pc_image,
                                        IMAGE_TYPE.PARTNERS,
                                        IMAGE_SIZE.ORIGINAL
                                    )}
                                    imageAlt={data.name}
                                    onChangeImage={this.handleChangeFile}
                                />
                                {errors.pc_image && (
                                    <div className="tooptip-text">
                                        <p className="text-danger">{errors.pc_image}</p>
                                    </div>
                                )}
                            </div>
                            <div className="right">
                                <div className="title-right">
                                    <h4>{data.pc_name}</h4>
                                </div>
                                <form action="" onSubmit={this.submitForm}>
                                    <div className="form-group row">
                                        <label
                                            htmlFor="inputPassword"
                                            className="col-sm-3 col-form-label"
                                        >
                                            {t("PARTNER_SETTING.PSG_label_company_name")}
                                            <span>*</span>
                                        </label>
                                        <div className="col-sm-9">
                                            <input
                                                type="text"
                                                className="form-control padding-lr-15 "
                                                id="pc_name"
                                                name="pc_name"
                                                value={data.pc_name}
                                                onChange={this.handleChangeInput}
                                                onFocus={this.onFocusInput}
                                                placeholder={t(
                                                    "PARTNER_SETTING.PSG_input_company_name"
                                                )}
                                            />
                                            {errors.pc_name && (
                                                <div className="tooptip-text">
                                                    <p className="text-danger">{errors.pc_name}</p>
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label
                                            htmlFor="colFormLabelSm"
                                            className="col-sm-3 col-form-label col-form-label-sm"
                                        >
                                            {t("PARTNER_SETTING.PSG_label_business")}
                                            <span>*</span>
                                        </label>

                                        {this.renderPartnerType()}
                                    </div>
                                    <div className="form-group row">
                                        <label
                                            htmlFor="inputPassword"
                                            className="col-sm-3 col-form-label"
                                        >
                                            {t("PARTNER_SETTING.PSG_label_contact")}
                                            {/*<span>*</span>*/}
                                        </label>
                                        <div className="col-sm-9">
                                            <input
                                                type="text"
                                                className="form-control padding-lr-15 "
                                                id="pc_phone"
                                                name="pc_phone"
                                                value={data.pc_phone ? data.pc_phone : ""}
                                                // onKeyPress={event => this.validateNumber(event)}
                                                onChange={this.handleChangeInputNumberOnly}
                                                onFocus={this.onFocusInput}
                                                placeholder={t("PARTNER_SETTING.PSG_input_contact")}
                                            />
                                            {errors.pc_phone && (
                                                <div className="tooptip-text">
                                                    <p className="text-danger">{errors.pc_phone}</p>
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label
                                            htmlFor="colFormLabelSm"
                                            className="col-sm-3 col-form-label col-form-label-sm"
                                        >
                                            {t("PARTNER_SETTING.PSG_label_address")}
                                            <span>*</span>
                                        </label>
                                        <div className="col-sm-9">
                      <textarea
                          className="form-control padding-lr-15 address-company "
                          id="pc_address"
                          name="pc_address"
                          value={data.pc_address}
                          onChange={this.handleChangeInput}
                          onFocus={this.onFocusInput}
                          placeholder={t(
                              "PARTNER_SETTING.PSG_input_company_name"
                          )}
                          rows="5"
                      />
                                            {errors.pc_address && (
                                                <div className="tooptip-text">
                                                    <p className="text-danger">{errors.pc_address}</p>
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label
                                            htmlFor="inputPassword"
                                            className="col-sm-3 col-form-label"
                                        >
                                            {t("PARTNER_SETTING.PSG_label_product_brand")}
                                            <span>*</span>
                                        </label>

                                        {this.renderBrand()}

                                        {/*<div className="col-sm-9">*/}
                                            {/*<input*/}
                                                {/*type="text"*/}
                                                {/*className="form-control padding-lr-15 "*/}
                                                {/*id="pc_brand"*/}
                                                {/*name="pc_brand"*/}
                                                {/*value={data.pc_brand}*/}
                                                {/*onChange={this.handleChangeInput}*/}
                                                {/*onFocus={this.onFocusInput}*/}
                                                {/*placeholder={t(*/}
                                                    {/*"PARTNER_SETTING.PSG_input_company_name"*/}
                                                {/*)}*/}
                                            {/*/>*/}
                                            {/*{errors.pc_brand && (*/}
                                                {/*<div className="tooptip-text">*/}
                                                    {/*<p className="text-danger">{errors.pc_brand}</p>*/}
                                                {/*</div>*/}
                                            {/*)}*/}
                                        {/*</div>*/}

                                    </div>
                                    <div className="form-group row">
                                        <label
                                            htmlFor="colFormLabelSm"
                                            className="col-sm-3 col-form-label col-form-label-sm"
                                        >
                                            {t("PARTNER_SETTING.PSG_label_matching_key")}
                                            <span>*</span>
                                        </label>
                                        <div className="col-sm-9 tag">
                                            <div className="text-tag col-md-12 col-sns">
                                                <p className="text-blue">
                                                    {t("PARTNER_SETTING.PSG_title_key")}
                                                </p>
                                                <p>{t("PARTNER_SETTING.PSG_text_key")}</p>
                                            </div>

                                            {this.renderKeyword()}

                                            <div className="col-md-12 col-sns">
                                                <p className="select-of text-center">
                          <span>
                            {t("PARTNER_SETTING.PSG_select")}{" "}
                              {data && data.keyword && data.keyword.length}
                          </span>{" "}
                                                    {t("PARTNER_SETTING.PSG_out_of")}{" "}
                                                    {keyword ? keyword.code.length : 0}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label
                                            htmlFor="colFormLabelSm"
                                            className="col-sm-3 col-form-label col-form-label-sm"
                                        >
                                            {t("PARTNER_SETTING.PSG_label_introduction")}
                                            <span>*</span>
                                        </label>
                                        <div className="col-sm-9 form-introduction">
                                            {/*<i className="fas fa-angle-up" />*/}
                                            {/*<i className="fas fa-angle-down" />*/}

                                            <textarea
                                                className="form-control padding-lr-15 "
                                                id="pc_introduction"
                                                name="pc_introduction"
                                                value={data.pc_introduction}
                                                onChange={this.handleChangeInput}
                                                onFocus={this.onFocusInput}
                                                placeholder={t(
                                                    "PARTNER_SETTING.PSG_input_introduction"
                                                )}
                                                rows="5"
                                            />
                                            {errors.pc_introduction && (
                                                <div className="tooptip-text">
                                                    <p className="text-danger">
                                                        {errors.pc_introduction}
                                                    </p>
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label
                                            htmlFor="colFormLabelSm"
                                            className="col-sm-3 col-form-label col-form-label-sm"
                                        >
                                            {t("PARTNER_SETTING.PSG_label_sns")}
                                        </label>
                                        {this.renderSns()}
                                    </div>
                                </form>
                                {/*{success && (*/}
                                {/*<div className="col-md-12">*/}
                                {/*<div className="alert alert-success" role="alert">*/}
                                {/*Updata data success!*/}
                                {/*</div>*/}
                                {/*</div>*/}
                                {/*)}*/}
                                <div className="group-button">
                                    <div className="row">
                                        <div className="col-md-9 offset-md-3 col-12">
                                            <div className="group ">
                                                <NavLink
                                                    className="back-dashboard"
                                                    to={routeName.PARTNER_DASHBOARD}
                                                >
                                                    {t("PARTNER_SETTING.PSG_button_cancel")}
                                                </NavLink>
                                                <button
                                                    className={`${
                                                        this.state.onChange == true ? " apply-button" : ""
                                                        }`}
                                                    onClick={this.submitForm}
                                                    disabled={this.state.onChange == false ? true : false}
                                                >
                                                    {loading ? (
                                                        <div className="spinner-border" role="status">
                              <span className="sr-only">
                                {t("LOADING.LOADING")}{" "}
                              </span>
                                                        </div>
                                                    ) : (
                                                        `${t("PARTNER_SETTING.PSG_button_apply")} `
                                                    )}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <Modal
                    size="lg"
                    show={this.state.show}
                    onHide={this.handleClose}
                    aria-labelledby="example-modal-sizes-title-lg"
                    dialogClassName="modal-partner-pass modal-dialog-centered"
                >
                    <div className="popup-partner-pass">
                        <div className="top">
                            <h4>{t("ANSWER_QUESTION.AQN_partner")}</h4>
                            <button
                                type="button"
                                className="close"
                                data-dismiss="modal"
                                aria-label="Close"
                                onClick={this.handleClose}
                            >
                                <img src={closePopup} alt=""/>
                            </button>
                        </div>
                        <div className="content">
                            <p>{t("PARTNER_SETTING.update_success")}</p>
                            <button onClick={this.handleClose}>
                                {t("POPUP_RESET_PASS.PRP_button_ok")}
                            </button>
                        </div>
                    </div>
                </Modal>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        code: state.code
    };
};

const mapDispatchToProps = dispatch => {
    return {
        inforProfile: data => dispatch(listProfileActionPartner(data)),
        updateCompanyInformation: (pid, data) =>
            dispatch(updateCompanyInfomationActionPartner(pid, data)),
        getCode: type => dispatch(getCodeAction(type)),
        verifySns: data => dispatch(checkSnsVerify(data)),
        getBrand: () => dispatch(getBrand())
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withTranslation("translations")(CompanyInformation));
