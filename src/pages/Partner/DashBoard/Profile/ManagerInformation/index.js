import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import imageUpload from "./../../../../../images/img-campaign.png";
import "./manager.scss";
// import PartnerProfileImagePreview from "./../../../../../components/fiStar/Auth/Profile/ImagePreview";
import PartnerProfileImagePreview from "./../../../../../components/fiStar/form/ImagePreview";
import {
  checkExistActionPartner,
  updateManagerInformationActionPartner,
  listProfileActionPartner
} from "../../../../../store/actions/auth";
import * as routeName from "../../../../../routes/routeName";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import closePopup from "./../../../../../images/close-popup.svg";
import { getImageLink } from "./../../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE, VALIDATION } from "./../../../../../constants/index";

class ManagerInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        pm_name: "",
        pm_phone: "",
        email: "",
        p_image: ""
      },
      show: false,
      email_backup: "",
      profile: {},
      onChange: false,
      imagePreviewUrlDescription: "",
      errors: {
        pm_name: "",
        pm_phone: "",
        email: "",
        message: ""
      },
      existID: 0,
      loading: false,
      success: false
    };
  }

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        pm_name: "",
        pm_phone: "",
        email: "",
        message: ""
      },
      existID: 0,
      success: false
    }));
  };
  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...this.state.data,
        [name]: value
      },
      onChange: true,
      success: false
    }));
  };

  componentDidMount() {
    this.props.inforProfile().then(response => {
      this.setState({
        data: response,
        email_backup: response.email
      });
    });
  }

  handleImageDescriptionChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    const fileType = file["type"];
    const validImageTypes = ["image/gif", "image/jpeg", "image/png"];
    if (validImageTypes.includes(fileType)) {
      reader.onloadend = () => {
        this.setState(prevState => ({
          ...this.state.file,
          file: file,
          data: {
            ...prevState.data,
            p_image: file
          },
          onChange: true,
          // imagePreviewUrlDescription:this.state.imagePreviewUrlDescription.concat(reader.result)
          imagePreviewUrlDescription: reader.result
        }));
      };
      reader.readAsDataURL(file);
    } else {
      this.setState(prevState => ({
        data: {
          ...prevState.data,
          cp_image: "isNotFileImage"
        }
      }));
    }
  }

  checkExistID = (e, emailValue) => {
    e.preventDefault();
    const { data } = this.state;

    if (!this.validateEmail(data.email)) {
      this.setState(prevState => ({
        existID: 1,
        errors: {
          ...prevState.errors,
          email: `${this.props.t("PARTNER_SETTING.PSG_erro_email_valid")}`
        }
      }));
    }
    else {
      if (data.email == this.state.email_backup) {
        this.setState(prevState => ({
          existID: 2
        }));
      } else {
        this.props
          .checkExist("email", data.email)
          .then(res => {
            this.setState(prevState => ({
              existID: 1,
              errors: {
                ...prevState.errors,
                email: `${this.props.t(
                  "PARTNER_SETTING.PSG_erro_email_existed"
                )}`
              },
              success: false
            }));
          })
          .catch(() => {
            this.setState(prevState => ({
              existID: 2
            }));
          });
      }
    }
  };

  validateEmail = email => {
    // var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //   var re = /^[a-zA-Z0-9\.]{2,}@[a-zA-Z0-9]{2}(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])?)(?=.*[\.])*$/;
      var re = /([a-z]+[a-z0-9]*[_\.]?[a-z0-9]+)@(([a-z0-9]{2,}\.)*[a-z0-9]{2,}\.)+[a-z]{2,}/;
    return re.test(String(email).toLowerCase());
  };

    validateEmailCustomer = email => {
        var re = /^[a-zA-Z0-9]{2,}@[a-zA-Z0-9]{2,}.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])*$/;
        return re.test(String(email).toLowerCase());
    };

  validateForm = () => {
    const errors = {
      pm_name: "",
      pm_phone: "",
      email: "",
      p_image: "",
      message: ""
    };
    const { data, profile } = this.state;
    const { t } = this.props;
    console.log(data.p_image);
    console.log(this.state.errors.p_image);
    if (!data.pm_name) {
      errors.pm_name = t("PARTNER_SETTING.PSG_erro_management_name");
    } else if (data.pm_name && data.pm_name.length > VALIDATION.MAX_NAME_LENGTH) {
      errors.pm_name = t("PARTNER_SETTING.PSG_erro_management_name_limit");
    }

    if (!data.pm_phone) {
      errors.pm_phone = t("PARTNER_SETTING.PSG_phone_number_require");
    } else if (isNaN(data.pm_phone) == true) {
      errors.pm_phone = t("PARTNER_SETTING.PSG_phone_number");
    } else if (data.pm_phone.length < 9) {
      errors.pm_phone = t("PARTNER_SETTING.PSG_phone_number_least_9");
    } else if (data.pm_phone.length > 15) {
      errors.pm_phone = t("PARTNER_SETTING.PSG_phone_number_maximum_15");
    }
    if (!data.email) {
      errors.email = t("PARTNER_SETTING.PSG_email_required");
    } else if (data.email && !this.validateEmail(data.email)) {
      errors.email = `${t("FISTAR_LOGIN.FL_toptip_email")}`;
    }

    // if (!data.p_image) {
    //   errors.p_image = t("PARTNER_SETTING.PSG_erro_add_image");
    // }
    if (this.state.errors.p_image) {
      errors.p_image = this.state.errors.p_image;
    }

    return errors;
  };

  handleChangeInputNumberOnly = e => {
    const name = e.target.name;
    const value = e.target.value;
    if (/^\d*$/.test(value)) {
      this.setState(prevState => ({
        data: {
          ...prevState.data,
          [name]: value
        },
        onChange: true
      }));
    }
  };

  submitForm = e => {
    e.preventDefault();
    const { data, errors } = this.state;
    const validate = this.validateForm();
    const { t } = this.props;

    let dataSubmit = {};
    if (typeof data.p_image == "string") {
      dataSubmit.pm_name = data.pm_name;
      dataSubmit.pm_phone = data.pm_phone;
      dataSubmit.email = data.email;
      dataSubmit.pid = data.pid;
    } else {
      dataSubmit = data;
    }

    if (
      validate.pm_name === "" &&
      validate.pm_phone === "" &&
      validate.email === "" &&
      validate.p_image === ""
    ) {
      if (!this.state.loading) {
        this.setState({ loading: true }, () => {
          console.log(this.state.data);
          if (data.email !== this.state.email_backup) {
            this.props
              .checkExist("email", data.email)
              .then(res => {
                this.setState(prevState => ({
                  existID: 1,
                  errors: {
                    ...prevState.errors,
                    email: t("PARTNER_SETTING.PSG_email_existed")
                  },
                  loading: false,
                  success: false
                }));
              })
              .catch(() => {
                let formData = new FormData();
                Object.keys(dataSubmit).map(key => {
                  formData.append(key, !!dataSubmit[key] ? dataSubmit[key] : '');
                });

                this.props
                  .updateManager(dataSubmit.pid, formData)
                  .then(response => {
                    this.setState({
                      success: true,
                      loading: false,
                      show: true,
                      email_backup: dataSubmit.email
                    });
                  })
                  .catch(e => {
                    const errors = {
                      pm_name: "",
                      pm_phone: "",
                      email: "",
                      p_image: "",
                      message: ""
                    };
                    if (e.data.errors) {
                      errors.pm_name = e.data.errors.pm_name
                        ? e.data.errors.pm_name[0]
                        : "";
                      errors.pm_phone = e.data.errors.pm_phone
                        ? e.data.errors.pm_phone[0]
                        : "";
                      errors.email = e.data.errors.email
                        ? e.data.errors.email[0]
                        : "";
                      errors.p_image = e.data.errors.p_image
                        ? e.data.errors.p_image[0]
                        : "";
                    }

                    this.setState({
                      errors,
                      loading: false
                    });
                  });

                this.setState(prevState => ({
                  existID: 2,
                  loading: false
                }));
              });
          } else {
            let formData = new FormData();
            Object.keys(dataSubmit).map(key => {
              formData.append(key, !!dataSubmit[key] ? dataSubmit[key] : '');
            });

            this.props
              .updateManager(dataSubmit.pid, formData)
              .then(response => {
                this.setState({
                  success: true,
                  loading: false,
                  show: true,
                  email_backup: dataSubmit.email
                });
              })
              .catch(e => {
                const errors = {
                  pm_name: "",
                  pm_phone: "",
                  email: "",
                  p_image: "",
                  message: ""
                };
                if (e.data.errors) {
                  errors.pm_name = e.data.errors.pm_name
                    ? e.data.errors.pm_name[0]
                    : "";
                  errors.pm_phone = e.data.errors.pm_phone
                    ? e.data.errors.pm_phone[0]
                    : "";
                  errors.email = e.data.errors.email
                    ? e.data.errors.email[0]
                    : "";
                  errors.p_image = e.data.errors.p_image
                    ? e.data.errors.p_image[0]
                    : "";
                }

                this.setState({
                  errors,
                  loading: false
                });
              });
          }
        });
      }
    } else {
      this.setState({
        errors: validate
      });
    }
  };
  handleChangeFile = e => {
    // let files = e.target.files;

    this.setState(prevState => ({
      data: {
        ...prevState.data,
        p_image: e
      },
      errors: {
        ...prevState.errors,
        p_image:
          e.size > process.env.REACT_APP_MAX_IMAGE_SIZE * 1000000
            ? "The may not be greater than " +
              process.env.REACT_APP_MAX_IMAGE_SIZE +
              "MB."
            : ""
      },
      onChange: true
    }));
  };

  validateNumber(event) {
    const { data } = this.state;

    var invalidChars = ["-", "+", "e", "."];

    if (invalidChars.includes(event.key)) {
      event.preventDefault();
    }
  }

  CloseModal = () => {
    this.props.closeModal();
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  handleShow = () => {
    this.setState({ show: true });
  };

  render() {
    const { t } = this.props;
    const {
      data,
      errors,
      existID,
      loading,
      success,
      imagePreviewUrlDescription,
      keywords,
      profile
    } = this.state;
    console.log(this.state.data);

    return (
      <Fragment>
        <div className="content-mypage-partner">
          <div className="container">
            <div className="wf-row">
              <div className="left image-page">
                <PartnerProfileImagePreview
                  imageSrc={getImageLink(
                    data.p_image,
                    IMAGE_TYPE.PARTNERS,
                    IMAGE_SIZE.ORIGINAL
                  )}
                  imageAlt={data.name}
                  onChangeImage={this.handleChangeFile}
                />

                {errors.p_image && (
                  <div className="tooptip-text">
                    <p className="text-danger">{errors.p_image}</p>
                  </div>
                )}
              </div>
              <div className="right">
                <div className="title-right">
                  <h4>{data.pm_name}</h4>
                </div>
                <form action="" onSubmit={this.submitForm}>
                  <div className="form-group row">
                    <label
                      htmlFor="inputPassword"
                      className="col-sm-3 col-form-label"
                    >
                      {t("PARTNER_SETTING.PSG_label_name")}
                      <span>*</span>
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        className="form-control padding-lr-15"
                        id="pm_name"
                        name="pm_name"
                        value={data.pm_name}
                        onChange={this.handleChangeInput}
                        onFocus={this.onFocusInput}
                        placeholder={t("PARTNER_SETTING.PSG_input_name")}
                      />
                      {errors.pm_name && (
                        <div className="tooptip-text">
                          <p className="text-danger">{errors.pm_name}</p>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="inputPassword"
                      className="col-sm-3 col-form-label"
                    >
                      {t("PARTNER_SETTING.PSG_label_contact")}
                      <span>*</span>
                    </label>
                    <div className="col-sm-9">
                      <input
                        type="text"
                        className="form-control padding-lr-15"
                        id="pm_phone"
                        value={data.pm_phone}
                        name="pm_phone"
                        onChange={this.handleChangeInputNumberOnly}
                        onFocus={this.onFocusInput}
                        placeholder={t("PARTNER_SETTING.PSG_input_name")}
                      />
                      {errors.pm_phone && (
                        <div className="tooptip-text">
                          <p className="text-danger">{errors.pm_phone}</p>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row">
                    <label
                      htmlFor="inputPassword"
                      className="col-sm-3 col-form-label"
                    >
                      {t("PARTNER_SETTING.PSG_label_email")}
                      <span>*</span>
                    </label>
                    <div className="col-sm-9 wf-group info-email">
                      <input
                        type="email"
                        className={`form-control padding-lr-15 form-control-sm${
                          errors.email ? " is-invalid" : ""
                        }${existID == 2 ? " is-valid" : ""}`}
                        id="email"
                        value={data.email}
                        name="email"
                        onChange={this.handleChangeInput}
                        onFocus={this.onFocusInput}
                        autoComplete="off"
                        placeholder={t("PARTNER_SETTING.PSG_input_name")}
                      />
                      {errors.email && (
                        <div className="tooptip-text">
                          <p className="text-danger">{errors.email}</p>
                        </div>
                      )}
                      <button
                        className="btn btn-check"
                        onClick={this.checkExistID}
                      >
                        {t("PARTNER_SETTING.PSG_button_checkmail")}
                      </button>
                    </div>
                  </div>
                </form>
                {/*{success && (*/}
                {/*<div className="col-md-12">*/}
                {/*<div className="alert alert-success" role="alert">*/}
                {/*Updata data success!*/}
                {/*</div>*/}
                {/*</div>*/}
                {/*)}*/}
                <div className="group-button">
                  <div className="row">
                    <div className="col-md-9 offset-md-3 col-12">
                      <div className="group">
                        <NavLink
                          className="back-dashboard"
                          to={routeName.PARTNER_DASHBOARD}
                        >
                          {t("PARTNER_SETTING.PSG_button_cancel")}
                        </NavLink>
                        <button
                          className={`${
                            this.state.onChange == true ? " apply-button" : ""
                          }`}
                          onClick={this.submitForm}
                          disabled={this.state.onChange == false ? true : false}
                        >
                          {loading ? (
                            <div className="spinner-border" role="status">
                              <span className="sr-only">
                                {t("LOADING.LOADING")}
                              </span>
                            </div>
                          ) : (
                            `${t("PARTNER_SETTING.PSG_button_apply")}`
                          )}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Modal
          size="lg"
          show={this.state.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="modal-partner-pass modal-dialog-centered"
        >
          <div className="popup-partner-pass">
            <div className="top">
              <h4>{t("POPUP_CHANGE_PASSWORD.PCP_partner")}</h4>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.handleClose}
              >
                <img src={closePopup} alt="" />
              </button>
            </div>
            <div className="content">
              <p>{t("PARTNER_SETTING.update_success")}</p>
              <button onClick={this.handleClose}>
                {t("POPUP_RESET_PASS.PRP_button_ok")}
              </button>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    inforProfile: data => dispatch(listProfileActionPartner(data)),
    updateManager: (pid, data) =>
      dispatch(updateManagerInformationActionPartner(pid, data)),
    checkExist: (name, value) => dispatch(checkExistActionPartner(name, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(ManagerInformation));
