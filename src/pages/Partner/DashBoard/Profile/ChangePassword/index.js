import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { createCampainActionPartner } from "./../../../../../store/actions/auth";
import { getCodeAction } from "../../../../../store/actions/code";
import { DatePicker, RangeDatePicker } from "@y0c/react-datepicker";
// import calendar style
// You can customize style by copying asset folder.
import "@y0c/react-datepicker/assets/styles/calendar.scss";
import { format } from "date-fns";
import {
  listProfileActionPartner,
  changePasswordProfileActionPartner,
  logoutAction
} from "../../../../../store/actions/auth";

import ClosePopupChangePassword from "./../../../../../components/partner/Dashboard/Profile/popupChangePassword";

import closePopup from "./../../../../../images/close-popup.svg";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import "./changePassword.scss";

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        password_current: "",
        new_password: "",
        password_confirmation: ""
      },
      inputData: false,
      show_pass: false,
      show_pass_confirm: false,
      show_pass_new: false,
      errors: {
        password_current: "",
        new_password: "",
        password_confirmation: "",
        message: ""
      },
      loading: false,
      show: false
    };
    this.previewVideo = React.createRef();
  }

  componentDidMount() {
    this.props.inforProfile().then(response => {
      console.log(response, "data profile");
      this.setState({
        data: response
      });
    });
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      },
      inputData: true
    }));
  };

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        password_current: "",
        new_password: "",
        password_confirmation: "",
        message: ""
      }
    }));
  };

  validateForm = () => {
    const errors = {
      password_current: "",
      new_password: "",
      password_confirmation: "",
      message: ""
    };
    const { data } = this.state;
    console.log(data);
    // if (!data.password_current) {
    //   errors.password_current = "The current password field is required.";
    // }
    // if (!data.new_password) {
    //   errors.new_password = "The password field is required.";
    // }
    if (data.new_password == data.password_current) {
      errors.new_password = `${this.props.t(
        "PROFILE_FISTAR.PFR_erro_change_new_pw"
      )}`;
    }
    // else if (data.new_password.length < 6) {
    //   errors.new_password = "The new password field at least 6 characters.";
    // }
    // if (!data.password_confirmation) {
    //   errors.password_confirmation =
    //     "The password confirmation field is required.";
    // }
    // else if (data.password_confirmation !== data.new_password) {
    //   errors.password_confirmation =
    //     "The password confirmation is not match password.";
    // }

    console.log(errors);
    return errors;
  };

  submitForm = e => {
    e.preventDefault();
    const { data, errors } = this.state;
    const validate = this.validateForm();

    console.log(data, "data submit");

    if (
      validate.password_current === "" &&
      validate.new_password === "" &&
      validate.password_confirmation === ""
    ) {
      if (!this.state.loading) {
        this.setState({ loading: true }, () => {
          let dataSubmit = {};
          dataSubmit.current = data.password_current;
          dataSubmit.password = data.new_password;
          dataSubmit.password_confirmation = data.password_confirmation;
          dataSubmit.pid = data.pid;
          console.log(dataSubmit, "dataSubmit submit");
          this.props
            .changePasswowrdProfile(dataSubmit.pid, dataSubmit)
            .then(response => {
              console.log(response, "tra vev");

              if (response.success == false) {
                this.setState({
                  loading: false,
                  errors: {
                    password_current:
                      response.errors && response.errors.not_match
                        ? response.errors.not_match
                        : "",
                    new_password:
                      response.errors &&
                      response.errors.validator &&
                      response.errors.validator.password
                        ? response.errors.validator.password
                        : "",
                    password_confirmation:
                      response.errors &&
                      response.errors.validator &&
                      response.errors.validator.password_confirmation
                        ? response.errors.validator.password_confirmation
                        : ""
                  }
                });
              } else {
                this.setState({
                  success: true,
                  loading: false,
                  show: true
                });

                // alert("change password success! Plese login again!");
                // this.props.logout();
              }
            })
            .catch(e => {
              const errors = {
                password_current: "",
                new_password: "",
                password_confirmation: "",
                message: ""
              };
              console.log(e);
              if (e.errors) {
                errors.password_current = e.errors.not_match
                  ? e.errors.not_match
                  : "";
                // errors.password_current = e.data.errors.password ? e.data.errors.password[0] : ''
                // errors.new_password = e.data.errors.new_password ? e.data.errors.new_password[0] : ''
                // errors.password_confirmation = e.data.errors.password_confirmation ? e.data.errors.password_confirmation[0] : ''
              }
              // errors.message = (e.data.message == false) ? 'Could not find this id' : ''
              this.setState({
                errors,
                loading: false
              });
            });
        });
      }
    } else {
      this.setState({
        errors: validate
      });
    }
  };

  CloseModal = () => {
    this.props.closeModal();
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  handleShow = () => {
    this.setState({ show: true });
  };
  logout = () => {
    this.props.logout();
    this.props.history.push("/partner/login");
  };

  showPassword = () => {
    this.setState({
      show_pass: !this.state.show_pass
    });
  };
  showPasswordNew = () => {
    this.setState({
      show_pass_new: !this.state.show_pass_new
    });
  };
  showPasswordConfirm = () => {
    this.setState({
      show_pass_confirm: !this.state.show_pass_confirm
    });
  };

  render() {
    const { t, auth } = this.props;
    const { data, errors, keywords, loading, inputData } = this.state;
    console.log(this.state);
    console.log(auth);

    return (
      <Fragment>
        <div className="content-mypage-partner">
          <div className="container-change">
            <div className="title-changepass">
              <h1 className="name-partner">{auth.name}</h1>
              <h4>{t("PROFILE_FISTAR.PFR_title")}</h4>
            </div>
            <form action="" onSubmit={this.submitForm}>
              <div className="pass">
                <div className="form-group row">
                  <div className="group-icon-pass">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fas fa-unlock" />
                      </span>
                    </div>
                    <i
                      className={
                        this.state.show_pass
                          ? "fa fa-eye show-pass"
                          : "fa fa-eye-slash show-pass"
                      }
                      onClick={this.showPassword}
                    />
                    <input
                      type={this.state.show_pass ? "text" : "password"}
                      className="form-input empty"
                      id="password_current"
                      value={data.password_current}
                      name="password_current"
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                      placeholder={t("PROFILE_FISTAR.PFR_title_pw")}
                    />
                    {errors.password_current && (
                      <div className="tooptip-pass">
                        <p className="text-danger">{errors.password_current}</p>
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className="pass new-pass">
                <div className="form-group row">
                  <div className="group-icon-pass">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fas fa-unlock" />
                      </span>
                    </div>
                    <i
                      className={
                        this.state.show_pass_new
                          ? "fa fa-eye show-pass"
                          : "fa fa-eye-slash show-pass"
                      }
                      onClick={this.showPasswordNew}
                    />
                    <input
                      type={this.state.show_pass_new ? "text" : "password"}
                      className="form-input empty"
                      id="new_password"
                      value={data.new_password}
                      name="new_password"
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                      placeholder="New Password"
                    />
                    {errors.new_password && (
                      <div className="tooptip-pass">
                        <p className="text-danger">{errors.new_password}</p>
                      </div>
                    )}
                  </div>
                </div>
                <div className="form-group row">
                  <div className="group-icon-pass">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fas fa-unlock" />
                      </span>
                    </div>
                    <i
                      className={
                        this.state.show_pass_confirm
                          ? "fa fa-eye show-pass"
                          : "fa fa-eye-slash show-pass"
                      }
                      onClick={this.showPasswordConfirm}
                    />
                    <input
                      type={this.state.show_pass_confirm ? "text" : "password"}
                      className="form-input empty"
                      id="password_confirmation"
                      value={data.password_confirmation}
                      name="password_confirmation"
                      onChange={this.handleChangeInput}
                      onFocus={this.onFocusInput}
                      placeholder="Confirm Password"
                    />
                    {errors.password_confirmation && (
                      <div className="tooptip-pass">
                        <p className="text-danger">
                          {errors.password_confirmation}
                        </p>
                      </div>
                    )}

                    {/*<div className="tooptip-pass">*/}
                    {/*<p>The Passwords do not match. Please check again!!</p>*/}
                    {/*</div>*/}
                    {/*<div className="tooptip-pass tooptip-pass-incorrect">*/}
                    {/*<p>Password is incorrect. Please check again !!</p>*/}
                    {/*</div>*/}
                  </div>
                </div>
              </div>
              <div className="btn-ok">
                <button
                  type="submit"
                  disabled={inputData == true ? false : true}
                  onClick={this.submitForm}
                >
                  {loading ? (
                    <div className="spinner-border" role="status">
                      <span className="sr-only">{t("LOADING.LOADING")}</span>
                    </div>
                  ) : (
                    "Ok"
                  )}
                </button>
              </div>
            </form>
          </div>
        </div>

        <Modal
          size="lg"
          show={this.state.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="modal-partner-pass modal-dialog-centered"
        >
          <div className="popup-partner-pass">
            <div className="top">
              <h4>Partner</h4>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.handleClose}
              >
                <img src={closePopup} alt="" />
              </button>
            </div>
            <div className="content">
              <p>The Password has been reset. </p>
              <p>Please login again.</p>
              <button onClick={this.logout}>OK</button>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    inforProfile: data => dispatch(listProfileActionPartner(data)),
    changePasswowrdProfile: (pid, data) =>
      dispatch(changePasswordProfileActionPartner(pid, data)),
    logout: data => dispatch(logoutAction())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(ChangePassword));
