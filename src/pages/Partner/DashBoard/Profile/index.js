import React, {Component, Fragment} from 'react';
import ReactDOM from 'react-dom';
import {withTranslation, Trans} from 'react-i18next';
import {Link, Redirect, Route} from 'react-router-dom'
import {connect} from 'react-redux'
import {createCampainActionPartner} from '../../../../store/actions/auth'
import {getCodeAction} from "../../../../store/actions/code";
import {DatePicker, RangeDatePicker} from '@y0c/react-datepicker';
// import calendar style
// You can customize style by copying asset folder.
import '@y0c/react-datepicker/assets/styles/calendar.scss';
import {format} from 'date-fns'
import ManagerInformation from './ManagerInformation'


class PartnerProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {

            },

        }

    }



    render() {
        const {t} = this.props;
        const {data, errors, keywords} = this.state;
        console.log(this.state)

        return (
            <Fragment>

                <ManagerInformation />

                {/*<Route path={`/partner/profile/manager-information`} component={ManagerInformation}/>*/}

            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {}
}

const mapDispatchToProps = dispatch => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation('translations')(PartnerProfile))
