import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";

// import calendar style
// You can customize style by copying asset folder.
import "@y0c/react-datepicker/assets/styles/calendar.scss";
import { format } from "date-fns";
// import PushMessageComponent from './../../../components/PushMessage'

import imgaeIdol from "./../../../../images/dash-02.png";
import closePopup from "./../../../../images/close-popup.svg";

import { Modal, Tabs, Tab, Nav } from "react-bootstrap";

class history extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }

  CloseModal = () => {
    this.props.closeModal();
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  handleShow = () => {
    this.setState({ show: true });
  };

  render() {
    const { t } = this.props;
    const { height } = this.state;

    return (
      <Fragment>
        <div className="history-information" onClick={this.handleShow}>
          <div className="text">
            <p>History</p>
            <h4>31</h4>
          </div>
        </div>

        <Modal
          size="lg"
          show={this.state.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="popup-history"
        >
          {/*<section className="modal fade show" id="exampleModal" tabindex="-1" role="dialog"*/}
          {/*aria-labelledby="exampleModalLabel" aria-hidden="true">*/}
          {/*<div className=" popup-history">*/}
          <div className=" history" role="document">
            <div className="modal-header">
              <h4 id="exampleModalLabel">History</h4>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.handleClose}
              >
                <img src={closePopup} alt="" />
              </button>
            </div>
            <div className="body-popup">
              <div className="content-proceed">
                <div className="information-user">
                  <div className="left">
                    <div className="image-user">
                      <img src={imgaeIdol} alt="" />
                    </div>
                    <div className="name">
                      <i className="fas fa-mars" />
                      <h4>
                        <a href="">Gianna.Jun</a>
                      </h4>
                    </div>
                  </div>
                  <div className="right">
                    <button>8 Cases</button>
                  </div>
                </div>
                <div className="tab-content">
                  <ul className="list-tab">
                    <li className="item">
                      <a href="" className="link-tab active">
                        Matched
                      </a>
                      <p className="amount active">1</p>
                    </li>
                    <li className="item">
                      <a href="" className="link-tab">
                        Considering
                      </a>
                      <p className="amount">1</p>
                    </li>
                    <li className="item">
                      <a href="" className="link-tab">
                        Rejected
                      </a>
                      <p className="amount">6</p>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="process-campaign">
                <div className="item">
                  <div className="top">
                    <h4>Etude House Campaign</h4>
                    <h4>Matched</h4>
                  </div>
                  <div className="process">
                    <p className="text-fistar">
                      2019. 06. 20 <span>fiStar Apply campaign</span>
                    </p>
                    <p className="dots" />
                    <p className="dots" />
                    <p className="dots" />
                    <p className="text-partner">
                      2019. 06. 20 <span>Partner Approved</span>
                    </p>
                  </div>
                </div>
                <div className="item">
                  <div className="top">
                    <h4>Etude House Campaign</h4>
                    <h4>Considering</h4>
                  </div>
                  <div className="process">
                    <p className="text-fistar">
                      2019. 06. 20 <span>fiStar Apply campaign</span>
                    </p>
                    <p className="dots" />
                    <p className="dots" />
                    <p className="dots" />
                    <p className="text-partner">
                      2019. 06. 20 <span>Partner Approved</span>
                    </p>
                  </div>
                </div>
                <div className="item item-rejected">
                  <div className="top">
                    <h4>Etude House Campaign</h4>
                    <h4>Rejected</h4>
                  </div>
                  <div className="process">
                    <p className="text-fistar">
                      2019. 06. 20 <span>fiStar Apply campaign</span>
                    </p>
                    <p className="dots" />
                    <p className="dots" />
                    <p className="dots" />
                    <p className="text-partner">
                      2019. 06. 20 <span>Partner Approved</span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/*</div>*/}
          {/*</section>*/}
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(history));
