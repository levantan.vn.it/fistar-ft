import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";

// import calendar style
// You can customize style by copying asset folder.
import "@y0c/react-datepicker/assets/styles/calendar.scss";

import "./myfistar.scss";
import {
  getMyFistarAction,
  getTotalStatusAction
} from "./../../../../store/actions/campaign";

import Fistar from "./../../../../components/partner/Dashboard/Myfistar/index";
import InfiniteScroll from "react-infinite-scroll-component";
import * as routeName from "./../../../../routes/routeName";

const TYPE = {
  ALL: "",
  MATCHING: "matching",
  SCRAP: "scrap_fitar",
  REQUEST: "request_fistar",
  APPLY: "apply_fistar",
  RECOMMENDED: "recommended"
};

class MyFistar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pid: "",
      type: TYPE.ALL,
      fistars: [],
      matching: 0,
      scrap: 0,
      request: 0,
      apply: 0,
      onGoing: 0,
      closed: 0,
      recommended: 0,
      total: 0,
      isLoadingFistar: false,
      textTitle: "All fistar",
      last_page: 1,
      page: 1,
      hasMore: true,
      isRedirect: false,
      isRedirectSearch: false,
      totalAll: 0,
      totalScrap: 0,
      totalMatching: 0,
      totalRequest: 0,
      totalApply: 0,
      totalRecommended: 0,
      scrapChoose: false
    };
  }

  initData = () => {
    const { auth } = this.props;
    this.props.totalStatus(auth.id).then(response => {
      this.setState({
        totalAll: response.all,
        totalScrap: response.scrap,
        totalMatching: response.matching,
        totalRequest: response.request,
        totalApply: response.apply,
        totalRecommended: response.recommended
      });
    });

    this.props
      .myFistarPartner(this.state.page, this.state.type, auth.id)
      .then(response => {
        this.setState({
          fistars: response.data,
          matching: response.matching,
          scrap: response.scrap,
          request: response.request,
          apply: response.apply,
          recommended: response.recommended,
          total: response.total,
          isLoadingFistar: false,
          last_page: response.last_page
        });
      });
  };

  callback = () => {
    const { auth } = this.props;
    this.props.totalStatus(auth.id).then(response => {
      this.setState({
        totalAll: response.all,
        totalScrap: response.scrap,
        totalMatching: response.matching,
        totalRequest: response.request,
        totalApply: response.apply,
        totalRecommended: response.recommended
      });
    });
  };

  componentDidMount() {
    this.setState(
      {
        isLoadingFistar: true
      },
      () => {
        this.initData();
      }
    );
  }

  fetchMoreData = () => {
    const { auth } = this.props;
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }
    this.props
      .myFistarPartner(this.state.page + 1, this.state.type, auth.id)
      .then(response => {
        this.setState({
          fistars: [...this.state.fistars, ...response.data],
          page: response.current_page
        });
      });
  };

  clickChangeTab = type => {
    let textTitle = "";
    let scrapChoose = false;
    const { t } = this.props;
    switch (type) {
      case TYPE.MATCHING: {
        textTitle = `${t("MYFISTAR.MFR_amount_matching")}`;
        break;
      }
      case TYPE.SCRAP: {
        scrapChoose = true;
        textTitle = `${t("MYFISTAR.MFR_amount_scrap")}`;
        break;
      }
      case TYPE.APPLY: {
        textTitle = `${t("MYFISTAR.MFR_amount_apply")}`;
        break;
      }
      case TYPE.RECOMMENDED: {
        textTitle = `${t("MYFISTAR.MFR_amount_recommend")}`;
        break;
      }
      case TYPE.REQUEST: {
        textTitle = `${t("MYFISTAR.MFR_amount_request")}`;
        break;
      }
      default: {
        textTitle = `${t("MYFISTAR.MFR_all_fistar")}`;
        break;
      }
    }
    this.setState(
      {
        type,
        page: 1,
        isLoadingFistar: true,
        hasMore: true,
        scrapChoose: scrapChoose,
        textTitle
      },
      () => {
        this.initData();
      }
    );
  };
  redirectSearch = () => {
    this.setState({
      isRedirectSearch: true
    });
  };

  render() {
    const { t, auth } = this.props;
    const {
      type,
      fistars,
      matching,
      scrap,
      request,
      onGoing,
      ready,
      closed,
      apply,
      recommended,
      total,
      isLoadingFistar,
      textTitle,
      last_page,
      page,
      hasMore,
      isRedirect,
      isRedirectSearch,
      totalAll,
      totalApply,
      totalRecommended,
      totalMatching,
      totalScrap,
      totalRequest
    } = this.state;

    let totalCam = total;
    switch (type) {
      case TYPE.MATCHING: {
        totalCam = totalMatching;
        break;
      }
      case TYPE.REQUEST: {
        totalCam = totalRequest;
        break;
      }
      case TYPE.APPLY: {
        totalCam = totalApply;
        break;
      }
      case TYPE.RECOMMENDED: {
        totalCam = totalRecommended;
        break;
      }
      case TYPE.SCRAP: {
        totalCam = totalScrap;
        break;
      }
      default: {
        totalCam = totalAll;
        break;
      }
    }

    if (isRedirectSearch) {
      return <Redirect to={routeName.PARTNER_SEARCH_FISTAR} />;
    }
    return (
      <Fragment>
        <div className="content">
          <div className="top w-100">
            <div className="left">
              <h4>
                <a href="javascript:void(0)">{t("MYFISTAR.MFR_my_fistar")}</a>
              </h4>
            </div>
            <div className="right">
              <input
                type="text"
                name="Sass"
                className="input-search"
                id="searchs"
              />
              <button onClick={this.redirectSearch}>
                <i className="fas fa-search" />
                <span>{t("MYFISTAR.MFR_button_search")}</span>
              </button>
            </div>
          </div>
          <div className="information-fistar">
            <ul className="list-information">
              <li
                className={`item ${type === TYPE.ALL ? " active" : ""}`}
                onClick={() => this.clickChangeTab(TYPE.ALL)}
              >
                <a href="javascript:void(0)" className="text">
                  <p className="number">
                    {isLoadingFistar && !total ? totalAll : totalAll}
                  </p>
                  <p>{t("MYFISTAR.MFR_amount_all")}</p>
                </a>
              </li>
              <li
                className={`item ${type === TYPE.MATCHING ? " active" : ""}`}
                onClick={() => this.clickChangeTab(TYPE.MATCHING)}
              >
                <a href="javascript:void(0)" className="text">
                  <p className="number">
                    {isLoadingFistar && !total ? totalMatching : totalMatching}
                  </p>
                  <p>{t("MYFISTAR.MFR_amount_matching")}</p>
                </a>
              </li>
              <li
                className={`item ${type === TYPE.SCRAP ? " active" : ""}`}
                onClick={() => this.clickChangeTab(TYPE.SCRAP)}
              >
                <a href="javascript:void(0)" className="text">
                  <p className="number">
                    {isLoadingFistar && !total ? totalScrap : totalScrap}
                  </p>
                  <p>{t("MYFISTAR.MFR_amount_scrap")}</p>
                </a>
              </li>
              <li
                className={`item ${type === TYPE.REQUEST ? " active" : ""}`}
                onClick={() => this.clickChangeTab(TYPE.REQUEST)}
              >
                <a href="javascript:void(0)" className="text">
                  <p className="number">
                    {isLoadingFistar && !total ? totalRequest : totalRequest}
                  </p>
                  <p>{t("MYFISTAR.MFR_amount_request")}</p>
                </a>
              </li>
              <li
                className={`item ${type === TYPE.APPLY ? " active" : ""}`}
                onClick={() => this.clickChangeTab(TYPE.APPLY)}
              >
                <a href="javascript:void(0)" className="text">
                  <p className="number">
                    {isLoadingFistar && !total ? totalApply : totalApply}
                  </p>
                  <p>{t("MYFISTAR.MFR_amount_apply")}</p>
                </a>
              </li>
              <li
                className={`item ${type === TYPE.RECOMMENDED ? " active" : ""}`}
                onClick={() => this.clickChangeTab(TYPE.RECOMMENDED)}
              >
                <a href="javascript:void(0)" className="text">
                  <p className="number">
                    {isLoadingFistar && !total ? totalRecommended : totalRecommended}
                  </p>
                  <p>{t("MYFISTAR.MFR_amount_recommend")}</p>
                </a>
              </li>
            </ul>
          </div>
          <div className="box-information">
            <div className="title">
              <h4>{`${textTitle} ${totalCam}`}</h4>
            </div>
            <div className="box-partner">
              {isLoadingFistar ? (
                <div id="loading" />
              ) : (
                <div className="box-partner">
                  {fistars.length > 0 ? (
                    <InfiniteScroll
                      dataLength={fistars.length}
                      next={this.fetchMoreData}
                      loader={<div id="loading" />}
                      hasMore={this.state.hasMore}
                      className="my-fistar-scroll"
                    >
                      {fistars.map((fistar, key) => (
                        <Fragment key={key}>
                          <Fistar
                            fistar={fistar}
                            scrapChoose={this.state.scrapChoose}
                            callback={this.callback}
                            pid={fistar.pid}
                          />
                        </Fragment>
                      ))}
                    </InfiniteScroll>
                  ) : (
                    ""
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    myFistarPartner: (page = 1, filter = "", pid) =>
      dispatch(getMyFistarAction(page, filter, pid)),
    totalStatus: pid => dispatch(getTotalStatusAction(pid))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(MyFistar));
