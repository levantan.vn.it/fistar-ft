import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import imageClose from "../../../../images/close-popup.svg";
// import { getReviewAction } from './../../../../store/actions/fistar'
import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import {
  MATCHING_STATUS,
  GENDER_TYPE,
  SNS_STATE,
  IMAGE_SIZE,
  IMAGE_TYPE
} from "./../../../../constants/index";
import { DateFormatYMD } from "./../../../../common/helper";
import { updateReviewAction, updateApproveReviewAdminAction, updateModifyReviewAdminAction } from "./../../../../store/actions/campaign";
import imageCancel from "./../../../../images/cancel.svg";
import { getImageLink } from "./../../../../common/helper";
import "./modalReviewSns.scss";

class ModalReviewSNS extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: null,
      loading: false,
      fistar: {},
      campaigns: [],
      showModalRequest: false,
      isLoadingSubmit: false,
      rv_status: "",
      actire: 0
    };
  }

  handleClose = () => {
    this.props.onCloseModal();
  };

  opened = () => {
    // this.setState({
    //   loading: true
    // }, () => {
    //   this.props.getReview(this.props.cp_id, this.props.uid, this.props.sns_id)
    //     .then(response => {
    //       this.setState({
    //         campaigns: response,
    //         loading: false
    //       })
    //     })
    //     .catch(() => {
    //       this.setState({
    //         loading: false
    //       })
    //     })
    // })
  };

  handleCloseModalRequest = () => {
    this.setState({
      showModalRequest: false
    });
  };

  handleSubmitRequest = () => {
    this.setState(
      {
        isLoadingSubmit: true
      },
      () => {
        this.callrequest(this.state.rv_status, this.state.active);
      }
    );
  };

  callrequest = (rv_status, m_ch_active_url) => {
    const { channel } = this.props;
    let m_id = channel.m_id;
    let m_ch_title = channel.m_ch_title;
    let m_ch_category = channel.m_ch_category;
    let m_ch_content = channel.m_ch_content;
    let m_ch_url = channel.m_ch_url;
    let sns_id = channel.sns_id;

    if (!m_id) {
      if (this.state.rv_status === 105) {
        this.props
          .updateApproveReviewAdmin(channel.id, 105)
          .then(response => {
            this.setState({
              isLoadingSubmit: false
            });
            this.handleCloseModalRequest();
            this.handleClose();
            this.props.resetData();
          });
      } else if (this.state.rv_status === 104) {
        this.props
          .updateModifyReviewAdmin(channel.id)
          .then(response => {
            this.setState({
              isLoadingSubmit: false
            });
            this.handleCloseModalRequest();
            this.handleClose();
            this.props.resetData();
          });
      }
      return;
    }

    this.props
      .updateReview(m_id, {
        m_ch_title,
        m_ch_category,
        m_ch_content,
        m_ch_url,
        sns_id,
        rv_status,
        m_ch_active_url
      })
      .then(response => {
        this.setState({
          isLoadingSubmit: false
        });
        this.handleCloseModalRequest();
        this.handleClose();
        this.props.resetData();
      });
  };

  handleSubmit = (rv_status, countRequest, active = 0) => {
    if (countRequest >= 3) {
      return;
    }
    // if (rv_status === SNS_STATE.MODIFY) {
    this.setState({
      showModalRequest: true,
      rv_status,
      active
    });
    return;
    // }
    // this.setState(
    //   {
    //     isLoadingSubmit: true
    //   },
    //   () => {
    //     this.callrequest(rv_status);
    //   }
    // );
  };

  render() {
    const { campaign, channel, fistar, t } = this.props;
      if(channel.m_ch_url) {
          let url_https=channel.m_ch_url.includes("https://");
          let url_http=channel.m_ch_url.includes("http://");
          if(!url_https && !url_http) {
              channel.m_ch_url="https://" + channel.m_ch_url
          }
      }

    const { showModalRequest } = this.state;
    let request = (channel.history_review || []).filter(
      e => e.rv_status == SNS_STATE.CHECKING
    );
    let countRequest = request.length - 1;

    return (
      <Fragment>
        <Modal
          size="lg"
          show={this.props.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="review-modify modal-dialog-centered"
          onEntered={this.opened}
        >
          <div className="header">
            <div className="top">
              <div className="left">
                <p>{t("POPUP_REVIEW_MODIFY.PRM_ready")}</p>
                <h4>{campaign ? campaign.cp_name : ""}</h4>
              </div>
              <div className="right">
                <button
                  type="button"
                  className="btn btn-close close close-modify"
                  onClick={this.handleClose}
                >
                  <img src={imageCancel} alt="close" />
                </button>
              </div>
            </div>
            <div className="tab customer-tab">
              <ul className="list-tab">
                <li className="item">
                  {t("POPUP_REVIEW_MODIFY.PRM_channel")}
                  <a href="javascript:void(0)">
                    {channel ? channel.sns.sns_name : ""}
                  </a>
                </li>
                <li className="item">
                  {t("POPUP_REVIEW_MODIFY.PRM_category")}
                  {/*<a href="javascript:void(0)">{channel ? channel.m_ch_category : ''}</a>*/}
                  <a href="javascript:void(0)">
                    {channel.category ? channel.category.cd_label : ""}
                  </a>
                </li>
              </ul>
                <p className="config-date">{channel ? DateFormatYMD(channel.review_status.updated_at) : ""}</p>
            </div>
          </div>
          <div className="main-content-check">
            <div
              className={
                channel.m_ch_url !== "" ? " content content-url" : " content"
              }
            >
              <div className="top">
                <div className="image-top">
                  <img
                    src={
                      fistar
                        ? getImageLink(
                            fistar.influencer.picture,
                            IMAGE_TYPE.FISTARS,
                            IMAGE_SIZE.ORIGINAL
                          )
                        : ""
                    }
                    alt=""
                  />
                  <h4 className="name">{channel ? channel.m_ch_title : ""}</h4>
                </div>
                <div className="date">
                  {/*<p>{channel ? DateFormatYMD(channel.updated_at) : ""}</p>*/}
                </div>
              </div>
              {/*<textarea
              className="form-control text"
              id="examplejFormdfdgContrffolTexftgarea2"
              rows="10"
              placeholder=""
              defaultValue={channel ? channel.m_ch_content : ''}
              readOnly
            ></textarea>*/}
                <div className="review-image d-flex justify-content-start">
                {(channel.galery || []).map((e, key) => (
                    <div className="item-img" key={key}>
                      <img src={
                        getImageLink(
                          e.image,
                          IMAGE_TYPE.REVIEWS,
                          IMAGE_SIZE.ORIGINAL
                        )
                      }
                      className="mt-0" />
                    </div>
                ))}
              </div>
              {channel.m_ch_url && (
                <p className="pt-3">
                  <a href={channel.m_ch_url} target="_blank">
                    {channel.m_ch_url}
                  </a>
                </p>
              )}
              <div
                className="content-text"
                dangerouslySetInnerHTML={{
                  __html: channel ? channel.m_ch_content : ""
                }}
              />
              <p className="you-could">
                {t("POPUP_REVIEW_MODIFY.PRM_text_content")}
              </p>
            </div>
          </div>
          <div className="group-btn">
            {channel &&
            channel.review_status.rv_status === SNS_STATE.CHECKING ? (
              <Fragment>
                <button
                  type="button"
                  onClick={() =>
                    this.handleSubmit(SNS_STATE.MODIFY, countRequest)
                  }
                >
                  {this.state.isLoadingSubmit ? (
                    <div className="spinner-border" role="status">
                      <span className="sr-only">{t("LOADING.LOADING")}</span>
                    </div>
                  ) : (
                    <Fragment>
                      {`${t("POPUP_REVIEW_MODIFY.PRM_button_request")}${
                        countRequest > 0 ? " (" + countRequest + ")" : ""
                      }`}
                    </Fragment>
                  )}
                </button>
                <button
                  type="button"
                  onClick={() => this.handleSubmit(SNS_STATE.COMPLETE, 1)}
                >
                  {this.state.isLoadingSubmit ? (
                    <div className="spinner-border" role="status">
                      <span className="sr-only">{t("LOADING.LOADING")}</span>
                    </div>
                  ) : (
                    `${t("POPUP_REVIEW_MODIFY.PRM_button_approve")}`
                  )}
                </button>
              </Fragment>
            ) : channel &&
            channel.review_status.rv_status === SNS_STATE.COMPLETE &&
            channel.m_ch_url &&
            !channel.m_ch_active_url ? (
              <Fragment>
                <button type="button" onClick={this.handleClose}>
                  {t("POPUP_REVIEW_MODIFY.PRM_close")}
                </button>
                <button
                  type="button"
                  onClick={() => this.handleSubmit(SNS_STATE.COMPLETE, 1, 1)}
                >
                  {this.state.isLoadingSubmit ? (
                    <div className="spinner-border" role="status">
                      <span className="sr-only">{this.props.t("LOADING.LOADING")}</span>
                    </div>
                  ) : (
                    "Approve"
                  )}
                </button>
              </Fragment>
            ) : (
              <button type="button" onClick={this.handleClose}>
                {t("POPUP_REVIEW_MODIFY.PRM_close")}
              </button>
            )}
          </div>
        </Modal>

        <Modal
          size="sm"
          show={showModalRequest}
          onHide={this.handleCloseModalRequest}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="howto-register modal-dialog-centered"
        >
          <div className="register">
            <div className="top modal-header">
              <h4>fiStar</h4>
              <button
                type="button"
                className="btn btn-close close"
                onClick={this.handleCloseModalRequest}
              >
                <img src={imageCancel} alt="" />
              </button>
            </div>
            <div className="body-popup">
              <p className="text-center">
                {this.state.rv_status === SNS_STATE.MODIFY &&
                  "Do you request to modify a review?"}
                {this.state.rv_status === SNS_STATE.COMPLETE &&
                  "Do you approve the review?"}
              </p>
              <button onClick={this.handleSubmitRequest}>
                {t("ANSWER_QUESTION.AQN_button_ok")}
              </button>
            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    updateReview: (id, data) => dispatch(updateReviewAction(id, data)),
    updateApproveReviewAdmin: (id, rv_status) => dispatch(updateApproveReviewAdminAction(id, rv_status)),
    updateModifyReviewAdmin: (id) => dispatch(updateModifyReviewAdminAction(id)),
    // getReview: (cp_id, uid, sns_id) => dispatch(getReviewAction(cp_id, uid, sns_id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(ModalReviewSNS));
