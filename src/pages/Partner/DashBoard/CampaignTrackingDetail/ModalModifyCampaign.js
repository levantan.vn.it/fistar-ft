import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import imageClose from "../../../../images/close-popup.svg";
import { getHistoryAction } from './../../../../store/actions/fistar'
import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import { MATCHING_STATUS, GENDER_TYPE } from './../../../../constants'
import { DateFormatYMD } from './../../../../common/helper'
import CreateCampain from './../CreateCampain'

class ModalModifyCampaign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: null,
      loading: false,
      fistar: {},
      campaigns: []
    };
  }

  handleClose = () => {
    this.props.onCloseModal();
  };

  reloadData = () => {
    this.props.reloadData(true)
    this.props.onCloseModal();
  }

  opened = () => {
    // this.setState({
    //   loading: true
    // }, () => {
    //   this.props.getHistory(this.props.uid)
    //     .then(response => {
    //       this.setState({
    //         fistar: response,
    //         campaigns: response.campaigns,
    //         loading: false
    //       })
    //     })
    //     .catch(() => {
    //       this.setState({
    //         loading: false
    //       })
    //     })
    // })
  }

  render() {
    return (
      <Fragment>
        <Modal
          show={this.props.show}
          onHide={this.handleClose}
          aria-labelledby="example-modal-sizes-title-lg"
          dialogClassName="popup-modify-campaign"
          onEntered={this.opened}
          backdrop={'static'}
        >
          <div className="history">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.handleClose}>
                <img src={imageClose} alt="" />
              </button>
            </div>
            <div className="body-popup pl-1 pr-1">

            {this.props.campaign ? <CreateCampain campaign={this.props.campaign} reloadData={this.reloadData} handleClose={this.handleClose}/> : ''}

            </div>
          </div>
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    getHistory: uid => dispatch(getHistoryAction(uid))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(ModalModifyCampaign));
