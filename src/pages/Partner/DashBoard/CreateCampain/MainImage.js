import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import {
  createCampainActionPartner,
  updateCampainActionPartner
} from "./../../../../store/actions/auth";
import SearchFiStar from "./search";
import imageUpload from "./../../../../images/img-campaign.png";
// import imageUpload from 'https://ppc.tools/wp-content/themes/ppctools/img/no-thumbnail.jpg'
import { getCodeAction } from "../../../../store/actions/code";
import { DatePicker, RangeDatePicker } from "@y0c/react-datepicker";
// import calendar style
// You can customize style by copying asset folder.
import "@y0c/react-datepicker/assets/styles/calendar.scss";
import { format } from "date-fns";
import "./index.scss";
import * as routeName from "../../../../routes/routeName";
import { DATE_FORMAT } from "../../../../constants";

import closePopup from "./../../../../images/close-popup.svg";
import { Modal, Tabs, Tab, Nav } from "react-bootstrap";
import { getImageLink } from "./../../../../common/helper";
import { IMAGE_SIZE, IMAGE_TYPE } from "./../../../../constants/index";

const ATTACHMENT_LENGTH = 4;

function getId(url) {
  if (typeof url !== "string") {
    return null;
  }
  var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
  var match = url.match(regExp);

  if (match && match[2].length == 11) {
    return match[2];
  } else {
    return "error";
  }
}

function scrollToTopAnimated(scrollDuration) {
  if (typeof window !== 'undefined' && window.document && window.document.createElement) {
    var scrollStep = -window.scrollY / (scrollDuration / 15),
      scrollInterval = setInterval(function() {
        if (window.scrollY != 0) {
          window.scrollBy(0, scrollStep);
        } else clearInterval(scrollInterval);
      }, 15);
  }
}

class MainImage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        cp_main_image: "",
        cp_main_image_type: 1,
        cp_attachment_url: [...Array(ATTACHMENT_LENGTH)],
        // cp_attachment_url_choose: false,
        // keywords: [],
        // cp_description: "",
        // cp_category: "",
        // cp_product_price: "",
        // cp_campaign_price: "",
        // cp_image: "",
        // cp_image_title: "test",
        // cp_brand: "",
        // cp_total_free: "",
        // uidFistars: [],
        // cp_name: "",
        // uids: [],
        // uidStateSearch: [],
        // cp_type: 1,
        // cp_status: "59",
        // m_status: "0",
        // cp_period_start: "",
        // cp_period_end: "",
        // cp_attachment_type: 1,
        // number_fistar: 1,
        // fistarCost: [],
        cp_attachment_url_remove: []
      },
      image: "",
      youtube: "",
      video: "",
      isChange: false,
      disabledUpdate: false,
      cp_id: "",
      upvideo: "",
      uidInfos: [],
      disnableStartDateWithCurrent: false,
      show: false,
      form_attach: 1,
      file: [],
      imagePreviewUrl: [...Array(ATTACHMENT_LENGTH)],
      imagePreviewUrlDescription: "",
      imagePreviewUrlVideo: "",
      isSearchFiStar: false,
      keywords: {},
      catalogs: {},
      selectedAttachment: null,
      bigSize: false,
      bigSizeMultiple: false,
      errors: {
        file_url: "",
        cp_main_image: "",
        cp_attachment_url: "",
        keywords: "",
        cp_description: "",
        cp_category: "",
        cp_product_price: "",
        cp_campaign_price: "",
        cp_image: "",
        cp_total_free: "",
        cp_brand: "",
        cp_name: "",
        type: "",
        cp_period_start: "",
        cp_period_end: "",
        cp_attachment_type: "",
        number_fistar: "",
        message: ""
      },
      startDate: new Date(),
      endDate: new Date(),
      fistarCost: [],
      sns: {},
      loading: false,
      loadingCreate: false,
      showSubmitEdit: false,
      dataUpdate: null,
      dataCreate: null,
      scrollTop: false
    };
    this.previewVideo = React.createRef();
    this.triggerClick = React.createRef();
  }

  componentDidMount() {
    if (
      this.props.cp_main_image
    ) {
      let cp_attachment_url = this.props.cp_attachment_url
        .filter(e => e != undefined)
        .map(e =>
          getImageLink(
            e.cp_attachment_url,
            IMAGE_TYPE.ATTACHMENTS,
            IMAGE_SIZE.ORIGINAL
          )
        );
      let attachmentLength = cp_attachment_url.length;
      let newLength =
        attachmentLength <= ATTACHMENT_LENGTH
          ? [...Array(ATTACHMENT_LENGTH - attachmentLength)]
          : [];
      this.setState(
        {
          data: {
            ...this.state.data,
            cp_main_image: this.props.cp_main_image,
            cp_main_image_type: +this.props.cp_main_image_type,
            cp_attachment_url: [...this.props.cp_attachment_url, ...newLength]
          },
          imagePreviewUrl: [...cp_attachment_url, ...newLength],
          imagePreviewUrlDescription:
            +this.props.cp_main_image_type == 1
              ? getImageLink(
                  this.props.cp_main_image,
                  IMAGE_TYPE.CAMPAIGNS,
                  IMAGE_SIZE.ORIGINAL
                )
              : "",
          imagePreviewUrlVideo:
            +this.props.cp_main_image_type == 3
              ? getImageLink(this.props.cp_main_image, IMAGE_TYPE.CAMPAIGNS)
              : "",
          video:
            +this.props.cp_main_image_type == 3
              ? getImageLink(this.props.cp_main_image, IMAGE_TYPE.CAMPAIGNS)
              : "",
          image:
            +this.props.cp_main_image_type == 1
              ? getImageLink(
                  this.props.cp_main_image,
                  IMAGE_TYPE.CAMPAIGNS,
                  IMAGE_SIZE.ORIGINAL
                )
              : "",
          youtube:
            +this.props.cp_main_image_type == 2
              ? getImageLink(this.props.cp_main_image, IMAGE_TYPE.CAMPAIGNS)
              : ""
        },
        () => {
        }
      );
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      !this.state.isChange &&
      this.props.cp_main_image != prevProps.cp_main_image
    ) {
      let cp_attachment_url = this.props.cp_attachment_url
        .filter(e => e != undefined)
        .map(e =>
          getImageLink(
            e.cp_attachment_url,
            IMAGE_TYPE.ATTACHMENTS,
            IMAGE_SIZE.ORIGINAL
          )
        );
      let attachmentLength = cp_attachment_url.length;
      let newLength =
        attachmentLength <= ATTACHMENT_LENGTH
          ? [...Array(ATTACHMENT_LENGTH - attachmentLength)]
          : [];
      this.setState(
        {
          data: {
            ...this.state.data,
            cp_main_image: this.props.cp_main_image,
            cp_main_image_type: +this.props.cp_main_image_type,
            cp_attachment_url: [...this.props.cp_attachment_url, ...newLength]
          },
          imagePreviewUrl: [...cp_attachment_url, ...newLength],
          imagePreviewUrlDescription:
            +this.props.cp_main_image_type == 1
              ? getImageLink(
                  this.props.cp_main_image,
                  IMAGE_TYPE.CAMPAIGNS,
                  IMAGE_SIZE.ORIGINAL
                )
              : "",
          imagePreviewUrlVideo:
            +this.props.cp_main_image_type == 3
              ? getImageLink(this.props.cp_main_image, IMAGE_TYPE.CAMPAIGNS)
              : "",
          video:
            +this.props.cp_main_image_type == 3
              ? getImageLink(this.props.cp_main_image, IMAGE_TYPE.CAMPAIGNS)
              : "",
          image:
            +this.props.cp_main_image_type == 1
              ? getImageLink(
                  this.props.cp_main_image,
                  IMAGE_TYPE.CAMPAIGNS,
                  IMAGE_SIZE.ORIGINAL
                )
              : "",
          youtube:
            +this.props.cp_main_image_type == 2
              ? getImageLink(this.props.cp_main_image, IMAGE_TYPE.CAMPAIGNS)
              : ""
        },
        () => {
        }
      );
    }
  }

  getData = data => {
    let errors = {
      cp_main_image: "",
      cp_attachment_url: this.state.bigSizeMultiple
        ? "Not be greater than " + process.env.REACT_APP_MAX_IMAGE_SIZE + "MB."
        : ""
    };
    switch (+data.cp_main_image_type) {
      case 1: {
        data.cp_main_image = this.state.image;
        errors.cp_main_image = this.state.bigSize
          ? "Not be greater than " +
            process.env.REACT_APP_MAX_IMAGE_SIZE +
            "MB."
          : "";
        break;
      }
      case 2: {
        data.cp_main_image = this.state.youtube;
        errors.cp_main_image = this.state.bigSize
          ? "Not be greater than " +
            process.env.REACT_APP_MAX_IMAGE_SIZE +
            "MB."
          : "";
        break;
      }
      case 3: {
        data.cp_main_image = this.state.video;
        errors.cp_main_image = this.state.bigSize
          ? "Not be greater than " +
            process.env.REACT_APP_MAX_VIDEO_SIZE +
            "MB."
          : "";
        break;
      }
      default: {
        break;
      }
    }
    // data.cp_attachment_url = data.cp_attachment_url.filter(e => {
    //   if (e != undefined && typeof e != 'string') {
    //     return e
    //   }
    // })
    // if (data.cp_main_image_type != 2 && typeof data.cp_main_image === 'string') {
    //   data.cp_main_image = null
    // }
    this.props.getData(data, errors);
  };

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;

    this.setState(
      {
        // data: {
        //   ...this.state.data,
        //   [name]: value
        // },
        youtube: value,
        disabledUpdate: this.state.cp_id ? true : false,
        isChange: true
      },
      () => {
        this.getData(this.state.data);
      }
    );
  };

  handleMainImageChange = e => {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    const fileType = file["type"];
    const validImageTypes = ["image/gif", "image/jpeg", "image/png"];
    if (validImageTypes.includes(fileType)) {
      reader.onloadend = () => {
        this.setState(
          {
            ...this.state.file,
            file: file,
            data: {
              ...this.state.data
              // 'image': files[0]
              // cp_main_image: file
            },
            image: file,
            isChange: true,
            disabledUpdate: this.state.cp_id ? true : false,
            bigSize:
              file &&
              file.size &&
              file.size > process.env.REACT_APP_MAX_IMAGE_SIZE * 1000000
                ? true
                : false,
            // imagePreviewUrlDescription:this.state.imagePreviewUrlDescription.concat(reader.result)
            imagePreviewUrlDescription: reader.result
          },
          () => {
            this.getData(this.state.data);
          }
        );
      };
      reader.readAsDataURL(file);
    }
  };

  handleImageChange = e => {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    const fileType = file["type"];
    const validImageTypes = ["image/gif", "image/jpeg", "image/png"];
    if (validImageTypes.includes(fileType)) {
      let imagePreviewUrl = this.state.imagePreviewUrl;
      let cp_attachment_url = this.state.data.cp_attachment_url;
      imagePreviewUrl = imagePreviewUrl.filter(e => e != undefined);
      cp_attachment_url = cp_attachment_url.filter(e => e != undefined);
      let bigSizeMultiple = false;
      if (this.state.bigSizeMultiple) {
        bigSizeMultiple = this.state.bigSizeMultiple;
      } else {
        bigSizeMultiple =
          file &&
          file.size &&
          file.size > process.env.REACT_APP_MAX_IMAGE_SIZE * 1000000
            ? true
            : false;
      }
      reader.onloadend = () => {
        imagePreviewUrl = [
          ...imagePreviewUrl,
          reader.result,
          ...Array(ATTACHMENT_LENGTH - imagePreviewUrl.length - 1)
        ];
        cp_attachment_url = [
          ...cp_attachment_url,
          file,
          ...Array(ATTACHMENT_LENGTH - cp_attachment_url.length - 1)
        ];
        this.setState(
          {
            ...this.state.file,
            file: file,
            data: {
              ...this.state.data,
              cp_attachment_url
            },
            disabledUpdate: this.state.cp_id ? true : false,
            isChange: true,
            bigSizeMultiple,
            // bigSizeMultiple:file && file.size && file.size > (process.env.REACT_APP_MAX_IMAGE_SIZE * 1000000) ? true : false,
            // bigSizeMultiple:file && file.size && file.size > 2000000 ? true : false,
            imagePreviewUrl
          },
          () => {
            this.getData(this.state.data);
          }
        );
      };

      reader.readAsDataURL(file);
    }
  };

  removeImage = (imgRemove, key) => {
    let {
      imagePreviewUrl,
      data: { cp_attachment_url, cp_attachment_url_remove }
    } = this.state;
    let remove_id = this.props.cp_attachment_url[key]
      ? this.props.cp_attachment_url[key].cpa_id
      : null;
    imagePreviewUrl[key] = undefined;
    cp_attachment_url[key] = undefined;
    imgRemove = imagePreviewUrl.filter(el => el != null);
    imgRemove = [...imgRemove, ...Array(ATTACHMENT_LENGTH - imgRemove.length)];
    cp_attachment_url = cp_attachment_url.filter(el => el != null);
    cp_attachment_url = [
      ...cp_attachment_url,
      ...Array(ATTACHMENT_LENGTH - cp_attachment_url.length)
    ];
    let bigSizeMultiple = false;
    cp_attachment_url.map(img => {
      if (
        img &&
        img.size &&
        img.size > process.env.REACT_APP_MAX_IMAGE_SIZE * 1000000
      ) {
        bigSizeMultiple = true;
      }
    });
    this.setState(
      {
        // ...this.state.imagePreviewUrl,
        imagePreviewUrl: imgRemove,
        data: {
          ...this.state.data,
          cp_attachment_url,
          cp_attachment_url_remove: remove_id
            ? [...this.state.data.cp_attachment_url_remove, remove_id]
            : this.state.data.cp_attachment_url_remove
        },
        bigSizeMultiple,
        disabledUpdate: this.state.cp_id ? true : false,
        // imagePreviewUrl: imgRemove,
        cp_attachment_url_choose: imgRemove.length === 0 ? true : false
      },
      () => {
        this.getData(this.state.data);
      }
    );
  };

  handleRadioChange = e => {
    this.setState(
      {
        form_attach: e.currentTarget.value,
        data: {
          ...this.state.data,
          cp_main_image_type: e.currentTarget.value,
          cp_main_image: ""
        },
        isChange: true,
        // imagePreviewUrlDescription: '',
        // imagePreviewUrlVideo: '',
        disabledUpdate: this.state.cp_id ? true : false
      },
      () => {
        this.getData(this.state.data);
      }
    );
  };

  handleVideoChange = e => {
    if (this.previewVideo.current) {
      this.previewVideo.current.pause();
    }
    let reader = new FileReader();
    let file = e.target.files[0];

    //if reading completed
    reader.onload = e => {
      let dataVideo = e.target.result;
      this.setState(
        {
          data: {
            ...this.state.data
            // cp_main_image: file
          },
          video: file,
          isChange: true,
          imagePreviewUrlVideo: dataVideo,
          // cp_attachment_url:file
          upvideo: file,
          bigSize:
            file &&
            file.size &&
            file.size > process.env.REACT_APP_MAX_VIDEO_SIZE * 1000000
              ? true
              : false
        },
        () => {
          if (this.previewVideo.current) {
            this.previewVideo.current.load();
            this.previewVideo.current.play();
          }
          this.getData(this.state.data);
        }
      );
    };

    //read the file
    reader.readAsDataURL(file);
  };

  renderPreview = () => {
    switch (+this.state.data.cp_main_image_type) {
      case 1: {
        return this.state.imagePreviewUrlDescription ? (
          <img src={this.state.imagePreviewUrlDescription} alt="" />
        ) : (
          <img
            src="https://ppc.tools/wp-content/themes/ppctools/img/no-thumbnail.jpg"
            alt=""
          />
        );
        break;
      }
      case 2: {
        return this.state.youtube ? (
          <div className="video-youtube mt-3">
            <iframe
              id="myIframe"
              width="515"
              className="react-player"
              height="315"
              frameBorder="0"
              src={
                this.state.youtube
                  ? "//www.youtube.com/embed/" + getId(this.state.youtube)
                  : "//www.youtube.com/embed/"
              }
              allowFullScreen
            />
          </div>
        ) : (
          <img
            src="https://ppc.tools/wp-content/themes/ppctools/img/no-thumbnail.jpg"
            alt=""
          />
        );
        break;
      }
      case 3: {
        return this.state.imagePreviewUrlVideo ? (
          <video ref={this.previewVideo} controls>
            <source src={this.state.imagePreviewUrlVideo} type="video/mp4" />
            Your browser does not support HTML5 video.
          </video>
        ) : (
          <img
            src="https://ppc.tools/wp-content/themes/ppctools/img/no-thumbnail.jpg"
            alt=""
          />
        );
        break;
      }
      default: {
        return (
          <img
            src="https://ppc.tools/wp-content/themes/ppctools/img/no-thumbnail.jpg"
            alt=""
          />
        );
        break;
      }
    }
  };

  render() {
    const { t, errors } = this.props;
    const {
      data,
      cp_id,
      keywords,
      imagePreviewUrl,
      imagePreviewUrlVideo,
      imagePreviewUrlDescription,
      file,
      isSearchFiStar,
      form_attach,
      loading,
      cp_attachment_url,
      loadingCreate,
      disabledUpdate,
      bigSize,
      bigSizeMultiple
    } = this.state;
    return (
      <Fragment>
        <div className="img">
          {this.renderPreview()}
          {errors.cp_main_image && (
            <p className="text-danger">{errors.cp_main_image}</p>
          )}
        </div>
          <h3 className="confix-margin">Campaign List</h3>
        <div className="add-photo">
          {this.state.data.cp_main_image_type == 1 && (
            <div className="add">
              <div className="icon-add">
                <i className="fas fa-camera" />
                <input
                  className="fileInput"
                  type="file"
                  accept="image/*"
                  onChange={e => this.handleMainImageChange(e)}
                  value=""
                />
              </div>
            </div>
          )}
          {this.state.data.cp_main_image_type == 3 && (
            <div className="add">
              <div className="icon-add">
                <i className="fas fa-camera" />
                <input
                  className="fileInput"
                  type="file"
                  accept="video/*"
                  onChange={this.handleVideoChange}
                />
              </div>
            </div>
          )}
          <div className="check-link">
            <div className="form-check">
              <input
                type="radio"
                id="image_upload"
                name="image"
                checked={this.state.data.cp_main_image_type == 1}
                onChange={this.handleRadioChange}
                value="1"
              />
              <label htmlFor="image_upload">Image Attachment (image size: 670px x 470px)</label>
            </div>
            <div className="form-check">
              <input
                type="radio"
                id="cp_main_image"
                name="youtube"
                checked={this.state.data.cp_main_image_type == 2}
                onChange={this.handleRadioChange}
                value="2"
              />
              <label htmlFor="cp_main_image">Youtube URL</label>
            </div>
            <div className="form-check">
              <input
                type="radio"
                id="file_url"
                name="video"
                checked={this.state.data.cp_main_image_type == 3}
                onChange={this.handleRadioChange}
                value="3"
              />
              <label htmlFor="file_url">Video Attachment</label>
            </div>
          </div>
        </div>

        {this.state.data.cp_main_image_type &&
          this.state.data.cp_main_image_type == 2 && (
            <div className="col-md-12 url-youtube">
              <input
                type="text"
                className="form-control input-url"
                aria-describedby="emailHelp"
                name="cp_main_image"
                onChange={this.handleChangeInput}
                value={this.state.youtube}
                placeholder="Youtube URL"
              />
            </div>
          )}

          <h3 className="confix-margin">Campaign Detail Image (size : 720px x 1280px)</h3>
        <div
          className={`col-md-12${
            imagePreviewUrl.length > 0 ? " group-add-photo" : " p-0"
          }`}
        >
          <div className="group-img group-add-photo">
            {imagePreviewUrl.map(
              (photo, key) =>
                photo ? (
                  <div className="item-img" key={key}>
                    <img src={photo} className="mt-0" />
                    <i
                      className="fa fa-times"
                      onClick={() => this.removeImage(photo, key)}
                    />
                  </div>
                ) : (
                  <div className="item add" key={key}>
                    <div className="icon-item icon-add">
                      <i className="fas fa-camera" />
                      <input
                        className="fileInput"
                        type="file"
                        accept="image/*"
                        onChange={this.handleImageChange}
                        value=""
                      />
                    </div>
                  </div>
                )
            )}
            {errors.cp_attachment_url && (
              <p className="text-danger pt-3">{errors.cp_attachment_url}</p>
            )}
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    uidFi: state.uidFi
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createCampain: data => dispatch(createCampainActionPartner(data)),
    updateCampain: (cp_id, data) =>
      dispatch(updateCampainActionPartner(cp_id, data)),
    getCode: type => dispatch(getCodeAction(type))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(MainImage));
