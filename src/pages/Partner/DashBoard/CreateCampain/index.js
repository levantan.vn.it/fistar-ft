import React, {Component, Fragment} from "react";
import ReactDOM from "react-dom";
import {withTranslation, Trans} from "react-i18next";
import {Link, Redirect, NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import {
    createCampainActionPartner,
    getBrand,
    updateCampainActionPartner
} from "./../../../../store/actions/auth";
import SearchFiStar from "./search";
import imageUpload from "./../../../../images/img-campaign.png";
// import imageUpload from 'https://ppc.tools/wp-content/themes/ppctools/img/no-thumbnail.jpg'
import {getCodeAction} from "../../../../store/actions/code";
import {DatePicker, RangeDatePicker} from "@y0c/react-datepicker";
// import calendar style
// You can customize style by copying asset folder.
import "@y0c/react-datepicker/assets/styles/calendar.scss";
import {format} from "date-fns";
import "./index.scss";
import * as routeName from "../../../../routes/routeName";
import {DATE_FORMAT} from "../../../../constants";

import closePopup from "./../../../../images/close-popup.svg";
import {Modal, Tabs, Tab, Nav} from "react-bootstrap";
import {getImageLink} from "./../../../../common/helper";
import {IMAGE_SIZE, IMAGE_TYPE, VALIDATION} from "./../../../../constants/index";
import MainImage from "./MainImage";
import {DateFormatYMDDatePicker} from "../../../../common/helper";
import EditorDescription from "./EditorDescription";
import {clickSelectSNSAction} from "../../../../store/actions/campaign";
import Flatpickr from 'react-flatpickr'
import 'flatpickr/dist/themes/material_blue.css'

function getId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return "error";
    }
}

function scrollToTopAnimated(scrollDuration) {
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
        var scrollStep = -window.scrollY / (scrollDuration / 15),
            scrollInterval = setInterval(function () {
                if (window.scrollY != 0) {
                    window.scrollBy(0, scrollStep);
                } else clearInterval(scrollInterval);
            }, 15);
    }
}

if (typeof window !== 'undefined' && window.document && window.document.createElement) {
    var showBack = false;
    window.onscroll = function () {
        scrollFunctionCreate()
    };

    function scrollFunctionCreate() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            showBack = true;
        } else {
            showBack = false;
        }
    }
}

class PartnerCreateCampain extends Component {
    constructor(props) {
        super(props);
        this.state = {
            prevScrollpos: (typeof window !== 'undefined' && window.document && window.document.createElement) ? window.pageYOffset : 0,
            visible: true,
            loaddingStatus: true,
            data: {
                cp_main_image: "",
                cp_main_image_type: 1,
                cp_attachment_url: [],
                cp_attachment_url_choose: false,
                keywords: [],
                cp_description: "",
                cp_category: "",
                cp_product_price: "",
                cp_campaign_price: "",

                cp_image: "",
                cp_image_title: "test",
                cp_brand: "",
                cp_total_free: "",
                uidFistars: [],
                cp_name: "",
                uids: [],
                uidStateSearch: [],
                cp_type: 1,
                cp_status: "59",
                m_status: "0",
                cp_period_start: "",
                cp_period_end: "",
                cp_attachment_type: 1,
                number_fistar: 1,
                fistarCost: [],
                cp_attachment_url_remove: [],
                cp_code_group: "10"
            },
            disabledUpdate: false,
            cp_id: "",
            upvideo: "",
            uidInfos: [],
            disnableStartDateWithCurrent: false,
            show: false,
            form_attach: 1,
            file: [],
            imagePreviewUrl: [],
            imagePreviewUrlDescription: "",
            imagePreviewUrlVideo: "",
            isSearchFiStar: false,
            keywords: {},
            catalogs: {},
            fashion: {},
            brands: [],
            selectedAttachment: null,
            bigSize: false,
            bigSizeMultiple: false,
            errors: {
                cp_main_image: "",
                cp_attachment_url: "",
                file_url: "",
                youtube_url: "",
                cp_attachment_url: "",
                keywords: "",

                cp_description: "",
                cp_category: "",
                cp_product_price: "",
                cp_campaign_price: "",
                cp_image: "",
                cp_total_free: "",
                cp_brand: "",
                cp_name: "",
                type: "",
                cp_period_start: "",
                cp_period_end: "",
                cp_attachment_type: "",
                number_fistar: "",
                message: ""
            },
            startDate: new Date(),
            endDate: new Date(),
            fistarCost: [],
            sns: {},
            loading: false,
            loadingCreate: false,
            showSubmitEdit: false,
            dataUpdate: null,
            dataCreate: null,
            scrollTop: false,
            isRedirect: false,
            isFinishInit: false
        };
        this.previewVideo = React.createRef();
        this.triggerClick = React.createRef();
        this.triggerClickUploadImage = React.createRef();
        if (typeof window !== 'undefined' && window.document && window.document.createElement) {
            window.scrollTo(0, 0);
        }
    }

    scrollToTop = () => {
        scrollToTopAnimated(700);
    };

    onSearchFiStar = () => {
        this.setState({
            isSearchFiStar: true,
            disabledUpdate: this.state.cp_id ? true : false
        });
    };

    handleChangeInputNumberOnly = e => {
        const name = e.target.name;
        const value = e.target.value;
        if (/^-?\d*[.,]?\d*$/.test(value)) {
            this.setState(prevState => ({
                data: {
                    ...prevState.data,
                    [name]: value
                },
                disabledUpdate: this.state.cp_id ? true : false
            }));
        }
    };

    handleChangeInput = e => {
        const name = e.target.name;
        const value = e.target.value;

        this.setState(prevState => ({
            data: {
                ...prevState.data,
                [name]: value
            },
            disabledUpdate: this.state.cp_id ? true : false
        }));
    };

    componentDidMount() {
        if (typeof window !== 'undefined' && window.document && window.document.createElement) {
            window.addEventListener("scroll", this.handleScrollCreate)
        }
        ;

        if (this.state.scrollTop) {
            if (typeof window !== 'undefined' && window.document && window.document.createElement) {
                window.scrollTo(0, 0)
            }
            ;
        }

        // const promises = [
        //     new Promise(resolve => setTimeout(resolve, 0, 1)),
        //     new Promise(resolve => setTimeout(resolve, 0, 2))
        // ];

        Promise.all([this.props.getCode("keyword")])
            .then(result => {
                this.setState({
                    keywords: result[0],
                    loaddingStatus:false
                })
            });


        this.props.getCode("Catalog").then(response => {
            this.setState({
                catalogs: response
            });
        });
        this.props.getCode("Fashion").then(response => {
            this.setState({
                fashion: response
            });
        });
        this.props.getBrand().then(response => {
            this.setState({
                brands: response
            });
        });

        if (this.props.campaign) {
            const {campaign} = this.props;
            let fistarChange = [];
            let mapFistar =
                campaign &&
                campaign.matchings &&
                campaign.matchings.map(fistar => {
                    fistar.influencer.name = fistar.influencer.fullname;
                    fistar.influencer.avatar = fistar.influencer.picture;
                    let selectedSNS = fistar.matching_channel
                        .map(
                            matching =>
                                matching.m_ch_selected === 1 ? matching.sns_id : null
                        )
                        .filter(e => e !== null);

                    selectedSNS.map(sns_id => {
                        let channelChange =
                            fistar.influencer &&
                            fistar.influencer.channels.length > 0 &&
                            fistar.influencer.channels.map(channel => {
                                let chan = {...channel};
                                chan.selected = false;
                                chan.show = true;
                                if (sns_id === chan.sns_id) {
                                    chan.selected = true;
                                    chan.show = true;
                                }
                                let sns_name = "";
                                if (chan.sns_id == 1) {
                                    sns_name = "fime";
                                } else if (chan.sns_id == 2) {
                                    sns_name = "facebook";
                                } else if (chan.sns_id == 3) {
                                    sns_name = "youtube";
                                } else if (chan.sns_id == 4) {
                                    sns_name = "instagram";
                                }
                                chan.sns = {
                                    sns_id: chan.sns_id,
                                    sns_name: sns_name
                                };

                                return chan;
                            });

                        fistarChange.push({
                            ...fistar.influencer,
                            channel: channelChange
                        });
                    });
                });
            this.setState(
                {
                    data: {
                        ...this.state.data,
                        cp_main_image: campaign.cp_main_image,
                        cp_main_image_type: campaign.cp_main_image_type,
                        cp_attachment_url:
                            campaign.attachments && campaign.attachments[0]
                                ? campaign.attachments
                                : [],
                        cp_attachment_url_choose: false,
                        keywords: campaign.keywords.map(e => e.cd_id),
                        cp_description: campaign.cp_description,
                        cp_category: campaign.cp_category,
                        cp_product_price: campaign.cp_product_price,
                        cp_campaign_price: campaign.cp_campaign_price,

                        cp_image: campaign.cp_image,
                        cp_image_title: campaign.cp_image_title,
                        cp_brand: campaign.cp_brand,
                        cp_total_free: campaign.cp_total_free,
                        uidFistars: fistarChange,
                        cp_name: campaign.cp_name,
                        uids: [],
                        uidStateSearch: [],
                        cp_type: campaign.cp_type,
                        cp_code_group: campaign.cp_code_group,
                        cp_status: "59",
                        m_status: "0",
                        cp_period_start: campaign.cp_period_start,
                        cp_period_end: campaign.cp_period_end,
                        cp_attachment_type:
                            campaign.attachments && campaign.attachments[0]
                                ? campaign.attachments[0].cp_attachment_type
                                : 1,
                        number_fistar: campaign.cp_total_influencer,
                        fistarCost: [],
                        cp_delivery_end_date:
                            new Date(campaign.cp_delivery_end_date) ? format(new Date(), DATE_FORMAT) : '',
                        cp_delivery_start_date:
                            new Date(campaign.cp_delivery_start_date) ?
                                format(new Date(), DATE_FORMAT) : '',
                        cp_state: campaign.cp_state || 0,
                        youtube_url:
                            campaign.attachments &&
                            campaign.attachments[0] &&
                            campaign.attachments[0].cp_attachment_type == 2
                                ? campaign.attachments[0].cp_attachment_url
                                : ""
                    },
                    cp_id: campaign.cp_id,
                    imagePreviewUrlDescription: campaign.cp_image,
                    form_attach:
                        campaign.attachments && campaign.attachments[0]
                            ? campaign.attachments[0].cp_attachment_type
                            : 1,
                    imagePreviewUrl:
                        campaign.attachments &&
                        campaign.attachments[0] &&
                        campaign.attachments[0].cp_attachment_type == 1
                            ? campaign.attachments.map(e =>
                                getImageLink(
                                    e.cp_attachment_url,
                                    IMAGE_TYPE.ATTACHMENTS,
                                    IMAGE_SIZE.ORIGINAL
                                )
                            )
                            : []
                },
                () => {
                    this.setState({isFinishInit: true});
                    this.resetUidFistars();
                }
            );
        } else {
            this.setState({isFinishInit: true});
        }
    }


    componentWillUnmount() {
        if (typeof window !== 'undefined' && window.document && window.document.createElement) {
            window.removeEventListener("scroll", this.handleScrollCreate);
        }

    }

    handleScrollCreate = () => {
        const {prevScrollpos} = this.state;

        const currentScrollPos = (typeof window !== 'undefined' && window.document && window.document.createElement) ? window.pageYOffset : 0;
        const visible = prevScrollpos > currentScrollPos;

        this.setState({
            prevScrollpos: currentScrollPos,
            visible
        });
    };

    handleChangeDate = (start, end) => {
        if (start) {
            if (new Date(start) < new Date()) {
                this.setState(prevState => ({
                    data: {
                        ...prevState.data,
                        cp_period_start: format(new Date(start), DATE_FORMAT),
                        // cp_period_end: format(new Date(end), DATE_FORMAT)
                    },
                    disnableStartDateWithCurrent: true,

                    errors: {
                        cp_period_start:
                            this.props.t("CREATE_NEW_CAMPAIGN.CNC_erro_start_due_date")
                    }
                }));
            } else {
                this.setState(prevState => ({
                    data: {
                        ...prevState.data,
                        cp_period_start: format(new Date(start), DATE_FORMAT),
                        // cp_period_end: format(new Date(end), DATE_FORMAT)
                    },
                    disabledUpdate: this.state.cp_id ? true : false,
                    disnableStartDateWithCurrent: false,
                    errors: {
                        cp_period_start: ""
                    }
                }));
            }
        }
        if (end) {
            if (new Date(end) < new Date(this.state.data.cp_period_start)) {
                this.setState(prevState => ({
                    data: {
                        ...prevState.data,
                        // cp_period_start: format(new Date(start), DATE_FORMAT),
                        cp_period_end: format(new Date(end), DATE_FORMAT)
                    },
                    disnableStartDateWithCurrent: true,

                    errors: {
                        cp_period_start:
                            this.props.t("CREATE_NEW_CAMPAIGN.CNC_erro_start_due_date")
                    }
                }));
            } else {
                this.setState(prevState => ({
                    data: {
                        ...prevState.data,
                        // cp_period_start: format(new Date(start), DATE_FORMAT),
                        cp_period_end: format(new Date(end), DATE_FORMAT)
                    },
                    disabledUpdate: this.state.cp_id ? true : false,
                    disnableStartDateWithCurrent: false,
                    errors: {
                        cp_period_start: ""
                    }
                }));
            }
        }
    };
    disableDay = date => {
        const currentDate = format(new Date(), DATE_FORMAT);
        let data = format(new Date(date), DATE_FORMAT);
    };

    onFocusInput = e => {
        this.setState(prevState => ({
            errors: {
                cp_attachment_url: "",
                keywords: "",
                cp_description: "",
                cp_category: "",
                cp_product_price: "",
                cp_campaign_price: "",

                cp_image: "",
                cp_name: "",
                cp_brand: "",
                cp_total_free: "",
                type: "",
                cp_period_start: "",
                cp_period_end: "",
                cp_attachment_type: "",
                message: ""
            }
        }));
    };

    handleImageDescriptionChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];
        const fileType = file["type"];
        const validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        if (validImageTypes.includes(fileType)) {
            reader.onloadend = () => {
                this.setState(prevState => ({
                    ...this.state.file,
                    file: file,
                    data: {
                        ...prevState.data,
                        // 'image': files[0]
                        cp_image: file
                    },
                    disabledUpdate: this.state.cp_id ? true : false,
                    bigSize:
                        file &&
                        file.size &&
                        file.size > process.env.REACT_APP_MAX_IMAGE_SIZE * 1000000
                            ? true
                            : false
                    // imagePreviewUrlDescription:this.state.imagePreviewUrlDescription.concat(reader.result)
                    // imagePreviewUrlDescription: reader.result
                }));
            };
            reader.readAsDataURL(file);
        } else {
            this.setState(prevState => ({
                data: {
                    ...prevState.data,
                    cp_image: "isNotFileImage"
                }
            }));
        }
    }


    removeImage = (imgRemove, key) => {
        let {
            imagePreviewUrl,
            data: {cp_attachment_url, cp_attachment_url_remove}
        } = this.state;
        let remove_id = cp_attachment_url[key]
            ? cp_attachment_url[key].cpa_id
            : null;
        imagePreviewUrl[key] = null;
        cp_attachment_url[key] = null;
        imgRemove = imagePreviewUrl.filter(el => el != null);
        cp_attachment_url = cp_attachment_url.filter(el => el != null);
        this.setState({
            // ...this.state.imagePreviewUrl,
            imagePreviewUrl: imgRemove,
            data: {
                ...this.state.data,
                cp_attachment_url,
                cp_attachment_url_remove: remove_id
                    ? [...this.state.data.cp_attachment_url_remove, remove_id]
                    : this.state.data.cp_attachment_url_remove
            },
            disabledUpdate: this.state.cp_id ? true : false,
            // imagePreviewUrl: imgRemove,
            cp_attachment_url_choose: imgRemove.length === 0 ? true : false
        });
    };

    handleImageChange(e) {
        e.preventDefault();
        if (this.state.cp_attachment_url_choose) {
            this.setState({
                cp_attachment_url: []
            });
        }
        for (var i = 0; i < e.target.files.length; i++) {
            let reader = new FileReader();
            let file = e.target.files[i];
            const fileType = file["type"];
            const validImageTypes = ["image/gif", "image/jpeg", "image/png"];
            if (validImageTypes.includes(fileType)) {
                if (
                    e.target.files.length + this.state.data.cp_attachment_url.length <
                    5
                ) {
                    reader.onloadend = () => {
                        this.setState(prevState => ({
                            ...this.state.file,
                            file: file,
                            data: {
                                ...prevState.data,
                                cp_attachment_url: [...this.state.data.cp_attachment_url, file]
                            },
                            disabledUpdate: this.state.cp_id ? true : false,
                            // bigSizeMultiple:file && file.size && file.size > (process.env.REACT_APP_MAX_IMAGE_SIZE * 1000000) ? true : false,
                            // bigSizeMultiple:file && file.size && file.size > 2000000 ? true : false,
                            imagePreviewUrl: [...this.state.imagePreviewUrl, reader.result]
                        }));
                    };

                    reader.readAsDataURL(file);
                } else {
                    this.setState(prevState => ({
                        ...this.state.file,
                        data: {
                            ...prevState.data,
                            cp_attachment_url: []
                        },
                        imagePreviewUrl: [],
                        errors: {
                            cp_attachment_url: this.props.t("CREATE_NEW_CAMPAIGN.CNC_erro_start_maximum_img")
                        }
                    }));
                }
            } else {
                this.setState(prevState => ({
                    data: {
                        ...prevState.data,
                        cp_attachment_url: 0
                    },
                    errors: {
                        cp_attachment_url: this.props.t("CREATE_NEW_CAMPAIGN.CNC_erro_not_img")
                    }
                }));
            }
        }
    }

    formatNumber = n => {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    validateForm = () => {
        const {t} = this.props;
        const errors = {
            file_url: "",
            youtube_url: "",
            cp_attachment_url: "",
            keywords: "",
            cp_description: "",
            cp_category: "",
            cp_product_price: "",
            cp_campaign_price: "",

            cp_image: "",
            cp_name: "",
            cp_brand: "",
            cp_total_free: "",
            cp_type: "",
            cp_code_group: "",
            cp_period_start: "",
            cp_period_end: "",
            cp_attachment_type: "",
            number_fistar: "",
            cp_main_image: "",
            uids: "",
            message: ""
        };
        const {data} = this.state;
        let sns_id = data.uids
            .map((id) => {
                id = id.split('-')
                return +id[1]
            })
            .filter(e => e === 1)
        // if (sns_id.length == 0) {
        //   errors.uids = t("CREATE_NEW_CAMPAIGN.CNC_erro_no_fime");
        // }

        if (!data.cp_description) {
            errors.cp_description = t("CREATE_NEW_CAMPAIGN.CNC_erro_description");
        } else if (data.cp_description.length > 1000) {
            errors.cp_description = t(
                "CREATE_NEW_CAMPAIGN.CNC_erro_description_limit"
            );
        }
        if (!data.cp_name) {
            errors.cp_name = t("CREATE_NEW_CAMPAIGN.CNC_erro_campaign_name");
        } else if (data.cp_name.length > VALIDATION.MAX_LENGTH) {
            errors.cp_name = t("CREATE_NEW_CAMPAIGN.CNC_erro_campaign_name_limit");
        }
        if (!data.cp_brand) {
            errors.cp_brand = t("CREATE_NEW_CAMPAIGN.CNC_erro_brand");
        } else if (data.cp_brand && data.cp_brand.length > VALIDATION.MAX_LENGTH) {
            errors.cp_brand = t("CREATE_NEW_CAMPAIGN.CNC_erro_brand_limit");
        }
        if (!data.cp_category) {
            errors.cp_category = t("CREATE_NEW_CAMPAIGN.CNC_erro_category");
        }
        if (!data.number_fistar) {
            errors.number_fistar = t("CREATE_NEW_CAMPAIGN.CNC_erro_fistar");
        }
        if (!data.cp_period_start) {
            errors.cp_period_start = t("CREATE_NEW_CAMPAIGN.CNC_erro_date");
        } else if (
            data.cp_period_start &&
            this.state.disnableStartDateWithCurrent
        ) {
            errors.cp_period_start = t(
                "CREATE_NEW_CAMPAIGN.CNC_erro_date_more_current"
            );
        }
        if (!data.cp_period_end) {
            errors.cp_period_end = t("CREATE_NEW_CAMPAIGN.CNC_erro_date_end");
        }
        if (data.keywords.length <= 0) {
            errors.keywords = t("CREATE_NEW_CAMPAIGN.CNC_erro_keyword");
        }

        if (data.cp_type == 0 && !data.cp_total_free) {
            errors.cp_total_free = t("CREATE_NEW_CAMPAIGN.CNC_erro_total_free");
        } else if (data.cp_total_free == "") {
            errors.cp_total_free = t("CREATE_NEW_CAMPAIGN.CNC_erro_total_free");
        }

        if (!data.cp_product_price) {
            errors.cp_product_price = t("CREATE_NEW_CAMPAIGN.CNC_erro_product_price");
        }
        console.log(data.cp_product_price, data.cp_campaign_price);
        if (data.cp_product_price && data.cp_campaign_price && +data.cp_product_price < +data.cp_campaign_price) {
            errors.cp_campaign_price = t("CREATE_NEW_CAMPAIGN.CNC_erro_sale_price_more");
        }

        // if (!data.cp_image) {
        //   errors.cp_image = t("CREATE_NEW_CAMPAIGN.CNC_erro_img_description");
        // }
        if (data.cp_image == "isNotFileImage") {
            errors.cp_image = t("CREATE_NEW_CAMPAIGN.CNC_erro_not_img");
        } else if (this.state.bigSize) {
            errors.cp_image =
                t("CREATE_NEW_CAMPAIGN.CNC_erro_not_img_than") +
                process.env.REACT_APP_MAX_IMAGE_SIZE +
                "MB.";
        }
        // if (data.cp_attachment_url == 0) {
        //   errors.cp_attachment_url = "The file is not image.";
        // }

        if (data.cp_attachment_type == 1 && this.state.bigSizeMultiple) {
            errors.cp_attachment_url =
                t("CREATE_NEW_CAMPAIGN.CNC_erro_not_be_than") + process.env.REACT_APP_MAX_IMAGE_SIZE + "MB.";
        }
        // if (!this.state.cp_id) {
        let cp_atts = data.cp_attachment_url;
        cp_atts = cp_atts.filter(e => e !== undefined);
        // if (cp_atts.length == 0) {
        //   errors.cp_attachment_url = "The campaign image field is required.";
        // }
        // }
        if (!this.state.data.cp_main_image) {
            switch (+this.state.data.cp_main_image_type) {
                case 1: {
                    errors.cp_main_image = t("CREATE_NEW_CAMPAIGN.CNC_erro_main_img");
                    break;
                }
                case 2: {
                    errors.cp_main_image = t("CREATE_NEW_CAMPAIGN.CNC_erro_youtube");
                    break;
                }
                case 3: {
                    errors.cp_main_image = t("CREATE_NEW_CAMPAIGN.CNC_erro_video");
                    break;
                }
            }
        }
        if (this.state.errors.cp_main_image) {
            errors.cp_main_image = this.state.errors.cp_main_image;
        }
        if (this.state.errors.cp_attachment_url) {
            errors.cp_attachment_url = this.state.errors.cp_attachment_url;
        }

        return errors;
    };

    handleRadioChange = e => {
        this.setState({
            form_attach: e.currentTarget.value,
            data: {
                ...this.state.data,
                cp_attachment_type: e.currentTarget.value
            },
            disabledUpdate: this.state.cp_id ? true : false
        });
    };
    handleRadioTypeChange = e => {
        this.setState({
            data: {
                ...this.state.data,
                cp_type: e.currentTarget.value
            },
            disabledUpdate: this.state.cp_id ? true : false
        });
    };
    handleRadioCategoryChange = e => {
        this.setState({
            data: {
                ...this.state.data,
                cp_code_group: e.target.value
            },
            disabledUpdate: this.state.cp_id ? true : false
        });
        console.log(this.state.data.cp_code_group);
    };

    submitUpdate = () => {
        this.setState(
            {
                loading: true
            },
            () => {
                this.props
                    .updateCampain(this.state.cp_id, this.state.dataUpdate)
                    .then(response => {
                        this.setState({
                            success: true,
                            loading: false
                        });
                        this.props.history.push('/partner/campaign/' + response.data.cp_slug)
                        this.props.reloadData();

                        // this.props.history.push("/partner/dashboard");
                    })
                    .catch(e => {
                        const errors = {
                            cp_name: "",
                            message: ""
                        };
                        if (e) {
                            if (e.data.errors) {
                                errors.cp_name = e.data.errors.cp_name
                                    ? e.data.errors.cp_name[0]
                                    : "";
                                errors.cp_main_image = e.data.errors.cp_main_image
                                    ? e.data.errors.cp_main_image[0]
                                    : "";
                            }
                            // errors.message = (e.data.message == false) ? 'Could not find this id' : ''
                        }
                        this.setState({
                            errors,
                            loading: false,
                            handleCloseUpdate: false
                        });
                    });
            }
        );
    };

    submitForm = e => {
        e.preventDefault();
        const {data, errors} = this.state;
        // return
        const validate = this.validateForm();
        if (
            validate.cp_description === "" &&
            validate.cp_brand === "" &&
            validate.cp_name === "" &&
            validate.cp_category === "" &&
            validate.cp_period_start === "" &&
            validate.cp_period_end === "" &&
            validate.keywords === "" &&
            validate.cp_product_price === "" &&
            validate.cp_campaign_price === "" &&
            validate.cp_image === "" &&
            // validate.cp_attachment_url === "" &&
            validate.number_fistar === "" &&
            validate.cp_main_image === "" &&
            validate.cp_total_free === "" &&
            validate.uids === ""
        ) {
            if (!this.state.loading) {
                this.setState(
                    {
                        loading: !this.state.cp_id ? true : false
                    },
                    () => {
                        let dataSubmit = "";
                        dataSubmit = {...data};
                        dataSubmit.cp_total_influencer = data.number_fistar;
                        // const test = data.cp_attachment_url.push(data.youtube_url)
                        const test = data.cp_attachment_url.concat([data.youtube_url]);
                        //  // let attachment_url = data.cp_attachment_url.filter(url => url.type)
                        //  let attachment_url = data.cp_attachment_url.map(url => {
                        //     if(url && url.type) {
                        //       return true
                        //     }
                        //  })
                        //   let dataFilter = attachment_url.filter(url =>url == true)
                        // data.cp_attachment_type = dataFilter ? data.cp_attachment_type : ""
                        if (data.youtube_url && data.cp_attachment_type == 2) {
                            if (
                                !this.state.cp_id ||
                                (this.state.cp_id &&
                                    data.youtube_url !==
                                    this.props.campaign.attachments[0].cp_attachment_url)
                            ) {
                                dataSubmit.cp_attachment_url = data.cp_attachment_url.concat([
                                    data.youtube_url
                                ]);
                            }
                        }
                        if (this.state.upvideo && data.cp_attachment_type == 3) {
                            dataSubmit.cp_attachment_url = data.cp_attachment_url.concat([
                                this.state.upvideo
                            ]);
                        }
                        if (this.state.cp_id) {
                            let is_update_file = false;
                            dataSubmit.cp_attachment_url = dataSubmit.cp_attachment_url
                                .map(att => {
                                    if (att && !att.cp_id) {
                                        return att;
                                    }
                                    return null;
                                })
                                .filter(e => e != null);
                            if (dataSubmit.cp_attachment_url.length == 0) {
                                dataSubmit.cp_attachment_url = "";
                            }
                            if (typeof dataSubmit.cp_image == "string") {
                                dataSubmit.cp_image = "";
                            }
                            if (
                                dataSubmit.cp_main_image_type != 2 &&
                                typeof dataSubmit.cp_main_image == "string"
                            ) {
                                dataSubmit.cp_main_image_type = "";
                            }
                        } else {
                            dataSubmit.cp_attachment_url = dataSubmit.cp_attachment_url.filter(
                                e => e != undefined
                            );
                        }
                        let formData = new FormData();
                        Object.keys(dataSubmit).map(key => {
                            if (
                                !!dataSubmit[key] ||
                                (key === "cp_campaign_price" && dataSubmit[key] !== null)
                            ) {
                                if (dataSubmit[key].constructor === Array) {
                                    dataSubmit[key].map(item => {
                                        formData.append(key + "[]", item);
                                    });
                                } else {
                                    formData.append(key, dataSubmit[key]);
                                }
                            } else if (dataSubmit[key] === 0) {
                                formData.append(key, dataSubmit[key]);
                            }
                        });
                        // let size = 0;
                        // var res = Array.from(formData.entries(), ([key, prop]) => {
                        //   size += typeof prop === "string"
                        //       ? prop.length
                        //       : prop.size
                        //   return (
                        //     {[key]: {
                        //       "ContentLength":
                        //       typeof prop === "string"
                        //       ? prop.length
                        //       : prop.size
                        //     }
                        //   })
                        // });

                        if (this.state.cp_id) {
                            this.setState({
                                showSubmitEdit: true,
                                dataUpdate: formData
                            });
                            return;
                        }

                        this.setState({
                            dataCreate: formData,
                            show: true
                        });
                    }
                );
            }
        } else {
            this.setState({
                errors: validate
            });
        }
    };

    handleVideoChange = e => {
        if (this.previewVideo.current) {
            this.previewVideo.current.pause();
        }
        let reader = new FileReader();
        let file = e.target.files[0];

        //if reading completed
        reader.onload = e => {
            let dataVideo = e.target.result;
            this.setState(
                {
                    imagePreviewUrlVideo: dataVideo,
                    // cp_attachment_url:file
                    upvideo: file
                },
                () => {
                    if (this.previewVideo.current) {
                        this.previewVideo.current.load();
                        this.previewVideo.current.play();
                    }
                }
            );
        };

        //read the file
        reader.readAsDataURL(file);
    };

    handleFileSelect = e => {
        e.preventDefault();
        this.inputImage.click();
    };

    onClickKeyword = id => {
        const {data} = this.state;
        const index = data.keywords.indexOf(id);
        if (index !== -1) {
            data.keywords.splice(index, 1);
            this.setState({
                data,
                disabledUpdate: this.state.cp_id ? true : false
            });
        } else {
            this.setState(prevState => ({
                data: {
                    ...prevState.data,
                    keywords: [...prevState.data.keywords, id]
                },
                errors: {
                    keywords: ""
                },

                disabledUpdate: this.state.cp_id ? true : false
            }));
        }
    };

    renderKeyword = () => {
        const {keywords, data, errors} = this.state;
        return (
            <div className="col-md-12 mt-4">
                {(keywords.code || []).map((keyword, key) => (
                    <a
                        className={data.keywords.includes(keyword.cd_id) ? "selected" : ""}
                        key={key}
                        onClick={() => this.onClickKeyword(keyword.cd_id)}
                    >
                        {keyword.cd_label}
                    </a>
                ))}
                {errors.keywords && <p className="text-danger">{errors.keywords}</p>}
            </div>
        );
    };
    renderCatagory = () => {
        const {catalogs, keywords, data, errors, fashion} = this.state;
        console.log(fashion);
        if(this.state.data.cp_code_group == '10'){
            return (
                <div className="col-sm-9 group-select select-arrow main-select d-block">
                    <select
                        className={`form-control form-control-sm select-catalog${
                            errors.catalogs ? " is-invalid" : ""
                            }`}
                        value={data.cp_category}
                        onChange={this.handleChangeInput}
                        name="cp_category"
                        id="cp_category"
                    >
                        <option/>
                        {(catalogs.code || []).map((cp_category, key) => (
                            <option value={cp_category.cd_id} key={key}>
                                {cp_category.cd_label}
                            </option>
                        ))}
                    </select>
                    {/*<i class="fa fa-chevron-down" onClick={this.handleChangeInput}></i>*/}
                    {errors.cp_category && (
                        <p className="text-danger">{errors.cp_category}</p>
                    )}
                </div>
            );
        } else if (this.state.data.cp_code_group == '74'){
            return (
                <div className="col-sm-9 group-select select-arrow main-select d-block">
                    <select
                        className={`form-control form-control-sm select-catalog${
                            errors.fashion ? " is-invalid" : ""
                            }`}
                        value={data.cp_category}
                        onChange={this.handleChangeInput}
                        name="cp_category"
                        id="cp_category"
                    >
                        <option/>
                        {(fashion.code || []).map((cp_category, key) => (
                            <option value={cp_category.cd_id} key={key}>
                                {cp_category.cd_label}
                            </option>
                        ))}
                    </select>
                    {/*<i class="fa fa-chevron-down" onClick={this.handleChangeInput}></i>*/}
                    {errors.cp_category && (
                        <p className="text-danger">{errors.cp_category}</p>
                    )}
                </div>
            );
        }
    };

    renderBrand = () => {
        const {brands, keywords, data, errors} = this.state;

        return (
            <div className="col-sm-9 group-select select-arrow main-select d-block">
                <select
                    className={`form-control form-control-sm select-catalog${
                        errors.cp_brand ? " is-invalid" : ""
                        }`}
                    value={data.cp_brand}
                    onChange={this.handleChangeInput}
                    name="cp_brand"
                    id="cp_brand"
                >
                    <option/>
                    {(brands || []).map((brand, key) => (
                        <option value={brand.CODE} key={key}>
                            {brand.CODE_NM}
                        </option>
                    ))}
                </select>
                {/*<i class="fa fa-chevron-down" onClick={this.handleChangeInput}></i>*/}
                {errors.cp_brand && (
                    <p className="text-danger">{errors.cp_brand}</p>
                )}
            </div>
        );


    };

    checkExistSNS = (fistar, channel) => {
        return (
            this.state.uidInfos.map(e => e.uid).includes(fistar.uid) &&
            this.state.uidInfos.map(e => e.sns_id).includes(channel.sns_id)
        );
    };

    resetUidFistars = () => {
        let {
            data: {uidFistars}
        } = this.state;
        let fistarCost = [];
        let fistars = uidFistars.map(fistar => {
            let channelsUsed = this.state.data.uidFistars.map(fistarUsed => {
                if (fistar.uid === fistarUsed.uid) {
                    return fistarUsed.channel
                        .map(channel => (channel.selected ? channel.sns_id : null))
                        .filter(el => el != null);
                }
            });
            channelsUsed = channelsUsed
                .map(e => (e ? e[0] : null))
                .filter(el => el != null);
            let channels = fistar.channel;
            channels = channels.map(channel => {
                let show = true;
                if (channelsUsed.includes(channel.sns_id)) {
                    show = false;
                }
                if (channel.selected) {
                    show = true;
                }
                return {
                    ...channel,
                    show
                };
            });
            fistar.channel = channels;
            return fistar;
        });
        let uid = fistars.map(fistar => {
            let uid = fistar.uid;
            let sns_id = "";
            fistar.channel.map(channel => {
                if (channel.selected) {
                    fistarCost = [...fistarCost, +channel.cost];
                    sns_id = channel.sns_id;
                }
            });
            return `${uid}-${sns_id}`;
        });
        this.setState({
            data: {
                ...this.state.data,
                uidFistars: fistars,
                uids: uid
            },
            fistarCost: fistarCost
        });
    };

    getFiStars = fistars => {
        if (fistars.length === 0) {
            return;
        }

        let fistarsSelected = fistars.map(fistar => {
            let channelsUsed = this.state.data.uidFistars.map(fistarUsed => {
                if (fistar.uid === fistarUsed.uid) {
                    return fistarUsed.channel
                        .map(channel => (channel.selected ? channel.sns_id : null))
                        .filter(el => el != null);
                }
            });
            channelsUsed = channelsUsed
                .map(e => (e ? e[0] : null))
                .filter(el => el != null);

            let channels = fistar.channel.map((channel, key) => {
                return {
                    ...channel,
                    selected: false,
                    show: true
                };
            });
            // set selected
            for (let i = 0; i < channels.length; i++) {
                if (!channelsUsed.includes(channels[i].sns_id)) {
                    channels[i].selected = true;
                    break;
                }
            }
            // if (channelsUsed.length === 0) {
            //     channels[0].selected = true
            // }

            if (channelsUsed.length === channels.length) {
                return null;
            }

            // return fistar
            fistar.channel = channels;
            return fistar;
        });

        fistarsSelected = fistarsSelected.filter(el => el != null);

        const uid = fistars.map(fistar => fistar.uid);
        const channel = fistars.map(fistar => fistar.channel[0]);
        const cost = channel.map(chan => +chan.cost);

        const sns = channel[0].sns.sns_name;

        const snschoose = "";

        const uidInfos = fistars.map(fistar => {
            let channels = fistar.channel;
            let sns = channels[channel.length - 1];
            for (let i = channels.length - 1; i >= 0; i--) {
                if (!this.checkExistSNS(fistar, channels[i])) {
                    sns = channels[i];
                }
            }
            return {
                uid: fistar.uid,
                sns_id: sns.sns_id,
                cost: sns.cost
            };
        });

        this.setState(
            {
                data: {
                    ...this.state.data,
                    uids: [...this.state.data.uids, ...uid],
                    uidFistars: [...this.state.data.uidFistars, ...fistarsSelected]
                },
                uidInfos: [...this.state.uidInfos, ...uidInfos],
                sns: {
                    ...this.state.sns,
                    sns
                },
                fistarCost: [...this.state.fistarCost, ...cost]
            },
            () => {
                this.resetUidFistars();
            }
        );
    };

    onChangeTextEdtitor = value => {
        this.setState({
            data: {
                ...this.state.data,
                cp_description: value.toString("html")
            },
            disabledUpdate: this.state.cp_id ? true : false
        });
    };

    __handleChangeSelectFistarSNS = (k, e) => {
        let select = +e.currentTarget.value.split("-")[1];
        let {data} = this.state;
        let {uidFistars} = data;
        let fistar = uidFistars[k];
        let channel = fistar.channel.map((channel, key) => {
            if (channel.selected) {
                return {
                    ...channel,
                    selected: false,
                    show: true
                };
            }
            if (channel.sns_id === select) {
                return {
                    ...channel,
                    selected: true,
                    show: true
                };
            }
            return channel;
        });
        fistar.channel = channel;
        uidFistars[k] = fistar;
        this.setState(
            {
                data: {
                    ...this.state.data,
                    uidFistars
                },
                disabledUpdate: true
            },
            () => {
                this.resetUidFistars();
            }
        );
    };

    _clickRemoveFistarSNS = (fistar, key) => {
        const {data} = this.state;
        let {uidFistars} = data;
        uidFistars[key] = null;
        uidFistars = uidFistars.filter(el => el != null);

        this.setState(
            {
                data: {
                    ...this.state.data,
                    uidFistars
                },
                disabledUpdate: this.state.cp_id ? true : false
            },
            () => {
                this.resetUidFistars();
            }
        );
    };
    __sum = data => {
        let total = 0;
        data.map((item, k) => {
            total += parseInt(item);
        });
        return total.toLocaleString();
    };
    renderFistar = () => {
        const {data, errors} = this.state;
        const {uidFistars} = this.state.data;

        const channel = uidFistars.map(e => e.channel);
        const sns = channel.map(e => e.map(item => item.cost));
        const uidGet = uidFistars.map(e => e.uid);
        return (
            <div className="">
                {uidFistars.map((uidFistar, key) => {
                    let selectedValue = uidFistar.uid;
                    let selectedPrice = 0;
                    uidFistar.channel.map(channel => {
                        if (channel.selected) {
                            selectedValue += "-" + channel.sns_id;
                            selectedPrice = +channel.cost;
                        }
                    });
                    return (
                        <div
                            className="form-group-wf row mb-4"
                            key={key}
                            data-content={key}
                        >
                            {/*{ let chanel = uidFistar.channel.map(chanel => chanel.sns)}*/}
                            <label
                                htmlFor="colFormLabelSm "
                                className="col-sm-3 col-form-label col-form-label-sm label-campaign nopadding "
                            >
                                <img
                                    src={
                                        uidFistar.avatar
                                            ? getImageLink(
                                            uidFistar.avatar,
                                            IMAGE_TYPE.FISTARS,
                                            IMAGE_SIZE.ORIGINAL
                                            )
                                            : "https://ppc.tools/wp-content/themes/ppctools/img/no-thumbnail.jpg"
                                    }
                                    className="img-fistar-search"
                                    alt=""
                                />
                            </label>

                            <div className="col-sm-9 fistar-number">
                                <div className="col-md-12 user-name">
                                    <span className="fas fa-mars"/>
                                    <h5>{uidFistar.name}</h5>
                                    <button
                                        type="button"
                                        className="close ml-3"
                                        aria-label="Close"
                                        onClick={() => this._clickRemoveFistarSNS(uidFistar, key)}
                                    >
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="col-md-12 group-form-username mt-3">
                                    <div className="input-username main-select">
                                        {
                                            //   key === 0 && !this.state.cp_id ? (
                                            //   <select
                                            //     className="form-control form-control-sm"
                                            //     id="sns_name"
                                            //     name="sns_name"
                                            //     value={selectedValue}
                                            //     readOnly
                                            //     disabled
                                            //   >
                                            //     {uidFistar.channel.map((chanel, key_1) => {
                                            //       return chanel.show ? (
                                            //         <option
                                            //           value={`${uidFistar.uid}-${chanel.sns_id}`}
                                            //           key={key_1}
                                            //         >
                                            //           {chanel.sns.sns_name} {chanel.show}{" "}
                                            //           {chanel.selected}
                                            //           {/*{chanel.selected ? "selected" : ""}*/}
                                            //         </option>
                                            //       ) : null;
                                            //     })}
                                            //   </select>
                                            // ) :
                                            (
                                                <select
                                                    className="form-control form-control-sm"
                                                    id="sns_name"
                                                    name="sns_name"
                                                    value={selectedValue}
                                                    onChange={e =>
                                                        this.__handleChangeSelectFistarSNS(key, e)
                                                    }
                                                >
                                                    {uidFistar.channel.map((chanel, key_1) => {
                                                        return chanel.show ? (
                                                            <option
                                                                value={`${uidFistar.uid}-${chanel.sns_id}`}
                                                                key={key_1}
                                                            >
                                                                {chanel.sns.sns_name} {chanel.show}{" "}
                                                                {chanel.selected}
                                                                {/*{chanel.selected ? "selected" : ""}*/}
                                                            </option>
                                                        ) : null;
                                                    })}
                                                </select>
                                            )}
                                    </div>
                                    <div className="input-username">
                                        <input
                                            type="text"
                                            className="wf-username"
                                            value={selectedPrice.toLocaleString()}
                                            readOnly
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        );
    };

    renderSearchFistar = () => {
        const {uidFistars} = this.state.data;
        const {numberFistar} = this.state.data.number_fistar;
        const itemSearch = [];
        let numberSearchChoose = "";

        if (this.state.data.uids.length > 0) {
            numberSearchChoose =
                this.state.data.number_fistar - this.state.data.uids.length > 0
                    ? this.state.data.number_fistar - this.state.data.uids.length
                    : 1;
        } else {
            numberSearchChoose = this.state.data.number_fistar;
        }
        for (var i = 1; i <= numberSearchChoose; i++) {
            itemSearch.push(i);
        }
        let itemSearchFistar = "";
        if (this.state.data.number_fistar) {
            return (
                <div className="">
                    {itemSearch.map((item, index) => (
                        <div className="form-group-wf row mt-4" key={index}>
                            <label
                                htmlFor="colFormLabelSm"
                                className="col-sm-3 col-form-label col-form-label-sm label-campaign label-btn nopadding mt-4"
                            >
                                <button
                                    className="btn btn-fistar-search"
                                    type={"button"}
                                    onClick={this.onSearchFiStar}
                                >
                                    <i className="fas fa-search"/>
                                    <p>{this.props.t("CREATE_NEW_CAMPAIGN.CNC_label_fistar_search")}</p>
                                </button>
                            </label>
                            <div className="col-sm-9 fistar-number ">
                                <div className="col-md-12 user-name">
                                    <p>
                                        {this.props.t("CREATE_NEW_CAMPAIGN.CNC_label_text_label")}
                                    </p>
                                </div>
                                <div className="col-md-12 group-form-username mt-3">
                                    <div className="input-username">
                                        <i className="fas fa-angle-down"/>
                                        <select className="form-control form-control-sm disabled-bg-customer"
                                                disabled={true}>
                                            <option>SNS</option>
                                        </select>
                                    </div>
                                    <div className="input-username">
                                        <input
                                            type="text"
                                            disabled={true}
                                            className="wf-price"
                                            placeholder="Posting Price"
                                        />
                                    </div>
                                </div>
                                {this.state.errors.uids && (
                                    <p className="text-danger">
                                        {this.state.errors.uids}
                                    </p>
                                )}
                            </div>
                        </div>
                    ))}
                </div>
            );
        }
    };

    renderSelectNumberFistar = () => {
        const {data, errors} = this.state;
        let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        return (
            <div className="group-select select-arrow main-select d-block">
                <select
                    className="form-control form-control-sm select-catalog"
                    value={data.number_fistar}
                    onChange={this.handleChangeInput}
                    name="number_fistar"
                    id="number_fistar"
                >
                    <option/>
                    {numbers.map((number, key) => (
                        <option value={number} key={number}>
                            {number}
                        </option>
                    ))}
                </select>
                {errors.number_fistar && (
                    <p className="text-danger">{errors.number_fistar}</p>
                )}
            </div>
        );
    };

    closeModal = () => {
        this.setState({
            isSearchFiStar: false
        });
    };

    handleClose = () => {
        this.setState({show: false, loading: false});
    };
    handleShow = () => {
        this.setState({show: true});
    };

    handleCloseUpdate = () => {
        this.setState({showSubmitEdit: false});
    };
    handleShowUpdate = () => {
        this.setState({showSubmitEdit: true});
    };
    logout = () => {
        this.props.logout();
    };
    loadDashboard = () => {
        // this.props.history.push("/partner/dashboard");
        const {dataCreate} = this.state;

        this.setState(
            {
                loadingCreate: true
            },
            () => {
                this.props
                    .createCampain(dataCreate)
                    .then(response => {
                        this.setState({
                            success: true,
                            loading: false,
                            show: true,
                            loadingCreate: false
                            //
                        });

                        this.props.history.push("/partner/dashboard");
                    })
                    .catch(e => {
                        const errors = {
                            cp_name: "",
                            cp_main_image: "",
                            cp_attachment_url: "",
                            message: ""
                        };

                        if (e.data.errors) {
                            errors.cp_name = e.data.errors.cp_name
                                ? e.data.errors.cp_name[0]
                                : "";
                            errors.cp_main_image = e.data.errors.cp_main_image
                                ? e.data.errors.cp_main_image[0]
                                : "";
                        }

                        this.setState(
                            {
                                errors,
                                loading: false,
                                loadingCreate: false,
                                show: false,
                                scrollTop: true
                            },
                            () => {
                            }
                        );
                        scrollToTopAnimated(700);
                    });
            }
        );
    };

    ClickDatePicker = (e, date) => {
        e.preventDefault();
    };
    submitFormNone = e => {
        e.preventDefault();
    };

    getDataImage = (data, errors) => {
        this.setState(
            {
                data: {
                    ...this.state.data,
                    ...data
                },
                errors: {
                    ...this.state.errors,
                    ...errors
                },
                disabledUpdate: this.state.cp_id ? true : false
            },
            () => {
            }
        );
    };

    onClickCancel = () => {
        if (this.state.cp_id) {
            this.props.handleClose();
            return;
        }
        this.setState({
            isRedirect: true
        });
    };

    OpenUploadImage = () => {
        this.triggerClickUploadImage.current.click();
    };

    render() {
        const {t} = this.props;
        let imagePreviewUrlRender;
        const {
            data,
            cp_id,
            errors,
            keywords,
            imagePreviewUrl,
            imagePreviewUrlVideo,
            imagePreviewUrlDescription,
            file,
            isSearchFiStar,
            form_attach,
            loading,
            cp_attachment_url,
            loadingCreate,
            disabledUpdate,
            isRedirect,
            loaddingStatus
        } = this.state;
        if (isRedirect) {
            return <Redirect to={routeName.PARTNER_DASHBOARD}/>;
        }
        if(loaddingStatus) {
            return <div id="loading" className="m-auto"></div>
        }
        imagePreviewUrlRender =
            this.state.imagePreviewUrl && this.state.imagePreviewUrl.length > 4
                ? this.state.imagePreviewUrl.splice(
                4,
                this.state.imagePreviewUrl.length - 4
                )
                : this.state.imagePreviewUrl;
        return (
            <Fragment>
                {/*{((!cp_id && !isSearchFiStar) || cp_id) && (*/}
                    <div
                    className="content"
                    style={{display: isSearchFiStar ? "none" : "block"}}
                    >
                    <div className="createnew-campaign">
                    <div className="container">
                    <div className="content-campaign">
                    <div className="row">
                    <div className="col-md-5">
                    <div className="left-campaign">
                    <div className="title mb-4 mt-5">
                    <h4>
                    {cp_id
                        ? "Update Campaign"
                        : t("DASHBOARD_FISTAR.DBF_button_create")}
                    </h4>
                    </div>

                    <MainImage
                    errors={errors}
                    cp_main_image={data.cp_main_image}
                    cp_main_image_type={data.cp_main_image_type}
                    cp_attachment_url={data.cp_attachment_url}
                    getData={this.getDataImage}
                    />
                    </div>
                    </div>
                    <div className="col-md-7">
                    <form
                    className="right-campaign"
                    onSubmit={this.submitFormNone}
                    >
                    <div className="title-right">
                    <span className="text-left">
                    {" "}
                    {t("CREATE_NEW_CAMPAIGN.CNC_basic_infor")}
                    </span>
                    <span className="text-right">
                    {t("CREATE_NEW_CAMPAIGN.CNC_requite")}
                    </span>
                    </div>
                    <div action="" className="campaign-form">
                    <div className="form-group-campaign group-campai row">
                    <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                    {t("CREATE_NEW_CAMPAIGN.CNC_label_campaign_name")}
                    <span>*</span>
                    </label>
                    <div className="col-sm-9 group-check d-block">
                    <input
                    type="text"
                    className="form-control form-control-sm input-campai"
                    id="cp_name"
                    name="cp_name"
                    onChange={this.handleChangeInput}
                    onFocus={this.onFocusInput}
                    value={data.cp_name}
                    placeholder={t(
                    "CREATE_NEW_CAMPAIGN.CNC_input_name"
                )}
                    />
                    {errors.cp_name && (
                        <p className="text-danger">{errors.cp_name}</p>
                    )}
                    </div>
                    </div>
                    <div className="form-group-campaign row">
                    <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                    {t("CREATE_NEW_CAMPAIGN.CNC_label_description")}
                    <span>*</span>
                    </label>
                    <div className="col-sm-9 group-select d-block edit-decription">
                    {this.state.isFinishInit && (
                        <EditorDescription
                            onChange={this.onChangeTextEdtitor}
                            onFocus={this.onFocusInput}
                            value={data.cp_description}
                        />
                    )}
                    {/*<textarea
                              rows="4"
                              id="cp_description"
                              name="cp_description"
                              placeholder={t(
                                "CREATE_NEW_CAMPAIGN.CNC_label_description"
                              )}
                              onChange={this.handleChangeInput}
                              onFocus={this.onFocusInput}
                              value={data.cp_description}
                              cols="50"
                            />*/}
                    {errors.cp_description && (
                        <p className="text-danger">
                            {errors.cp_description}
                        </p>
                    )}
                    </div>
                    </div>
                    <div className="form-group-campaign  group-campai row">
                    <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                    {t("CREATE_NEW_CAMPAIGN.CNC_category_group")}
                    <span>*</span>
                    </label>
                    <div className="col-sm-9 group-check">
                    <div className="col-md-12 gp-input-price">
                    <div className="group-check-price">
                    <div className="form-check-price">
                    <input
                    type="radio"
                    id="test6"
                    onChange={this.handleRadioCategoryChange}
                    checked={this.state.data.cp_code_group == '10'}
                    value="10"
                    />
                    <label htmlFor="test6">
                    {t(
                        "CREATE_NEW_CAMPAIGN.CNC_input_Beauty"
                    )}
                    </label>
                    </div>
                    </div>
                    <div className="group-check-price">
                    <div className="form-check-price">
                    <input
                    type="radio"
                    id="test7"
                    onChange={this.handleRadioCategoryChange}
                    checked={this.state.data.cp_code_group == '74'}
                    value="74"
                    />
                    <label htmlFor="test7">
                    {t(
                        "CREATE_NEW_CAMPAIGN.CNC_input_Fashion"
                    )}
                    </label>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    <div className="form-group-campaign row">
                    <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                    {t("CREATE_NEW_CAMPAIGN.CNC_catalog_group")}
                    <span>*</span>
                    </label>
                    {this.renderCatagory()}
                    </div>
                    <div className="form-group-campaign  group-campai row">
                    <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                    {t("CREATE_NEW_CAMPAIGN.CNC_label_brand")}
                    <span>*</span>
                    </label>
                    {this.renderBrand()}
                    </div>
                    <div className="form-group-campaign row">
                    <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                    {t("CREATE_NEW_CAMPAIGN.CNC_label_period")}
                    <span>*</span>
                    </label>
                    <div className="col-sm-9">
                    <div className="col-md-12 pd-0">
                    <p className="title-period">
                    {t("CREATE_NEW_CAMPAIGN.CNC_title_period")}
                    </p>
                    </div>
                    <div className="col-md-12 group-input-period">
                    <div className="input-calender-date customer-date">
                    {/*<i className="far fa-calendar-alt"></i>*/}
                    <div className="ererere">
                    <Flatpickr
                    data-enable-time
                    options={{
                      disableMobile: "true"
                    }}
                    value={data.cp_period_start}
                    onChange={date => {
                    this.handleChangeDate(date, null)
                }}
                    options={{
                    enableTime: true,
                    dateFormat: "Y-m-d H:i",
                    time_24hr: true,
                    static: true
                }}
                    className=""
                    />
                    </div>
                    <div className="ererere">
                    <Flatpickr
                    data-enable-time
                    options={{
                      disableMobile: "true"
                    }}
                    value={data.cp_period_end}
                    onChange={date => {
                    this.handleChangeDate(null, date)
                }}
                    options={{
                    enableTime: true,
                    dateFormat: "Y-m-d H:i",
                    time_24hr: true,
                    static: true
                }}
                    className=""
                    />
                    </div>
                    {/*<RangeDatePicker
                                  showDefaultIcon
                                  startDay={data.cp_period_start}
                                  endDay={data.cp_period_end}
                                  value={data.cp_period_start}
                                  startPlaceholder={data.cp_period_start}
                                  endPlaceholder={data.cp_period_end}
                                  onChange={this.handleChangeDate}
                                  disableDay={this.disableDay}
                                  onClick={this.ClickDatePicker}
                                />*/}
                    {/*{errors.cp_period_end && (*/}
                    {/*<p className="text-danger">*/}
                    {/*{errors.cp_period_end}*/}
                    {/*</p>*/}
                    {/*)}*/}
                    </div>
                    </div>
                    {errors.cp_period_start && (
                        <p className="text-danger">
                            {errors.cp_period_start}
                        </p>
                    )}
                    {errors.cp_period_end && (
                        <p className="text-danger">
                            {errors.cp_period_end}
                        </p>
                    )}
                    </div>
                    </div>
                    <div className="form-group-campaign  group-campai row">
                    <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                    {t("CREATE_NEW_CAMPAIGN.CNC_label_type")}
                    <span>*</span>
                    </label>
                    <div className="col-sm-9 group-check">
                    <div className="col-md-12 gp-input-price">
                    <div className="group-check-price">
                    <div className="form-check-price">
                    <input
                    type="radio"
                    id="test5"
                    name="radio-group"
                    onChange={this.handleRadioTypeChange}
                    checked={this.state.data.cp_type == 0}
                    value="0"
                    />
                    <label htmlFor="test5">
                    {t(
                        "CREATE_NEW_CAMPAIGN.CNC_input_type_free"
                    )}
                    </label>
                    </div>
                    </div>
                    <div className="group-check-price">
                    <div className="form-check-price">
                    <input
                    type="radio"
                    id="test4"
                    name="radio-group"
                    onChange={this.handleRadioTypeChange}
                    checked={this.state.data.cp_type == 1}
                    value="1"
                    />
                    <label htmlFor="test4">
                    {t(
                        "CREATE_NEW_CAMPAIGN.CNC_input_type_paid"
                    )}
                    </label>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    <div className="form-group-campaign  group-campai row">
                    <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                    {t("CREATE_NEW_CAMPAIGN.CNC_label_product_price")}
                    <span>*</span>
                    </label>
                    <div className="col-sm-9 group-check d-block">
                    <div className="col-md-12 group-input-period">
                    <div className="input-calender">
                    <input
                    type="text"
                    className="input-campai"
                    id="cp_product_price"
                    name="cp_product_price"
                    onChange={this.handleChangeInputNumberOnly}
                    onFocus={this.onFocusInput}
                    value={data.cp_product_price}
                    placeholder={t(
                    "CREATE_NEW_CAMPAIGN.CNC_input_product_price"
                )}
                    />
                    {errors.cp_product_price && (
                        <p className="text-danger">
                            {errors.cp_product_price}
                        </p>
                    )}
                    </div>
                    <div className="input-calender">
                    <input
                    type="text"
                    className="input-campai"
                    id="cp_campaign_price"
                    name="cp_campaign_price"
                    onChange={this.handleChangeInputNumberOnly}
                    onFocus={this.onFocusInput}
                    value={data.cp_campaign_price}
                    placeholder={t(
                    "CREATE_NEW_CAMPAIGN.CNC_input_campaign_price"
                )}
                    />
                    {errors.cp_campaign_price && (
                        <p className="text-danger">
                            {errors.cp_campaign_price}
                        </p>
                    )}
                    </div>
                    </div>
                    </div>
                    </div>


                    <div className="form-group-campaign row">
                    <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm label-campaign mt-4"
                    >
                    {t("CREATE_NEW_CAMPAIGN.CNC_label_keyword")}
                    <span>*</span>
                    </label>
                    <div className="col-sm-9 tag">
                    <div className="text-tag-campai col-md-12">
                    <p className="text-blue">
                    {t("CREATE_NEW_CAMPAIGN.CNC_title_keyword")}
                    </p>
                    <p>{t("CREATE_NEW_CAMPAIGN.CNC_text_keyword")}</p>
                    </div>
                    {this.renderKeyword()}
                    </div>
                    </div>
                    <div className="form-group-campaign row">
                    <label
                    htmlFor="colFormLabelSm"
                    onClick={this.OpenUploadImage}
                    className="col-sm-3 col-form-label label-campaign col-form-label-sm"
                    >
                    {t("CREATE_NEW_CAMPAIGN.CNC_image_description")}
                    </label>
                    <div className="col-sm-9 group-check">
                    <input
                    type="text"
                    className="form-control form-control-sm input-campai customer-upload-file-input d-none"
                    onClick={this.OpenUploadImage}
                    readOnly
                    placeholder={
                    data.cp_image
                        ? typeof data.cp_image === "string"
                        ? data.cp_image
                        : data.cp_image.name
                        : ""
                }
                    />
                    <div
                    className="customer-upload-content"
                    onClick={this.OpenUploadImage}
                    >
                    <span>
                    {data.cp_image
                        ? typeof data.cp_image === "string"
                            ? data.cp_image
                            : data.cp_image.name
                        : ""}
                    </span>
                    </div>

                    <input
                    className="fileInput form-control form-control-sm input-campai upload-image"
                    type="file"
                    accept="image/gif,image/jpeg,image/png"
                    ref={input => (this.inputImage = input)}
                    id="cp_image"
                    onFocus={this.onFocusInput}
                    // value={this.state.data.cp_image.name}
                    name="cp_image"
                    onChange={e =>
                    this.handleImageDescriptionChange(e)
                }
                    />
                    {errors.cp_image && (
                        <p className="text-danger text-image-description image-description-erro">
                            {errors.cp_image}
                        </p>
                    )}
                    <button
                    className="btn btn-selec-photo"
                    ref={this.triggerClickUploadImage}
                    onClick={this.handleFileSelect}
                    >
                    {t("CREATE_NEW_CAMPAIGN.CNC_button_select")}
                    </button>
                    </div>
                    </div>

                    <div className="form-group-campaign  group-campai row">
                    <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                    {t("CREATE_NEW_CAMPAIGN.CNC_label_try_tree")}
                    {data.cp_type == 0 ? <span className="abc"></span> : ""}
                    <span>*</span>
                    </label>
                    <div className="col-sm-9 group-check d-block">
                    <input
                    type="text"
                    id="cp_total_free"
                    name="cp_total_free"
                    onChange={this.handleChangeInputNumberOnly}
                    onFocus={this.onFocusInput}
                    value={data.cp_total_free}
                    className="form-control form-control-sm input-campai"
                    placeholder={t(
                    "CREATE_NEW_CAMPAIGN.CNC_input_try_tree"
                )}
                    />
                    {errors.cp_total_free && (
                        <p className="text-danger">
                            {errors.cp_total_free}
                        </p>
                    )}
                    </div>
                    </div>

                    </div>
                    <div className="col-md-12 fistar-infor-campaign">
                    <div className="title">
                    <h4>{t("CREATE_NEW_CAMPAIGN.CNC_fistar_infor")}</h4>
                    <span>
                    {t("CREATE_NEW_CAMPAIGN.CNC_text_fistar")}
                    </span>
                    </div>
                    <div action="" className="campaign-form">
                    <div className="form-group-campaign row">
                    <label
                    htmlFor="colFormLabelSm"
                    className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                    >
                    {t("CREATE_NEW_CAMPAIGN.CNC_label_number")}
                    </label>
                    <div className="col-sm-9 fistar-number">
                    {this.renderSelectNumberFistar()}
                    <div className="col-md-12 pd-fi-infor mt-3">
                    <p className="text-fistar-infor">
                    {t("CREATE_NEW_CAMPAIGN.CNC_text_number")}
                    </p>
                    </div>
                    </div>
                    </div>

                    {this.renderFistar()}

                    {this.renderSearchFistar()}

                    <div className="form-group-wf row mt-4">
                    <div className="col-sm-9 fistar-number offset-3">
                    <div className="total-price text-right">
                    <p className="text-right">
                    {t("CREATE_NEW_CAMPAIGN.CNC_text_money")}{" "}
                    <span>
                    {this.state.data.uidFistars
                        ? this.state.data.uidFistars.length
                        : 0}{" "}
                    fiStar
                    </span>
                    </p>
                    <h4 className="text-right">
                    {this.__sum(this.state.fistarCost)}{" "}
                    <span>VND</span>
                    </h4>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    <div className="row">
                    <div className="col-md-12">
                    <div
                    className={`button-group button-redirect${
                    cp_id ? " mb-0" : ""
                    }`}
                    >
                    <button type="button" onClick={this.onClickCancel}>
                    <Fragment>
                    {t("CREATE_NEW_CAMPAIGN.CNC_button_cancel")}
                    </Fragment>
                    </button>
                    {/*<NavLink to={routeName.PARTNER_DASHBOARD}>
                              {t("CREATE_NEW_CAMPAIGN.CNC_button_cancel")}
                            </NavLink>*/}
                    <button
                    type="submit"
                    disabled={cp_id && !disabledUpdate ? true : false}
                    value="SubmitForm"
                    onClick={this.submitForm}
                    className={
                    cp_id && !disabledUpdate ? "disabled-hover" : ""
                }
                    >
                    {loading ? (
                        <div className="spinner-border" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    ) : (
                        <Fragment>
                            {cp_id
                                ? t("CREATE_NEW_CAMPAIGN.CNC_button_save")
                                : t(
                                    "CREATE_NEW_CAMPAIGN.CNC_button_register"
                                )}
                        </Fragment>
                    )}
                    </button>
                    </div>
                    </div>
                    </div>
                    </form>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>

                    <section className={showBack ? "d-block" : "d-none"}>
                    <div
                    className="drop-up btn create-campaign-top"
                    onClick={this.scrollToTop}
                    duration={100000}
                    ref={this.triggerClick}
                    >
                    <i className="fas fa-long-arrow-alt-up" />
                    </div>
                    </section>

                    <Modal
                    size="lg"
                    show={this.state.show}
                    onHide={this.handleClose}
                    aria-labelledby="example-modal-sizes-title-lg"
                    dialogClassName="howto-register modal-dialog-centered"
                    >
                    <div className="register">
                    <div className="top modal-header">
                    <h4>{t("CREATE_NEW_CAMPAIGN.CNC_create")}</h4>
                    <button
                    type="button"
                    className="btn btn-close close"
                    data-dismiss="modal"
                    aria-label="Close"
                    onClick={this.handleClose}
                    >
                    <img src={closePopup} alt="" />
                    </button>
                    </div>
                    <div className="body-popup">
                    <p className="text-center">
                    {t("CREATE_NEW_CAMPAIGN.CNC_fistar_matching_change")}
                    </p>
                    <button
                    onClick={this.loadDashboard}
                    disabled={loadingCreate ? true : false}
                    >
                    {loadingCreate ? (
                        <div className="spinner-border" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    ) : (
                        t("CREATE_NEW_CAMPAIGN.CNC_button_ok")
                    )}
                    </button>
                    </div>
                    </div>
                    </Modal>

                    <Modal
                    size="lg"
                    show={this.state.showSubmitEdit}
                    onHide={this.handleCloseUpdate}
                    aria-labelledby="example-modal-sizes-title-lg"
                    dialogClassName="howto-register modal-dialog-centered"
                    >
                    <div className="register">
                    <div className="top modal-header">
                    <h4>{t("CREATE_NEW_CAMPAIGN.CNC_modify_label")}</h4>
                    <button
                    type="button"
                    className="btn btn-close close"
                    data-dismiss="modal"
                    aria-label="Close"
                    onClick={this.handleCloseUpdate}
                    >
                    <img src={closePopup} alt="" />
                    </button>
                    </div>
                    <div className="body-popup">
                    <p className="text-center">
                    {t("CREATE_NEW_CAMPAIGN.CNC_modify_campaign")}
                    </p>
                    <button onClick={this.submitUpdate}>{t("POPUP_CREATE_CAMPAGN.PCC_button_ok")}</button>
                    </div>
                    </div>
                    </Modal>
                    </div>
                {/*)}*/}

                {isSearchFiStar && (
                    <SearchFiStar
                        getFiStars={this.getFiStars}
                        closeModal={this.closeModal}
                        showSelectButton={true}
                    />
                )}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        uidFi: state.uidFi
    };
};

const mapDispatchToProps = dispatch => {
    return {
        createCampain: data => dispatch(createCampainActionPartner(data)),
        updateCampain: (cp_id, data) =>
            dispatch(updateCampainActionPartner(cp_id, data)),
        getCode: type => dispatch(getCodeAction(type)),
        clickSelectSNS: (m_id, sns_id) =>
            dispatch(clickSelectSNSAction(m_id, sns_id)),
        getBrand: () => dispatch(getBrand())
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withTranslation("translations")(withRouter(PartnerCreateCampain)));
