import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import Pagination from "react-js-pagination";
import * as routeName from "./../../../../routes/routeName";
import { getQAFistarAction } from "../../../../store/actions/auth";
import './QA.scss'

class PartnerQA extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      page: 1,
      type: 0,
      data: {
        isLoading: false
      }
    };
  }

  componentDidMount() {
    this.setState(
      {
        isLoading: true
      },
      () => {
        this.initData();
      }
    );
  }

  initData = () => {
    this.props
      .getQa(this.state.page, this.state.type, this.state.search)
      .then(response => {
        console.log(response, "data qa");
        this.setState({
          data: response,
          isLoading: false
        });
      })
      .catch(() => {
        this.setState({
          isLoading: false
        });
      });
  };

  renderAnswer = () => {
    let dataMap = {};
    dataMap = this.state.data.data;
    const { data } = this.state.data;
    const { t } = this.props;
    console.log(dataMap);

    console.log(data);
    if (dataMap) {
      console.log(dataMap);
      return (
        <tbody>
          {dataMap.map((data, key) => (
            <tr key={key}>
              <th scope="row" className="text-center order">
                {key +
                  1 +
                  (this.state.page - 1) * parseInt(this.state.data.per_page)}
              </th>
              <td className="text p-0">
                <Link
                  to={`/partner/qa/detail/${data.qa_id}`}
                  params={data.qa_id}
                >
                  {data.qa_title}
                </Link>
              </td>
              <td className="text-center">
                <button
                  className={
                    data.qa_state == 0
                      ? `${t("QA_DETAIL.QDL_button_preparing")}`
                      : `${t("QA_DETAIL.QDL_button_completed")}`
                  }
                >
                  {data.qa_state == 0
                    ? `${t("QA_DETAIL.QDL_button_preparing")}`
                    : `${t("QA_DETAIL.QDL_button_completed")}`}
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      );
    }
  };

  fetchMoreData = () => {
    if (this.state.page >= this.state.last_page) {
      this.setState({ hasMore: false });
      return;
    }
    console.log(this.state.page);
    this.props.getQa(this.state.page + 1, this.state.type).then(response => {
      this.setState({
        ...this.state.data,
        ...response,
        page: this.state.page + 1
      });
    });
    console.log(this.state.data);
  };

  handlePageChange = pageNumber => {
    console.log(`active page is ${pageNumber}`);
    this.setState({ page: pageNumber }, () => {
      this.initData();
    });
  };

  handleInputChange = e => {
    this.setState({ search: e.target.value });
  };

  submitSearch = e => {
    e.preventDefault()
    this.initData()
  }

  render() {
    const { t } = this.props;
    const { data, errors, success, isLoading } = this.state;
    if (isLoading) {
      return (
        <div className="campaign-track">
          <h1>{t("LOADING.LOADING")}</h1>
        </div>
      );
    }

    return (
      <div className="content campaign-tracking-campaign-01">
        <div className="container">
          <div className="top-partner-qa">
            <div className="left">
              <h4>
                {t("QA_DETAIL.QDL_qa")}
                <span>{t("QA_DETAIL.QDL_text_title")}</span>
              </h4>
            </div>
            <div className="right trwf-fistar d-flex">
              <form className="form-inline search-fistar-qa search-partner-qa my-2 my-lg-0" onSubmit={this.submitSearch}>
                <input
                  className="form-control mr-sm-2 input-search"
                  type="search"
                  aria-label="Search"
                  id="search"
                  name="search"
                  value={this.state.search}
                  onChange={this.handleInputChange}
                />
                <button
                  className="btn btn-outline-success my-2 my-sm-0"
                  type="submit"
                  onClick={this.submitSearch}
                >
                  <i className="fas fa-search"></i>
                </button>
              </form>
              <NavLink
                to={routeName.PARTNER_QA_ANSWER_QUESTION}
                className="contact-us contact-us-partner"
              >
                {t("CONTACT_US.CUS_contact_us")}
              </NavLink>
            </div>
          </div>
          <div className="table-partner-qa">
            <table className="table table-hover">
              <thead className="table-active">
                <tr>
                  <th scope="col" className="text-center key">
                    {t("QA_CONTACT_US.QCU_no")}
                  </th>
                  <th scope="col" className="text-center title">
                    {t("QA_CONTACT_US.QCU_title")}
                  </th>
                  <th scope="col" className="text-center status">
                    {t("QA_CONTACT_US.QCU_status")}
                  </th>
                </tr>
              </thead>

              {this.renderAnswer()}
            </table>
          </div>

          <nav aria-label="Page navigation example">
            {this.state.data.total > this.state.data.per_page ? (
              <Pagination
                activePage={this.state.page}
                itemsCountPerPage={this.state.data.per_page}
                totalItemsCount={this.state.data.total}
                pageRangeDisplayed={5}
                onChange={this.handlePageChange}
                itemClass={"page-item"}
                linkClass={"page-link"}
                firstPageText={"⟨⟨"}
                prevPageText={"⟨"}
                nextPageText={"⟩"}
                lastPageText={"⟩⟩"}
              />
            ) : (
              ""
            )}
          </nav>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    getQa: (page = 1, type = "", search = "") => dispatch(getQAFistarAction(page, type, search))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerQA));
