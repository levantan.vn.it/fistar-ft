import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { changePasswordAction } from "./../../../../store/actions/auth";
import { changePasswordActionPartner } from "../../../../store/actions/auth";
import * as routeName from "./../../../../routes/routeName";
class PartnerRegisterStep3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        password: "",
        password_confirmation: ""
      },
      errors: {
        password: "",
        password_confirmation: "",
        message: ""
      },
      success: false,
      loading: false,
      redirectFistar: false,
      redirectCampaign: false
    };
  }
  redirectFistar = () => {
    this.setState({
      redirectFistar: true
    });
  };
  redirectCampaign = () => {
    this.setState({
      redirectCampaign: true
    });
  };

  render() {
    const { t } = this.props;
    const {
      data,
      errors,
      success,
      loading,
      redirectFistar,
      redirectCampaign
    } = this.state;
    if (redirectFistar) {
      return <Redirect to={routeName.FISTAR_FRONT} />;
    }
    if (redirectCampaign) {
      return <Redirect to={routeName.CAMPAIGN} />;
    }

    return (
      <div className="col-md-12 content-step-form">
        <div className="content-step">
          <div className="item-circle ">
            <label>{t("PARTNER_JOIN.PJN_maneger")}</label>
          </div>
          <span />
          <div className="item-circle">
            <label>{t("PARTNER_JOIN.PJN_company")}</label>
          </div>
          <span />
          <div className="item-circle active">
            <label>{t("PARTNER_JOIN.PJN_complete")}</label>
          </div>
        </div>
        <div className="notification">
          <div className="row content-text">
            <div className="col-md-12 thanks-for">
              <h2 className="text-center">{t("PARTNER_JOIN.PJN_thanks")}</h2>
              <p className="mt-2 text-center">
                {t("PARTNER_JOIN.PJN_text_thanks")}
              </p>
            </div>
            <div className="time-approvals col-md-12">
              <p>
                {t("PARTNER_JOIN.PJN_text_notical_text1")}{" "}
                <a href="javascript:void(0)" className="disabled-hover">
                  {t("PARTNER_JOIN.PJN_text_notical_text2")}
                </a>{" "}
                {t("PARTNER_JOIN.PJN_text_notical_text3")}
              </p>
              <p>{t("PARTNER_JOIN.PJN_text_notical")}</p>
            </div>
            <div className="col-md-12 main-redirect">
              <div className="button-group-thanks">
                {/*<NavLink className="text-center" to={routeName.PARTNER_LOGIN} >About Partner</NavLink>*/}
                <button onClick={this.redirectFistar}>
                  {t("PARTNER_JOIN.PJN_button_about_fistar")}
                </button>
                <button onClick={this.redirectCampaign}>
                  {t("PARTNER_JOIN.PJN_button_about_campaign")}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    changePassword: data => dispatch(changePasswordActionPartner(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerRegisterStep3));
