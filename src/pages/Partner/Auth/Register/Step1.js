import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  registerActionPartner,
  checkExistActionPartner
} from "../../../../store/actions/auth";
// import './register.scss'
import { VALIDATION } from './../../../../constants/index'

class PartnerRegisterStep1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        pm_name: "",
        pm_phone: "",
        email: "",
        pm_id: "",
        password: "",
        password_confirmation: ""
      },
      errors: {
        pm_name: "",
        pm_phone: "",
        email: "",
        pm_id: "",
        password: "",
        password_confirmation: "",
        message: ""
      },
      show_pass: false,
      show_pass_confirm: false,
      existID: 0,
      existIDPm: 0,
      validateEmail: false,
      success: false,
      loading: false
    };
  }

  handleChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }));
  };

  componentDidMount() {
    if (this.props && this.props.onData) {
      this.setState({
        data: this.props.onData
      });
    }

    // window.addEventListener("beforeunload", this.handleLeavePage);
  }

  handleLeavePage(e) {
    const confirmationMessage = "load page";
    e.returnValue = confirmationMessage;
    return confirmationMessage;
  }

  onFocusInput = e => {
    this.setState(prevState => ({
      errors: {
        pm_name: "",
        pm_phone: "",
        email: "",
        pm_id: "",
        password: "",
        password_confirmation: "",
        message: ""
      },
      existID: 0,
      existIDPm: 0
    }));
  };


    handleChangeInputConfirmPW = e => {
        const { password } = this.state.data;
        const name = e.target.name;
        const value = e.target.value;
        if(password && e.target.value !== password) {
            this.setState(prevState => ({
                data: {
                    ...prevState.data,
                    [name]: value
                },
                errors: {
                    password_confirmation: this.props.t("PARTNER_JOIN.PJN_erro_pw_confirm")
                }
            }));
        }else {

            this.setState(prevState => ({
                data: {
                    ...prevState.data,
                    [name]: value
                },

                errors: {
                    email: "",
                    password: "",
                    password_confirmation: "",
                    message: ""
                },
            }));
        }

    };

  handleChangeInputNumberOnly = e => {
    const name = e.target.name;
    const value = e.target.value;
      var reg = /^-?\d+\.?\d*$/
    console.log(value)
    console.log(this.state)
      if(!this.state.data.pm_phone || this.state.data.pm_phone == "") {
          console.log(value)
          this.setState(prevState => ({
              data: {
                  ...prevState.data,
                  [name]: ""
              }
          }));
      }
    if (/^\d*$/.test(value)) {
    // if (reg.test(value)) {
        console.log(value)
      this.setState(prevState => ({
        data: {
          ...prevState.data,
          [name]: value
        }
      }));
    }
  };

  validateEmail = email => {
    // var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // var re = /^[a-zA-Z0-9\.]{2,}@[a-zA-Z0-9]{2}(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])?)(?=.*[\.])*$/;
    var re = /([a-z]+[a-z0-9]*[_\.]?[a-z0-9]+)@(([a-z0-9]{2,}\.)*[a-z0-9]{2,}\.)+[a-z]{2,}/;
    return re.test(String(email).toLowerCase());
  };

    validateEmailCustomer = email => {
        var re = /^[a-zA-Z0-9]{2,}@[a-zA-Z0-9]{2,}.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])*$/;
        return re.test(String(email).toLowerCase());
    };

  checkExistID = e => {
    e.preventDefault();
    const { data } = this.state;
    if (!this.validateEmail(data.email)) {
      this.setState(prevState => ({
        existID: 1,
        errors: {
          ...prevState.errors,
          email: `${this.props.t("PARTNER_JOIN.PJN_email")}`
        }
      }));
    }
    else {
      this.props
        .checkExist("email", data.email)
        .then(res => {
          this.setState(prevState => ({
            existID: 1,
            errors: {
              ...prevState.errors,
              email: `${this.props.t("PARTNER_JOIN.PJN_email_existed")}`
            }
          }));
        })
        .catch(() => {
          this.setState(prevState => ({
            existID: 2
          }));
        });
    }
  };

  checkExistIDPm = e => {
    e.preventDefault();
    const { data } = this.state;
    if (!data.pm_id) {
      this.setState(prevState => ({
        existIDPm: 1,
        errors: {
          ...prevState.errors,
          pm_id: `${this.props.t("FISTAR_JOIN.FJN_erro_id")}`
        }
      }));
    } else {
      this.props
        .checkExist("pm_id", data.pm_id)
        .then(res => {
          this.setState(prevState => ({
            existIDPm: 1,
            errors: {
              ...prevState.errors,
              pm_id: "pm_id is exist"
            }
          }));
        })
        .catch(() => {
          this.setState(prevState => ({
            existIDPm: 2
          }));
        });
    }
  };

  validateForm = () => {
    const errors = {
      pm_name: "",
      pm_phone: "",
      email: "",
      pm_id: "",
      password: "",
      password_confirmation: "",
      message: ""
    };
    const { data, validateEmail } = this.state;
    const { t } = this.props;
    if (!data.pm_name) {
      errors.pm_name = `${t("PARTNER_JOIN.PJN_ero_name")}`;
    } else if (data.pm_name && data.pm_name.length > VALIDATION.MAX_NAME_LENGTH) {
      errors.pm_name = `${t("PARTNER_JOIN.PJN_ero_name_limit")}`;
    } else if (data.pm_name.length < 6) {
      errors.pm_name = `${t("PARTNER_JOIN.PJN_ero_name_limit_6")}`;
    }else if (data.pm_name && /[\!\@\#\$\%\{\}\^\&\*\(\)\<\>\_\-\=\+\/\.\,\`\~\"\?\[\]\|]/.test(data.pm_name)
    ) {
        errors.pm_name = t("PARTNER_JOIN.PJN_erro_name_symbol_special");
    }

    // if (!data.pm_id) {
    //   errors.pm_id = "The id field is required.";
    // }
    if (!data.pm_phone) {
      errors.pm_phone = `${t("PARTNER_JOIN.PJN_erro_phone")}`;
    } else if (isNaN(data.pm_phone) == true) {
      errors.pm_phone = `${t("PARTNER_JOIN.PJN_erro_phone_is_number")}`;
    } else if (data.pm_phone.length < 9) {
      errors.pm_phone = `${t("PARTNER_JOIN.PJN_erro_phone_limit_9")}`;
    } else if (data.pm_phone.length > 15) {
      errors.pm_phone = `${t("PARTNER_JOIN.PJN_erro_phone_limit_15")}`;
    }
    if (!data.password) {
      errors.password = `${t("PARTNER_JOIN.PJN_erro_password")}`;
    } else if (data.password.length < 6) {
      errors.password = `${t("PARTNER_JOIN.PJN_erro_password_limit_6")}`;
    }
    if (!data.password_confirmation) {
      errors.password_confirmation = `${t(
        "PARTNER_RESETPW.PRW_erro_forget_new_pw"
      )}`;
    } else if (data.password_confirmation.length < 6) {
      errors.password_confirmation = `${t(
        "PARTNER_JOIN.PJN_erro_confirm_password_limit_6"
      )}`;
    } else if (data.password_confirmation && data.password_confirmation !== data.password) {
      errors.password_confirmation = `${t(
        "PARTNER_JOIN.PJN_erro_pw_confirm"
      )}`;
    }
    if (!data.email) {
      errors.email = `${t("PARTNER_JOIN.PJN_erro_email")}`;
    } else if (!this.validateEmail(data.email)) {
      errors.email = `${t("PARTNER_JOIN.PJN_email")}`;
    }

    return errors;
  };

  onNextStep = e => {
    e.preventDefault();
    const { data, errors } = this.state;
    const validate = this.validateForm();
    data.pm_id = data.pm_name && data.pm_name.split(" ").join("_");
    if (
      !validate.pm_name &&
      !validate.pm_phone &&
      !validate.password &&
      !validate.email &&
      !validate.password_confirmation
    ) {
      if (!this.state.loading) {
        this.setState(
          {
            loading: true
          },
          () => {
            this.props
              .checkExist("email", data.email)
              .then(res => {
                this.setState(prevState => ({
                  existID: 1,
                  errors: {
                    ...prevState.errors,
                    email: `${this.props.t("PARTNER_JOIN.PJN_email_existed")}`
                  },
                  loading: false
                }));
              })
              .catch(() => {
                this.props.onNextStep(2, data);
                this.setState(prevState => ({
                  existID: 2,
                  loading: false
                }));
              });
          }
        );
      }
    } else {
      this.setState(
        {
          errors: validate
        },
        () => {}
      );
    }
  };

  showPassword = () => {
    this.setState({
      show_pass: !this.state.show_pass
    });
  };

  showPasswordConfirm = () => {
    this.setState({
      show_pass_confirm: !this.state.show_pass_confirm
    });
  };

  validateNumber(event) {
    const { data } = this.state;

    var invalidChars = ["-", "+", "e", "."];

    if (invalidChars.includes(event.key)) {
      event.preventDefault();
    }
  }

  render() {
      console.log(this.state)
    const { t } = this.props;
    const { data, errors, existID, existIDPm, success, loading } = this.state;
    return (
      <div className="col-md-12 content-step-form">
        <div className="content-step">
          <div className="item-circle active">
            <label>{t("PARTNER_JOIN.PJN_maneger")}</label>
          </div>
          <span />
          <div className="item-circle">
            <label>{t("PARTNER_JOIN.PJN_company")}</label>
          </div>
          <span />
          <div className="item-circle">
            <label>{t("PARTNER_JOIN.PJN_complete")}</label>
          </div>
        </div>
        <form className="form-register step1" onSubmit={this.onNextStep}>
          <div className="text-require">
            <span>{t("PARTNER_JOIN.PJN_please_input_partner")}</span>
            <label>
              <i className="fa fa-star" />
              {t("PARTNER_JOIN.PJN_required")}
            </label>
          </div>
          <div className="center-form ">


            <div
              className={`input-group row${
                errors.pm_name ? " error-message" : ""
              }`}
            >
              <div className="col-md-2 item-text">
                <label>
                  {t("PARTNER_JOIN.PJN_label_name")} <strong>*</strong>
                </label>
              </div>
              <div className="col-md-10 item-form">
                <input
                  placeholder={t("PARTNER_JOIN.PJN_label_name")}
                  type="text"
                  name="pm_name"
                  id="pm_name"
                  value={data.pm_name}
                  className={`form-control${
                    errors.pm_name ? " is-invalid" : ""
                  }`}
                  onChange={this.handleChangeInput}
                  onFocus={this.onFocusInput}
                />
                {errors.pm_name && (
                  <div className="tooptip-text">
                    <p>{errors.pm_name}</p>
                  </div>
                )}
              </div>
            </div>
            <div
              className={`input-group row${
                errors.pm_phone ? " error-message" : ""
              }`}
            >
              <div className="col-md-2 item-text">
                <label>
                  {t("PARTNER_JOIN.PJN_label_contact")} <strong>*</strong>
                </label>
              </div>
              <div className="col-md-10 item-form">
                <input
                  placeholder={t("PARTNER_JOIN.PJN_input_contact")}
                  type="text"
                  name="pm_phone"
                  id="pm_phone"
                  value={data.pm_phone}
                  className={`form-control${
                    errors.pm_phone ? " is-invalid" : ""
                  }`}
                  onChange={this.handleChangeInputNumberOnly}
                  onFocus={this.onFocusInput}
                />
                {errors.pm_phone && (
                  <div className="tooptip-text">
                    <p>{errors.pm_phone}</p>
                  </div>
                )}
              </div>
            </div>
            <div
              className={`input-group row${
                errors.email ? " error-message" : ""
              }`}
            >
              <div className="col-md-2 item-text">
                <label>
                  {t("PARTNER_JOIN.PJN_label_email")} <strong>*</strong>
                </label>
              </div>
              <div className="col-md-7 item-form-center">
                <input
                  placeholder={t("PARTNER_JOIN.PJN_label_email")}
                  type="email"
                  name="email"
                  id="email"
                  autoComplete="off"
                  value={data.email}
                  className={`form-control${errors.email ? " is-invalid" : ""}${
                    existID == 2 ? " is-valid" : ""
                  }`}
                  onChange={this.handleChangeInput}
                  onFocus={this.onFocusInput}
                />
                {errors.email && (
                  <div className="tooptip-text">
                    <p>{errors.email}</p>
                  </div>
                )}
              </div>
              <div className="col-md-3 item-form">
                <button className="btn check-email" onClick={this.checkExistID}>
                  {t("PARTNER_JOIN.PJN_button_check_mail")}
                </button>
                {errors.email && (
                  <div className="tooptip-text">
                    <p className="opacity0">{errors.email}</p>
                  </div>
                )}
              </div>
            </div>
            <div
              className={`input-group row${
                errors.password ? " error-message" : ""
              }`}
            >
              <div className="col-md-2 item-text">
                <label>
                  {t("PARTNER_JOIN.PJN_label_password")} <strong>*</strong>
                </label>
              </div>
              <div className="col-md-10 item-form">
                <i
                  className={
                    this.state.show_pass
                      ? "fa fa-eye show-pass"
                      : "fa fa-eye-slash show-pass"
                  }
                  onClick={this.showPassword}
                />
                <input
                  placeholder={t("PARTNER_JOIN.PJN_label_password")}
                  type={this.state.show_pass ? "text" : "password"}
                  name="password"
                  id="password"
                  value={data.password}
                  className={`form-control${
                    errors.password ? " is-invalid" : ""
                  }`}
                  onChange={this.handleChangeInput}
                  onFocus={this.onFocusInput}
                />
                {errors.password && (
                  <div className="tooptip-text">
                    <p>{errors.password}</p>
                  </div>
                )}
              </div>
            </div>
            <div
              className={`input-group row${
                errors.password_confirmation ? " error-message" : ""
              }`}
            >
              <div className="col-md-2 item-text">
                <label>
                  {t("PARTNER_JOIN.PJN_label_confirm_password")}{" "}
                  <strong>*</strong>
                </label>
              </div>
              <div className="col-md-10 item-form">
                <i
                  className={
                    this.state.show_pass_confirm
                      ? "fa fa-eye show-pass"
                      : "fa fa-eye-slash show-pass"
                  }
                  onClick={this.showPasswordConfirm}
                />
                <input
                  placeholder={t("PARTNER_JOIN.PJN_label_confirm_password")}
                  type={this.state.show_pass_confirm ? "text" : "password"}
                  name="password_confirmation"
                  id="password_confirmation"
                  value={data.password_confirmation}
                  className={`form-control${
                    errors.password_confirmation ? " is-invalid" : ""
                  }`}
                  onChange={this.handleChangeInputConfirmPW}
                  onFocus={this.onFocusInput}
                />
                {errors.password_confirmation && (
                  <div className="tooptip-text">
                    <p>{errors.password_confirmation}</p>
                  </div>
                )}
              </div>
            </div>
            <div className="input-group row content-button">
              <div className="col-md-2 item-text" />
              <div className="col-md-10 item-button">
                <div className="row item-row">
                  <div className="col-md-6 item-prev">
                    <button className="prev" type="button" disabled={true}>
                      {t("PARTNER_JOIN.PJN_button_previous")}
                    </button>
                  </div>
                  <div className="col-md-6 item-next">
                    <button className="next" onClick={this.onNextStep}>
                      {loading ? (
                        <div className="spinner-border" role="status">
                          <span className="sr-only">
                            {t("LOADING.LOADING")}
                          </span>
                        </div>
                      ) : (
                        `${t("PARTNER_RESETPW.PRW_button_next")}`
                      )}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    checkExist: (name, value) => dispatch(checkExistActionPartner(name, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerRegisterStep1));
