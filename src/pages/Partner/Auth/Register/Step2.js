import React, {Component, Fragment} from "react";
import {withTranslation, Trans} from "react-i18next";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {registerActionPartner, checkSnsVerify, getBrand} from "../../../../store/actions/auth";
import {getCodeAction} from "../../../../store/actions/code";
import "./register.scss";
import {VALIDATION} from './../../../../constants/index'
import api from "../../../../api";

class PartnerRegisterStep2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                pc_brand:
                    this.props.onData && this.props.onData.pc_brand
                        ? this.props.onData.pc_brand
                        : "",
                keyword:
                    this.props.onData && this.props.onData.keyword
                        ? this.props.onData.keyword
                        : [],
                // channel:[],
                pc_tob:
                    this.props.onData && this.props.onData.pc_tob
                        ? this.props.onData.pc_tob
                        : "",
                pc_phone:
                    this.props.onData && this.props.onData.pc_phone
                        ? this.props.onData.pc_phone
                        : "",
                pc_name:
                    this.props.onData && this.props.onData.pc_name
                        ? this.props.onData.pc_name
                        : "",
                youtube:
                    this.props.onData && this.props.onData.youtube
                        ? this.props.onData.youtube
                        : "",
                instagram:
                    this.props.onData && this.props.onData.instagram
                        ? this.props.onData.instagram
                        : "",
                facebook:
                    this.props.onData && this.props.onData.facebook
                        ? this.props.onData.facebook
                        : "",
                pc_tob_test:
                    this.props.onData && this.props.onData.pc_tob_test
                        ? this.props.onData.pc_tob_test
                        : "",
                pc_address:
                    this.props.onData && this.props.onData.pc_address
                        ? this.props.onData.pc_address
                        : "",
                pc_introduction:
                    this.props.onData && this.props.onData.pc_introduction
                        ? this.props.onData.pc_introduction
                        : ""
            },
            verifySnsInstagram: false,
            verifySnsYoutube: false,
            verifySnsFacebook: false,
            validSnsInstagram: false,
            validSnsYoutube: false,
            validSnsFacebook: false,
            brands: [],
            errors: {
                pc_name: "",
                keyword: [],
                // channel:[],
                pc_tob: "",
                pc_name: "",
                pc_phone: "",
                youtube: "",
                instagram: "",
                pc_tob_test: "",
                facebook: "",
                agree_term: false,
                agree_privacy: false,
                pc_address: "",
                pc_introduction: "",
                verifySnsInstagram: "",
                verifySnsYoutube: "",
                verifySnsFacebook: "",
                message: ""
            },
            success: false,
            loadingStatus: false,
            agree_term: false,
            agree_privacy: false,
            agree_all: false,
            keywords: {}
            // channels: [],
        };
    }

    componentDidMount() {
        this.props.getCode("keyword,partner_type");
        this.props.getBrand().then(response => {
            console.log(response)
            this.setState({
                brands: response
            });
        });
    }

    handleCheckboxAll = e => {
        const name = e.target.name;
        const checked = e.target.checked;

        if (checked && checked == true) {
            this.setState(prevState => ({
                agree_privacy: checked,
                agree_term: checked,
                agree_all: checked
            }));
        } else {
            this.setState(prevState => ({
                agree_privacy: false,
                agree_term: false,
                agree_all: false,
                errors: {
                    agree_privacy: false,
                    agree_term: false,
                    agree_all: false
                }
            }));
        }
    };
    handleCheckboxChangePrivacy = e => {
        const name = e.target.name;
        const checked = e.target.checked;

        if (checked && checked == true) {
            this.setState(prevState => ({
                ...prevState,
                agree_privacy: checked
            }));
            if (this.state.agree_term) {
                this.setState(prevState => ({
                    agree_all: checked
                }));
            }
        } else {
            this.setState(prevState => ({
                agree_privacy: false,
                agree_all: false,
                errors: {
                    agree_privacy: false,
                    agree_all: false
                }
            }));
        }
    };
    handleCheckboxChangeTerm = e => {
        const name = e.target.name;
        const checked = e.target.checked;

        if (checked && checked == true) {
            this.setState(prevState => ({
                ...prevState,
                agree_term: checked
            }));
            if (this.state.agree_privacy) {
                this.setState(prevState => ({
                    agree_all: checked
                }));
            }
        } else {
            this.setState(prevState => ({
                agree_term: false,
                agree_all: false,
                errors: {
                    agree_term: false,
                    agree_all: false
                }
            }));
        }
        if (this.state.agree_privacy && this.state.agree_term) {
            this.setState(prevState => ({
                agree_all: checked
            }));
        }
    };

    handleChangeInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState(prevState => ({
            data: {
                ...prevState.data,
                [name]: value
            }
        }));
    };


    // handleChangeInputSns = (e, type) => {
    //     console.log(e.currentTarget.name)
    //     console.log(type)
    //     console.log(e.currentTarget.value)
    //     if(e.currentTarget.value) {
    //         var snsValid = this.validUrl(e.currentTarget.value, type);
    //     }else {
    //         snsValid=true
    //     }
    //
    //     switch (e.currentTarget.name) {
    //         case "facebook":
    //             var facebook = e.currentTarget.value;
    //
    //
    //             console.log(snsValid);
    //             console.log(facebook);
    //             if(snsValid == true) {
    //
    //                 this.setState(prevState => ({
    //                     data: {
    //                         ...prevState.data,
    //                         facebook: facebook
    //                     },
    //                     onChange: true,
    //                     validSnsFacebook:false
    //                 }));
    //                 var data = {};
    //                 data.sns_id = 2;
    //                 data.url = facebook
    //
    //                 if(facebook) {
    //
    //                     this.props
    //                         .verifySns(data)
    //                         .then(response => {
    //                             console.log(response)
    //
    //
    //                             if (response.data == false) {
    //                                 this.setState(prevState => ({
    //                                     verifySnsFacebook: true,
    //                                     errors: {
    //                                         verifySnsFacebook: "Url Facebook is exist"
    //                                     },
    //
    //                                 }))
    //                             }
    //                             if(response.data == true) {
    //
    //                                 this.setState(prevState => ({
    //                                     verifySnsFacebook: false,
    //                                     errors: {
    //                                         verifySnsFacebook: ""
    //                                     },
    //
    //                                 }))
    //
    //                             }
    //
    //                         }).catch(e => {
    //                         console.log(e)
    //                     });
    //
    //                 }else {
    //
    //                     this.setState(prevState => ({
    //                         verifySnsFacebook: false,
    //                     }))
    //
    //                 }
    //
    //             }else {
    //                 console.log('zozozozoz else')
    //                 this.setState(prevState => ({
    //                     validSnsFacebook: true,
    //                     data: {
    //                         ...prevState.data,
    //                         facebook: facebook
    //                     },
    //                     errors: {
    //                         verifySnsFacebook: "Url Facebook is invalid"
    //                     }
    //                 }))
    //
    //             }
    //
    //             break;
    //         case "youtube":
    //             var youtube = e.currentTarget.value;
    //
    //             console.log(snsValid);
    //             if(snsValid == true) {
    //                 this.setState(prevState => ({
    //                     data: {
    //                         ...prevState.data,
    //                         youtube: youtube
    //                     },
    //                     onChange: true,
    //                     validSnsYoutube: false
    //                 }));
    //
    //                 var data = {};
    //                 data.sns_id = 3;
    //                 data.url = youtube
    //                 if(youtube) {
    //                     this.props
    //                         .verifySns(data)
    //                         .then(response => {
    //                             console.log(response)
    //                             console.log('zozozo if')
    //                             if (response.data == false) {
    //                                 this.setState(prevState => ({
    //                                     verifySnsYoutube: true,
    //
    //                                     errors: {
    //                                         verifySnsYoutube: "Url Youtube is exist"
    //                                     }
    //                                 }))
    //                             }
    //                             if (response.data == true) {
    //                                 this.setState(prevState => ({
    //                                     verifySnsYoutube: false,
    //
    //                                     errors: {
    //                                         verifySnsYoutube: ""
    //                                     }
    //                                 }))
    //                             }
    //
    //                         }).catch(e => {
    //                         console.log(e)
    //                     });
    //                 }else {
    //
    //                     this.setState(prevState => ({
    //                         verifySnsYoutube: false,
    //                     }))
    //
    //                 }
    //
    //
    //
    //             }else {
    //                 console.log('zozozozoz else')
    //                 this.setState(prevState => ({
    //                     validSnsYoutube: true,
    //                     data: {
    //                         ...prevState.data,
    //                         youtube:youtube
    //                     },
    //                     errors: {
    //                         verifySnsYoutube: "Url Youtube is invalid"
    //                     }
    //                 }))
    //
    //             }
    //
    //
    //
    //             break;
    //         case "instagram":
    //             var instagram = e.currentTarget.value;
    //
    //             if(snsValid == true) {
    //
    //                 this.setState(prevState => ({
    //                     data: {
    //                         ...prevState.data,
    //                         instagram: {
    //                             value: instagram
    //                         }
    //                     },
    //                     validSnsInstagram: false,
    //                     onChange: true
    //                 }));
    //
    //                 var data = {};
    //                 data.sns_id = 4;
    //                 data.url = instagram
    //                 if(instagram) {
    //                     this.props
    //                         .verifySns(data)
    //                         .then(response => {
    //                             console.log(response)
    //
    //                             if (response.data == false) {
    //                                 this.setState(prevState => ({
    //                                     verifySnsInstagram: true,
    //                                     onChange: false,
    //                                     errors: {
    //                                         verifySnsInstagram: "Url instagram is exist"
    //                                     }
    //                                 }))
    //                             }
    //                             if (response.data == true) {
    //                                 this.setState(prevState => ({
    //                                     verifySnsInstagram: false,
    //                                     onChange: false,
    //                                     errors: {
    //                                         verifySnsInstagram: ""
    //                                     }
    //                                 }))
    //                             }
    //                             console.log(this.state)
    //
    //                         }).catch(e => {
    //                         console.log(e)
    //                     });
    //                 }else {
    //
    //                     this.setState(prevState => ({
    //                         verifySnsInstagram: false,
    //                     }))
    //
    //                 }
    //
    //
    //             }else {
    //                 console.log('zozozozoz else')
    //                 this.setState(prevState => ({
    //                     validSnsInstagram: true,
    //                     data: {
    //                         ...prevState.data,
    //                         instagram: {
    //                             value: instagram
    //                         }
    //                     },
    //                     errors: {
    //                         verifySnsInstagram: "Url Instagram is invalid"
    //                     }
    //                 }))
    //
    //             }
    //
    //             break;
    //     }
    //
    // };
    handleChangeInputSns = (e, type) => {
        console.log(e, type)
        let url = e.target.value;
        let valid;
        if(url) {
             valid = this.validUrl(url, type);
        }else{
            valid = true
        }
        this.props.verifySns({sns_id: type, url: url}).then((res)=>{
            if(res.data){
                this.setValueSNS(url, type);
                if(!valid){
                    this.setValueSNS('Invalid URL format', type, 'errors');
                }else{
                    this.setValueSNS('', type, 'errors');
                }
            }else{
                if(!valid){
                    this.setValueSNS('Invalid URL format', type, 'errors');
                }else{
                    this.setValueSNS('This SNS URL already exists', type, 'errors');
                }
            }
        })
        // this.setState()
    }

    setValueSNS(url, type, vari= 'data'){
        let field
        switch (type){
            case 2: field = 'facebook'; break;
            case 3: field = 'youtube'; break;
            case 4: field = 'instagram'; break;
        }
        this.setState({
            [vari]:{
                ...this.state[vari],
                [field]: url
            }

        })
    }


    validUrl(url, type){
        switch(type){
            case 'facebook': return /facebook\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 2: return /facebook\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 'instagram': return /instagram\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 4: return /instagram\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 'youtube': return /youtube\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            case 3: return /youtube\.com\/+[\w\.\?\=\_\/\-\&]+$/.test(url);
            default: return /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm.test(url);
        }

    }



    handleChangeInputNumberOnly = e => {
        const name = e.target.name;
        const value = e.target.value;
        if (/^\d*$/.test(value)) {
            this.setState(prevState => ({
                data: {
                    ...prevState.data,
                    [name]: value
                }
            }));
        }
    };

    onFocusInput = e => {
        this.setState(prevState => ({
            errors: {
                pc_name: "",
                keyword: [],
                // channel:[],
                pc_tob: "",
                pc_brand: "",
                pc_name: "",
                pc_phone: "",
                // youtube: "",
                // instagram: "",
                // facebook: "",
                pc_address: "",
                pc_introduction: "",
                message: ""
            }
        }));
    };

    validateForm = () => {
        const errors = {
            pc_name: "",
            keyword: [],
            // channel:[],
            pc_tob: "",
            pc_brand: "",
            pc_phone: "",
            youtube: "",
            instagram: "",
            pc_address: "",
            facebook: "",
            agree_term: "",
            pc_introduction: "",
            verifySnsInstagram: "",
            verifySnsFacebook: "",
            verifySnsYoutube: "",
            agree_privacy: ""
        };
        const {data} = this.state;
        const {t} = this.props;
        if (!data.pc_name) {
            errors.pc_name = `${t("PARTNER_JOIN.PJN_erro_company")}`;
        } else if (data.pc_name && data.pc_name.length > VALIDATION.MAX_NAME_LENGTH) {
            errors.pc_name = `${t("PARTNER_JOIN.PJN_erro_company_limit")}`;
        }
        if (!data.pc_tob) {
            errors.pc_tob = `${t("PARTNER_JOIN.PJN_erro_business")}`;
        }
        // else if (!isNaN(data.pc_tob)) { errors.pc_tob = 'The pc_tob is number.' }
        if (!data.pc_address) {
            errors.pc_address = `${t("PARTNER_JOIN.PJN_erro_address")}`;
        } else if (data.pc_address && data.pc_address.length > VALIDATION.MAX_LENGTH) {
            errors.pc_address = `${t("PARTNER_JOIN.PJN_erro_address_limit")}`;
        } else if (
            data.pc_address &&
            /[\!\@\#\$\%\{\}\^\&\*\(\)]/.test(data.pc_address)
        ) {
            errors.pc_address = t("FISTAR_JOIN.FJN_erro_address_symbol_special");
        }
        if (!data.pc_introduction) {
            errors.pc_introduction = `${t("PARTNER_SETTING.PSG_erro_introduction")}`;
        } else if (data.pc_introduction && data.pc_introduction.length > VALIDATION.MAX_LENGTH) {
            errors.pc_introduction = `${t(
                "PARTNER_JOIN.PJN_erro_introduction_limit"
            )}`;
        }

        if (this.state.instagram) {
            errors.verifySnsInstagram = "Url instagram is exist";
        }
        if (this.state.youtube) {
            errors.verifySnsYoutube = "Url Youtube is exist";
        }
        if (this.state.facebook) {
            errors.verifySnsFacebook = "Url Facebook is exist";
        }

        if (this.state.instagram) {
            errors.verifySnsInstagram = "Url instagram is exist";
        }
        if (this.state.youtube) {
            errors.verifySnsYoutube = "Url Youtube is exist";
        }
        if (this.state.facebook) {
            errors.verifySnsFacebook = "Url Facebok is exist";
        }

        if (!data.pc_brand) {
            errors.pc_brand = `${t("PARTNER_JOIN.PJN_erro_product")}`;
        }
        // else if (data.pc_brand && data.pc_brand.length > VALIDATION.MAX_LENGTH) {
        //     errors.pc_brand = `${t(
        //         "PARTNER_JOIN.PJN_erro_product_branch_limit"
        //     )}`;
        // }
        // if (!data.pc_phone) {
        //   errors.pc_phone = "The phone field is required.";
        // }
        if (data.pc_phone && isNaN(data.pc_phone) == true) {
            errors.pc_phone = `${t("PARTNER_JOIN.PJN_erro_pc_phone")}`;
        } else if (data.pc_phone && data.pc_phone.length < 9) {
            errors.pc_phone = `${t("PARTNER_JOIN.PJN_erro_pc_phone_limit_9")}`;
        } else if (data.pc_phone && data.pc_phone.length > 15) {
            errors.pc_phone = `${t("PARTNER_JOIN.PJN_erro_pc_phone_limit_15")}`;
        }

        // if (!data.facebook) {
        //   errors.facebook = `${t("PARTNER_JOIN.PJN_erro_face")}`;
        // }
        if (data.keyword.length <= 0) {
            errors.keyword = `${t("PARTNER_JOIN.PJN_erro_keyword")}`;
        }
        if (this.state.agree_privacy == false) {
            errors.agree_privacy = "The checkbox field is required.";
        }
        if (this.state.agree_term == false) {
            errors.agree_term = "The checkbox field is required.";
        }
        // if (data.channel.length <= 0) { errors.channel = 'The channel field is required.' }
        return errors;
    };

    onNextStep = e => {
        e.preventDefault();

        const {data, errors} = this.state;
        const validate = this.validateForm();
        if (
            validate.pc_name === "" &&
            validate.pc_tob === "" &&
            validate.keyword.length == 0 &&
            validate.pc_name === "" &&
            validate.facebook === "" &&
            validate.agree_privacy === "" &&
            validate.pc_introduction === "" &&
            validate.pc_address === "" &&
            validate.agree_term === "" &&
            validate.pc_phone === "" &&
            validate.verifySnsFacebook === "" &&
            validate.verifySnsYoutube === "" &&
            validate.verifySnsInstagram === "" &&
            validate.pc_brand === ""
        // && validate.channel.length == 0
        ) {
            if (!this.state.loadingStatus) {
                this.setState(
                    {
                        loadingStatus: true
                    },
                    () => {
                        this.props.onNextStep(3, data);
                    }
                );
            }
        } else {
            this.setState(
                {
                    errors: validate
                },
                () => {
                }
            );
        }
    };

    onClickKeyword = id => {
        const {data} = this.state;
        const index = data.keyword ? data.keyword.indexOf(id) : "";

        if (index !== -1 && data.keyword) {
            data.keyword.splice(index, 1);
            this.setState({
                data
            });
        } else {
            this.setState(prevState => ({
                data: {
                    ...prevState.data,
                    keyword: [...prevState.data.keyword, id]
                }
            }));
        }
    };

    renderKeyword = () => {
        const {keywords, data, errors} = this.state;
        const {
            code: {
                data: {keyword}
            }
        } = this.props;
        if (!keyword) return;
        return (
            <div className="tag">
                {(keyword.code || []).map((keyword, key) => (
                    <a
                        className={
                            data.keyword && data.keyword.includes(keyword.cd_id)
                                ? "selected"
                                : ""
                        }
                        key={key}
                        onClick={() => this.onClickKeyword(keyword.cd_id)}
                    >
                        {keyword.cd_label}
                    </a>
                ))}
                {errors.keyword && <p className="text-danger">{errors.keyword}</p>}
            </div>
        );
    };

    renderTypeBusiness = () => {
        const {keywords, data, errors} = this.state;
        const {t} = this.props;
        const {
            code: {
                data: {partner_type}
            }
        } = this.props;

        if (!partner_type) return;

        return (
            <div className="input-group row">
                <div className="col-md-3 item-text">
                    <label>
                        {t("PARTNER_JOIN.PJN_type_business")}
                        <strong>*</strong>
                    </label>
                </div>
                <div className="col-md-9 item-form item-select item-select-form">
                    <select
                        className={`form-control form-control-sm${
                            errors.pc_tob ? " is-invalid" : ""
                            }`}
                        value={data.pc_tob}
                        onChange={this.handleChangeInput}
                        onFocus={this.onFocusInput}
                        name="pc_tob"
                        id="pc_tob"
                    >
                        <option/>
                        {(partner_type.code || []).map((partner_type, key) => (
                            <option value={partner_type.cd_id} key={key}>
                                {partner_type.cd_label}
                            </option>
                        ))}
                    </select>
                    {errors.pc_tob && (
                        <div className="tooptip-text">
                            <p className="text-danger">{errors.pc_tob}</p>
                        </div>
                    )}
                </div>
            </div>
        );
    };


    renderBrand = () => {
        const {keywords,brands, data, errors} = this.state;
        const {t} = this.props;
        const {
            code: {
                data: {partner_type}
            }
        } = this.props;

        if (!partner_type) return;



        return (


                <div className="col-md-9 item-form item-select item-select-form">
                    <select
                        className={`form-control form-control-sm${
                            errors.pc_brand ? " is-invalid" : ""
                            }`}
                        value={data.pc_brand}
                        onChange={this.handleChangeInput}
                        onFocus={this.onFocusInput}
                        name="pc_brand"
                        id="pc_brand"
                    >
                        <option/>
                        {(brands || []).map((brand, key) => (
                            <option value={brand.CODE} key={key}>
                                {brand.CODE_NM}
                            </option>
                        ))}
                    </select>
                    {errors.pc_brand && (
                        <div className="tooptip-text">
                            <p className="text-danger">{errors.pc_brand}</p>
                        </div>
                    )}
                </div>

        );
    };


    submitFormPrevious = () => {
        const {data} = this.state;
        this.props.onNextStep(1);
    };

    validateNumber(event) {
        const {data} = this.state;

        var invalidChars = ["-", "+", "e", "."];

        if (invalidChars.includes(event.key)) {
            event.preventDefault();
        }
    }

    render() {
        const {t, email} = this.props;
        const {
            code: {
                data: {keyword}
            }
        } = this.props;
        const {
            data,
            keywords,
            errors,
            agree_privacy,
            agree_term,
            success,
            loadingStatus
        } = this.state;
        console.log(errors);
        return (
            <div className="col-md-12 content-step-form">
                <div className="content-step">
                    <div className="item-circle ">
                        <label>{t("PARTNER_JOIN.PJN_maneger")}</label>
                    </div>
                    <span/>
                    <div className="item-circle active">
                        <label>{t("PARTNER_JOIN.PJN_company")}</label>
                    </div>
                    <span/>
                    <div className="item-circle">
                        <label>{t("PARTNER_JOIN.PJN_complete")}</label>
                    </div>
                </div>
                <form className="form-register step2" onSubmit={this.onNextStep}>
                    <div className="text-require">
                        <span>{t("PARTNER_JOIN.PJN_please_input")}</span>
                        <label>
                            <i className="fa fa-star"/>
                            {t("PARTNER_JOIN.PJN_required")}
                        </label>
                    </div>
                    <div className="center-form ">
                        <div
                            className={`input-group row${
                                errors.pc_name ? " error-message" : ""
                                }`}
                        >
                            <div className="col-md-3 item-text">
                                <label>
                                    {t("PARTNER_JOIN.PJN_company_name")} <strong>*</strong>
                                </label>
                            </div>
                            <div className="col-md-9 item-form">
                                <input
                                    placeholder={t("PARTNER_JOIN.PJN_company_name")}
                                    type="text"
                                    name="pc_name"
                                    id="pc_name"
                                    value={data.pc_name}
                                    className={`form-control${
                                        errors.pc_name ? " is-invalid" : ""
                                        }`}
                                    onChange={this.handleChangeInput}
                                    onFocus={this.onFocusInput}
                                />
                                {errors.pc_name && (
                                    <div className="tooptip-text">
                                        <p className="text-danger">{errors.pc_name}</p>
                                    </div>
                                )}
                            </div>
                        </div>


                        {this.renderTypeBusiness()}

                        <div
                            className={`input-group row${
                                errors.pc_tob ? " error-message" : ""
                                }`}
                        >
                            <div className="col-md-3 item-text">
                                <label>{t("PARTNER_JOIN.PJN_label_contact")}</label>
                            </div>
                            <div className="col-md-9 item-form">
                                <input
                                    type="text"
                                    placeholder={t("PARTNER_JOIN.PJN_label_contact")}
                                    name="pc_phone"
                                    id="pc_phone"
                                    onKeyPress={event => this.validateNumber(event)}
                                    value={data.pc_phone}
                                    className={`form-control${
                                        errors.pc_phone ? " is-invalid" : ""
                                        }`}
                                    onChange={this.handleChangeInputNumberOnly}
                                    onFocus={this.onFocusInput}
                                />
                                {errors.pc_phone && (
                                    <div className="tooptip-text">
                                        <p className="text-danger">{errors.pc_phone}</p>
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="input-group row group-area align-items-start">
                            <div className="col-md-3 item-text">
                                <label>
                                    {t("PARTNER_JOIN.PJN_label_address")} <strong>*</strong>
                                </label>
                            </div>
                            <div className="col-md-9 item-form">
                                <textarea
                                    className={`form-control${
                                        errors.pc_address ? " is-invalid" : ""
                                        }`}
                                    name="pc_address"
                                    id="pc_address"
                                    value={data.pc_address}
                                    onChange={this.handleChangeInput}
                                    onFocus={this.onFocusInput}
                                    rows="5"
                                />
                                {errors.pc_address && (
                                    <div className="tooptip-text">
                                        <p className="text-danger">{errors.pc_address}</p>
                                    </div>
                                )}
                            </div>
                        </div>
                        <div
                            className={`input-group row${
                                errors.pc_name ? " error-message" : ""
                                }`}
                        >
                            <div className="col-md-3 item-text">
                                <label>
                                    {t("PARTNER_JOIN.PJN_label_product_branch")}{" "}
                                    {/*<strong>*</strong>*/}
                                </label>
                            </div>
                            {/*<div className="col-md-9 item-form">*/}
                                {/*<input*/}
                                    {/*placeholder={t("PARTNER_JOIN.PJN_label_product_branch")}*/}
                                    {/*type="text"*/}
                                    {/*name="pc_brand"*/}
                                    {/*id="pc_brand"*/}
                                    {/*value={data.pc_brand}*/}
                                    {/*className={`form-control${*/}
                                        {/*errors.pc_brand ? " is-invalid" : ""*/}
                                        {/*}`}*/}
                                    {/*onChange={this.handleChangeInput}*/}
                                    {/*onFocus={this.onFocusInput}*/}
                                {/*/>*/}
                                {/*{errors.pc_brand && (*/}
                                    {/*<div className="tooptip-text">*/}
                                        {/*<p className="text-danger">{errors.pc_brand}</p>*/}
                                    {/*</div>*/}
                                {/*)}*/}
                                {/**/}
                            {/*</div>*/}

                            {this.renderBrand()}

                        </div>
                        <div className="input-group row group-keyword align-items-baseline">
                            <div className="col-md-3 item-text pr-0">
                                <label className="pr-0 d-block">
                                    {t("PARTNER_JOIN.PJN_label_keyword_matching")}{" "}
                                </label>
                                <label className="pr-0">
                                    {t("PARTNER_JOIN.PJN_label_keyword")} <strong>*</strong>
                                </label>
                            </div>
                            <div className="col-md-9 item-form item-keyword">
                                <div className="text-tag">
                                    <p className="text-note-partner">
                                        {t("PARTNER_JOIN.PJN_please_select")}
                                    </p>
                                    <p>{t("PARTNER_JOIN.PJN_if_you_have")}</p>
                                </div>
                                {this.renderKeyword()}
                                {/*<div className="tag">*/}
                                {/*<span>Skin care</span>*/}
                                {/*<span className="selected">Sun care</span>*/}
                                {/*<span className="selected">Base Make-ups</span>*/}
                                {/*<span>Color Make-ups</span>*/}
                                {/*<span className="selected">Masks</span>*/}
                                {/*<span>Hair care</span>*/}
                                {/*<span>Hair styling</span>*/}
                                {/*<span>Body care</span>*/}
                                {/*<span>Nail care</span>*/}
                                {/*<span className="selected">Cleansing</span>*/}
                                {/*<span>Perfume</span>*/}
                                {/*<span className="selected">Beauty accessories</span>*/}
                                {/*</div>*/}
                            </div>
                        </div>

                        <div className="info-extend">
                            <div className="row">
                                <div className="col-md-3"/>
                                <div className="info-title col-md-9">
                                    <h4 className="text-center">
                    <span>
                      {t("PARTNER_SETTING.PSG_select")}{" "}
                        {data.keyword && data.keyword.length
                            ? data.keyword.length
                            : 0}
                    </span>{" "}
                                        {t("PARTNER_SETTING.PSG_out_of")}{" "}
                                        {keyword ? keyword.code.length : 0}
                                    </h4>
                                </div>
                            </div>

                            <div className="input-group row group-area align-items-start">
                                <div className="col-md-3 item-text">
                                    <label>
                                        {t("PARTNER_SETTING.PSG_label_introduction")}{" "}
                                        <strong>*</strong>
                                    </label>
                                </div>
                                <div className="col-sm-9 item-select d-block ">
                                    <div className="item-select">
                                        {/*<i className="fas fa-angle-up" />*/}
                                        {/*<i className="fas fa-angle-down" />*/}
                                        <textarea
                                            rows="5"
                                            cols="50"
                                            className={`form-control${
                                                errors.pc_introduction ? " is-invalid" : ""
                                                }`}
                                            name="pc_introduction"
                                            id="pc_introduction"
                                            value={data.pc_introduction}
                                            onChange={this.handleChangeInput}
                                            onFocus={this.onFocusInput}
                                            placeholder="About 200 characters"
                                        />
                                    </div>
                                    {errors.pc_introduction && (
                                        <div className="tooptip-text">
                                            <p className="text-danger">{errors.pc_introduction}</p>
                                        </div>
                                    )}
                                </div>
                            </div>
                            <div className="input-group row group-sns align-items-start">
                                <div className="col-md-3 item-text">
                                    <label>{t("PARTNER_SETTING.PSG_label_sns")}</label>
                                </div>
                                <div className="col-sm-9 item-sns">
                                    <div
                                        className={`content-sns`}
                                    >
                                        <div className="input-group-prepend">
                                          <span className="input-group-text">
                                            {t("PARTNER_JOIN.PJN_label_facebook")} : facebook.com{" "}
                                              <b> </b>
                                          </span>
                                        </div>
                                        {/*<input placeholder="User name(URL)" type="text" className="form-input empty"/>*/}

                                        <input
                                            placeholder={t("PARTNER_SETTING.PSG_input_sns")}
                                            type="text"
                                            name="facebook"
                                            id="facebook"
                                            className={`form-input empty${
                                                errors.facebook ? " is-invalid" : ""
                                                }`}
                                            onBlur={(e) =>this.handleChangeInputSns(e, 2)}
                                        />



                                    </div>
                                    {errors.facebook && (
                                        <div className="tooptip-text confix-mt-15">
                                            <p className="text-danger">{errors.facebook}</p>
                                        </div>
                                    )}

                                    <div className="content-sns">
                                        <div className="input-group-prepend">
                                          <span className="input-group-text">
                                            {t("PARTNER_JOIN.PJN_label_youtube")} : youtube.com
                                          </span>
                                        </div>
                                        {/*<input placeholder="User name(URL)" type="text" className="form-input empty"/>*/}
                                        <input
                                            placeholder={t("PARTNER_SETTING.PSG_input_sns")}
                                            type="text"
                                            name="youtube"
                                            id="youtube"
                                            className="form-input empty"
                                            onBlur={(e) =>this.handleChangeInputSns(e, 3)}
                                        />

                                    </div>
                                    {errors.youtube && (
                                        <div className="tooptip-text confix-mt-15">
                                            <p className="text-danger">{errors.youtube}</p>
                                        </div>
                                    )}
                                    <div className="content-sns">
                                        <div className="input-group-prepend">
                                          <span className="input-group-text">
                                            {t("PARTNER_JOIN.PJN_label_instagram")} : instagram.com
                                          </span>
                                        </div>
                                        {/*<input placeholder={t("PARTNER_SETTING.PSG_input_sns")} type="text" className="form-input empty"/>*/}
                                        <input
                                            placeholder={t("PARTNER_SETTING.PSG_input_sns")}
                                            type="text"
                                            name="instagram"
                                            id="instagram"
                                            className={`form-input empty${
                                                errors.instagram ? " is-invalid" : ""
                                                }`}
                                            onBlur={(e) =>this.handleChangeInputSns(e, 4)}

                                        />

                                    </div>
                                    {errors.instagram && (
                                        <div className="tooptip-text confix-mt-15">
                                            <p className="text-danger">{errors.instagram}</p>
                                        </div>
                                    )}

                                </div>
                            </div>
                            <div className="input-group row group-condition align-items-baseline">
                                <div className="col-md-3 item-text">
                                    <label>{t("PARTNER_JOIN.PJN_label_terms_and")}</label>
                                    <label>{t("PARTNER_JOIN.PJN_label_conditions")}</label>
                                    <label>
                                        /{t("PARTNER_JOIN.PJN_label_privacy")} <strong>*</strong>
                                    </label>
                                </div>
                                <div className="col-sm-9 content-condition">
                                    <div className="item-condition">
                                        <div className="item-checkbox">
                                            <div className="checkbox">
                                                <label>
                                                    <input
                                                        type="checkbox"
                                                        checked={this.state.agree_all}
                                                        onChange={this.handleCheckboxAll}
                                                        value=""
                                                    />
                                                    <span className="cr">
                            <i className="cr-icon fa fa-check"/>
                          </span>
                                                    <strong>{t("PARTNER_JOIN.PJN_select_all")}</strong>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="item-condition">
                                        <div className="item-select">
                                            {/*<i className="fas fa-angle-up" />*/}
                                            {/*<i className="fas fa-angle-down" />*/}
                                            <textarea
                                                rows="5"
                                                cols="50"
                                                className="form-control read-only"
                                                placeholder={t("PARTNER_JOIN.PJN_textarea")}
                                                readOnly
                                                disabled={true}
                                            />
                                        </div>
                                    </div>
                                    <div className="item-condition">
                                        <div className="item-checkbox agree-condition">
                                            <div className="checkbox">
                                                <label
                                                    className={errors.agree_term ? "error-checkbox" : ""}
                                                >
                                                    <input
                                                        type="checkbox"
                                                        name="agree_term"
                                                        id="agree_term"
                                                        checked={this.state.agree_term}
                                                        onChange={this.handleCheckboxChangeTerm}
                                                        value=""
                                                    />
                                                    <span className="cr">
                            <i className="cr-icon fa fa-check"/>
                          </span>
                                                    <strong>{t("PARTNER_JOIN.PJN_select_tern")}</strong>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="item-condition">
                                        <div className="item-select">
                                            {/*<i className="fas fa-angle-up" />*/}
                                            {/*<i className="fas fa-angle-down" />*/}
                                            <textarea
                                                rows="5"
                                                cols="50"
                                                className="form-control read-only"
                                                placeholder={t("PARTNER_JOIN.PJN_textarea")}
                                                readOnly
                                                disabled={true}
                                            />
                                        </div>
                                    </div>
                                    <div className="item-condition">
                                        <div className="item-checkbox agree-condition">
                                            <div className="checkbox">
                                                <label
                                                    className={
                                                        errors.agree_privacy ? "error-checkbox" : ""
                                                    }
                                                >
                                                    <input
                                                        type="checkbox"
                                                        name="agree_privacy"
                                                        id="agree_privacy"
                                                        checked={this.state.agree_privacy}
                                                        onChange={this.handleCheckboxChangePrivacy}
                                                        value=""
                                                    />
                                                    <span className="cr">
                            <i className="cr-icon fa fa-check"/>
                          </span>
                                                    <strong>
                                                        {t("PARTNER_JOIN.PJN_select_privacy")}
                                                    </strong>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="input-group row content-button">
                            <div className="col-md-3 item-text"/>
                            <div className="col-md-9 item-button">
                                <div className="row item-row">
                                    <div className="col-md-6 item-prev">
                                        <button
                                            className="prev"
                                            type="button"
                                            onClick={this.submitFormPrevious}
                                        >
                                            {t("PARTNER_JOIN.PJN_button_previous")}
                                        </button>
                                    </div>
                                    <div className="col-md-6 item-next">
                                        <button className="next" onClick={this.onNextStep}>
                                            {loadingStatus ? (
                                                <div className="spinner-border" role="status">
                          <span className="sr-only">
                            {t("LOADING.LOADING")}
                          </span>
                                                </div>
                                            ) : (
                                                `${t("PARTNER_JOIN.PJN_button_apply")}`
                                            )}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        code: state.code,
        info: state.register
    };
};

const mapDispatchToProps = dispatch => {
    return {
        register: data => dispatch(registerActionPartner(data)),
        getCode: type => dispatch(getCodeAction(type)),
        verifySns: data => dispatch(checkSnsVerify(data)),
        getBrand: () => dispatch(getBrand())
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withTranslation("translations")(PartnerRegisterStep2));
