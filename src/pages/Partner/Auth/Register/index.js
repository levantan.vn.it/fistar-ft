import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { registerActionPartner } from "./../../../../store/actions/auth";
import PartnerRegisterStep1 from "./Step1";
import PartnerRegisterStep2 from "./Step2";
import PartnerRegisterStep3 from "./Step3";
import "./register.scss";
class PartnerRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      data: {}
    };
  }

  // clickNextStep = (step, data = {}) => {
  //     this.setState(prevState => ({
  //         step: step,
  //         ...data
  //     }))
  // }

  clickNextStep = (step, data) => {
    if (step == 1) {
      this.setState(prevState => ({
        step: step,
        data: {
          ...prevState.data,
          ...data
        }
      }));
    } else if (step == 2) {
      // return this.props.register({...this.state.data, ...data})
      //   .then(() => {
      this.setState(prevState => ({
        step: step,
        data: {
          ...prevState.data,
          ...data
        }
      }));
      // })
    } else if (step == 3) {
      console.log("sag step 3");
      console.log(this.state, data);
      let userInfo = { ...this.state.data, ...data };
      let formData = new FormData();
      Object.keys(userInfo).map(key => {
        if (userInfo[key].constructor === Array) {
          userInfo[key].map(item => {
            formData.append(key + "[]", item);
          });
        } else {
          formData.append(key, userInfo[key]);
        }
      });

      return this.props.register(formData).then(() => {
        console.log("sag buoc 3");
        this.setState(prevState => ({
          step: step,
          data: {
            ...prevState.data,
            ...data
          }
        }));
      });
    }
  };

  renderStep = () => {
    const { step } = this.state;
    switch (step) {
      case 1:
        return (
          <PartnerRegisterStep1
            onData={this.state.data}
            onNextStep={this.clickNextStep}
          />
        );
        break;
      case 2:
        return (
          <PartnerRegisterStep2
            onData={this.state.data}
            onNextStep={this.clickNextStep}
          />
        );
        break;
      case 3:
        return <PartnerRegisterStep3 onNextStep={this.clickNextStep} />;
        break;

      default:
        return null;
        break;
    }
  };

  render() {
    const { t } = this.props;
    const { data, errors } = this.state;
    console.log(process.env);

    return <Fragment>{this.renderStep()}</Fragment>;
  }
}

const mapStateToProps = state => {
  return {
  };
};

const mapDispatchToProps = dispatch => {
  return {
    register: data => dispatch(registerActionPartner(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(PartnerRegister));
