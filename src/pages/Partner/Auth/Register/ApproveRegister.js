import React, { Component, Fragment } from "react";
import { withTranslation, Trans } from "react-i18next";
import { Link, NavLink, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { changePasswordAction } from "./../../../../store/actions/auth";
import { changePasswordActionPartner } from "../../../../store/actions/auth";
import * as routeName from "./../../../../routes/routeName";
import "./approveRegister.scss";

class ApproveRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { t } = this.props;

    return (
      <section className="register-partner-form approve-register approve-register-parter">
        <div className="container">
          <div className="content-form-register">
            <div className="row text-form">
              <div className="col-md-12">
                <h3>
                  <strong>{t("PARTNER_JOIN.PJN_partner_join")}</strong>
                </h3>
              </div>
            </div>
            <div className="row step-form">
              <div className="col-md-12 content-step-form">
                <div className="title-approve" />
                <div className="notification">
                  <div className="row content-text">
                    <div className="col-md-12 thanks-for text-title-approve">
                      <h2 className="text-center">
                        {t("PARTNER_JOIN_CONFIRM.PJC_congratulations")}
                      </h2>
                      <p className="mt-2 text-center">
                        {t("PARTNER_JOIN_CONFIRM.PJC_text_1")}
                      </p>
                    </div>
                    <div className="description-text col-md-12">
                      <div className="content-text-description">
                        <p>{t("PARTNER_JOIN_CONFIRM.PJC_text_2")}</p>
                        <p>
                          <strong>
                            {t("PARTNER_JOIN_CONFIRM.PJC_text_3")}
                          </strong>
                        </p>
                      </div>
                      <div className="content-text-description">
                        <p>{t("PARTNER_JOIN_CONFIRM.PJC_text_4")}</p>
                        {/* <p>{t("PARTNER_JOIN_CONFIRM.PJC_text_3")}to have a smooth campaign.</p> */}
                      </div>
                    </div>
                    <div className="col-md-12 main-redirect">
                      <div className="button-group-thanks">
                        <NavLink
                          className="text-center w-100"
                          to={routeName.PARTNER_DASHBOARD}
                        >
                          Partner DashBoard
                        </NavLink>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation("translations")(ApproveRegister));
