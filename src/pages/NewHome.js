import React, {Component, Fragment} from "react";
import {withTranslation, Trans} from "react-i18next";
import {Helmet} from "react-helmet";

import {incrementCount, decrementCount} from "./../store/actions/count";
import "./home.scss";
import {connect} from "react-redux";
import home01 from "./../../src/images/home/home-01.png";
import product from "./../../src/images/home/thumnal-product.png";
import iconHeat from "./../../src/images/home/icon-heat.svg";
import iconComment from "./../../src/images/home/icon-comment.svg";
import iconShare from "./../../src/images/home/icon-share.svg";
import layout18 from "./../../src/images/lay18.png";
import fime from "./../../src/images/home/fime.png";
import facebook from "./../../src/images/face.png";
import instar from "./../../src/images/intar.png";
import youtube from "./../../src/images/toutube.png";
import imgLogo from "./../../src/images/home/img-logo.png";
import user01 from "./../../src/images/home/user-01.png";
import bg01 from "./../../src/images/home/bgr-01.png";
import imgUrl from "./../../src/images/home/bg-banner1.png";
import scrollDown from "./../../src/images/home/left-arrow.png";
import jennie from "./../../src/images/jennie.jpg";
import logolg from "./../../src/images/home/logo-lg.jpg";
import api from "../api";
import PieChart from "./../components/charts/PieChartCampaignHome/index";
import CampaignMainImage from "./../components/CampaignMainImage/index"

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {Carousel} from "react-bootstrap";
import {
    getBannerAction,
    getCampaignAction,
    getCountMainMenuAction,
    getFistarAction,
    getDataFistarCampaign
} from "../store/actions/auth";
import * as routeName from "./../routes/routeName";

import {Link, NavLink, Redirect} from "react-router-dom";
import {getImageLink} from "./../common/helper";
import {IMAGE_SIZE, IMAGE_TYPE} from "../constants";
import {nFormatter} from "./../common/helper"
import { Modal } from "react-bootstrap";
import cancelImage from "./../../src/images/cancel.svg";
import PageHead from './../components/PageHead'

function SampleNextArrow(props) {
    const {classNameName, style, onClick} = props;
    return (
        <div
            className={classNameName}
            style={{...style, display: "block", background: "red"}}
            onClick={onClick}
        />
    );
}

function SamplePrevArrow(props) {
    const {classNameName, style, onClick} = props;
    return (
        <div
            className={classNameName}
            style={{...style, display: "block", background: "green"}}
            onClick={onClick}
        />
    );
}

function getId(url) {
    if (typeof url !== "string") {
        return null;
    }
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return "error";
    }
}



function scrollToTopAnimated(scrollDuration) {
    var elmnt = document.getElementById("slider-main-banner");
    var scrollStep = elmnt.offsetHeight / (scrollDuration / 15),
        scrollInterval = setInterval(function() {
            if (elmnt.offsetHeight != 0) {
                window.scrollBy(0, scrollStep);
            } else clearInterval(scrollInterval);
        }, 15);
}



function scrollToSmoothly(pos, time){
    if (typeof window !== 'undefined' && window.document && window.document.createElement) {
        /*Time is exact amount of time the scrolling will take (in milliseconds)*/
        /*Pos is the y-position to scroll to (in pixels)*/
        /*Code written by hev1*/
        if(typeof pos!== "number"){
            pos = parseFloat(pos);
        }
        time = time || 500;
        if(typeof time!== "number"){
            time = parseFloat(time);
        }
        if(isNaN(pos)){
            throw "Position must be a number";
        }
        if(isNaN(time)){
            throw "Time must be a number";
        }
        if(pos<0||time<0){
            return;
        }
        var currentPos = window.scrollY || window.screenTop;
        var start = null;
        var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
        requestAnimationFrame(function step(currentTime){
            start = !start? currentTime: start;
            var progress = currentTime - start;
            if(currentPos<pos){
                window.scrollTo(0, ((pos-currentPos)*progress/time)+currentPos);
                if(progress < time){
                    requestAnimationFrame(step);
                } else {
                    window.scrollTo(0, pos);
                }
            } else {
                window.scrollTo(0, currentPos-((currentPos-pos)*progress/time));
                if(progress < time){
                    requestAnimationFrame(step);
                } else {
                    window.scrollTo(0, pos);
                }
            }
        });
    }
}

function scrollToSection(id){
    var elem = document.getElementById(id);
    scrollToSmoothly(elem.offsetTop, 500);
}

var showScrollToSection = false;

if (typeof window !== 'undefined' && window.document && window.document.createElement) {

    window.onscroll = function() {scrollFunction()};


    function scrollFunction() {
        let scrollTopSection = document.getElementById('scroll-top-section');
        if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
            if(scrollTopSection) {
                scrollTopSection.classList.remove('d-block')
                scrollTopSection.classList.add('d-none')
            }

        } else {
            if(scrollTopSection) {
                scrollTopSection.classList.add('d-block')
                scrollTopSection.classList.remove('d-none')
            }
        }



    }

}


const defaultValidate = {
    name: false,
    email: false,
    location: false,
    phone: false,
    content: false
};

const defaultFormData = {
    type: 1,
    name: "",
    email: "",
    location: "",
    phone: "",
    content: ""
};

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            banners: true,
            countMainMenu: "",
            type: "",
            sort: "rate_scrap",
            order: "desc",
            fistars: "",
            stateStatus: 2,
            activeState: 1,
            campaigns: "",
            status: [60, 61, 62],
            redirectCampaign: false,
            isLoadingCampaign: false,
            redirectService: "",
            redirectSignUp: "",
            redirectSignUpPartner: false,
            fistarTop:[],
            campaignsTop:[],
            form_data: defaultFormData,
            form_validate: defaultValidate,
            loading: false,
            success: false,
            count_fistar:0,
            count_partner:0,
            count_campaign:0,
            prevScrollpos: (typeof window !== 'undefined' && window.document && window.document.createElement) ? window.pageYOffset : 0,
            visible: true,
            isFixed: false,
            isFlexEnd: false,
        };
        this.myRef = React.createRef();
    }

    scrollStep = () => {
        if (typeof window !== 'undefined' && window.document && window.document.createElement) {
            if (window.pageYOffset === 0) {
                clearInterval(this.state.intervalId);
            }
            window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
        }
    };

    scrollToTop = () => {
        let intervalId = setInterval(this.scrollStep, 16.66);
        this.setState({intervalId: intervalId});
    };

    componentDidMount() {

        if (typeof window !== 'undefined' && window.document && window.document.createElement) {window.addEventListener("scroll", this.handleScroll);}

        this.props.banners(this.state.banners).then(response => {
            this.setState({
                banners: response
            }, () => {
                this.slider.slickNext()
            });
        });
        this.props.getCounMainMenu().then(res => {
            this.setState({
                countMainMenu: res
            });
        });
        this.props.getDataFistarCampaign().then(response => {

            this.setState({
                fistarTop: response.fistar,
                campaignsTop:response.campaign || {},
                count_fistar:response.count_fistar,
                count_partner:response.count_partner,
                count_campaign:response.count_campaign,

            });
        });

        this.props
            .getFistar(this.state.page, this.state.sort, this.state.order, this.state.stateStatus, this.state.activeState)
            .then(response => {
                this.setState({
                    fistars: response.data
                });
            })
            .catch(() => {
                this.setState({});
            });

        this.setState(
            {
                isLoadingCampaign: true
            },
            () => {
                this.initData();
            }
        );
    }

    componentWillUnmount() {
        if (typeof window !== 'undefined' && window.document && window.document.createElement) { window.removeEventListener("scroll", this.handleScroll)};
    }

    handleScroll = () => {
        if (typeof window !== 'undefined' && window.document && window.document.createElement) {
            if (window.innerWidth >= 992) {
                let rowFistarList = document.getElementById('row-fistar-list')
                let rowFistarViewMore = document.getElementById('row-fistar-view-more')
                let fistarPrimary = document.getElementById('fistar-primary')
                let header = document.getElementsByClassName('header-container')[0]
                if (rowFistarList.getBoundingClientRect().top - header.offsetHeight <= 0) {
                    fistarPrimary.classList.add('fistar-fixed')
                    // this.setState({
                //         isFixed: true,
                //         isFlexEnd: false
                //     })
                } else {
                    fistarPrimary.classList.remove('fistar-fixed')
                //     this.setState({
                //         isFixed: false,
                //         isFlexEnd: false
                //     })
                }
                if (rowFistarViewMore.getBoundingClientRect().top <= (fistarPrimary.offsetHeight + header.offsetHeight)) {
                    rowFistarList.classList.remove('initial')
                    fistarPrimary.classList.remove('fistar-fixed')
                //     this.setState({
                //         isFixed: false,
                //         isFlexEnd: true
                //     })
                }else {
                    // rowFistarList.classList.add('initial')
                    // fistarPrimary.classList.remove('flex-end')
                }
            }
        }
    }

    initData = () => {
        this.props
            .getCampaign(this.state.page, this.state.type, this.state.status)
            .then(response => {
                this.setState({
                    campaigns: response.data,
                    isLoadingCampaign: false
                });
            })
            .catch(() => {
                this.setState({});
            });
    };

    redirectService = link => {
        this.setState({
            redirectService: link
        });
    };
    SignUpFistar = link => {
        this.setState({
            redirectSignUp: link
        });
    };
    SignPartner = () => {
        this.setState({
            redirectSignUpPartner: true
        });
    };

    scrollToMyRef = id => {

        scrollToSection(id)

    };


    validateEmail = email => {
        var re = /^[a-zA-Z0-9\.]{2,}@[a-zA-Z0-9]{2}(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z](?:[a-zA-Z-]{0,61}[a-zA-Z])?)(?=.*[\.])*$/;
        return re.test(String(email).toLowerCase());
    };

    __handleChangeInput = e => {
        this.setState({
            form_data: {
                ...this.state.form_data,
                [e.target.name]: e.target.value
            }
        });
    };

    handleChangeInputNumberOnly = e => {
        const name = e.target.name;
        const value = e.target.value;
        if (/^\d*$/.test(value)) {
            this.setState(prevState => ({
                form_data: {
                    ...prevState.form_data,
                    [name]: value
                }
            }));
        }
    };

    __handleSubmit = e => {
        this.setState({
            loading: true
        });
        const { form_data } = this.state;
        if (this.validateInput(form_data)) {
            api.contact
                .sendContact(form_data)
                .then(res => {
                    this.handleShow();
                })
                .catch(e => {
                });
        } else {
        }
        e.preventDefault();
    };

    validateInput = formData => {
        let validate = { ...defaultValidate };
        let status = 0;
        if (!formData.name || formData.name == "") {
            validate.name = "The name field is required";
            status++;
        }
        if (!formData.email) {
            validate.email = "The email field is required";
            status++;
        } else if (formData.email && !this.validateEmail(formData.email)) {
            validate.email = `${this.props.t("FISTAR_LOGIN.FL_toptip_email")}`;
            status++;
        }
        // if (!formData.location || formData.location == "") {
        //   validate.location = "The location field is required";
        //   status++;
        // }
        if (!formData.phone || formData.phone == "") {
            validate.phone = "The phone field is required";
            status++;
        } else if (formData.phone && formData.phone.length < 9) {
            validate.phone = "The phone field is min 9 numbers.";
            status++;
        } else if (formData.phone && formData.phone.length > 15) {
            validate.phone = "The phone field is maximums 15 numbers.";
            status++;
        }
        if (!formData.phone || formData.content == "") {
            validate.content = "Please enter your question.";
            status++;
        }
        this.setState({
            form_validate: validate,
            loading: false
        });
        if (status > 0) return false;
        else return true;
    };

    __handleForcus = () => {
        this.setState({
            form_validate: defaultValidate,
            loading: false
        });
    };

    handleClose = () => {
        this.setState({
            success: false,
            form_data: defaultFormData,
            form_validate: defaultValidate
        });
    };

    handleShow = () => {
        this.setState({ success: true });
    };

    onExited = () => {
        this.setState({
            isRedirect: true
        });
    };


    render() {
        const {t, i18n} = this.props;
        const settings = {
            // customPaging: function(i) {
            //     return (
            //
            //             <span>{i + 1}</span>
            //
            //     );
            // },
            accessibility: true,
            autoplay: true,
            autoplaySpeed: 5000,
            arrows: true,
            fade: true,
            cssEase: 'linear',
            dots: true,
            dotsClass: "slick-dots slick-thumb customer-slick-thumb",
            infinite: true,
            speed: 1000,
            slidesToShow: 1,
            slidesToScroll: 1,
            

            appendDots: dots => (
                <div>
                    <ol> {dots} </ol>
                </div>
            ),
            customPaging: i => <span>{i + 1}</span>
        };
        const settings01 = {
            accessibility: true,
            // autoplay: true,
            autoplaySpeed: 6000,
            arrows: true,
            dots: true,
            infinite: true,
            fade: true,
            cssEase: 'linear',
            speed: 1000,
            slidesToShow: 1,
            slidesToScroll: 1,
            //   indicators:true
            // nextArrow: <SampleNextArrow />,
            // prevArrow: <SamplePrevArrow />,
            appendDots: dots => (
                <div>
                    <ol> {dots} </ol>
                </div>
            ),
            customPaging: i => <span>{i + 1}</span>
        };

        const settings2 = {
            accessibility: true,
            // autoplay: true,
            autoplaySpeed: 5000,
            arrows: true,
            fade: true,
            cssEase: 'ease-in-out',
            dots: false,
            infinite: false,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 4,
            swipeToSlide: true,
            nextArrow: <SampleNextArrow/>,
            prevArrow: <SamplePrevArrow/>,
            responsive: [
                {
                    breakpoint: 1202,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        };
        const {
            form_data, loading, form_validate,
            banners,
            countMainMenu,
            fistars,
            campaigns,
            redirectCampaign,
            isLoadingCampaign,
            redirectService,
            redirectSignUp,
            redirectSignUpPartner,
            count_campaign,
            fistarTop,
            campaignsTop,
            count_partner,
            count_fistar
        } = this.state;
        if (redirectSignUp) {
            return <Redirect to={redirectSignUp}/>;
        }
        if (redirectService !== "") {
            return <Redirect to={redirectService}/>;
        }
        if (redirectSignUpPartner) {
            return <Redirect to={routeName.PARTNER_REGISTER}/>;
        }
        var currentLocation = (typeof window !== 'undefined' && window.document && window.document.createElement) ? window.location.origin : '';

        const banner01 = {
            color: "blue",
            backgroundImage: "url(" + imgUrl + ")"
        };
        let likeFime = 0;
        let likeFacebook = 0;
        let likeYouube = 0;
        let likeInstagram = 0;
        let sumLike = 0;
        let sumComment = 0;
        let sumShare = 0;
        // if (!campaignsTop || Object.keys(campaignsTop).length === 0) {
        //     return <Fragment>
        //         <PageHead title="Home"/>
        //     </Fragment>
        // }
        campaignsTop && campaignsTop.channel && campaignsTop.channel.length > 0 &&
        campaignsTop.channel.map(like => {
            sumLike+=like.total_scrap
            sumComment+=like.total_comment
            sumShare+=like.total_share
            if (like.sns_id == 1) {
                likeFime = like.total_scrap;
            }
            if (like.sns_id == 2) {
                likeFacebook = like.total_scrap;
            }
            if (like.sns_id == 3) {
                likeYouube = like.total_scrap;
            }
            if (like.sns_id == 4) {
                likeInstagram = like.total_scrap;
            }
        });
        console.log(sumLike, sumComment, sumShare)
        let totaLike = likeFime + likeFacebook + likeYouube + likeInstagram;
        likeFime = likeFime !== 0 ? ((likeFime / totaLike) * 100) : 0;
        likeFacebook = likeFacebook !== 0 ? ((likeFacebook / totaLike) * 100) : 0;
        likeYouube = likeYouube !== 0? ((likeYouube / totaLike) * 100) : 0;
        likeInstagram = likeInstagram !== 0 ? ((likeInstagram / totaLike) * 100) : 0;
        const bg = {
            color: "blue",
            backgroundImage: "url(" + getImageLink(campaignsTop.cp_main_image, IMAGE_TYPE.CAMPAIGNS, IMAGE_SIZE.ORIGINAL) + ")"
        };
        return (
            <Fragment>
                <PageHead title="fistar - Influencer Marketing Platform in Vietnam"/>

                <main className="main-container">
                    <section className="fistar-content home-main">
                        <div className="slider-main" id="slider-main-banner">
                            <div
                                id="carouselExampleIndicators"
                                className="carousel slide"
                                data-ride="carousel"
                            >
                                <div className="carousel-inner list-slider">
                                    <Slider {...settings} ref={c => (this.slider = c)}>
                                        {banners.length > 0 &&
                                        banners.slice(0,10).map((banner, key) => {
                                            banner.link_button_one =
                                                banner.link_button_one == "undefined"
                                                    ? ""
                                                    : banner.link_button_one;
                                            banner.link_button_two =
                                                banner.link_button_two == "undefined"
                                                    ? ""
                                                    : banner.link_button_two;
                                            banner.target_button_one =
                                                banner.target_button_one == "undefined"
                                                    ? ""
                                                    : banner.target_button_one;
                                            banner.target_button_two =
                                                banner.target_button_two == "undefined"
                                                    ? ""
                                                    : banner.target_button_two;
                                            if (banner.sb_cover_type == 1) {
                                                return (
                                                    <div className="carousel-item active  slider-item" key={key}>
                                                        {banner.image_url ? (
                                                            banner.image_url &&
                                                            banner.image_url !== null &&
                                                            banner.image_url.includes(currentLocation) ? (
                                                                <Link
                                                                    to={banner.image_url}
                                                                    target={banner.image_url_target}
                                                                >
                                                                    <img
                                                                        src={
                                                                            banner.sb_cover
                                                                                ? getImageLink(
                                                                                banner.sb_cover,
                                                                                IMAGE_TYPE.BANNER,
                                                                                IMAGE_SIZE.ORIGINAL
                                                                                )
                                                                                : jennie
                                                                        }
                                                                        className="d-block w-100 image-item"
                                                                        alt="..."
                                                                    />

                                                                </Link>
                                                            ) : (
                                                                <a
                                                                    href={banner.image_url}
                                                                    target={banner.image_url_target}
                                                                >
                                                                    <img
                                                                        src={
                                                                            banner.sb_cover
                                                                                ? getImageLink(
                                                                                banner.sb_cover,
                                                                                IMAGE_TYPE.BANNER,
                                                                                IMAGE_SIZE.ORIGINAL
                                                                                )
                                                                                : jennie
                                                                        }
                                                                        className="d-block w-100 image-item"
                                                                        alt="..."
                                                                    />

                                                                </a>
                                                            )
                                                        ) : (
                                                            <img
                                                        src={
                                                        banner.sb_cover
                                                        ? getImageLink(
                                                        banner.sb_cover,
                                                        IMAGE_TYPE.BANNER,
                                                        IMAGE_SIZE.ORIGINAL
                                                        )
                                                        : jennie
                                                        }
                                                        className="d-block w-100 image-item"
                                                        alt="..."
                                                        />
                                                        )}

                                                        <div className="container">
                                                            <div className="carousel-caption d-none d-md-block caption-slider">
                                                                <h4>{banner.sb_name ? banner.sb_name : ""}</h4>
                                                                <p> {banner.main_description ? banner.main_description : ""}</p>
                                                                <p>{banner.sub_description ? banner.sub_description : ""}</p>
                                                                {/*<a href="#">VIEW MORE</a>*/}
                                                                <div className="link-button">
                                                                {banner.button_one &&
                                                                banner.button_one !== "undefined" &&
                                                                banner.button_one !== "null" ? (
                                                                    banner.link_button_one &&  banner.link_button_one.includes(
                                                                        currentLocation
                                                                    ) ? (
                                                                        <Link
                                                                            to={banner.link_button_one ? banner.link_button_one  : "javascript:void(0)"}
                                                                            target={banner.target_button_one}
                                                                        >
                                                                            {banner.button_one}
                                                                        </Link>
                                                                    ) : (
                                                                        <a
                                                                            href={banner.link_button_one ? banner.link_button_one  : "javascript:void(0)"}
                                                                            className="btn-serve link-redirect"
                                                                            target={banner.target_button_one}
                                                                        >
                                                                            {banner.button_one}
                                                                        </a>
                                                                    )
                                                                ) : (
                                                                    ""
                                                                )}

                                                                {banner.button_two &&
                                                                banner.button_two !== "undefined" &&
                                                                banner.button_two !== "null" ? (
                                                                    banner.link_button_two && banner.link_button_two.includes(
                                                                        currentLocation
                                                                    ) ? (
                                                                        <Link
                                                                            className="ml-3"
                                                                            to={banner.link_button_two ? banner.link_button_two  : "javascript:void(0)"}
                                                                            target={banner.target_button_two}
                                                                        >
                                                                            {banner.button_two}
                                                                        </Link>
                                                                    ) : (
                                                                        <a
                                                                            href={banner.link_button_two ? banner.link_button_two  : "javascript:void(0)"}
                                                                            className="btn-serve link-redirect ml-3"
                                                                            target={banner.target_button_two}
                                                                        >
                                                                            {banner.button_two}
                                                                        </a>
                                                                    )
                                                                ) : (
                                                                    ""
                                                                )}
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                );
                                            }


                                            else if (banner.sb_cover_type == 2) {
                                                return (
                                                    <div className="carousel-item slider-item" key={key}>
                                                        <iframe
                                                            id="myIframe"
                                                            className="react-player w-100"
                                                            height="665"
                                                            frameBorder="0"
                                                            src={
                                                                banner.sb_cover
                                                                    ? "//www.youtube.com/embed/" +
                                                                    getId(banner.sb_cover)
                                                                    : "//www.youtube.com/embed/"
                                                            }
                                                            allowFullScreen
                                                        />

                                                        <div className="container">
                                                            <div className="carousel-caption d-none d-md-block caption-slider">
                                                                <h4>{banner.sb_name ? banner.sb_name : ""}</h4>
                                                                <p> {banner.sb_content ? banner.sb_content : ""}</p>
                                                                {/*<p>{banner.sb_sub_name ? banner.sb_sub_name : ""}</p>*/}
                                                                {/*<a href="#">VIEW MORE</a>*/}
                                                                <div className="link-button">
                                                                    {banner.button_one &&
                                                                    banner.button_one !== "undefined" &&
                                                                    banner.button_one !== "null" ? (
                                                                        banner.link_button_one &&  banner.link_button_one.includes(
                                                                            currentLocation
                                                                        ) ? (
                                                                            <Link
                                                                                to={banner.link_button_one ? banner.link_button_one  : "javascript:void(0)"}
                                                                                target={banner.target_button_one}
                                                                            >
                                                                                {banner.button_one}
                                                                            </Link>
                                                                        ) : (
                                                                            <a
                                                                                href={banner.link_button_one ? banner.link_button_one  : "javascript:void(0)"}
                                                                                className="btn-serve link-redirect"
                                                                                target={banner.target_button_one}
                                                                            >
                                                                                {banner.button_one}
                                                                            </a>
                                                                        )
                                                                    ) : (
                                                                        ""
                                                                    )}

                                                                    {banner.button_two &&
                                                                    banner.button_two !== "undefined" &&
                                                                    banner.button_two !== "null" ? (
                                                                        banner.link_button_two && banner.link_button_two.includes(
                                                                            currentLocation
                                                                        ) ? (
                                                                            <Link
                                                                                className="ml-3"
                                                                                to={banner.link_button_two ? banner.link_button_two  : "javascript:void(0)"}
                                                                                target={banner.target_button_two}
                                                                            >
                                                                                {banner.button_two}
                                                                            </Link>
                                                                        ) : (
                                                                            <a
                                                                                href={banner.link_button_two ? banner.link_button_two  : "javascript:void(0)"}
                                                                                className="btn-serve link-redirect ml-3"
                                                                                target={banner.target_button_two}
                                                                            >
                                                                                {banner.button_two}
                                                                            </a>
                                                                        )
                                                                    ) : (
                                                                        ""
                                                                    )}
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                );
                                            }
                                            else if (banner.sb_cover_type == 3) {
                                                return (
                                                    <div className="carousel-item slider-item" key={key}>
                                                        <div className="text-center">
                                                            <video
                                                                // ref={this.previewVideo}
                                                                className="w-100"
                                                                controls
                                                            >
                                                                <source
                                                                    // ref={this.previewVideoSource}
                                                                    src={banner.sb_cover}
                                                                    type="video/mp4"
                                                                />
                                                                Your browser does not support HTML5 video.
                                                            </video>
                                                        </div>

                                                        <div className="container">
                                                            <div className="carousel-caption d-none d-md-block caption-slider">
                                                                <h4>{banner.sb_name ? banner.sb_name : ""}</h4>
                                                                <p> {banner.sb_content ? banner.sb_content : ""}</p>
                                                                {/*<p>{banner.sb_sub_name ? banner.sb_sub_name : ""}</p>*/}
                                                                {/*<a href="#">VIEW MORE</a>*/}
                                                                <div className="link-button">
                                                                    {banner.button_one &&
                                                                    banner.button_one !== "undefined" &&
                                                                    banner.button_one !== "null" ? (
                                                                        banner.link_button_one &&  banner.link_button_one.includes(
                                                                            currentLocation
                                                                        ) ? (
                                                                            <Link
                                                                                to={banner.link_button_one ? banner.link_button_one  : "javascript:void(0)"}
                                                                                target={banner.target_button_one}
                                                                            >
                                                                                {banner.button_one}
                                                                            </Link>
                                                                        ) : (
                                                                            <a
                                                                                href={banner.link_button_one ? banner.link_button_one  : "javascript:void(0)"}
                                                                                className="btn-serve link-redirect"
                                                                                target={banner.target_button_one}
                                                                            >
                                                                                {banner.button_one}
                                                                            </a>
                                                                        )
                                                                    ) : (
                                                                        ""
                                                                    )}

                                                                    {banner.button_two &&
                                                                    banner.button_two !== "undefined" &&
                                                                    banner.button_two !== "null" ? (
                                                                        banner.link_button_two && banner.link_button_two.includes(
                                                                            currentLocation
                                                                        ) ? (
                                                                            <Link
                                                                                className="ml-3"
                                                                                to={banner.link_button_two ? banner.link_button_two  : "javascript:void(0)"}
                                                                                target={banner.target_button_two}
                                                                            >
                                                                                {banner.button_two}
                                                                            </Link>
                                                                        ) : (
                                                                            <a
                                                                                href={banner.link_button_two ? banner.link_button_two  : "javascript:void(0)"}
                                                                                className="btn-serve link-redirect ml-3"
                                                                                target={banner.target_button_two}
                                                                            >
                                                                                {banner.button_two}
                                                                            </a>
                                                                        )
                                                                    ) : (
                                                                        ""
                                                                    )}
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                );
                                            }


                                        })}




                                    </Slider>

                                    <div
                                        id="scroll-top-section"
                                        className="scroll-main d-block"
                                        onClick={() => this.scrollToMyRef("sub-slider")}
                                    >
                                        <p>
                                            {/*<a href="#sub-slider">*/}
                                            <a href="javascript:void(0)">
                                                <img src={scrollDown} alt=""/>
                                            </a>
                                            <span>Scroll</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="sub-slider" id="sub-slider" ref={this.myRef}>
                            <div
                                id="carouselExampleIndicatorsss"
                                className="carousel slide"
                                data-ride="carousel"
                            >


                                <div className="carousel-inner list-slider slider-bottom">
                                    <Slider {...settings01}>
                                        <div className="carousel-item active slider-item">
                                            <div className="slider-content">
                                                <div className="container">
                                                    <div className="content-item">
                                                        <div className="content-item-1">
                                                            <h4>{t("HOME.HOM_campaign")}</h4>
                                                            <h2>{count_campaign}</h2>
                                                            <p>
                                                                {t("HOME.HOM_campaign_text")}
                                                            </p>
                                                        </div>
                                                        <div className="content-item-2 img-bg" style={bg} >

                                                            <img
                                                                src={getImageLink(
                                                                    campaignsTop.cp_main_image,
                                                                    IMAGE_TYPE.CAMPAIGNS,
                                                                    IMAGE_SIZE.ORIGINAL
                                                                )}
                                                                alt={campaignsTop.cp_name}
                                                                className="card-img-top"
                                                            />
                                                        </div>
                                                        <div className="content-item-3">
                                                            <h4 className="title-chart">
                                                                {campaignsTop.cp_name}
                                                            </h4>
                                                            <p dangerouslySetInnerHTML={{ __html: campaignsTop.cp_description }}>

                                                            </p>
                                                            <div className="chart-campaign">

                                                                <PieChart
                                                                    // style={{ height: "5%" }}
                                                                    data={[
                                                                        { name: "Fi:me", value: likeFime, color: "#EE467F" },
                                                                        { name: "Facebook", value: likeFacebook, color: "#48BAFD" },
                                                                        { name: "Youtube", value: likeYouube, color: "#7FC4FD" },
                                                                        { name: "Instagram", value: likeInstagram, color: "#BCE0FD" }
                                                                    ]}
                                                                />

                                                            </div>
                                                            <div className="list-solical-item">
                                                                <div className="item">
                                                                    <img src={iconShare} alt=""/>
                                                                    <span>{campaignsTop && campaignsTop.channel && campaignsTop.channel.length > 0 && nFormatter(sumLike, 0)}</span>
                                                                </div>
                                                                <div className="item">
                                                                    <img src={iconComment} alt=""/>
                                                                    <span>{campaignsTop && campaignsTop.channel && campaignsTop.channel.length > 0 && nFormatter(sumComment, 0)}</span>
                                                                </div>

                                                                <div className="item">
                                                                    <img src={iconHeat} alt=""/>
                                                                    <span>{campaignsTop && campaignsTop.channel && campaignsTop.channel.length > 0 && nFormatter(sumShare, 0)}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="carousel-item slider-item slider-item-fistar ">
                                            <div className="slider-content">
                                                <div className="container">
                                                    <div className="content-item">
                                                        <div className="content-item-1">
                                                            <h4>{t("HOME.HOM_our_fistar")}</h4>
                                                            {/*<h2>{countMainMenu.fistar}</h2>*/}
                                                            <h2>{count_fistar}</h2>
                                                            <p>
                                                                {t("HOME.HOM_fistar_text")}
                                                            </p>
                                                        </div>
                                                        <div className="content-item-2">
                                                            <div className="list-fistar">

                                                                {fistarTop.slice(0, 3).map((fistar, key) => {
                                                                    return (<div className="list-item" key={key}>
                                                                    <div className="left-item">
                                                                        <div className="img-thumnail" >
                                                                            <a href="javascript:void(0)"className="disabled-hover">
                                                                                <img
                                                                                    src={getImageLink(
                                                                                        fistar.picture,
                                                                                        IMAGE_TYPE.FISTARS,
                                                                                        IMAGE_SIZE.ORIGINAL
                                                                                    )}
                                                                                    alt={fistar.fullname}
                                                                                />
                                                                            </a>
                                                                            <div className="star-number">
                                                                                <button className="btn btn-star">
                                                                                    <i className="fas fa-star"/>
                                                                                    {fistar.scrap_total ? fistar.scrap_total : 0}
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="right-item">
                                                                        <div className="user-name">
                                                                            <h4>
                                                                                <a href="javascript:void(0)"className="disabled-hover">{fistar.name}</a>
                                                                            </h4>
                                                                            <p>
                                                                                <span>{new Date(fistar.dob)
                                                                            .toDateString()
                                                                            .split(" ")
                                                                            .pop()}</span>
                                                                                <span className="address">{fistar.location_label}</span>
                                                                            </p>
                                                                        </div>
                                                                        <div className="text-content">
                                                                            <p>
                                                                            {fistar.self_intro}
                                                                            </p>
                                                                        </div>
                                                                        <div className="list-solical-like">
                                                                            <div className="item-solical">
                                                                                <img src={fime} alt=""/>
                                                                                <span>{nFormatter(fistar.fime_follower, 0)}</span>
                                                                            </div>
                                                                            <div className="item-solical">
                                                                                <img src={facebook} alt=""/>
                                                                                <span>{nFormatter(fistar.facebook_follower, 0)}</span>
                                                                            </div>
                                                                            <div className="item-solical">
                                                                                <img src={instar} alt=""/>
                                                                                <span>{nFormatter(fistar.instagram_follower, 0)}</span>
                                                                            </div>
                                                                            <div className="item-solical">
                                                                                <img src={youtube} alt=""/>
                                                                                <span>{nFormatter(fistar.youtube_follower, 0)}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>);
                                                                })}                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="carousel-item slider-item slider-item-partner">
                                            <div className="slider-content">
                                                <div className="container">
                                                    <div className="content-item">
                                                        <div className="content-item-1">
                                                            <h4>{t("HOME.HOM_our_partner")}</h4>
                                                            {/*<h2>{countMainMenu.partner}</h2>*/}
                                                            <h2>{count_partner}</h2>
                                                            <p>
                                                                {t("HOME.HOM_partner_text")}
                                                            </p>
                                                        </div>
                                                        <div className="content-item-2">
                                                            <div className="list-item-logo">
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                                <div className="item-logo">
                                                                    <a href="">
                                                                        <img src={logolg} alt=""/>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Slider>

                                </div>
                            </div>
                        </div>
                        <div className="our-fistar">
                            <div className="container-custom" id="container-custom">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="top-title">
                                            <p>{t("HOME.HOM_our_fistar_title")}</p>
                                            <h3>{t("HOME.HOM_our_fistar")}</h3>
                                        </div>
                                    </div>
                                </div>
                                <div className="row" id="row-fistar-list" style={{alignItems: !this.state.isFlexEnd ? 'initial' : 'flex-end'}}>
                                    <div className="col-lg-4">
                                        <div className={'fistar-primary' + (this.state.isFixed ? ' fistar-fixed' : '')} id="fistar-primary">
                                            {fistars &&
                                            fistars.length > 0 &&
                                            fistars.slice(0, 1).map((fistar, key) =>{
                                                    const bgFistar = {
                                                        backgroundImage: "url(" + getImageLink(fistar.picture, IMAGE_TYPE.FISTARS, IMAGE_SIZE.ORIGINAL) + ")"
                                                    };
                                                return (
                                                <div className="item-fistar" key={key}>
                                                    <div className="img-thumnail" style={bgFistar}>
                                                        {/*<a*/}
                                                            {/*href="javascript:void(0)"*/}
                                                            {/*className="disabled-hover"*/}
                                                        {/*>*/}
                                                        <Link to={routeName.FISTAR_FRONT}>
                                                            <img
                                                                src={getImageLink(
                                                                    fistar.picture,
                                                                    IMAGE_TYPE.FISTARS,
                                                                    IMAGE_SIZE.ORIGINAL
                                                                )}
                                                                alt={fistar.fullname}
                                                            />
                                                        </Link>
                                                        <div className="star-number">
                                                            <button className="btn btn-star disabled-hover">
                                                                <i className="fas fa-star"/>
                                                                {fistar.rate_scrap ? fistar.rate_scrap : 0}
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div className="content-item">
                                                        {/*<p>AUG. WEEKLY FISTAR 02</p>*/}
                                                        <h4 className="user-name">
                                                            <Link to={routeName.FISTAR_FRONT}>{fistar.fullname}</Link>
                                                        </h4>
                                                    </div>

                                                </div>
                                            )}
                                                )}

                                        </div>
                                    </div>
                                    <div className="col-lg-8 right">
                                        <div className="list-fistar-right">
                                            <div className="row">
                                                <div className="col-lg-4">
                                                    <div className="list-item-fistar">


                                                        {fistars &&
                                                        fistars.length > 0 &&
                                                        fistars.slice(0, 3).map((fistar, key) => {
                                                            const bgFistar = {
                                                                backgroundImage: "url(" + getImageLink(fistar.picture, IMAGE_TYPE.FISTARS, IMAGE_SIZE.ORIGINAL) + ")"
                                                            };
                                                            return (
                                                            <div className="item-fistar" key={key}>
                                                                <div className="img-thumnail" style={bgFistar}>
                                                                    <Link to={routeName.FISTAR_FRONT}>
                                                                        <img
                                                                            src={getImageLink(
                                                                                fistar.picture,
                                                                                IMAGE_TYPE.FISTARS,
                                                                                IMAGE_SIZE.ORIGINAL
                                                                            )}
                                                                            alt={fistar.fullname}
                                                                        />
                                                                    </Link>
                                                                    <div className="star-number">
                                                                        <button className="btn btn-star disabled-hover">
                                                                            <i className="fas fa-star"/>
                                                                            {fistar.rate_scrap ? fistar.rate_scrap : 0}
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <div className="content-item">
                                                                    <p><Link to={routeName.FISTAR_FRONT}>{fistar.fullname}</Link></p>
                                                                    <Link to={routeName.FISTAR_FRONT}> <p>
                                                                        <span>{new Date(fistar.dob)
                                                                            .toDateString()
                                                                            .split(" ")
                                                                            .pop()}</span>
                                                                        <span className="address">{fistar.location.cd_label}</span>
                                                                    </p></Link>
                                                                </div>

                                                            </div>
                                                        )})}


                                                    </div>
                                                </div>
                                                <div className="col-lg-4">
                                                    <div className="list-item-fistar">


                                                        {fistars &&
                                                        fistars.length > 3 &&
                                                        fistars.slice(3, 6).map((fistar, key) => {

                                                            const bgFistar = {
                                                                backgroundImage: "url(" + getImageLink(fistar.picture, IMAGE_TYPE.FISTARS, IMAGE_SIZE.ORIGINAL) + ")"
                                                            };

                                                            return(<div className="item-fistar" key={key}>
                                                                <div className="img-thumnail" style={bgFistar}>
                                                                    <Link to={routeName.FISTAR_FRONT}>
                                                                        <img
                                                                            src={getImageLink(
                                                                                fistar.picture,
                                                                                IMAGE_TYPE.FISTARS,
                                                                                IMAGE_SIZE.ORIGINAL
                                                                            )}
                                                                            alt={fistar.fullname}
                                                                        />
                                                                    </Link>
                                                                    <div className="star-number">
                                                                        <button className="btn btn-star disabled-hover">
                                                                            <i className="fas fa-star"/>
                                                                            {fistar.rate_scrap ? fistar.rate_scrap : 0}
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <div className="content-item">
                                                                    <p><Link to={routeName.FISTAR_FRONT}>{fistar.fullname}</Link></p>
                                                                    <Link to={routeName.FISTAR_FRONT}><p>
                                                                        <span>{new Date(fistar.dob)
                                                                            .toDateString()
                                                                            .split(" ")
                                                                            .pop()}</span>
                                                                        <span className="address">{fistar.location.cd_label}</span>
                                                                    </p></Link>
                                                                </div>

                                                            </div>
                                                        )})}

                                                    </div>
                                                </div>
                                                <div className="col-lg-4">
                                                    <div className="list-item-fistar">


                                                        {fistars &&
                                                        fistars.length > 6 &&
                                                        fistars.slice(6, 9).map((fistar, key) => {

                                                            const bgFistar = {
                                                                backgroundImage: "url(" + getImageLink(fistar.picture, IMAGE_TYPE.FISTARS, IMAGE_SIZE.ORIGINAL) + ")"
                                                            };

                                                            return (<div className="item-fistar" key={key}>
                                                                <div className="img-thumnail" style={bgFistar}>
                                                                    <Link to={routeName.FISTAR_FRONT}>
                                                                        <img
                                                                            src={getImageLink(
                                                                                fistar.picture,
                                                                                IMAGE_TYPE.FISTARS,
                                                                                IMAGE_SIZE.ORIGINAL
                                                                            )}
                                                                            alt={fistar.fullname}
                                                                        />
                                                                    </Link>
                                                                    <div className="star-number">
                                                                        <button className="btn btn-star disabled-hover">
                                                                            <i className="fas fa-star"/>
                                                                            {fistar.rate_scrap ? fistar.rate_scrap : 0}
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <div className="content-item">
                                                                    <p><Link to={routeName.FISTAR_FRONT}>{fistar.fullname}</Link></p>
                                                                    <Link to={routeName.FISTAR_FRONT}><p>
                                                                        <span>{new Date(fistar.dob)
                                                                            .toDateString()
                                                                            .split(" ")
                                                                            .pop()}</span>
                                                                        <span className="address">{fistar.location.cd_label}</span>
                                                                    </p></Link>
                                                                </div>

                                                            </div>
                                                        )})}

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row" id="row-fistar-view-more">
                                    <div className="col-md-12">
                                        <div className="class-view-fistar ">
                                            <Link to={routeName.FISTAR_FRONT}>VIEW MORE</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="main-fistar-campaign">
                            <div className="top-fistar-campaign">
                                <p>{t("HOME.HOM_our_fistar_campaign_title")}</p>
                                <h3 className="title">{t("HOME.HOM_our_fistar_campaign")}</h3>
                            </div>
                            <div className="container-custom">
                                <div className="row">
                                    {campaigns &&
                                    campaigns.length > 0 &&
                                    campaigns.slice(0, 9).map((campaign, key) => {

                                        return <div className="col-lg-4" key={key}>
                                            <div className="card card-campaign">
                                                <div className="img-card">
                                                    <Link to={routeName.CAMPAIGN}>

                                                        <CampaignMainImage campaign={campaign} />
                                                    </Link>
                                                </div>
                                                <Link to={routeName.CAMPAIGN} className="card-body body-campaign">
                                                    <h5 className="card-title">
                                                        <Link to={routeName.CAMPAIGN}>{campaign.cp_name}</Link>
                                                    </h5>
                                                    <p className="text-content" dangerouslySetInnerHTML={{
                                                        __html: campaign ? campaign.cp_description : ""
                                                    }}>

                                                    </p>
                                                </Link>
                                                <Link to={routeName.CAMPAIGN}>
                                                <div className="card-footer">
                                                    <div className="solical-item">
                                                        <div className="item">
                                                            <img src={iconShare} alt=""/>
                                                            <span>{campaign.review_statitics &&
                                                            campaign.review_statitics.sum_like
                                                                ? campaign.review_statitics.sum_like
                                                                : 0}</span>
                                                        </div>

                                                        <div className="item">
                                                            <img src={iconComment} alt=""/>
                                                            <span>{campaign.review_statitics &&
                                                            campaign.review_statitics.sum_comment
                                                                ? campaign.review_statitics.sum_comment
                                                                : 0}</span>
                                                        </div>
                                                        <div className="item">
                                                            <img src={iconHeat} alt="H"/>
                                                            <span>{campaign.review_statitics &&
                                                            campaign.review_statitics.sum_share
                                                                ? campaign.review_statitics.sum_share
                                                                : 0}</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                </Link>
                                            </div>
                                        </div>;

                                    })}

                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="class-view-fistar ">
                                            <Link to={routeName.CAMPAIGN}>{t("HOME.HOM_view_more")}</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="banner-ready" style={banner01}>
                            <div className="content-banner">
                                <p>{t("HOME.HOM_check_out_title")}</p>
                                <h3>{t("HOME.HOM_check_out_title_text")}</h3>
                                <div className="gr-btn-banner">
                                    <Link to={routeName.FISTAR_LOGIN} className="btn btn-banner">{t("HOME.HOM_check_out_title_fistar")}</Link>
                                    <Link to={routeName.PARTNER_LOGIN} className="btn btn-banner">{t("HOME.HOM_check_out_title_partner")}</Link>
                                </div>
                            </div>
                        </div>
                        <div className="contact-main">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-6 col-md-5">
                                        <div className="content-contact">
                                            <h4>{t("CONTACT_US.CUS_contact_us")}</h4>
                                            <p className="text">{t("HOME.HOM_contact_text")} </p>
                                            <p className="text">{t("HOME.HOM_contact_text_2")}</p>
                                            <p className="text-contact mt-60">
                                                <span>{t("HOME.HOM_contact_text_tel")}</span> 09 0302 9423
                                            </p>
                                            <p className="text-contact">
                                                <span> {t("HOME.HOM_contact_text_email")}</span> fistar@dmnc.vn
                                            </p>
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-md-7">
                                        <div className="form-contact">
                                            <form onSubmit={this.__handleSubmit}>
                                                <div className="form-group row">
                                                    <label
                                                        htmlFor="colFormLabelSm"
                                                        className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                                                    >
                                                        {t("CONTACT_US.CUS_label_type_ select")}
                                                        <span>*</span>
                                                    </label>
                                                    <div className="col-sm-9 services-select">
                                                        <i className="fas fa-angle-down" />
                                                        <select
                                                            className="form-control form-control-sm"
                                                            name="type"
                                                            value={form_data.type}
                                                            onChange={this.__handleChangeInput}
                                                            onFocus={this.__handleForcus}
                                                        >
                                                            <option value="1">
                                                                {t("CONTACT_US.CUS_option_about_fistar")}
                                                            </option>
                                                            <option value="2">
                                                                {t("CONTACT_US.CUS_option_about_partner")}
                                                            </option>
                                                            <option value="3">
                                                                {t("CONTACT_US.CUS_option_etc")}
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label
                                                        htmlFor="colFormLabelSm"
                                                        className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                                                    >
                                                        {t("CONTACT_US.CUS_label_name")}
                                                        <span>*</span>
                                                    </label>
                                                    <div className="col-sm-9 ">
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="examfpleInputtext1"
                                                            aria-describedby="textHelp"
                                                            name="name"
                                                            value={form_data.name}
                                                            onChange={this.__handleChangeInput}
                                                            onFocus={this.__handleForcus}
                                                        />
                                                        {form_validate.name && (
                                                            <div
                                                                className="tooptip-text-error"
                                                                style={{ width: "250px" }}
                                                            >
                                                                <p>{form_validate.name}</p>
                                                            </div>
                                                        )}
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label
                                                        htmlFor="colFormLabelSm"
                                                        className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                                                    >
                                                        {t("CONTACT_US.CUS_label_email")}
                                                        <span>*</span>
                                                    </label>
                                                    <div className="col-sm-9 ">
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="examgplefgInputtext1"
                                                            aria-describedby="textHelp"
                                                            name="email"
                                                            value={form_data.email}
                                                            onChange={this.__handleChangeInput}
                                                            onFocus={this.__handleForcus}
                                                        />
                                                        {form_validate.email && (
                                                            <div
                                                                className="tooptip-text-error"
                                                                style={{ width: "250px" }}
                                                            >
                                                                <p>{form_validate.email}</p>
                                                            </div>
                                                        )}
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label
                                                        htmlFor="colFormLabelSm"
                                                        className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                                                    >
                                                        {t("CONTACT_US.CUS_location")}
                                                        {/*<span>*</span>*/}
                                                    </label>
                                                    <div className="col-sm-9 ">
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="exagfmpleInputtext1"
                                                            aria-describedby="textHelp"
                                                            name="location"
                                                            value={form_data.location}
                                                            onChange={this.__handleChangeInput}
                                                            onFocus={this.__handleForcus}
                                                        />
                                                        {form_validate.location && (
                                                            <div
                                                                className="tooptip-text-error"
                                                                style={{ width: "250px" }}
                                                            >
                                                                <p>{form_validate.location}</p>
                                                            </div>
                                                        )}
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label
                                                        htmlFor="colFormLabelSm"
                                                        className="col-sm-3 col-form-label col-form-label-sm label-campaign"
                                                    >
                                                        {t("CONTACT_US.CUS_phone")}
                                                        <span>*</span>
                                                    </label>
                                                    <div className="col-sm-9 ">
                                                        <input
                                                            type="text"
                                                            className="form-control"
                                                            id="exfamspleInputtext1"
                                                            aria-describedby="textHelp"
                                                            name="phone"
                                                            value={form_data.phone}
                                                            onChange={this.handleChangeInputNumberOnly}
                                                            onFocus={this.__handleForcus}
                                                        />
                                                        {form_validate.phone && (
                                                            <div
                                                                className="tooptip-text-error"
                                                                style={{ width: "250px" }}
                                                            >
                                                                <p>{form_validate.phone}</p>
                                                            </div>
                                                        )}
                                                    </div>
                                                </div>
                                                <div className="text-area position-relative">
                    <textarea
                        className="form-control"
                        id="exampleFormControlTextarea1"
                        rows={6}
                        placeholder={t("CONTACT_US.CUS_textarea")}
                        name="content"
                        value={form_data.content}
                        onChange={this.__handleChangeInput}
                        onFocus={this.__handleForcus}
                    />
                                                    {form_validate.content && (
                                                        <div
                                                            className="tooptip-text-error"
                                                            style={{ width: "250px", height: "auto" }}
                                                        >
                                                            <p>{form_validate.content}</p>
                                                        </div>
                                                    )}
                                                </div>
                                                <div className="btn-send">
                                                    <button className={"btn-submit"} type={"submit"}>
                                                        {" "}
                                                        {t("CONTACT_US.CUS_button_send")}
                                                        {loading ? (
                                                            <div
                                                                className="spinner-border"
                                                                role="status"
                                                                style={{
                                                                    marginLeft: "15px",
                                                                    verticalAlign: "middle"
                                                                }}
                                                            >
                          <span className="sr-only">
                            {t("LOADING.LOADING")}
                          </span>
                                                            </div>
                                                        ) : (
                                                            ""
                                                        )}
                                                    </button>
                                                </div>
                                            </form>
                                            <Modal
                                                show={this.state.success}
                                                onHide={this.handleClose}
                                                onExited={this.onExited}
                                                dialogClassName="howto-register modal-dialog-centered"
                                            >
                                                <div className="modal-content">
                                                    <div className="register">
                                                        <div className="top modal-header">
                                                            <h4>{t("HEADER.HED_fistar")}</h4>
                                                            <button
                                                                type="button"
                                                                className="btn btn-close close"
                                                                onClick={this.handleClose}
                                                            >
                                                                <img src={cancelImage} alt="close" />
                                                            </button>
                                                        </div>
                                                        <div className="body-popup">
                                                            <p>{t("CONTACT_US.CTU_sent_successfully")}</p>
                                                            <button type="button" onClick={this.handleClose}>
                                                                {t("POPUP_RESET_PASS.PRP_button_ok")}
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Modal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </main>
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    const {count} = state;

    return {count};
}

const mapDispatchToProps = dispatch => {
    return {
        banners: by_period => dispatch(getBannerAction(by_period)),
        getCounMainMenu: () => dispatch(getCountMainMenuAction()),
        getFistar: (page = 1, sort = "", order = "", state, active) =>
            dispatch(getFistarAction(page, sort, order, state, active)),
        getCampaign: (page = 1, type = "", status = []) =>
            dispatch(getCampaignAction(page, type, status)),
        getDataFistarCampaign:() => dispatch(getDataFistarCampaign()),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withTranslation("translations")(Home));
