import axios from "./axios";
import Cookies from "js-cookie";

if (Cookies.get("token")) {
    setAuthorizationHeader(Cookies.get("token"));
}

export function setAuthorizationHeader(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
}

export default {
    auth: {
        login: data => axios.post("auth/fistar/login", data),
        createAccessTokenFB: (uid, accessToken, type, isRegister, isNew) =>
            axios.post("crawler/create-access-token", {
                uid,
                access_token: accessToken,
                type,
                isRegister,
                isNew
            })
        // logout: () =>
        //   axios.post("logout"),
    },
    ProvideAccessToken: data =>
        axios.post("auth/fistar/check-provide-fb-account", data),
    getAccessToken: (app, action, code) => {
        return axios.post(
            "auth/fistar/get-access-token?app=" +
            app +
            "&callback=" +
            action +
            "&code=" +
            code
        );
    },
    mainMenu: {
        faq: (type, page) =>
            axios.get(`v1/faq?faq_type=${type}&page=${page}&faq_state=1`),
        faqFistar: page => axios.get(`v1/faq?faq_type=1&page=${page}&faq_state=1`),
        faqPartner: page => axios.get(`v1/faq?faq_type=0&page=${page}&faq_state=1`),
        banners: by_period => axios.get(`/v1/banners?by_period=${by_period}`),
        getCounMainMenu: () => axios.get(`/v1/get-count-main-menu`)
    },
    fiStar: {
        auth: {
            loginFacebook: data => axios.post("auth/fistar/login-facebook", data),
            findEmail: data => axios.post("auth/fistar/find-account", data),
            forgotPassword: data => axios.post("auth/fistar/forgot-password", data),
            resetPassword: data => axios.post("auth/fistar/reset-password", data),
            changePassword: data => axios.post("auth/fistar/change-password", data),
            checkExist: data => axios.post("auth/fistar/check-exist", data),
            fistarJoin: data => axios.post("auth/fistar/join", data),
            register: data => axios.post("auth/fistar/register", data),
            getProfile: () => axios.get("auth/fistar/profile"),
            updateProfileGeneral: data =>
                axios.post("auth/fistar/profile/general", data),
            updateProfileInformation: data =>
                axios.post("auth/fistar/profile/information", data),
            updateProfilePassword: data =>
                axios.post("auth/fistar/profile/password", data)
        },
        searchFistar: (search, page, state, active) =>
            axios.get(
                `v1/fistar?${search}&page=${page}&state=${state}&active=${active}`
            ),
        getNewFistar: () => axios.get("v1/influencers?by_new=1&state=2&active=1"),
        getQa: (page = 1, type = "", search = "", state = "", by_relate=1) =>
            axios.get(
                `v1/qa?page=${page}&qa_type=${type}&qa_state=${state}&keyword=${search}&by_relate=${by_relate}`
            ),
        getQaDetail: id => axios.get(`v1/qa/${id}`),
        createQa: data => axios.post("v1/qa", data),
        getFistar: (uid, search = "") =>
            axios.get("v1/influencers/" + uid + "?search=" + search),
        getHistory: uid => axios.get("v1/history/" + uid)
    },
    code: {
        getCode: type => axios.get("v1/code?type=" + type)
    },
    checkSnsVerify: {
        verifySns: (data) =>
            axios.post("v1/check-sns-url", data),
    },
    brands: {
        getBrand: () => axios.get(`/v1/get-brand`)
    },
    partner: {
        auth: {
            login: data => axios.post("auth/partner/login", data),
            findEmail: data => axios.post("auth/partner/find-account", data),
            forgotPassword: data => axios.post("auth/partner/forgot-password", data),
            resetPassword: data => axios.post("auth/partner/reset-password", data),
            changePassword: data => axios.post("auth/partner/change-password", data),
            register: data => axios.post("auth/partner/register", data),
            checkExist: data => axios.post("auth/partner/check-exist", data),
            createCampain: data => axios.post("v1/campaigns/create", data),
            updateCampain: (cp_id, data) =>
                axios.post("admin/edit-campaign/" + cp_id, data),
            inforProfile: data => axios.post("auth/partner/profile", data),
            updateManager: (pid, data) =>
                axios.post("auth/partner/update-company-manager/" + pid, data),
            changePasswowrdProfile: (pid, data) =>
                axios.put("auth/partner/partner-change-password/" + pid, data),
            updateCompanyInformation: (pid, data) =>
                axios.post("auth/partner/update-company/" + pid, data)
        }
    },
    campaign: {
        getPushMessageCampaign: (page = 1, by_relate = "") =>
            axios.get(
                `v1/push-message-partners?sort=cp_id&order=desc&page=${page}&by_relate=${by_relate}`
            ),
        getPushMessageFistar: (page = 1) =>
            axios.get(`v1/push-message-fistars?sort=cp_id&order=desc&page=${page}`),
        getPushMessageNotice: (page = 1) => axios.get(`v1/notices?page=${page}&notice_state=1`),
        getPushMessageCount: () => axios.get(`v1/notices/count?notice_state=1`),
        getCampaign: (page = 1, type = "", by_relate, fime_like) =>
            axios.get(
                `v1/campaigns?page=${page}&type=${type}&state[]=0&state[]=1&by_relate=${by_relate}&fime_like=${fime_like}`
            ),
        getFistar: (page = 1, type = "") =>
            axios.get(
                `v1/influencers?by_relate=1&page=${page}&type=${type}&state=2&active=1`
            ),

        getMyCampaign: (page,
                        type = "",
                        search = "",
                        campaign = "",
                        keyword = "") =>
            axios.get(
                `v1/campaigns?page=${page}&by_relate=1&type=${type}&search=${search}&state[]=0&state[]=1&status[]=60&status[]=61&status[]=62&status[]=59&campaign=${campaign}&keyword=${keyword}`
            ),
        getMyCampaignDashBoard: (page, type = "", search = "") =>
            axios.get(
                `v1/campaigns?page=${page}&by_relate=1&state[]=0&state[]=1&type=${type}&search=${search}`
            ),
        searchCampaign: (page,
                         kind = "",
                         search = "",
                         campaign = "",
                         keyword = "",
                         by_relate = "") =>
            axios.get(
                `v1/campaigns?page=${page}&by_relate=${by_relate}&kind=${kind}&search=${search}&state[]=0&state[]=1&status[]=59&status[]=60&status[]=61&status[]=62&campaign=${campaign}&keyword=${keyword}`
            ),
        getNewCampaign: (page, type = "") =>
            axios.get(`v1/campaigns?page=${page}&state[]=0&state[]=1&by_new=1&kind=${type}&status[]=59`),
        getCampaignReview: () =>
            axios.get(`v1/campaign-review?sort&order&paginate`),
        getCountCampaign: () => axios.get(`v1/count-campaigns?by_relate=1&state[]=0&state[]=1`),
        applyCampaign: id => axios.post(`v1/apply-campaign/${id}`),
        getCampaignDetail: id => axios.get(`v1/campaigns/${id}?active=${id}`),
        getFistarOfCampaignDetail: (id, type) => axios.get(`v1/fistars/detail/campaign/${id}?type=${type}`),
        getCampaignDetailFistar: id => axios.get(`v1/campaigns/${id}`),
        nextStep: (m_id, stt_id) =>
            axios.post(`v1/update-status-match/${m_id}`, {stt_id}),
        updateReview: (m_id, data) =>
            axios.post(`v1/update-content-match/${m_id}`, data),
        updateApproveReviewAdmin: (m_id, rv_status) =>
            axios.get(`admin/campaign-admin-review/admin-approve-review/${m_id}/${rv_status}`),
        updateModifyReviewAdmin: (m_id) =>
            axios.get(`admin/campaign-admin-review/modify-status/${m_id}`),
        myFistarPartner: (page, filter = "", pid) =>
            axios.get(`v1/my-fistars?page=${page}&filter_by=${filter}&pid=${pid}`),
        totalStatus: pid => axios.get(`v1/my-fistars/statitis?pid=${pid}`),
        countScrap: (uid, cp_id) =>
            axios.post(`v1/scrap?uid=${uid}&cp_id=${cp_id}`),
        changeStatus: (id, status) =>
            axios.post(`v1/update-campaign-status`, {id, status}),
        clickSelectSNS: id => axios.post(`v1/update-match-channel`, {id}),
        changeStatus: (id, status) =>
            axios.post(`v1/update-campaign-status`, {id, status}),
        clickSelectSNS: (m_id, sns_id) =>
            axios.post(`v1/update-match-channel`, {m_id, sns_id})
    },
    front: {
        getDataFistarCampaign: () => axios.get(
            `v1/data-home-front`
        ),
        getMyCampaignFront: (page, status = "") => {
            console.log(status);
            let statusParam = "";
            if (status) {
                statusParam = "&status[]=" + status;
            } else {
                statusParam = "&status[]=60&status[]=61&status[]=62";
            }
            return axios.get(`v1/campaigns-front?page=${page}${statusParam}&state[]=0&state[]=1`);
        },
        getCampaign: (page = 1, type = "", status = "") =>
            axios.get(
                `v1/campaigns-front?page=${page}&type=${type}&status[]=${
                    status[0]
                    }&status[]=${status[1]}&status[]=${status[2]}&state[]=0&state[]=1`
            ),
        getFistar: (page = 1, sort = "", order = "", state = "", active = "") =>
            axios.get(
                `v1/influencers-front?page=${page}&sort=${sort}&order=${order}&state=${state}&active=${active}`
            )
    },
    contact: {
        sendContact: data => axios.post(`/v1/contact`, data)
    },
    channel: {
        getAll: () => axios.get("/v1/channel"),
        getRecentSNS: (uid, channel) =>
            axios.get(`/crawler/recent-${channel}/${uid}`)
    },
    scrap: id => axios.post(`v1/scrap`, {id})
};
