import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Route } from "react-router-dom";

const CustomRoute = ({component: Component, layout: Layout, wrapper: Wrapper, ...rest}) => {
  // If have layout
  if(Layout) {
    if (Wrapper) {
      return (
        <Route
        {...rest}
        render={props =>
          <Layout>
            <Wrapper>
              <Component {...props} />
            </Wrapper>
          </Layout>
        }
      />
      )
    }
    return (
      <Route
      {...rest}
      render={props =>
        <Layout>
          <Component {...props} />
        </Layout>
      }
    />
    )
  }
  if (Wrapper) {
    return (
      <Route
      {...rest}
      render={props =>
        <Wrapper>
          <Component {...props} />
        </Wrapper>
      }
    />
    )
  }

  return (
    <Route
      {...rest}
      render={props =>
        <Component {...props} />
      }
    />
  )
};

export default CustomRoute
