import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";
// import PageNotFound from './../pages/errors/PageNotFound'
// import { hasRole, hasPermission } from './../utils/entrust'

const UserRoute = ({
  isAuthenticated,
  auth,
  component: Component,
  layout: Layout,
  wrapper: Wrapper,
  ...rest
}) => {
  console.log(auth, rest.usertype, "INIT ROUTE");

  if (!isAuthenticated || (auth && auth.type != rest.usertype)) {
    return <Route {...rest} render={props => <Redirect to="/" />} />;
  }

  // const role = !rest.role || (rest.role && hasRole(auth.user, rest.role))
  // const permission = !rest.permission || (rest.permission && hasPermission(auth.user, rest.permission))

  // if (!role || !permission) {
  // return (
  //   <Route
  //     {...rest}
  //     render={props => <PageNotFound {...props} />}
  //   />
  // )
  // }

  // If have layout
  if (Layout) {
    // if have Wrapper
    if (Wrapper) {
      return (
        <Route
          {...rest}
          render={props => (
            <Layout>
              <Wrapper>
                <Component {...props} />
              </Wrapper>
            </Layout>
          )}
        />
      );
    }
    return (
      <Route
        {...rest}
        render={props => (
          <Layout>
            <Component {...props} />
          </Layout>
        )}
      />
    );
  }

  if (Wrapper) {
    return (
      <Route
        {...rest}
        render={props => (
          <Wrapper>
            <Component {...props} />
          </Wrapper>
        )}
      />
    );
  }

  return <Route {...rest} render={props => <Component {...props} />} />;
};

UserRoute.propTypes = {
  component: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
  return {
    isAuthenticated: !!state.auth.token,
    auth: state.auth
  };
};

export default connect(mapStateToProps)(UserRoute);
