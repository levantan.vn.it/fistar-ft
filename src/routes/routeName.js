export const FISTAR = "/fi-star";
export const HOME = "/";
export const AUTH = "/auth";
export const FAQ = "/faqs/all";
export const FAQ_FISTAR = "/faqs/fistar";
export const FAQ_PARTNER = "/faqs/partner";

export const CAMPAIGN = "/campaign";

export const FISTAR_FRONT = "/fistar-front";

export const PRIVACY_TERMS = "/privacy-terms";
export const TERMS = "/terms";

export const CONTACTUS = "/contact-us";
export const SERVICE = "/service";
export const SERVICE_FISTAR = "/service-fistar";
export const SERVICE_PARTNER = "/service-partner";

// auth
export const FISTAR_LOGIN = "/login";
export const FISTAR_FIND_EMAIL = "/fi-star/find-email";
export const FISTAR_FORGOT_PASSWORD = "/fi-star/forgot-password";
export const FISTAR_PRIVATE = "/fi-star/private";
export const FISTAR_JOIN = "/fi-star/join";
export const FISTAR_REGISTER = "/fi-star/register";
export const FISTAR_APPROVE_REGISTER = "/fi-star/join/approval-register";

// profile
export const FISTAR_PROFILE = "/fi-star/profile";
export const FISTAR_PROFILE_GANERAL = "/fi-star/profile/general";
export const FISTAR_PROFILE_INFORMARTION = "/fi-star/profile/information";
export const FISTAR_PROFILE_RESET_PASSWORD = "/fi-star/profile/reset-password";

export const FISTAR_DASHBOARD = "/fi-star/dashboard";
export const FISTAR_SEARCH_CAMPAIGN = "/fi-star/search-campaign";
export const FISTAR_MY_CAMPAIGN = "/fi-star/my-campaign";
export const FISTAR_CAMPAIGN_DETAIL = "/fi-star/my-campaign-detail/:id";

export const FISTAR_NEW_CAMPAIGN = "/fi-star/new-campaign";

export const FISTAR_QA = "/fi-star/qa";
export const FISTAR_QA_DETAIL = "/fi-star/qa/detail/:id";
export const FISTAR_QA_ANSWER_QUESTION = "/fi-star/qa/answer-question";

export const WRITE_REVIEW = "/write-review";
export const FISTAR_WRITE_REVIEW =
  FISTAR_CAMPAIGN_DETAIL + WRITE_REVIEW + "/:sns_id";
export const FISTAR_READ_REVIEW = "/fi-star/my-campaign-detail/read-review";
export const FISTAR_EDIT_REVIEW = "/fi-star/my-campaign-detail/edit-review";

export const PARTNER_LOGIN = "/partner/login";
export const PARTNER_REGISTER = "/partner/register";
export const PARTNER_FIND_EMAIL = "/partner/find-email";
export const PARTNER_FORGOT_PASSWORD = "/partner/forgot-password";
export const PARTNER_FORGOT = "/partner/forgot";

export const PARTNER_APPROVE_REGISTER = "/partner/approval-register";

export const PARTNER_CREATE_CAMPAIN = "/partner/create-campain";

export const PARTNER_PROFILE = "/partner/profile";
export const PARTNER_MANAGER_INFORMATION =
  "/partner/profile/manager-information";
export const PARTNER_COMPANY_INFORMATION =
  "/partner/profile/company-information";
export const PARTNER_CHANGE_PASSWORD = "/partner/profile/change-password";

export const PARTNER_DASHBOARD = "/partner/dashboard";

export const PARTNER_CAMPAIGN_TRACKING = "/partner/campaign-tracking";
export const PARTNER_CAMPAIGN_TRACKING_DETAIL = "/partner/campaign/:id";

export const PARTNER_QA = "/partner/qa";
export const PARTNER_QA_DETAIL = "/partner/qa/detail/:id";
export const PARTNER_QA_ANSWER_QUESTION = "/partner/qa/answer-question";

export const PARTNER_MY_FISTAR = "/partner/my-fistar";
export const PARTNER_SEARCH_FISTAR = "/partner/fistar-search";
